var mapsData = new function () {
    var self = this;
    this.init = function (data) {
        var address = data.options.address;
        var latitude = data.options.lat;
        var longitude = data.options.lng;
        var myLatLng = this.geo(latitude, longitude, address, function (center, data) {
            var mapOptions = {
                zoom: data.options.zoom,
                center: center,
                panControl: data.options.panControl,
                scaleControl: data.options.scaleControl,
                streetViewControl: data.options.streetViewControl,
                overviewMapControl: data.options.overviewMapControl,
                mapTypeControl: data.options.mapTypeControl,
                zoomControl: data.options.zoomControl
            };
            
            var maps = new google.maps.Map(document.getElementById(data.id), mapOptions);
            $(window).on("resize", function () {
                var center = maps.getCenter();
                google.maps.event.trigger(maps, "resize");
                maps.setCenter(center);
            });
            google.maps.event.addDomListener(window, "resize", function () {
                var center = maps.getCenter();
                google.maps.event.trigger(maps, "resize");
                maps.setCenter(center);
            });
            var marker = new google.maps.Marker({
                position: center,
                map: maps,
                title: data.title
            });
        }, data);
    };
    this.geo = function (lat, lng, address, callback, data) {
        var myLatLng = new google.maps.LatLng(lat, lng);
        if (myLatLng.lat && myLatLng.lng) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': address}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    myLatLng = new google.maps.LatLng(latitude, longitude);
                    callback(myLatLng, data);
                }
            });
        } else {
            callback(myLatLng, data);
        }
    };
};