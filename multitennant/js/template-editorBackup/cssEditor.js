var globalCssEditor = new function(){
    var selfGlobal = this;
    //создаем глобальную переменную currentItem
    this.currentItem;
    // Bg
    this.getStyleItem = function(item){
        selfGlobal.currentItem = item;

    };
    this.bgEditor = function(item, backgroundColorClear){
        $('.bgRepeatsbut button').unbind();
        bodySelector.on('click', '.bgRepeatsbut button', function () {
            $('.bgRepeatsbut button').removeClass('ActiveBgButton');
            $(this).addClass('ActiveBgButton');
            // add bg repeat in row
            var repeatVal = $('.bgRepeatsbut').find('button.ActiveBgButton').attr('data-original-title');
            item.css('backgroundRepeat', repeatVal);
            panels.changeContent();
        });
        //перенести в init от -----
        $('#assetsBgModal').unbind();
        bodySelector.on('click', '#assetsBgModal', function () {
            $('.overlayUniversal').show('10');
            $('.modalWithBgImg').show('10');
        });
        $('#assetsBgModalClose, #item-editor-tab li a').unbind();
        bodySelector.on('click', '#assetsBgModalClose, #item-editor-tab li a', function () {
            $('.overlayUniversal').hide('10');
            $('.modalWithBgImg').hide('10');
        });
        $('.uploadImgBg').unbind();
        bodySelector.on('click', '.uploadImgBg', function () {
            $('#uploadImg').click()
        });
        //-----до------

        //Color picker
     /*   $('#inputSpinnerColorBg').colorpicker().on('changeColor.colorpicker', function (event) {
            $('.colorpicker.dropdown-menu.colorpicker-with-alpha.colorpicker-right.colorpicker-hidden').on('mousedown mousemove', function () {
                var borderColors = event.color.toHex();
                //add bg in row
                item.css('background', borderColors);
            })
        });
*/
        $('#inputSpinnerColorBg').colorpicker().on('changeColor.colorpicker', function(event){
            var borderColors = event.color.toHex();
            //add bg in row
            item.css('background', borderColors);
        });


        //$('#inputSpinnerColorBg').colorpicker('setValue', backgroundColorClear);
        $('.contWithprevBg .preview').unbind();
        bodySelector.on('click', '.contWithprevBg .preview', function (e) {
            e.preventDefault();
            var selfBg = $(this);
            $('.contWithprevBg .preview').attr('id', '');
            selfBg.attr('id', 'activeBgImg');
            var srcBgImg = selfBg.find('img').attr('data-img');
            var prevBgCont = $('img.bgPrevievImg');
            prevBgCont.attr('src', 'http://' + window.location.hostname + srcBgImg);
            getWeightObj(srcBgImg);
            //add bg in row
            item.css('background', 'url(http://' + window.location.hostname + srcBgImg + ')');
            panels.changeContent();
        });
        // Backgroun position (preset) change
        $('.presetColBg button').unbind();
        bodySelector.on('click', ".presetColBg button", function () {
            var locthis = $(this);
            $('.presetColBg button').attr('id', '');
            locthis.attr('id', 'activeBgPreset');
            var bgPresetValH = locthis.attr('data-positionh');
            var bgPresetValV = locthis.attr('data-positionv');
            $('#posBgValH').text(bgPresetValH + '%');
            $('#posBgValV').text(bgPresetValV + '%');
        });
        // add bg position val in row
        $('.presetColBg button, .adjustpostBg button').unbind();
        bodySelector.on('click', '.presetColBg button, .adjustpostBg button', function(){
            var posBgValH = $('#posBgValH').text();
            var posBgValV = $('#posBgValV').text();
            item.css('backgroundPosition', posBgValH + posBgValV);
            panels.changeContent();
        });
        //add bg cover/contain in row
        $('.divWithInfoPrevBg button').unbind();
        bodySelector.on('click', '.divWithInfoPrevBg button', function () {
            var locthis = $(this);
            $('.divWithInfoPrevBg button').removeClass('ActiveBgButton');
            locthis.addClass('ActiveBgButton');
            item.css('backgroundSize', locthis.attr('data-original-title'));
            panels.changeContent();
        });
        //add size bg in row
        $('#inputWithSpinner-BgWidth, #inputWithSpinner-BgHeight').unbind();
        bodySelector.on('click', '#inputWithSpinner-BgWidth, #inputWithSpinner-BgHeight', function () {
            var inputWithSpinnerBgWidthInput =  $('#inputWithSpinner-BgWidth input');
            var inputWithSpinnerBgWidth = inputWithSpinnerBgWidthInput.val();
            var inputWithSpinnerBgHeight = $('#inputWithSpinner-BgHeight input').val();
            var inputWithSpinnerBgWidthIzm = inputWithSpinnerBgWidthInput.attr('data-position');
            item.css('backgroundSize',inputWithSpinnerBgWidth + inputWithSpinnerBgWidthIzm + ' ' + inputWithSpinnerBgHeight + inputWithSpinnerBgWidthIzm );
            panels.changeContent();
        });
    };
    // pos, disp, marg, padding
    this.posButtonEditor = function (positionButtonThis, item) {
        //var selected = $('.optionDisplay').find('.selectedDisplay')
        var selected = positionButtonThis.parent('.optionDisplay').find('.selectedDisplay');
        $(selected).removeClass('selectedDisplay');
        positionButtonThis.addClass('selectedDisplay');
        //display options selection
        var valueDisplay = positionButtonThis.attr('data-position');
        //var vald = valueDisplay;
        item.css('display', valueDisplay);
        //Float
        var valueFloat = positionButtonThis.attr('data-float');
        item.css('float', valueFloat);
        //Clear
        var valueClear = positionButtonThis.attr('data-clear');
        item.css('clear', valueClear);
        //Overflow
        var valueOverflow = positionButtonThis.attr('data-overflow');
        item.css('overflow', valueOverflow);
        //Position
        var valuePosition = positionButtonThis.attr('data-position');
        item.css('position', valuePosition);
        panels.changeContent();
    };
    // Position
    this.positionCssEditor = function(item){
        var tabBtn = $('.tabBtnSM');
        tabBtn.unbind();
        tabBtn.on('click', function () {
            var positionButtonThis = $(this);
            selfGlobal.posButtonEditor(positionButtonThis, item)
        });

        //position absolute, relative, fixed
        $('#relativePosition, #absolutePosition, #fixedPosition').unbind();
        $('#relativePosition, #absolutePosition, #fixedPosition').on('click', function () {
            panels.changeContent();
            $('.wrapperHiddenPositionSettings').show(600);
        });
        $('#staticPosition').unbind();
        $('#staticPosition').on('click', function () {
            panels.changeContent();
            $('.wrapperHiddenPositionSettings').hide(400);
        });
        //margin paddings arrows
        $('.upArrow').unbind();
        $('.upArrow').on('click', function () {
            panels.changeContent();
            var currentValue = $(this).parents('div.spinnerFullEditTool').find('input');
            var attr = currentValue.attr('data-margin');
            var currentValueChanged = currentValue.val();
            if (currentValueChanged == 'auto') {
                currentValueChanged = 0
            }
            $(currentValue).val(++currentValueChanged);
            item.css(attr, currentValueChanged);
        });
        $('.downArrow').unbind();
        $('.downArrow').on('click', function () {
            panels.changeContent();
            var currentValue = $(this).parents('div.spinnerFullEditTool').find('input');
            var attr = currentValue.attr('data-margin');
            var currentValueChanged = currentValue.val();
            if (currentValueChanged == 'auto') {
                currentValueChanged = 0

            }
            $(currentValue).val(--currentValueChanged);
            item.css(attr, currentValueChanged);
        });
    };
    // margin padding input post
    this.margPadding = function (item) {
        $('input#inputMarginTop').unbind();
        $('input#inputMarginTop').on('blur click',function () {
            panels.changeContent();
            var newMarginTop = ($(this).val()) + 'px';
            item.css('margin-top', newMarginTop);
        });
        $('input#inputMarginBottom').unbind();
        $('input#inputMarginBottom').on('blur click',function () {
            panels.changeContent();
            var newMarginBottom = ($(this).val()) + 'px';
            item.css('margin-bottom', newMarginBottom);
        });
        $('input#inputMarginLeft').unbind();
        $('input#inputMarginLeft').on('blur click',function () {
            panels.changeContent();
            var newMarginLeft = ($(this).val()) + 'px';
            item.css('margin-left', newMarginLeft);
        });
        $('input#inputMarginRight').unbind();
        $('input#inputMarginRight').on('blur click',function () {
            panels.changeContent();
            var newMarginRight = ($(this).val()) + 'px';
            item.css('margin-right', newMarginRight);
        });
        $('input#inputPaddingTop').unbind();
        $('input#inputPaddingTop').on('blur click',function () {
            panels.changeContent();
            var newPaddingTop = ($(this).val()) + 'px';
            item.css('padding-top', newPaddingTop);
        });
        $('input#inputPaddingBottom').unbind();
        $('input#inputPaddingBottom').on('blur click',function () {
            panels.changeContent();
            var newPaddingBottom = ($(this).val()) + 'px';
            item.css('padding-bottom', newPaddingBottom);
        });
        $('input#inputPaddingLeft').unbind();
        $('input#inputPaddingLeft').on('blur click',function () {
            panels.changeContent();
            var newPaddingLeft = ($(this).val()) + 'px';
            item.css('padding-left', newPaddingLeft);
        });
        $('input#inputPaddingRight').unbind();
        $('input#inputPaddingRight').on('blur click',function () {
            panels.changeContent();
            var newPaddingRight = ($(this).val()) + 'px';
            item.css('padding-right', newPaddingRight);
        });
    };
    // Position top,left,bottom,right
    this.posTLBR = function (item){
        $('input#inputPositionTop').parent('.chekInput').unbind();
        $('input#inputPositionTop').parent('.chekInput').on('click', function() {
            var self = $(this).find('input');
            panels.changeContent();
            var newPositionTop = self.val();
            var typeOfData = self.attr('data-position');
            if (typeOfData == 'px') {
                var newPositionTopPX = newPositionTop + 'px';
                item.css('top', newPositionTopPX);
            } else if (typeOfData == '%') {
                var newPositionTopPersent = newPositionTop + '%';
                item.css('top', newPositionTopPersent);
            } else if (typeOfData == 'auto') {
                var newPositionTopAuto = 'auto';
                item.css('top', newPositionTopAuto);
            }
        });
        $('input#inputPositionBottom').parent('.chekInput').unbind();
        $('input#inputPositionBottom').parent('.chekInput').on('click', function() {
            var self = $(this).find('input');
            panels.changeContent();
            var newPositionBottom = self.val();
            var typeOfData = self.attr('data-position');
            if (typeOfData == 'px') {
                var newPositionBottomPX = newPositionBottom + 'px';
                item.css('bottom', newPositionBottomPX);
            } else if (typeOfData == '%') {
                var newPositionBottomPersent = newPositionBottom + '%';
                item.css('bottom', newPositionBottomPersent);
            } else if (typeOfData == 'auto') {
                var newPositionBottomAuto = 'auto';
                item.css('bottom', newPositionBottomAuto);
            }
        });
        $('input#inputPositionLeft').parent('.chekInput').unbind();
        $('input#inputPositionLeft').parent('.chekInput').on('click', function() {
            var self = $(this).find('input');
            panels.changeContent();
            var newPositionLeft = self.val();
            var typeOfData = self.attr('data-position');
            if (typeOfData == 'px') {
                var newPositionLeftPX = newPositionLeft + 'px';
                item.css('left', newPositionLeftPX);
            } else if (typeOfData == '%') {
                var newPositionLeftPersent = newPositionLeft + '%';
                item.css('left', newPositionLeftPersent);
            } else if (typeOfData == 'auto') {
                var newPositionLeftAuto = 'auto';
                item.css('left', newPositionLeftAuto);
            }
        });
        $('input#inputPositionRight').parent('.chekInput').unbind();
        $('input#inputPositionRight').parent('.chekInput').on('click', function() {
            var self = $(this).find('input');
            panels.changeContent();
            var newPositionRigh = self.val();
            var typeOfData = self.attr('data-position');
            if (typeOfData == 'px') {
                var newPositionRighPX = newPos5650R5GFitionTop + 'px';
                item.css('right', newPositionRighPX);
            } else if (typeOfData == '%') {
                var newPositionRighPersent = newPositionRigh + '%';
                item.css('right', newPositionRighPersent);
            } else if (typeOfData == 'auto') {
                var newPositionRighAuto = 'auto';
                item.css('right', newPositionRighAuto);
            }
        });
    };
    // Border editor
    this.borderEditor = function(item){
    var items = item;
        $('.prevSizeRectangle').unbind();
        $('.prevSizeRectangle').on('click', function () {
            $('.prevSizeSquare').removeClass('activeSize');
            $('.prevSizeRectangle').addClass('activeSize');
            $('.prevDivBorder').css({
                width: '70px',
                height: '50px',
                left: 'calc(50% - 45px)',
                top: 'calc(50% - -1px)'
            })
        });
        $('.prevSizeSquare').unbind();
        $('.prevSizeSquare').on('click', function () {
            $('.prevSizeRectangle').removeClass('activeSize');
            $('.prevSizeSquare').addClass('activeSize');
            $('.prevDivBorder').css({
                width: '70px',
                height: '70px',
                left: 'calc(50% - 44px)',
                top: 'calc(50% - 14px)'
            })
        });
        $('.borderStyles').unbind();
        $('.borderStyles').on('click', function () {
            var borderStyles = $(this).attr('data-border');
            var activeElem = fr.find('.edit-item');
            if ($('.innerBorderTop').is('.activeBorder') == true) {
                $('.borderEdit').find('.prevDivBorder').css('border-top-style', borderStyles);
                activeElem.css('border-top-style', borderStyles);
            } else if ($('.innerBorderBottom').is('.activeBorder') == true) {
                $('.borderEdit').find('.prevDivBorder').css('border-bottom-style', borderStyles);
                activeElem.css('border-bottom-style', borderStyles);
            } else if ($('.innerBorderRight').is('.activeBorder') == true) {
                $('.borderEdit').find('.prevDivBorder').css('border-right-style', borderStyles);
                activeElem.css('border-right-style', borderStyles);
            } else if ($('.innerBorderLeft').is('.activeBorder') == true) {
                $('.borderEdit').find('.prevDivBorder').css('border-left-style', borderStyles);
                activeElem.css('border-left-style', borderStyles);
            }
        });
        $('#inputWithSpinner-borderWidth').unbind();
        $('#inputWithSpinner-borderWidth').on('click', function (items) {
            var inputBorderWidth = $(this).find('input.inputSpinners').val();
            var inputBorderWidthPx = $(this).find('input.inputSpinners').attr('data-position');
            $(items).css('border-width', inputBorderWidth + inputBorderWidthPx);
        });
        $('#inputWithSpinner-borderColor input').colorpicker().on('changeColor.colorpicker', function (event) {
            $('.colorpicker.dropdown-menu.colorpicker-with-alpha.colorpicker-right.colorpicker-visible').on('mousedown mousemove', function () {
                var borderColors = event.color.toHex();
                //add bg in row
                items.css('borderColor', borderColors);
            })
        });
        $('.borderEdit div').unbind();
        $('.borderEdit div').on('click', function () {
            var selfBorder = $(this);
            var selfBorderA = item;
            var dataBorder = selfBorder.attr('data-border');
            var bordePreview = document.getElementById('prevDivBorder');
            $('.borderEdit div').removeClass('activeBorder');
            // var ThisIsClass = fr.contentWindow.document.querySelectorAll('.edit-item');
            selfBorder.addClass('activeBorder');
            var borderRad = selfBorder.attr('data-borderdef');
            var defStyle = $('.prevDivBorder').attr('style');
            var borderRads = defStyle + borderRad;
            $('.prevDivBorder').attr('style', borderRads);
            var valBords = selfBorderA.attr('style');
            selfBorderA.attr('style', valBords + borderRads);
            var bordePreviewCompStyle = window.getComputedStyle(bordePreview);
            var defValBorder1 = dataBorder.charAt(0) + dataBorder.charAt(1);
            var defValBorder2 = "border" + defValBorder1;
            var slt = {};
            slt.borderTL = bordePreviewCompStyle["borderTopLeftRadius"].replace(/\D/g, '');
            slt.borderTR = bordePreviewCompStyle["borderTopRightRadius"].replace(/\D/g, '');
            slt.borderBR = bordePreviewCompStyle["borderBottomRightRadius"].replace(/\D/g, '');
            slt.borderBL = bordePreviewCompStyle["borderBottomLeftRadius"].replace(/\D/g, '');
            $(function () {
                $("#sliderBorderRad").slider({
                    orientation: "vertical",
                    range: "min",
                    min: 0,
                    max: 50,
                    value: slt[defValBorder2],
                    animate: "slow",
                    slide: function (event, ui) {
                        $("#amountBorder").text(ui.value);
                        var bordePreviewCompStyle = window.getComputedStyle(bordePreview);
                        var borderTL = bordePreviewCompStyle["borderTopLeftRadius"].replace(/\D/g, '');
                        var borderTR = bordePreviewCompStyle["borderTopRightRadius"].replace(/\D/g, '');
                        var borderBR = bordePreviewCompStyle["borderBottomRightRadius"].replace(/\D/g, '');
                        var borderBL = bordePreviewCompStyle["borderBottomLeftRadius"].replace(/\D/g, '');
                        if (dataBorder === "TL-BL") {
                            borderTL = ui.value;
                            borderBL = ui.value;
                        }
                        if (dataBorder === "TL-TR") {
                            borderTL = ui.value;
                            borderTR = ui.value;
                        }
                        if (dataBorder === "TR-BR") {
                            borderTR = ui.value;
                            borderBR = ui.value;
                        }
                        if (dataBorder === "BR-BL") {
                            borderTR = ui.value;
                            borderBR = ui.value;
                        }
                        if (dataBorder === "TL") {
                            borderTL = ui.value
                        }
                        if (dataBorder === "TR") {
                            borderTR = ui.value;
                        }
                        if (dataBorder === "BR") {
                            borderBR = ui.value;
                        }
                        if (dataBorder === "BL") {
                            borderBL = ui.value;
                        }
                        var completeBordStyle = borderTL + "% " + borderTR + "% " + borderBR + "% " + borderBL + "%";
                        bordePreview.style.borderRadius = completeBordStyle;
                        //bordePreview.attr('style');
                        item.css('borderRadius', completeBordStyle)
                    }
                });
                $('#sliderBorderRad .ui-slider-handle').append('<p id="amountBorder"></p>');
                $("#amountBorder").text($("#sliderBorderRad").slider("value"));
            });
        });
    };
    // z-index
    this.zIndexInput = function(item, thisZIndex){
        panels.changeContent();
        var valZindex = thisZIndex.find('input').val();
        item.css('z-index', valZindex);
    };
    // box-shadow
    this.tabBtnShadowEditor = function(item, valueShadow){
        function valuesOfShadowBox(shadowColor) {
            panels.changeContent();
            if ($('#inputShadowHorizntalLenght').val() != '') {
                var sliderShadowHorizntalLenght = $('#inputShadowHorizntalLenght').val() + "px ";
            } else {
                var sliderShadowHorizntalLenght = '0px ';
            }

            if ($('#inputShadowVerticalLenght').val() != '') {
                var sliderShadowVerticalLenght = ($('#inputShadowVerticalLenght').val()) + "px ";
            } else {
                var sliderShadowVerticalLenght = '0px ';
            }

            if ($('#inputShadowBlur').val() != '') {
                var sliderShadowBlur = ($('#inputShadowBlur').val()) + "px ";
            } else {
                var sliderShadowBlur = '0px ';
            }

            if ($('#inputShadowSpread').val() != '') {
                var sliderShadowSpread = ($('#inputShadowSpread').val()) + "px ";
            } else {
                var sliderShadowSpread = '0px ';
            }
            var firstValueShadow = valueShadow + " " + sliderShadowHorizntalLenght + sliderShadowVerticalLenght + sliderShadowBlur + sliderShadowSpread;
            var colorValues = "rgba(" + r + "," + g + "," + b + "," + a + " )";
            var shadowValues = firstValueShadow + colorValues;
            item.css('box-shadow', shadowValues)
                .attr('attrtype', valueShadow)
                .attr('attrhorlength', sliderShadowHorizntalLenght)
                .attr('attrvertlength', sliderShadowVerticalLenght)
                .attr('attrblur', sliderShadowBlur)
                .attr('attrspread', sliderShadowSpread)
                .attr('attr-r', r)
                .attr('attr-g', g)
                .attr('attr-b', b)
                .attr('attr-a', a);
        }
        $('.tabBtnShadow').unbind();
        $('.tabBtnShadow').on('click', function () {
            var thisBtnShado = $(this);
            panels.changeContent();
            var selected = thisBtnShado.parent('.tabShadowGroup').find('.selectedShadowType');
            $(selected).removeClass('selectedShadowType');
            thisBtnShado.addClass('selectedShadowType');
            valueShadow = thisBtnShado.attr('data-shadow');
            valuesOfShadowBox(valueShadow);
        });
        //color picker Shadow Box
        $('#inputColorPickerShadowBox').colorpicker().on('changeColor.colorpicker', function (event) {
            panels.changeContent();
            var shadowColor = event.color.toRGB();
            r = shadowColor.r;
            g = shadowColor.g;
            b = shadowColor.b;
            a = shadowColor.a;
            valuesOfShadowBox();
        });

        // slider shadow horizontal
        $('#sliderShadowHorizntalLenght').slider({
            min: -200,
            max: 200,
            value: 20
        });
        $("#sliderShadowHorizntalLenght").unbind();
        $("#sliderShadowHorizntalLenght").on("slide", function (event, ui) {
            panels.changeContent();
            var valueHorizontalLengthSlider = $(this).slider("value");
            $('#inputShadowHorizntalLenght').val(valueHorizontalLengthSlider);
            valuesOfShadowBox();
        });

        $('#sliderShadowVerticalLenght').slider({
            min: -200,
            max: 200,
            value: 20
        });
        $("#sliderShadowVerticalLenght").unbind();
        $("#sliderShadowVerticalLenght").on("slide", function (event, ui) {
            panels.changeContent();
            var valueHorizontalLengthSlider = $(this).slider("value");
            $('#inputShadowVerticalLenght').val(valueHorizontalLengthSlider);
            valuesOfShadowBox();
        });
        $('#sliderShadowBlur').slider({
            min: 0,
            max: 300,
            value: 50
        });
        $("#sliderShadowBlur").unbind();
        $("#sliderShadowBlur").on("slide", function (event, ui) {
            panels.changeContent();
            var valueHorizontalLengthSlider = $(this).slider("value");
            $('#inputShadowBlur').val(valueHorizontalLengthSlider);
            valuesOfShadowBox();
        });
        $('#sliderShadowSpread').slider({
            min: -200,
            max: 200,
            value: 20
        });
        $("#sliderShadowSpread").unbind();
        $("#sliderShadowSpread").on("slide", function (event, ui) {
            panels.changeContent();
            var valueHorizontalLengthSlider = $(this).slider("value");
            $('#inputShadowSpread').val(valueHorizontalLengthSlider);
            valuesOfShadowBox();
        });
        $('#sliderShadowOpacity').slider({
            min: 0,
            max: 1,
            step: 0.01,
            value: 0.65
        });
    };
    // init
    this.initEditor = function(item){
        var buttonPostDisp = $('.optionDisplay button');
        var buttonPostFloat = $('.contWirthBtnForFloat button');
        var buttonPostClear = $('.contWirthBtnForClear button');
        var buttonPostOverflow = $('.contWirthBtnForOverflow button');
        var buttonPostPosition = buttonPostOverflow;
        var style = window.getComputedStyle(item[0], null);
        var marginTop = style.marginTop;
        var marginBottom = style.marginBottom;
        var marginLeft = style.marginLeft;
        var marginRight = style.marginRight;
        var paddingTop = style.paddingTop;
        var paddingBottom = style.paddingBottom;
        var paddingLeft = style.paddingLeft;
        var paddingRight = style.paddingRight;
        var display = style.display;
        var float = style.float;
        var clear = style.clear;
        var overflow = style.overflow;
        var position = style.position;
        var positionTop = style.top;
        var positionBottom = style.bottom;
        var positionLeft = style.left;
        var positionRight = style.right;
        var zIndex = style.zIndex;
        //for background
        var backgroundAttachment = style.backgroundAttachment;
        var backgroundColor = style.backgroundColor;
        var backgroundImage = style.backgroundImage;
        var backgroundPosition = style.backgroundPosition;
        var backgroundRepeat = style.backgroundRepeat;
        var backgroundSize = style.backgroundSize;
        var divWithInfoPrevBgCover = $('.divWithInfoPrevBg button[data-original-title="Cover"]');
        var divWithInfoPrevBgContain = $('.divWithInfoPrevBg button[data-original-title="Contain"]');
        var idzIndex = $('#zIndex');
        //Border def
        /*var borderBottomWidth = style.borderBottomWidth;
        var borderBottomLeftRadius = style.borderBottomLeftRadius;
        var borderBottomRightRadius = style.borderBottomRightRadius;
        var borderTopLeftRadius = style.borderTopLeftRadius;
        var borderTopRightRadius = style.borderTopRightRadius;
        var borderColorDef = style.borderColor;*/

        //Preview Position
            //Display
            if (display === 'block') {
                buttonPostPosition.removeClass('selectedDisplay');
                $('#block').addClass('selectedDisplay');
            } else if (display === 'inline-block') {
                buttonPostPosition.removeClass('selectedDisplay');
                $('#inline-block').addClass('selectedDisplay');
            } else if (display === 'inline') {
                buttonPostPosition.removeClass('selectedDisplay');
                $('#inline').addClass('selectedDisplay');
            } else if (display === 'none') {
                buttonPostPosition.removeClass('selectedDisplay');
                $('#none').addClass('selectedDisplay');
            }
            //float
            if (float === 'none') {
                buttonPostFloat.removeClass('selectedDisplay');
                $('#noneFloat').addClass('selectedDisplay');
            } else if (float === 'left') {
                buttonPostFloat.removeClass('selectedDisplay');
                $('#leftFloat').addClass('selectedDisplay');
            } else if (float === 'right') {
                buttonPostFloat.removeClass('selectedDisplay');
                $('#rightFloat').addClass('selectedDisplay');
            }
            //clear
            if (clear === 'none') {
                buttonPostClear.removeClass('selectedDisplay');
                $('#noneClear').addClass('selectedDisplay');
            } else if (clear === 'left') {
                buttonPostClear.removeClass('selectedDisplay');
                $('#leftClear').addClass('selectedDisplay');
            } else if (clear === 'right') {
                buttonPostClear.removeClass('selectedDisplay');
                $('#rightClear').addClass('selectedDisplay');
            } else if (clear === 'both') {
                buttonPostClear.removeClass('selectedDisplay');
                $('#bothClear').addClass('selectedDisplay');
            }
            //overflow
            if (overflow === 'visible') {
                buttonPostOverflow.removeClass('selectedDisplay');
                $('#visibleOverflow').addClass('selectedDisplay');
            } else if (overflow === 'hidden') {
                buttonPostOverflow.removeClass('selectedDisplay');
                $('#hiddenOverflow').addClass('selectedDisplay');
            } else if (overflow === 'scroll') {
                buttonPostOverflow.removeClass('selectedDisplay');
                $('#scrollOverflow').addClass('selectedDisplay');
            } else if (overflow === 'auto') {
                buttonPostOverflow.removeClass('selectedDisplay');
                $('#autoOverflow').addClass('selectedDisplay');
            }
            //position
            if (position === 'static') {
                buttonPostOverflow.removeClass('selectedDisplay');
                $('#staticPosition').addClass('selectedDisplay');
            } else if (position === 'relative') {
                buttonPostOverflow.removeClass('selectedDisplay');
                $('#relativePosition').addClass('selectedDisplay');
            } else if (position === 'absolute') {
                buttonPostOverflow.removeClass('selectedDisplay');
                $('#absolutePosition').addClass('selectedDisplay');
            } else if (position === 'fixed') {
                buttonPostOverflow.removeClass('selectedDisplay');
                $('#fixedPosition').addClass('selectedDisplay');
            }
            //computed value for position absolute, relative, fixed get
            $('#inputPositionTop').val(positionTop);
            $('#inputPositionBottom').val(positionBottom);
            $('#inputPositionLeft').val(positionLeft);
            $('#inputPositionRight').val(positionRight);
            //z-index
            idzIndex.val(zIndex);
            //margin padding input get
            $('#inputMarginTop').val(marginTop.replace(/\D/g, ''));
            $('#inputMarginBottom').val(marginBottom.replace(/\D/g, ''));
            $('#inputMarginLeft').val(marginLeft.replace(/\D/g, ''));
            $('#inputMarginRight').val(marginRight.replace(/\D/g, ''));
            $('#inputPaddingTop').val(paddingTop.replace(/\D/g, ''));
            $('#inputPaddingBottom').val(paddingBottom.replace(/\D/g, ''));
            $('#inputPaddingLeft').val(paddingLeft.replace(/\D/g, ''));
            $('#inputPaddingRight').val(paddingRight.replace(/\D/g, ''));

        //Default value for Background
        //Bg image
            if (backgroundImage === "none") {
            } else {
                var srcBgImg = backgroundImage.slice(5).slice(0, -2);
                //var weightActiveBG = backgroundImageClear.naturalWidth + 'x' + backgroundImageClear.naturalHeight;
                $(".bgPrevievImg").attr('src', srcBgImg);
                getWeightObj(srcBgImg)
            }
        // Preview color for bg
        var backgroundColorClear = rgb2hex(backgroundColor);
        //Preview Size for bg
            if (backgroundSize === "cover") {
                $('#BgWidthinputSpinner1').val('auto');
                $('#BgHeightinputSpinner1').val('auto');
                divWithInfoPrevBgContain.removeClass('ActiveBgButton');
                divWithInfoPrevBgCover.addClass('ActiveBgButton');
            } else if (backgroundSize === "contain") {
                $('#BgWidthinputSpinner1').val('auto');
                $('#BgHeightinputSpinner1').val('auto');
                divWithInfoPrevBgCover.removeClass('ActiveBgButton');
                divWithInfoPrevBgContain.addClass('ActiveBgButton');
            } else if (backgroundSize === "auto") {
                $('#BgWidthinputSpinner1').val('auto');
                $('#BgHeightinputSpinner1').val('auto');
                divWithInfoPrevBgCover.removeClass('ActiveBgButton');
                divWithInfoPrevBgContain.removeClass('ActiveBgButton');
            } else {
                var backgroundSizeSplit = backgroundSize.split(' ');
                var pxproc = backgroundSizeSplit[0].substr(backgroundSizeSplit[0].length - 1);
                //backgroundSizeSplit
                if (backgroundSizeSplit.length === 1) {
                    $('#BgWidthinputSpinner1').val(backgroundSizeSplit[0].replace(/\D/g, ''));
                    $('#BgHeightinputSpinner1').val(backgroundSizeSplit[0].replace(/\D/g, ''));
                }
                if (backgroundSizeSplit.length === 2) {
                    $('#BgWidthinputSpinner1').val(backgroundSizeSplit[0].replace(/\D/g, ''));
                    $('#BgHeightinputSpinner1').val(backgroundSizeSplit[1].replace(/\D/g, ''));
                }
            }
        var checkRash = $('#inputWithSpinner-BgHeight .hiddenDropdaunInSpinner, #inputWithSpinner-BgWidth .hiddenDropdaunInSpinner');
        var checkRashHeightWidth = $('#inputWithSpinner-BgHeight, #inputWithSpinner-BgWidth ');
        var checkRashHeightCover = divWithInfoPrevBgCover;
        var checkRashHeightContain = divWithInfoPrevBgContain;
            if (pxproc === "x") {
                checkRash.find('li').removeClass('activeExSpinner');
                checkRash.find('li:contains("px")').addClass('activeExSpinner');
                checkRashHeightWidth.find('input[type="text"]').attr('data-position', 'px');
                checkRashHeightWidth.find('span.listOfpx').text('px');
                checkRashHeightCover.removeClass('ActiveBgButton');
                checkRashHeightContain.removeClass('ActiveBgButton');
            } else if (pxproc === "%") {
                checkRash.find('li').removeClass('activeExSpinner');
                checkRash.find('li:contains("%")').addClass('activeExSpinner');
                checkRashHeightWidth.find('input[type="text"]').attr('data-position', '%');
                checkRashHeightWidth.find('span.listOfpx').text('%');
                checkRashHeightCover.removeClass('ActiveBgButton');
                checkRashHeightContain.removeClass('ActiveBgButton');
            } else {
                checkRash.find('li').removeClass('activeExSpinner');
                checkRash.find('li:contains("auto")').addClass('activeExSpinner');
                checkRashHeightWidth.find('input[type="text"]').attr('data-position', 'auto');
                checkRashHeightWidth.find('span.listOfpx').text('auto');
            }
            function sizeBgFunc(inputBorderWidth) {
                item.css('backgroundSize', inputBorderWidth)
            }
        //Preview position for bg
        var presetColBgButton = $('.presetColBg button');
            var backgroundPositionSplit = backgroundPosition.split(' ');
            var backgroundPositionSplitH = backgroundPositionSplit[0].replace(/\D/g, '');
            var backgroundPositionSplitV = backgroundPositionSplit[1].replace(/\D/g, '');
            if (backgroundPositionSplitH === "0" && backgroundPositionSplitV === "0") {
                presetColBgButton.attr('id', '');
                $('.firstColumnPreset').find('button[data-positionh="0"][data-positionv="0"]').trigger('click');
            } else if (backgroundPositionSplitH === "0" && backgroundPositionSplitV === "50") {
                presetColBgButton.attr('id', '');
                $('.firstColumnPreset').find('button[data-positionh="0"][data-positionv="50"]').trigger('click');
            } else if (backgroundPositionSplitH === "0" && backgroundPositionSplitV === "100") {
                presetColBgButton.attr('id', '');
                $('.firstColumnPreset').find('button[data-positionh="0"][data-positionv="100"]').trigger('click');
            } else if (backgroundPositionSplitH === "50" && backgroundPositionSplitV === "0") {
                presetColBgButton.attr('id', '');
                $('.secondColumnPreset').find('button[data-positionh="50"][data-positionv="0"]').trigger('click');
            } else if (backgroundPositionSplitH === "50" && backgroundPositionSplitV === "50") {
                presetColBgButton.attr('id', '');
                $('.secondColumnPreset').find('button[data-positionh="50"][data-positionv="50"]').trigger('click');
            } else if (backgroundPositionSplitH === "50" && backgroundPositionSplitV === "100") {
                presetColBgButton.attr('id', '');
                $('.secondColumnPreset').find('button[data-positionh="50"][data-positionv="0"]').trigger('click');
            } else if (backgroundPositionSplitH === "100" && backgroundPositionSplitV === "0") {
                presetColBgButton.attr('id', '');
                $('.ThreeColumnPreset').find('button[data-positionh="100"][data-positionv="0"]').trigger('click');
            } else if (backgroundPositionSplitH === "100" && backgroundPositionSplitV === "50") {
                presetColBgButton.attr('id', '');
                $('.ThreeColumnPreset').find('button[data-positionh="100"][data-positionv="50"]').trigger('click');
            } else if (backgroundPositionSplitH === "100" && backgroundPositionSplitV === "100") {
                presetColBgButton.attr('id', '');
                $('.ThreeColumnPreset').find('button[data-positionh="100"][data-positionv="100"]').trigger('click');
            } else {
                presetColBgButton.attr('id', '');
                $('#posBgValH').text(backgroundPositionSplitH + '%');
                $('#posBgValV').text(backgroundPositionSplitV + '%');
            }
        //Preview repeat for bg
        // backgroundRepeat
        var bgRepeatsbutButton = $('.bgRepeatsbut button');
        var bgRepeatsbutNoButton = $('.bgRepeatsbut');
            if (backgroundRepeat === "repeat") {
                bgRepeatsbutButton.removeClass('ActiveBgButton');
                bgRepeatsbutNoButton.find('button[data-original-title="Repeat"]').trigger('click');
            } else if (backgroundRepeat === "repeat-x") {
                bgRepeatsbutButton.removeClass('ActiveBgButton');
                bgRepeatsbutNoButton.find('button[data-original-title="repeat-x"]').trigger('click');
            } else if (backgroundRepeat === "repeat-y") {
                bgRepeatsbutButton.removeClass('ActiveBgButton');
                bgRepeatsbutNoButton.find('button[data-original-title="repeat-y"]').trigger('click');
            } else if (backgroundRepeat === "no-repeat") {
                bgRepeatsbutButton.removeClass('ActiveBgButton');
                bgRepeatsbutNoButton.find('button[data-original-title="no-repeat"]').trigger('click');
            }
        //Bg cover/content add in row
        var divWithInfoPrevBg = $('.divWithInfoPrevBg button');
        divWithInfoPrevBg.on('click', function(){
            divWithInfoPrevBg.removeClass('ActiveBgButton');
            $(this).addClass('ActiveBgButton');
        });
        var spinnerSelects;
        $('.chekInput').unbind();
        $("body").on('click', '.chekInput', function () {
            if (spinnerSelects !== $(this).attr("id")) {
                spinnerSelects = $(this).attr("id");
                var spinnerSelect = "#" + spinnerSelects;
                spinnerUniver(spinnerSelect);
            } else {


            }
        });
        function spinnerUniver(spinnerSelect) {
            $(spinnerSelect).find('.listOfpx').unbind();
            $(spinnerSelect).find('.listOfpx').on("click", function () {
                var dropDownSpinner = $(spinnerSelect).find('.hiddenDropdaunInSpinner');
                if (dropDownSpinner.css('display') == 'none') {
                    dropDownSpinner.show();
                    var ListEx = $(spinnerSelect).find('.hiddenDropdaunInSpinner');
                    ListEx.find('li').on('click', function () {
                        ListEx.find('li').removeClass('activeExSpinner');
                        $(this).addClass('activeExSpinner');
                        var valExSpinner = $(this).text();
                        $(spinnerSelect).find('.listOfpx').attr('data-position', valExSpinner);
                        $(spinnerSelect).find('.listOfpx').text(valExSpinner);
                        $(spinnerSelect).find('.listOfpx').attr('data-position', valExSpinner);
                        if (valExSpinner == "auto") {
                            $(spinnerSelect).find('input').attr('placeholder', valExSpinner);
                            $(spinnerSelect).find('input').val(valExSpinner);
                            $(spinnerSelect).find('input').attr('readonly', true);
                        } else {
                            $(spinnerSelect).find('input').attr('readonly', false);
                            $(spinnerSelect).find('input').attr('placeholder', "")
                        }
                        $(spinnerSelect).find('input').attr('data-position', valExSpinner);
                        dropDownSpinner.hide();
                    })
                } else {
                    dropDownSpinner.hide();
                }
            });
            $(spinnerSelect).find('.upArrowInSpinner').unbind();
            $(spinnerSelect).find('.upArrowInSpinner').on("click", function () {
                var self = $(spinnerSelect);
                var workInput = self.find('input');
                var valInputFontSize = self.find('input').val();
                if (valInputFontSize == 'auto') {
                    valInputFontSize = 0;

                }
                self.find('input').val(++valInputFontSize);
            });
            $(spinnerSelect).find('.downArrowInSpinner').unbind();
            $(spinnerSelect).find('.downArrowInSpinner').on("click", function () {
                var self = $(spinnerSelect);
                var workInput = self.find('input');
                var valInputFontSize = self.find('input').val();
                if (valInputFontSize == 'auto') {
                    valInputFontSize = 0
                }
                self.find('input').val(--valInputFontSize);
            });
        }
        idzIndex.parent('.chekInput').unbind();
        idzIndex.parent('.chekInput').on('click', function () {
            var thisZIndex = $(this);
            selfGlobal.zIndexInput(item, thisZIndex);
        });

    };
};