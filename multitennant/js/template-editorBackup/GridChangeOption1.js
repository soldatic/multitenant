"use strict";

var row;
var countColInRow;
var zoneCont1;
var zoneCont2;
var zoneCont3;
var zoneCont4;
var zoneCont5;
var zoneCont6;
var countsGridInner;
var zoneNumb = [];
var dragSpinnerPos = ['', '15', '33', '51', '69', '87', '105', '127', '141', '159', '177', '195'];
var returnDynamycNumb = 0;
var column;
$(".gridOptin li, #organized").click(function () {
    $(".gridOptin li").removeClass("gridOptinActive");
    $(this).addClass("gridOptinActive");
    var countsGrid = $(this).attr('data-buttonId');
    $('.contForGridSpinner').attr('data-buttonId', countsGrid);
    switch (countsGrid) {
        case "1":
        case "4":
        case "5":
            TwoGridColumn();
            break;
        case "2":
        case "6":
            ThreeGridColumn();
            break;
        case "3":
        case "7":
            fourGridColumn();
            break;
        case "8":
            fiveGridColumn();
            break;
        case "9":
            sixGridColumn();
            break;
    }
    row = grid.currentItem.parent();
    /* console.log(row)*/
    countColInRow = row.find('.column').length;
    column = row.find('.column');
    dragElement();
});

var GridOptionSetting = function GridOptionSetting() {
    zoneCont1 = $('#zoneCont1');
    zoneCont2 = $('#zoneCont2');
    zoneCont3 = $('#zoneCont3');
    zoneCont4 = $('#zoneCont4');
    zoneCont5 = $('#zoneCont5');
    zoneCont6 = $('#zoneCont6');
    countsGridInner = $('.contForGridSpinner').attr('data-buttonId');
    if ($("#drag1").length !== 0 && $("#drag2").length === 0) {
        (function () {
            var firstchangesGrid1 = function firstchangesGrid1() {
                // console.log(2312313123123);
                var drag1 = parseInt($('#drag1').css("left"));
                var DivWithHandle = parseInt($('.DivWithHandle').css("width"));
                var intervalDrag1Right = DivWithHandle - drag1;
                zoneCont1.css("width", drag1);
                zoneCont2.css({
                    left: drag1 + 20,
                    width: intervalDrag1Right - 6
                });
                var zoneCont1Numb = parseInt(zoneCont1.css("width"));
                zoneNumb['zone1'] = Math.ceil(zoneCont1Numb / 18);
                zoneCont1.find("p").text(zoneNumb['zone1']);

                var zoneCont2Numb = parseInt(zoneCont2.css("width"));
                zoneNumb['zone2'] = Math.ceil(zoneCont2Numb / 18);
                zoneCont2.find("p").text(zoneNumb['zone2']);
                zoneNumb['zone3'] = 0;
                zoneNumb['zone4'] = 0;
                zoneNumb['zone5'] = 0;
                zoneNumb['zone6'] = 0;
            };

            $(".DivWithHandle *").on("drag", function (event, ui) {
                firstchangesGrid1();
            });
            firstchangesGrid1();

            $('#drag1').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var drag1 = parseInt($('#drag1').css("left")) - 15;
                    var startPosition = 15;
                    if (ui.position.left > 195) ui.position.left = 195;
                    if (ui.position.left < 15) ui.position.left = 15;
                }
            });
            switch (countsGridInner) {
                case "1":
                    $('.TwoGridColumn #drag1').css("left", "105px");
                    firstchangesGrid1();
                    break;
                case "4":
                    $('.TwoGridColumn #drag1').css("left", "51px");
                    firstchangesGrid1();
                    break;
                case "5":
                    $('.TwoGridColumn #drag1').css("left", "159px");
                    firstchangesGrid1();
                    break;
            }
        })();
    }
    // 2
    if ($("#drag2").length !== 0 && $("#drag3").length === 0) {
        (function () {
            var firstchangesGrid2 = function firstchangesGrid2() {
                var drag1 = parseInt($('#drag1').css("left"));
                var drag2 = parseInt($('#drag2').css("left"));
                var drag2and1Interval = drag2 - drag1;
                var DivWithHandle = parseInt($('.DivWithHandle').css("width"));
                var intervalDrag2Right = DivWithHandle - drag2;
                zoneCont1.css("width", drag1);
                zoneCont2.css({
                    left: drag1 + 20,
                    width: drag2and1Interval
                });
                zoneCont3.css({
                    left: drag2 + 24,
                    width: intervalDrag2Right - 9
                });
                var zoneCont1Numb = parseInt(zoneCont1.css("width"));
                zoneNumb['zone1'] = Math.ceil(zoneCont1Numb / 18);
                zoneCont1.find("p").text(zoneNumb['zone1']);

                var zoneCont2Numb = parseInt(zoneCont2.css("width"));
                zoneNumb['zone2'] = Math.ceil(zoneCont2Numb / 18);
                zoneCont2.find("p").text(zoneNumb['zone2']);

                var zoneCont3Numb = parseInt(zoneCont3.css("width"));
                zoneNumb['zone3'] = Math.ceil(zoneCont3Numb / 18);
                zoneCont3.find("p").text(zoneNumb['zone3']);
                zoneNumb['zone4'] = 0;
                zoneNumb['zone5'] = 0;
                zoneNumb['zone6'] = 0;
            };

            firstchangesGrid2();
            $(".DivWithHandle *").on("drag", function (event, ui) {
                $(this).css("borderColor", "#337ab7");
                firstchangesGrid2();
            });
            $('#drag1').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var drag2 = parseInt($('#drag2').css("left")) - 18;
                    var startPosition = 15;
                    if (ui.position.left > drag2) ui.position.left = drag2;
                    if (ui.position.left < 15) ui.position.left = 15;
                }
            });
            $('#drag2').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var DivWithHandle = parseInt($('.DivWithHandle').css("width")) - 27;
                    var Zone1Width = parseInt($('#zoneCont1').css("width")) + 18;
                    var startPosition = DivWithHandle;
                    if (ui.position.left > startPosition) ui.position.left = startPosition;
                    if (ui.position.left < Zone1Width) ui.position.left = Zone1Width;
                }
            });
            switch (countsGridInner) {
                case "2":
                    $('#drag1').css("left", "69px");
                    $('#drag2').css("left", "141px");
                    firstchangesGrid2();
                    break;
                case "6":
                    $('#drag1').css("left", "51px");
                    $('#drag2').css("left", "158px");
                    firstchangesGrid2();
                    break;
            }
        })();
    }
    // 3
    if ($("#drag3").length !== 0 && $("#drag4").length === 0) {
        (function () {
            var firstchangesGrid3 = function firstchangesGrid3() {
                var drag1 = parseInt($('#drag1').css("left"));
                var drag2 = parseInt($('#drag2').css("left"));
                var drag3 = parseInt($('#drag3').css("left"));
                var drag2and1Interval = drag2 - drag1;
                var drag2and3Interval = drag3 - drag2;
                var DivWithHandle = parseInt($('.DivWithHandle').css("width"));
                var intervalDrag2Right = DivWithHandle - drag3;
                zoneCont1.css("width", drag1);
                zoneCont2.css({
                    left: drag1 + 21,
                    width: drag2and1Interval
                });
                zoneCont3.css({
                    left: drag2 + 27,
                    width: drag2and3Interval
                });
                zoneCont4.css({
                    left: drag3 + 32,
                    width: intervalDrag2Right - 17
                });
                var zoneCont1Numb = parseInt(zoneCont1.css("width"));
                zoneNumb['zone1'] = Math.ceil(zoneCont1Numb / 18);
                zoneCont1.find("p").text(zoneNumb['zone1']);

                var zoneCont2Numb = parseInt(zoneCont2.css("width"));
                zoneNumb['zone2'] = Math.ceil(zoneCont2Numb / 18);
                zoneCont2.find("p").text(zoneNumb['zone2']);

                var zoneCont3Numb = parseInt(zoneCont3.css("width"));
                zoneNumb['zone3'] = Math.ceil(zoneCont3Numb / 18);
                zoneCont3.find("p").text(zoneNumb['zone3']);

                var zoneCont4Numb = parseInt(zoneCont4.css("width"));
                zoneNumb['zone4'] = Math.ceil(zoneCont4Numb / 18);
                zoneCont4.find("p").text(zoneNumb['zone4']);
                zoneNumb['zone5'] = 0;
                zoneNumb['zone6'] = 0;
            };

            firstchangesGrid3();
            $(".DivWithHandle *").on("drag", function (event, ui) {
                //$(this).css("borderColor", "#337ab7");
                firstchangesGrid3();
            });
            $('#drag1').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var drag2 = parseInt($('#drag2').css("left")) - 18;
                    var startPosition = 18;
                    if (ui.position.left > drag2) ui.position.left = drag2;
                    if (ui.position.left < 18) ui.position.left = 18;
                }
            });
            $('#drag2').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var Zone1Width = parseInt($('#zoneCont1').css("width")) + 18;
                    var drag2 = parseInt($('#zoneCont4').css("left")) - 40;
                    if (ui.position.left > drag2) ui.position.left = drag2;
                    if (ui.position.left < Zone1Width) ui.position.left = Zone1Width;
                }
            });
            $('#drag3').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var DivWithHandle = parseInt($('.DivWithHandle').css("width")) - 35;
                    //var Zone1Width = parseInt($('#zoneCont2').css("width"))+68;

                    var drag1Z = parseInt($('#drag2').css("left"));
                    var Zone3Full = drag1Z + 17;

                    var startPosition = DivWithHandle;
                    if (ui.position.left > startPosition) ui.position.left = startPosition;
                    if (ui.position.left < Zone3Full) ui.position.left = Zone3Full;
                }
            });
            switch (countsGridInner) {
                case "3":
                    $('.fourGridColumn #drag1').css("left", "51px");
                    $('.fourGridColumn #drag2').css("left", "105px");
                    $('.fourGridColumn #drag3').css("left", "159px");
                    firstchangesGrid3();
                    break;
                case "7":
                    $('.fourGridColumn #drag1').css("left", "36px");
                    $('.fourGridColumn #drag2').css("left", "87px");
                    $('.fourGridColumn #drag3').css("left", "177px");
                    firstchangesGrid3();
                    break;
            }
        })();
    }

    if ($("#drag4").length !== 0 && $("#drag5").length === 0) {
        (function () {
            var firstchangesGrid4 = function firstchangesGrid4() {
                var drag1 = parseInt($('#drag1').css("left"));
                var drag2 = parseInt($('#drag2').css("left"));
                var drag3 = parseInt($('#drag3').css("left"));
                var drag4 = parseInt($('#drag4').css("left"));
                var drag2and1Interval = drag2 - drag1;
                var drag2and3Interval = drag3 - drag2;
                var drag3and4Interval = drag4 - drag3;
                var DivWithHandle = parseInt($('.DivWithHandle').css("width"));
                var intervalDrag2Right = DivWithHandle - drag4;
                zoneCont1.css("width", drag1);
                zoneCont2.css({
                    left: drag1 + 21,
                    width: drag2and1Interval
                });
                zoneCont3.css({
                    left: drag2 + 27,
                    width: drag2and3Interval
                });
                zoneCont4.css({
                    left: drag3 + 32,
                    width: drag3and4Interval
                });
                zoneCont5.css({
                    left: drag4 + 37,
                    width: intervalDrag2Right - 22
                });
                var zoneCont1Numb = parseInt(zoneCont1.css("width"));
                zoneNumb['zone1'] = Math.ceil(zoneCont1Numb / 18);
                zoneCont1.find("p").text(zoneNumb['zone1']);

                var zoneCont2Numb = parseInt(zoneCont2.css("width"));
                zoneNumb['zone2'] = Math.ceil(zoneCont2Numb / 18);
                zoneCont2.find("p").text(zoneNumb['zone2']);

                var zoneCont3Numb = parseInt(zoneCont3.css("width"));
                zoneNumb['zone3'] = Math.ceil(zoneCont3Numb / 18);
                zoneCont3.find("p").text(zoneNumb['zone3']);

                var zoneCont4Numb = parseInt(zoneCont4.css("width"));
                zoneNumb['zone4'] = Math.ceil(zoneCont4Numb / 18);
                zoneCont4.find("p").text(zoneNumb['zone4']);

                var zoneCont5Numb = parseInt(zoneCont5.css("width"));
                zoneNumb['zone5'] = Math.ceil(zoneCont5Numb / 18);
                zoneCont5.find("p").text(zoneNumb['zone5']);
                zoneNumb['zone6'] = 0;
            };

            firstchangesGrid4();
            $(".DivWithHandle *").on("drag", function (event, ui) {
                firstchangesGrid4();
            });
            $('#drag1').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var drag2 = parseInt($('#drag2').css("left")) - 18;
                    var startPosition = 15;
                    if (ui.position.left > drag2) ui.position.left = drag2;
                    if (ui.position.left < 15) ui.position.left = 15;
                }
            });
            $('#drag2').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var drag3 = parseInt($('#drag3').css("left")) - 18;
                    var Zone1Width = parseInt($('#zoneCont1').css("width")) + 15;
                    if (ui.position.left > drag3) ui.position.left = drag3;
                    if (ui.position.left < Zone1Width) ui.position.left = Zone1Width;
                }
            });
            $('#drag3').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var drag3 = parseInt($('#drag4').css("left")) - 18;
                    var drag1Z = parseInt($('#drag2').css("left"));
                    var Zone1Full = drag1Z + 15;
                    if (ui.position.left > drag3) ui.position.left = drag3;
                    if (ui.position.left < Zone1Full) ui.position.left = Zone1Full;
                }
            });
            $('#drag4').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var DivWithHandle = parseInt($('.DivWithHandle').css("width")) - 37;
                    var drag1Z = parseInt($('#drag3').css("left"));
                    var Zone3Full = drag1Z + 15;
                    var startPosition = DivWithHandle;
                    if (ui.position.left > startPosition) ui.position.left = startPosition;
                    if (ui.position.left < Zone3Full) ui.position.left = Zone3Full;
                }
            });
            switch (countsGridInner) {
                case "8":
                    $('#drag1').css("left", "33px");
                    $('#drag2').css("left", "69px");
                    $('#drag3').css("left", "105px");
                    $('#drag4').css("left", "159px");
                    firstchangesGrid4();
                    break;
            }
        })();
    }
    if ($("#drag5").length !== 0) {
        (function () {
            var firstchangesGrid5 = function firstchangesGrid5() {
                var drag1 = parseInt($('#drag1').css("left"));
                var drag2 = parseInt($('#drag2').css("left"));
                var drag3 = parseInt($('#drag3').css("left"));
                var drag4 = parseInt($('#drag4').css("left"));
                var drag5 = parseInt($('#drag5').css("left"));
                var drag2and1Interval = drag2 - drag1;
                var drag2and3Interval = drag3 - drag2;
                var drag3and4Interval = drag4 - drag3;
                var drag4and5Interval = drag5 - drag4;
                var DivWithHandle = parseInt($('.DivWithHandle').css("width"));
                var intervalDrag2Right = DivWithHandle - drag5;
                zoneCont1.css("width", drag1);
                zoneCont2.css({
                    left: drag1 + 24,
                    width: drag2and1Interval - 3
                });
                zoneCont3.css({
                    left: drag2 + 26,
                    width: drag2and3Interval - 1
                });
                zoneCont4.css({
                    left: drag3 + 35,
                    width: drag3and4Interval - 2
                });
                zoneCont5.css({
                    left: drag4 + 40,
                    width: drag4and5Interval
                });
                zoneCont6.css({
                    left: drag5 + 45,
                    width: intervalDrag2Right - 27
                });
                var zoneCont1Numb = parseInt(zoneCont1.css("width"));
                zoneNumb['zone1'] = Math.ceil(zoneCont1Numb / 18);
                zoneCont1.find("p").text(zoneNumb['zone1']);

                var zoneCont2Numb = parseInt(zoneCont2.css("width"));
                zoneNumb['zone2'] = Math.ceil(zoneCont2Numb / 18);
                zoneCont2.find("p").text(zoneNumb['zone2']);

                var zoneCont3Numb = parseInt(zoneCont3.css("width"));
                zoneNumb['zone3'] = Math.ceil(zoneCont3Numb / 18);
                zoneCont3.find("p").text(zoneNumb['zone3']);

                var zoneCont4Numb = parseInt(zoneCont4.css("width"));
                zoneNumb['zone4'] = Math.ceil(zoneCont4Numb / 18);
                zoneCont4.find("p").text(zoneNumb['zone4']);

                var zoneCont5Numb = parseInt(zoneCont5.css("width"));
                zoneNumb['zone5'] = Math.ceil(zoneCont5Numb / 18);
                zoneCont5.find("p").text(zoneNumb['zone5']);

                var zoneCont6Numb = parseInt(zoneCont6.css("width"));
                zoneNumb['zone6'] = Math.ceil(zoneCont6Numb / 18);
                zoneCont6.find("p").text(zoneNumb['zone6']);
            };

            $('#drag1').css("left", "33px");
            $('#drag2').css("left", "66px");
            $('#drag3').css("left", "103px");
            $('#drag4').css("left", "133px");
            $('#drag5').css("left", "159px");

            firstchangesGrid5();
            $(".DivWithHandle *").on("drag", function (event, ui) {
                firstchangesGrid5();
            });
            $('#drag1').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var drag2 = parseInt($('#drag2').css("left")) - 18;
                    var startPosition = 15;
                    if (ui.position.left > drag2) ui.position.left = drag2;
                    if (ui.position.left < 15) ui.position.left = 15;
                }
            });
            $('#drag2').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var drag3 = parseInt($('#drag3').css("left")) - 18;
                    var Zone1Width = parseInt($('#zoneCont1').css("width")) + 15;
                    if (ui.position.left > drag3) ui.position.left = drag3;
                    if (ui.position.left < Zone1Width) ui.position.left = Zone1Width;
                }
            });
            $('#drag3').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var drag3 = parseInt($('#drag4').css("left")) - 18;
                    var drag1Z = parseInt($('#drag2').css("left"));
                    var Zone1Full = drag1Z + 15;
                    if (ui.position.left > drag3) ui.position.left = drag3;
                    if (ui.position.left < Zone1Full) ui.position.left = Zone1Full;
                }
            });
            $('#drag4').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var drag5 = parseInt($('#drag5').css("left")) - 18;
                    var drag1Z = parseInt($('#drag3').css("left"));
                    var Zone3Full = drag1Z + 15;
                    if (ui.position.left > drag5) ui.position.left = drag5;
                    if (ui.position.left < Zone3Full) ui.position.left = Zone3Full;
                }
            });
            $('#drag5').draggable({
                axis: "x",
                containment: "parent",
                grid: [18],
                drag: function drag(event, ui) {
                    var DivWithHandle = parseInt($('.DivWithHandle').css("width")) - 45;
                    var drag1Z = parseInt($('#drag4').css("left"));
                    var Zone3Full = drag1Z + 15;
                    var startPosition = DivWithHandle;
                    if (ui.position.left > startPosition) ui.position.left = startPosition;
                    if (ui.position.left < Zone3Full) ui.position.left = Zone3Full;
                }
            });
        })();
    }
    $(".dragElement").on("drag", function (event, ui) {
        dragElement();
    });
};

var TwoGridColumn = function TwoGridColumn() {
    $(".contForGridSpinner").html('<div class="DivWithHandle TwoGridColumn"> \
                          <div class="ZoneContFull" id="zoneCont1"><p>1</p></div> \
                          <div id="drag1" class="dragElement"></div> \
                          <div class="ZoneContFull" id="zoneCont2"><p>2</p></div> \
                       </div>');
    GridOptionSetting();
};
var ThreeGridColumn = function ThreeGridColumn() {
    $(".contForGridSpinner").html('<div class="DivWithHandle threeGridColumn"> \
                       <div class="ZoneContFull" id="zoneCont1"><p>1</p></div> \
                       <div id="drag1" class="dragElement"></div> \
                       <div class="ZoneContFull" id="zoneCont2"><p>2</p></div> \
                       <div id="drag2" class="dragElement"></div> \
                       <div class="ZoneContFull" id="zoneCont3"><p>3</p></div> \
                   </div>');
    GridOptionSetting();
};
var fourGridColumn = function fourGridColumn() {
    $(".contForGridSpinner").html('<div class="DivWithHandle fourGridColumn"> \
                            <div class="ZoneContFull" id="zoneCont1"><p>1</p></div> \
                            <div id="drag1" class="dragElement"></div> \
                            <div class="ZoneContFull" id="zoneCont2"><p>2</p></div> \
                            <div id="drag2" class="dragElement"></div> \
                            <div class="ZoneContFull" id="zoneCont3"><p>3</p></div> \
                            <div id="drag3" class="dragElement"></div> \
                            <div class="ZoneContFull" id="zoneCont4"><p>4</p></div> \
                         </div>');
    GridOptionSetting();
};
var fiveGridColumn = function fiveGridColumn() {
    $(".contForGridSpinner").html('<div class="DivWithHandle FiveGridColumn"> \
                          <div class="ZoneContFull" id="zoneCont1"><p>1</p></div> \
                          <div id="drag1" class="dragElement"></div> \
                          <div class="ZoneContFull" id="zoneCont2"><p>2</p></div> \
                          <div id="drag2" class="dragElement"></div> \
                          <div class="ZoneContFull" id="zoneCont3"><p>3</p></div> \
                          <div id="drag3" class="dragElement"></div> \
                          <div class="ZoneContFull" id="zoneCont4"><p>4</p></div> \
                          <div id="drag4" class="dragElement"></div> \
                          <div class="ZoneContFull" id="zoneCont5"><p>4</p></div> \
                    </div>');
    GridOptionSetting();
};
var sixGridColumn = function sixGridColumn() {
    $(".contForGridSpinner").html('<div class="DivWithHandle SixGridColumn"> \
                         <div class="ZoneContFull" id="zoneCont1"><p>1</p></div> \
                         <div id="drag1" class="dragElement"></div> \
                         <div class="ZoneContFull" id="zoneCont2"><p>2</p></div> \
                         <div id="drag2" class="dragElement"></div> \
                         <div class="ZoneContFull" id="zoneCont3"><p>3</p></div> \
                         <div id="drag3" class="dragElement"></div> \
                         <div class="ZoneContFull" id="zoneCont4"><p>4</p></div> \
                         <div id="drag4" class="dragElement"></div> \
                         <div class="ZoneContFull" id="zoneCont5"><p>4</p></div> \
                         <div id="drag5" class="dragElement"></div> \
                         <div class="ZoneContFull" id="zoneCont6"><p>4</p></div> \
                     </div>');
    GridOptionSetting();
};
$(".gridOptinTablet:not(.gridOptinPhone) li").click(function () {
    $(".gridOptinTablet:not(.gridOptinPhone) li").removeClass("gridOptinActive");
    $(this).addClass("gridOptinActive");
    var id = $(this).attr('data-buttonid');
    newColClassTabletMobile(id, 'sm');
});
$(".gridOptinTablet.gridOptinPhone li").click(function () {
    $(".gridOptinTablet.gridOptinPhone li").removeClass("gridOptinActive");
    $(this).addClass("gridOptinActive");
    var id = $(this).attr('data-buttonid');
    newColClassTabletMobile(id, 'xs');
});
function newColClassTabletMobile(id, colType) {
    var arr = [];
    switch (Number(id)) {
        case 0:
            arr = [12];
            break;
        case 1:
            arr = [6, 6];
            break;
        case 2:
            arr = [4, 4, 4];
            break;
        case 3:
            arr = [3, 3, 3, 3];
            break;
        case 4:
            arr = [3, 9];
            break;
        case 5:
            arr = [9, 3];
            break;
        case 6:
            arr = [3, 6, 3];
            break;
        case 7:
            arr = [2, 3, 5, 2];
            break;
        case 8:
            arr = [2, 2, 2, 4, 2];
            break;
        case 9:
            arr = [2, 2, 2, 2, 2, 2];
            break;
        case 10:
            break;
        case 11:
            arr = [6, 6, 6, 6];
            break;
        case 12:
            arr = [4, 4, 4, 4, 4, 4];
            break;
        default:
            arr = [];
            break;
    }
    changeMobileDeviceCol(arr, colType);
}
function changeMobileDeviceCol(arr, colType) {
    if (arr.length > 0) {
        var row = grid.currentItem.parent();
        var column = row.find('.column');
        var index = 0;
        $.each(row.find('.column'), function () {
            removeColClass($(this), colType);
            $(this).addClass('col-' + colType + '-' + arr[index]);
            if (arr.length !== 1) {
                index++;
            }
        });
        panels.changeContent();
        panels.changeHeightIframe();
    }
}
function dragElement() {
    countColInRow = row.find('.column').length;
    fr.find('.remove-item').hide();
    var drag1 = $('#drag1').css("left");
    var drag2 = $('#drag2').css("left");
    var drag3 = $('#drag3').css("left");
    var drag4 = $('#drag4').css("left");
    var drag5 = $('#drag5').css("left");
    deletActiveGrid();
    customGrid();
    // one dragable element
    if (countsGridInner === "1" || countsGridInner === "4" || countsGridInner === "5" || countsGridInner === "two") {
        if (drag1 === "105px") {
            deletActiveGrid();
            $("#GridOption1").addClass("gridOptinActive");
        } else if (drag1 === "51px") {
            deletActiveGrid();
            $("#GridOption4").addClass("gridOptinActive");
        } else if (drag1 === "159px") {
            deletActiveGrid();
            $("#GridOption5").addClass("gridOptinActive");
        } else {
            customGrid();
        }
        changeColumnGrid(2);
    }
    // two dragable element
    if (countsGridInner === "2" || countsGridInner === "6" || countsGridInner === "three") {
        if (drag1 === "69px" && drag2 === "141px" || drag2 === "140px") {
            deletActiveGrid();
            $("#GridOption2").addClass("gridOptinActive");
        } else if (drag1 === "51px" && drag2 === "158px" || drag2 === "159px") {
            deletActiveGrid();
            $("#GridOption6").addClass("gridOptinActive");
        } else {
            customGrid();
        }
        changeColumnGrid(3);
    }
    // three dragable element
    if (countsGridInner === "3" || countsGridInner === "7" || countsGridInner === 'four') {
        if (drag1 === "51px" || drag2 === "54px" && drag3 === "105px" && drag3 === "159px" ) {
            deletActiveGrid();
            $("#GridOption3").addClass("gridOptinActive");
        } else if (drag1 === "36px" && drag2 === "87px" && drag3 === "177px") {
            deletActiveGrid();
            $("#GridOption7").addClass("gridOptinActive");
        } else {
            customGrid();
        }
        changeColumnGrid(4);
    }
    // four dragable element
    if (countsGridInner === "8") {
        if (drag1 === "33px" && drag2 === "69px" && drag3 === "105px" && drag4 === "159px" || countsGridInner === 'five') {
            deletActiveGrid();
            $("#GridOption8").addClass("gridOptinActive");
        } else {
            customGrid();
        }
        changeColumnGrid(5);
    }
    // five dragable element
    if (countsGridInner === "9") {
        if (drag1 === "33px" && drag2 === "66px" && drag3 === "103px" && drag4 === "133px" && drag5 === "159px" || drag5 === "158px") {
            deletActiveGrid();
            $("#GridOption9").addClass("gridOptinActive");
        } else if (drag1 === "33px" && drag2 === "66px" && drag3 === "97px" && drag4 === "123px" && drag5 === "159px") {
            deletActiveGrid();
            $("#GridOption9").addClass("gridOptinActive");
        } else {
            customGrid();
        }
        changeColumnGrid(6);
    }
    panels.changeContent();
    panels.changeHeightIframe();
}

function deletActiveGrid() {
    $(".gridOptin li").removeClass("gridOptinActive");
}
function customGrid() {
    $(".gridOptin li").removeClass("gridOptinActive");
    $("#GridOption10").addClass("gridOptinActive");
}
function removeColClass(element, col) {
    var classes = element.attr('class').split(/\s+/);
    var pattern = /^col-/;
    if (col && col === 'sm') {
        pattern = /^col-sm-/;
    }
    if (col && col === 'xs') {
        pattern = /^col-xs-/;
    }
    for (var i = 0; i < classes.length; i++) {
        var className = classes[i];
        if (className.match(pattern)) {
            element.removeClass(className);
        }
    }
}
function newColClass() {
    var index = 1;
    $.each(row.find('.column'), function () {
        removeColClass($(this));
        $(this).addClass('col-lg-' + zoneNumb['zone' + index]);
        $(this).addClass('col-md-' + zoneNumb['zone' + index]);
        $(this).addClass('col-sm-' + zoneNumb['zone' + index]);
        $(this).addClass('col-xs-' + zoneNumb['zone' + index]);
        index++;
    });
}
function changeColumnDeviceTablet() {
    var dragElementSel = $('.dragElement');
    var gridOptionTablet = $('.gridOptinTablet');
    var idbuttonId = $('.gridOptin').find('li.gridOptinActive').attr('data-buttonId');
    var buttonActTablet = gridOptionTablet.find('li[data-buttonId=' + idbuttonId + ']');
    gridOptionTablet.find('li').hide();
    gridOptionTablet.find('li[data-buttonId="0"]').css('display', 'inline-block');
    switch (idbuttonId) {
        case "1":
            gridOptionTablet.find('li[data-buttonId="4"]').css('display', 'inline-block');
            gridOptionTablet.find('li[data-buttonId="5"]').css('display', 'inline-block');
            buttonActTablet.css('display', 'inline-block');
            break;
        case "2":
            gridOptionTablet.find('li[data-buttonId="6"]').css('display', 'inline-block');
            buttonActTablet.css('display', 'inline-block');
            break;
        case "3":
            gridOptionTablet.find('li[data-buttonId="7"]').css('display', 'inline-block');
            gridOptionTablet.find('li[data-buttonId="11"]').css('display', 'inline-block');
            buttonActTablet.css('display', 'inline-block');
            break;
        case "4":
            gridOptionTablet.find('li[data-buttonId="5"]').css('display', 'inline-block');
            gridOptionTablet.find('li[data-buttonId="1"]').css('display', 'inline-block');
            buttonActTablet.css('display', 'inline-block');
            break;
        case "5":
            gridOptionTablet.find('li[data-buttonId="1"]').css('display', 'inline-block');
            gridOptionTablet.find('li[data-buttonId="4"]').css('display', 'inline-block');
            buttonActTablet.css('display', 'inline-block');
            break;
        case "6":
            gridOptionTablet.find('li[data-buttonId="2"]').css('display', 'inline-block');
            buttonActTablet.css('display', 'inline-block');
            break;
        case "7":
            gridOptionTablet.find('li[data-buttonId="3"]').css('display', 'inline-block');
            gridOptionTablet.find('li[data-buttonId="11"]').css('display', 'inline-block');
            buttonActTablet.css('display', 'inline-block');
            break;
        case "8":
            gridOptionTablet.find('li[data-buttonId="8"]').css('display', 'inline-block');
            buttonActTablet.css('display', 'inline-block');
            break;
        case "9":
            gridOptionTablet.find('li[data-buttonId="9"]').css('display', 'inline-block');
            gridOptionTablet.find('li[data-buttonId="12"]').css('display', 'inline-block');
            buttonActTablet.css('display', 'inline-block');
            break;
        case "10":
            if ($('#drag1').length != "" && $('#drag2').length == "") {
                gridOptionTablet.find('li[data-buttonId="4"]').css('display', 'inline-block');
                gridOptionTablet.find('li[data-buttonId="5"]').css('display', 'inline-block');
                gridOptionTablet.find('li[data-buttonId="1"]').css('display', 'inline-block');
                buttonActTablet.css('display', 'inline-block');
            }
            if ($('#drag2').length != "" && $('#drag3').length == "") {
                gridOptionTablet.find('li[data-buttonId="6"]').css('display', 'inline-block');
                gridOptionTablet.find('li[data-buttonId="2"]').css('display', 'inline-block');
                buttonActTablet.css('display', 'inline-block');
            }
            if ($('#drag3').length != "" && $('#drag4').length == "") {
                gridOptionTablet.find('li[data-buttonId="3"]').css('display', 'inline-block');
                gridOptionTablet.find('li[data-buttonId="7"]').css('display', 'inline-block');
                gridOptionTablet.find('li[data-buttonId="11"]').css('display', 'inline-block');
                buttonActTablet.css('display', 'inline-block');
            }
            if ($('#drag4').length != "" && $('#drag5').length == "") {
                gridOptionTablet.find('li[data-buttonId="8"]').css('display', 'inline-block');
                buttonActTablet.css('display', 'inline-block');
            }
            if ($('#drag5').length != "") {
                gridOptionTablet.find('li[data-buttonId="9"]').css('display', 'inline-block');
                gridOptionTablet.find('li[data-buttonId="12"]').css('display', 'inline-block');
                buttonActTablet.css('display', 'inline-block');
            }
            break;
    }
}

function getColumnSize(item) {
    var self = item;
    if (self.hasClass('column')) {
        var parentElemRow = self.parent('.row');
        var childrenElemRow = parentElemRow.children('.column');
    } else {
        var childrenElemRow = self.children('.column');
    }
    //get col size
    function getColSize(pattern, classColElem) {
        var classIndex = classColElem.getAttribute('class').indexOf(pattern) + pattern.length;
        var searchedClass = classColElem.getAttribute('class').substring(classColElem.getAttribute('class').indexOf(pattern), classColElem.getAttribute('class').indexOf(pattern) + pattern.length + 2).trim();
        var splitedArray = searchedClass.split("-");
        var numbCol = splitedArray[splitedArray.length - 1];
        return numbCol;
    }
    var numbCol = [];
    var drags1;
    var drags2;
    var drags3;
    var drags4;
    var drags5;
    var contForGridSpinner = $('.contForGridSpinner');
    var colColumn = childrenElemRow.length;
    var i = 0;
    console.log('colColumn' + colColumn);

    if(colColumn === 2){
        TwoGridColumn();

        console.log('case 2:');
        childrenElemRow.each(function (index) {
            var classColElem = childrenElemRow[index];
            var pattern = "col-lg-";
            numbCol.push(getColSize(pattern, classColElem));
        });


        drags1 = $('#drag1');

        for (i = 0; i < numbCol.length; i++) {
            if (i === 0) {
                drags1.css('left', dragSpinnerPos[numbCol[0]] + 'px');
            }
        }
        setTimeout(function(){
            $('#organized').click();
            contForGridSpinner.attr('data-buttonid', 'two');
            GridOptionSetting();
            contForGridSpinner.attr('data-buttonid', '');
            numbCol = [];
        },300)


    } else if(colColumn === 3){

        ThreeGridColumn();
        childrenElemRow.each(function (index) {
            var classColElem = childrenElemRow[index];
            var pattern = "col-lg-";
            numbCol.push(getColSize(pattern, classColElem));
        });
        drags1 = $('#drag1');
        drags2 = $('#drag2');
        //var i = 0;
        for (i = 0; i < numbCol.length; i++) {
            if (i === 0) {
                drags1.css('left', dragSpinnerPos[numbCol[0]] + 'px');
            } else if (i === 1) {
                var posFirstSpin = parseInt(numbCol[0]);
                var posSecondSpin = parseInt(numbCol[1]);
                var firstWithSec = posFirstSpin + posSecondSpin;
                drags2.css('left', dragSpinnerPos[firstWithSec] + 'px');
            }
        }
        setTimeout(function(){
            // dragSpinnerPos
            $('#organized1').click();
            contForGridSpinner.attr('data-buttonid', 'three');
            GridOptionSetting();
            contForGridSpinner.attr('data-buttonid', '');
            numbCol = [];
        }, 300)

    } else if(colColumn === 4){
        console.log('case 4:');
        fourGridColumn();
        childrenElemRow.each(function (index) {
            var classColElem = childrenElemRow[index];
            var pattern = "col-lg-";
            numbCol.push(getColSize(pattern, classColElem));
        });
        drags1 = $('#drag1');
        drags2 = $('#drag2');
        drags3 = $('#drag3');
        //i = 0;
        for (i = 0; i < numbCol.length; i++) {
            if (i === 0) {
                drags1.css('left', dragSpinnerPos[numbCol[0]] + 'px');
            } else if (i === 1) {
                posFirstSpin = parseInt(numbCol[0]);
                posSecondSpin = parseInt(numbCol[1]);
                firstWithSec = posFirstSpin + posSecondSpin;
                drags2.css('left', dragSpinnerPos[firstWithSec] + 'px');
            } else if (i === 2) {
                posFirstSpin = parseInt(numbCol[0]);
                posSecondSpin = parseInt(numbCol[1]);
                var posTheerSpin = parseInt(numbCol[2]);
                firstWithSec = posFirstSpin + posSecondSpin + posTheerSpin;
                drags3.css('left', dragSpinnerPos[firstWithSec] + 'px');
            }
        }
        setTimeout(function(){
            // dragSpinnerPos
            $('#organized2').click();
            contForGridSpinner.attr('data-buttonid', 'four');
            GridOptionSetting();
            contForGridSpinner.attr('data-buttonid', '');
            numbCol = [];
        }, 300)

    } else if(colColumn === 5){
        console.log('case 5:');
        fiveGridColumn();
        childrenElemRow.each(function (index) {
            var classColElem = childrenElemRow[index];
            var pattern = "col-lg-";
            numbCol.push(getColSize(pattern, classColElem));
        });
        console.log(numbCol);
        console.log(numbCol.length);
        drags1 = $('#drag1');
        drags2 = $('#drag2');
        drags3 = $('#drag3');
        drags4 = $('#drag4');
        //var i = 0;
        for (i = 0; i < numbCol.length; i++) {
            if (i === 0) {
                drags1.css('left', dragSpinnerPos[numbCol[0]] + 'px');
            } else if (i === 1) {
                posFirstSpin = parseInt(numbCol[0]);
                posSecondSpin = parseInt(numbCol[1]);
                firstWithSec = posFirstSpin + posSecondSpin;
                drags2.css('left', dragSpinnerPos[firstWithSec] + 'px');
            } else if (i === 2) {
                posFirstSpin = parseInt(numbCol[0]);
                posSecondSpin = parseInt(numbCol[1]);
                posTheerSpin = parseInt(numbCol[2]);
                firstWithSec = posFirstSpin + posSecondSpin + posTheerSpin;
                drags3.css('left', dragSpinnerPos[firstWithSec] + 'px');
            } else if (i === 3) {
                posFirstSpin = parseInt(numbCol[0]);
                posSecondSpin = parseInt(numbCol[1]);
                posTheerSpin = parseInt(numbCol[2]);
                var posFiveSpin = parseInt(numbCol[3]);
                firstWithSec = posFirstSpin + posSecondSpin + posTheerSpin + posFiveSpin;
                drags4.css('left', dragSpinnerPos[firstWithSec] + 'px');
            }
        }
        setTimeout(function(){
            // dragSpinnerPos
            $('#organized3').click();
            contForGridSpinner.attr('data-buttonid', 'five');
            GridOptionSetting();
            contForGridSpinner.attr('data-buttonid', '');
            numbCol = [];
        }, 300)
    }
    //console.log(childrenElemRow)
};
function changeColumnGrid(num) {
    var resCol = countColInRow - num;
    if (resCol > 0) {
        for (var i = 0; i < resCol; i++) {
            var res = countColInRow - (i + 1);
            $(column[res]).remove();
        }
    }
    var resCol = num - countColInRow;
    if (resCol > 0) {
        for (var i = 0; i < resCol; i++) {
            var rand = getRandomInt(1111111, 9999999);
            row.append('<div class="column" data-e-id="' + rand + '"></div>');
        }
    }
    changeColumnDeviceTablet();
    newColClass();
    grid.initElements();
    pages.recurseDomChildrenUpdate();
}