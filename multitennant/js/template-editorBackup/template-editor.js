var globalSettings = new function () {
    var self = this;
    this.globalItem = $('#xgate-template-editor');
    this.modalCssId = '#modalGlobalCss';
    this.load = function (data) {
        if (data.src) {
            if (data.src !== 'bg_custom_img') {
                self.globalItem.attr('data-src', data.src);
                self.globalItem.addClass(data.src);
                var li = $('.globalSettings').find('.dropdown-menu li[data-value="' + data.src + '"]');
                li.parent().parent().addClass('active');
                li.addClass('active');
            } else {
                self.globalItem.addClass(data.src);
                self.globalItem.attr('data-src', data.src);
                self.globalItem.attr('data-img555', data.bgImage);
                self.globalItem.css('background-image', 'url(' + data.bgImage + ')');
            }
        }
    };
    this.on = function () {
        $('.globalSettings').find('.uniform-color li').on('mouseover', function () {
            var eL = $(this);
            if (eL.attr('data-value')) {
                var style = eL.attr('data-value');
                self.removeBg();
                self.globalItem.removeAttr('style');
                self.globalItem.addClass(style);
            }
        });
        $('.globalSettings').find('.uniform-color li').on('mouseout', function () {
            self.removeBg();
            if (self.globalItem.attr('data-img')) {
                self.globalItem.addClass('bg_custom_img');
                self.globalItem.attr('data-src', 'bg_custom_img');
                self.globalItem.css('background-image', 'url(' + self.globalItem.attr('data-img') + ')');
            } else {
                self.globalItem.addClass($('.globalSettings .uniform-color li.active').attr('data-value'));
            }
        });

        $('.globalSettings').find('.uniform-color li').on('click', function () {
            var eL = $(this);
            if (eL.attr('data-value')) {
                $('.globalSettings .bg-menu li').removeClass('active');
                if (eL.hasClass('active')) {
                } else {
                    self.globalItem.removeAttr('style');
                    self.globalItem.removeAttr('data-img');
                    var style = eL.attr('data-value');
                    self.globalItem.attr('data-src', style);
                    self.globalItem.addClass(style);
                    eL.addClass('active');
                    eL.parent().parent().addClass('active');
//                    self.save();
                    panels.changeContent();
                }
            }
        });

        $('.globalSettings').find('.body-bg-remove').on('click', function () {
            self.removeAllBgData();
//            self.save();
            panels.changeContent();
        });

        $('.globalSettings').find('.body-bg-choose').on('click', function () {
            parent.galleryManager.bgBody();
        });

        self.cssEditorOn();
    };
    this.cssEditorOn = function () {
        $('#s2').bind('afterShow', function () {
            var value = "dfg";
            var editor = CodeMirror(document.getElementById("code-import"), {
                value: value,
                lineNumbers: true,
                mode: "javascript",
                keyMap: "sublime",
                autoCloseBrackets: true,
                matchBrackets: true,
                showCursorWhenSelecting: true,
                theme: "monokai"
            });
        });
    };
    this.removeBg = function () {
        var classes = self.globalItem.attr('class').split(/\s+/);
        var pattern = /^bg_/;
        for (var i = 0; i < classes.length; i++) {
            var className = classes[i];
            if (className.match(pattern)) {
                self.globalItem.removeClass(className);
            }
        }
    };
    this.removeAllBgData = function () {
        $('.globalSettings .bg-menu li').removeClass('active');
        self.globalItem.removeAttr('data-src');
        self.globalItem.removeAttr('data-img');
        self.globalItem.removeAttr('style');
        self.removeBg();
    };
    this.save = function () {
        var res = {
            src: (self.globalItem.attr('data-src')) ? self.globalItem.attr('data-src') : false,
            bgImage: (self.globalItem.attr('data-img')) ? self.globalItem.attr('data-img') : false,
        };
        res = JSON.stringify(res);
        parent.ajaxData.request({
            url: 'setElements',
            data: {
                content: res,
                pageId: parent.pageId,
                tpl: parent.templateId,
                typeContent: 'settings'
            }
        });
    };
};

parent.pages = new function () {
    var self = this;
    this.currentEditPageId;
    this.typeAction = 'add'; // add or edit
    this.typeActionMenu = 'add'; // add or edit
    this.load = function() {
        parent.ajaxData.request({
            url: 'getPageData',
            data: {
                page: parent.pageId,
                tpl: parent.templateId
            },
            success: function (data) {
                parent.pageChanges = 0;
                $('#saveparent.pageChanges').attr('disabled', '');
                if (data.tpl_page_name) {
                    parent.p.find('#current-page-name').html(data.tpl_page_name);
                }
                if (data.tpl_page_content !== undefined) {
                    $('#grid').html(data.tpl_page_content);
                    $('#grid').find('#edit-text').removeAttr('contenteditable').removeAttr('id');
                    $('#grid').find('.edit-item').removeClass('edit-item');
                }
                if (data.tpl_page_content_options !== undefined && data.tpl_page_content_options !== '') {
                    var contentOptions = JSON.parse(data.tpl_page_content_options);
                    var menuOptions = contentOptions.menu;
                    if (menuOptions) {
                        var lengthMenuOptions = menuOptions.length;
                        for (var i = 0; i < lengthMenuOptions; i++) {
                            parent.menuTool.init(menuOptions[i]);
                        }
                    }
                    var mapOptions = contentOptions.map;
                    if (mapOptions) {
                        var lengthMapOptions = mapOptions.length;
                        for (var i = 0; i < lengthMapOptions; i++) {
                            parent.mapTool.init(mapOptions[i]);
                        }
                    }
                }
                if (tinymce) {
                    //remove all tinymce data
                    tinymce.remove();
                    tinymce.execCommand('mceRemoveControl', true, 'edit-text');
                }

                self.recurseDomChildrenUpdate();
            }
        });
    };
    this.init = function () {
        self.load();
        $('#grid').on('click', 'a', function () {
            return false;
        });
    };
    this.save = function () {
        var menuOptions = [];
        $.each($('.menuJS'), function () {
            var menuId = $(this).attr('id');
            var dataMenu = parent.menuTool.listMenu[menuId];
            var itemData = {
                id_menu: menuId,
                data_menu: {
                    menu_items: dataMenu
                }
            };
            menuOptions.push(itemData);
        });
        var mapOptions = [];
        $.each($('.item-map'), function () {
            var mapId = $(this).attr('id');
            mapOptions.push(parent.mapTool.listMaps[mapId]);
        });
        var contentOptions = {
            menu: menuOptions,
            map: mapOptions
        };
        var newContent = $('#grid').clone();
        newContent.find('.item-map').html('').end();
        newContent.find('script,style,p div,span div').remove().end();
        newContent.removeClass('hover');
        newContent = newContent.html();
        parent.ajaxData.request({
            url: 'setElements',
            data: {
                content: newContent,
                pageId: parent.pageId,
                tpl: parent.templateId,
                typeContent: 'content',
                c_options: JSON.stringify(contentOptions)
            }
        });
    };
    this.recurseDomChildrenUpdate = function () {
        parent.p.find('#DOM-navigator').find('#DOM-navigator-main').html('');
        self.recurseDomChildren(document.getElementById('grid'), true);
        self.structureList();
    };
    this.recurseDomChildren = function (start, output, parentNode) {
        var nodes;
        if (start.childNodes) {
            nodes = start.childNodes;
            if (parentNode) {
                parent.p.find('#DOM-navigator').find('span[data-id=' + $(parentNode).attr('data-e-id') + ']').after('<ul style="display:block;"></ul>'); //
            }
            self.loopNodeChildren(nodes, output, parentNode);
        }
    };
    this.loopNodeChildren = function (nodes, output, parentNode) {
        var node;
        for (var i = 0; i < nodes.length; i++) {
            node = nodes[i];
            if (output) {
                if (node.tagName) {
                    if ($(node).attr('data-e-id')) {
                        if (parentNode) {
                            if ($(parentNode).attr('data-e-id') === undefined) {
                                parentNode = $(parentNode).parent();
                            }
                        }
                        self.outputNode(node, parentNode);
                    }
                }
            }
            if (node.childNodes) {
                self.recurseDomChildren(node, output, node);

            }
        }
    };


    this.outputNode = function (node, parentNode) {
        var whitespace = /^\s+$/g;
        var tagName = node.tagName;
        var name = tagName;
        var dataType = '';
        var iconPic = '<i class="fa fa-fw fa-file-image-o"></i>';
        if ($(node).hasClass('row')) {
            name = 'ROW';
        } else if ($(node).hasClass('column')) {
            name = 'COLUMN';
        } else if ($(node).hasClass('item-map')) {
            name = 'MAP';
            dataType = ' data-type="map"';
        } else if ($(node).hasClass('item-youtube')) {
            dataType = ' data-type="youtube"';
            name = 'YOUTUBE VIDEO';
        }
        var eId = ($(node).attr('data-e-id')) ? ' data-id="' + $(node).attr('data-e-id') + '"' : '';

        if (name === 'ROW') {
            iconPic = ' <i style="transform: rotate(90deg)" class="fa fa-bars"></i> ';
        } else if (name === 'COLUMN') {
            iconPic = ' <i style="transform: rotate(90deg)" class="fa fa-bars"></i> ';
        } else if (name === 'IMG') {
            iconPic = ' <i class="fa fa-picture-o"></i> ';
        } else if (name === 'H1' || name === 'H2' || name === 'H3' || name === 'H4' || name === 'H5') {
            iconPic = ' <i class="fa fa-header"></i> ';
        } else if (name === 'P') {
            iconPic = ' <i class="fa fa-font"></i> ';
        } else if (name === 'MAP') {
            iconPic = ' <i class="fa fa-map-marker"></i> ';
        } else if (name === 'YOUTUBE VIDEO') {
            iconPic = ' <i class="fa fa-youtube-play"></i> ';
        } else {
            iconPic = '<i class="fa fa-fw fa-file-image-o"></i>'
        }
        var el = '<li> \
                        <span href="#" ' + eId + ' ' + dataType + '>' + iconPic + name + '</span> \
                    </li>';
        if (parentNode) {
            parent.p.find('#DOM-navigator').find('span[data-id=' + $(parentNode).attr('data-e-id') + ']').next().append(el);
        } else {
            parent.p.find('#DOM-navigator').find('#DOM-navigator-main').append(el);
        }
    };


    this.structureList = function () {
        parent.p.find('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
        parent.p.find('#DOM-navigator-main').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', '').on('click', function (e) {
            var children = $(this).parent('li.parent_li').find(' > ul > li');
            //if (children.is(':visible')) {
            //    children.hide('fast');
            //    //$(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
            //} else {
            //    children.show('fast');
            //    //$(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
            //}
            e.stopPropagation();
        });
    };

};
parent.gridContent = new function () {
    var self = this;
    this.listPageElements = {};
    this.typeElement;
    this.addNewElement = function (data, dropObj, callback) {
        var htmlElement;
        var typeElement = (data.options !== undefined) ? data.options.typeElement : false;
        switch (typeElement) {
            case 'map':
                htmlElement = data.html;
                setTimeout(function () {
                    parent.mapTool.init(data.options);
                }, 100);
                break;
            case 'menu':
                htmlElement = data.html;
                parent.menuTool.init(data.options);
                break;
            default:
                htmlElement = data.html;
                break;
        }
        console.log(dropObj);
        dropObj.replaceWith(htmlElement);
        callback();
    };
    this.upEventsText = function () {
        //get all elements in container
        if (window.getSelection) {
            var select = document.getSelection();
            var first_select = select.anchorNode.textContent;
            var end_select = select.focusNode.textContent;
            if (first_select == end_select) {
                var string = first_select;
            } else {
                var indexOf = jQuery.trim(select.getRangeAt(0).toString()).indexOf(jQuery.trim(first_select.substring(0, select.anchorOffset)));
                if (indexOf == -1) {
                    string = first_select.substring(0, select.anchorOffset) + select + end_select.substring(select.focusOffset);
                } else if (indexOf == 0) {
                    string = select + end_select.substring(select.focusOffset);
                } else if (indexOf > 0) {
                    string = end_select.substring(0, select.focusOffset) + select + first_select.substring(select.anchorOffset);
                }
            }

            var getComp = window.getComputedStyle(select.focusNode.parentElement);
            var CompfontSize = getComp["fontSize"].replace(/\D/g, '').replace(/((\d{2})(\d*?))$/, '$2');
            var CompLineHeight = getComp["lineHeight"].replace(/\D/g, '').replace(/((\d{2})(\d*?))$/, '$2');
            var CompColor = getComp["color"];
            var CompBackColor = getComp["backgroundColor"];
            var CompLetterSpacing = getComp["letterSpacing"];
            var CompWordSpacing = getComp["wordSpacing"].replace(/\D/g, '').replace(/((\d{2})(\d*?))$/, '$2');
            var CompfontWeight = getComp["fontWeight"];
            var CompColorHex = rgb2hex(CompColor);
            var CompBgColorHex = rgb2hex(CompBackColor);
            //var CompBackColorHex = rgb2hex(CompBackColor);
            if (CompLetterSpacing === "normal") {
                CompLetterSpacing = 0;
            }
            if (CompfontWeight === "normal") {
                CompfontWeight = 300;
            }
            //set default value
            parent.p.find("input#fontSizeSpin").val(CompfontSize);
            parent.p.find("input#LineHeightSpin").val(CompLineHeight);
            parent.p.find("input#LetterSpaceSpin").val(CompLetterSpacing);
            parent.p.find("input#WordSpaceSpin").val(CompWordSpacing);
            parent.p.find('#colorPickerJsTextBG').val(CompBgColorHex);
            parent.p.find('#contForColorText').val(CompColorHex);
            parent.p.find('label[for="colorPickerJsTextBG"]').css("background", CompBgColorHex);
            parent.p.find('label[for="colorPickerJsTextBG"] i').css("background", CompBgColorHex);
            parent.p.find('.mce-ico.mce-i-forecolor').css("color", CompColorHex);
        }
    };
    this.tinyToolbar = function () {
        // Edit text button
        var toolbar = parent.p.find('#toolbarLocation');
        toolbar.find(".testText").click(function () {
            var typeTextEdit = $(this).attr("data-type");
            tinymce.execCommand(typeTextEdit);
            parent.panels.changeContent();
        });
        toolbar.find(".textFontSize").change(function () {
            var typeTextFontSizeEdit = $(this).find("option:selected").attr("data-type");
            tinymce.execCommand("fontSize", false, typeTextFontSizeEdit + "px");
            parent.panels.changeContent();
        });
        toolbar.find(".textFontFamily").change(function () {
            var typeTextFontFamilyEdit = $(this).find("option:selected").attr("data-type");
            tinymce.execCommand("FontName", false, typeTextFontFamilyEdit);
            parent.panels.changeContent();
        });

        parent.p.find(".fontSizeSpin .arrowContrl .upArrow i").unbind();
        parent.p.find(".fontSizeSpin .arrowContrl .upArrow i").on("click", function () {
            var valInputFontSize = parent.p.find("#fontSizeSpin").val();
            parent.p.find("#fontSizeSpin").val(++valInputFontSize);
            tinymce.execCommand("fontSize", false, valInputFontSize + "px");
        });
        parent.p.find(".fontSizeSpin .arrowContrl .downArrow i").unbind();
        parent.p.find(".fontSizeSpin .arrowContrl .downArrow i").on("click", function () {
            var valInputFontSize = parent.p.find("#fontSizeSpin").val();
            parent.p.find("#fontSizeSpin").val(--valInputFontSize);
            tinymce.execCommand("fontSize", false, valInputFontSize + "px");
        });
        parent.p.find("#fontSizeSpin").on("blur", function () {
            var valInputFontSize = parent.p.find("#fontSizeSpin").val();
            tinymce.execCommand("fontSize", false, valInputFontSize + "px");
        });
        parent.p.find(".LineHeightSpin .arrowContrl .upArrow i").unbind();
        parent.p.find(".LineHeightSpin .arrowContrl .upArrow i").on("click", function () {
            var LineHeightSpin = parent.p.find("#LineHeightSpin").val();
            parent.p.find("#LineHeightSpin").val(++LineHeightSpin);
            var dataSelect = Math.random();
            document.execCommand("insertHTML", false, "<span id='" + dataSelect + "'  style='line-height: " + LineHeightSpin + "px;'>" + document.getSelection() + "</span>");
            var select = document.getSelection();
            var first_select = select.anchorNode.textContent;
            var end_select = select.focusNode.textContent;
            if (first_select == end_select) {
                var string = first_select;
            } else {
                var indexOf = jQuery.trim(select.getRangeAt(0).toString()).indexOf(jQuery.trim(first_select.substring(0, select.anchorOffset)));
                if (indexOf == -1) {
                    string = first_select.substring(0, select.anchorOffset) + select + end_select.substring(select.focusOffset);
                } else if (indexOf == 0) {
                    string = select + end_select.substring(select.focusOffset);
                } else if (indexOf > 0) {
                    string = end_select.substring(0, select.focusOffset) + select + first_select.substring(select.anchorOffset);
                }
            }
            var q = document.getElementById(dataSelect);
            var range = document.createRange();
            range.selectNode(q);
            select.addRange(range);
        });

        parent.p.find(".LineHeightSpin .arrowContrl .downArrow i").unbind();
        parent.p.find(".LineHeightSpin .arrowContrl .downArrow i").on("click", function () {
            var LineHeightSpin = parent.p.find("#LineHeightSpin").val();
            parent.p.find("#LineHeightSpin").val(--LineHeightSpin);
            var dataSelect = Math.random();
            document.execCommand("insertHTML", false, "<span id='" + dataSelect + "'  style='line-height: " + LineHeightSpin + "px;'>" + document.getSelection() + "</span>");
            var select = document.getSelection();
            var first_select = select.anchorNode.textContent;
            var end_select = select.focusNode.textContent;
            if (first_select == end_select) {
                var string = first_select;
            } else {
                var indexOf = jQuery.trim(select.getRangeAt(0).toString()).indexOf(jQuery.trim(first_select.substring(0, select.anchorOffset)));
                if (indexOf == -1) {
                    string = first_select.substring(0, select.anchorOffset) + select + end_select.substring(select.focusOffset);
                } else if (indexOf == 0) {
                    string = select + end_select.substring(select.focusOffset);
                } else if (indexOf > 0) {
                    string = end_select.substring(0, select.focusOffset) + select + first_select.substring(select.anchorOffset);
                }
            }
            var q = document.getElementById(dataSelect);
            var range = document.createRange();
            range.selectNode(q);
            select.addRange(range);
        });
        parent.p.find("#LineHeightSpin").on("blur", function () {
            var LineHeightSpin = parent.p.find("#LineHeightSpin").val();
            document.execCommand("insertHTML", false, "<span style='line-height: " + LineHeightSpin + ";'>" + document.getSelection() + "</span>");
        });
        parent.p.find(".LetterSpaceSpin .arrowContrl .upArrow i").unbind();
        parent.p.find(".LetterSpaceSpin .arrowContrl .upArrow i").on("click", function () {
            var LetterSpaceSpin = parent.p.find("#LetterSpaceSpin").val();
            parent.p.find("#LetterSpaceSpin").val(++LetterSpaceSpin);
            var dataSelect = Math.random();
            document.execCommand("insertHTML", false, "<span id='" + dataSelect + "'  style='letter-spacing: " + LetterSpaceSpin + "px;'>" + document.getSelection() + "</span>");
            var select = document.getSelection();
            var first_select = select.anchorNode.textContent;
            var end_select = select.focusNode.textContent;
            if (first_select == end_select) {
                var string = first_select;
            } else {
                var indexOf = jQuery.trim(select.getRangeAt(0).toString()).indexOf(jQuery.trim(first_select.substring(0, select.anchorOffset)));
                if (indexOf == -1) {
                    string = first_select.substring(0, select.anchorOffset) + select + end_select.substring(select.focusOffset);
                } else if (indexOf == 0) {
                    string = select + end_select.substring(select.focusOffset);
                } else if (indexOf > 0) {
                    string = end_select.substring(0, select.focusOffset) + select + first_select.substring(select.anchorOffset);
                }
            }
            var q = document.getElementById(dataSelect);
            var range = document.createRange();
            range.selectNode(q);
            select.addRange(range);
        });
        parent.p.find(".LetterSpaceSpin .arrowContrl .downArrow i").unbind();
        parent.p.find(".LetterSpaceSpin .arrowContrl .downArrow i").on("click", function () {
            var LetterSpaceSpin = parent.p.find("#LetterSpaceSpin").val();
            parent.p.find("#LetterSpaceSpin").val(--LetterSpaceSpin);
            var dataSelect = Math.random();
            document.execCommand("insertHTML", false, "<span id='" + dataSelect + "'  style='letter-spacing: " + LetterSpaceSpin + "px;'>" + document.getSelection() + "</span>");
            var select = document.getSelection();
            var first_select = select.anchorNode.textContent;
            var end_select = select.focusNode.textContent;
            if (first_select == end_select) {
                var string = first_select;
            } else {
                var indexOf = jQuery.trim(select.getRangeAt(0).toString()).indexOf(jQuery.trim(first_select.substring(0, select.anchorOffset)));
                if (indexOf == -1) {
                    string = first_select.substring(0, select.anchorOffset) + select + end_select.substring(select.focusOffset);
                } else if (indexOf == 0) {
                    string = select + end_select.substring(select.focusOffset);
                } else if (indexOf > 0) {
                    string = end_select.substring(0, select.focusOffset) + select + first_select.substring(select.anchorOffset);
                }
            }
            var q = document.getElementById(dataSelect);
            var range = document.createRange();
            range.selectNode(q);
            select.addRange(range);
        });
        parent.p.find("#LetterSpaceSpin").on("blur", function () {
            var LetterSpaceSpin = parent.p.find("#LetterSpaceSpin").val();
            document.execCommand("insertHTML", false, "<span style='letter-spacing: " + LetterSpaceSpin + ";'>" + document.getSelection() + "</span>");
        });
        parent.p.find(".WordSpaceSpin .arrowContrl .upArrow i").unbind();
        parent.p.find(".WordSpaceSpin .arrowContrl .upArrow i").on("click", function () {
            var WordSpaceSpin = parent.p.find("#WordSpaceSpin").val();
            parent.p.find("#WordSpaceSpin").val(++WordSpaceSpin);
            var dataSelect = Math.random();
            document.execCommand("insertHTML", false, "<span id='" + dataSelect + "'  style='word-spacing: " + WordSpaceSpin + "px;'>" + document.getSelection() + "</span>");
            var select = document.getSelection();
            var first_select = select.anchorNode.textContent;
            var end_select = select.focusNode.textContent;
            if (first_select == end_select) {
                var string = first_select;
            } else {
                var indexOf = jQuery.trim(select.getRangeAt(0).toString()).indexOf(jQuery.trim(first_select.substring(0, select.anchorOffset)));
                if (indexOf == -1) {
                    string = first_select.substring(0, select.anchorOffset) + select + end_select.substring(select.focusOffset);
                } else if (indexOf == 0) {
                    string = select + end_select.substring(select.focusOffset);
                } else if (indexOf > 0) {
                    string = end_select.substring(0, select.focusOffset) + select + first_select.substring(select.anchorOffset);
                }
            }
            var q = document.getElementById(dataSelect);
            var range = document.createRange();
            range.selectNode(q);
            select.addRange(range);
        });
        parent.p.find(".WordSpaceSpin .arrowContrl .downArrow i").unbind();
        parent.p.find(".WordSpaceSpin .arrowContrl .downArrow i").on("click", function () {
            var WordSpaceSpin = parent.p.find("#WordSpaceSpin").val();
            parent.p.find("#WordSpaceSpin").val(--WordSpaceSpin);
            var dataSelect = Math.random();
            document.execCommand("insertHTML", false, "<span id='" + dataSelect + "'  style='word-spacing: " + WordSpaceSpin + "px;'>" + document.getSelection() + "</span>");
            var select = document.getSelection();
            var first_select = select.anchorNode.textContent;
            var end_select = select.focusNode.textContent;
            if (first_select == end_select) {
                var string = first_select;
            } else {
                var indexOf = jQuery.trim(select.getRangeAt(0).toString()).indexOf(jQuery.trim(first_select.substring(0, select.anchorOffset)));
                if (indexOf == -1) {
                    string = first_select.substring(0, select.anchorOffset) + select + end_select.substring(select.focusOffset);
                } else if (indexOf == 0) {
                    string = select + end_select.substring(select.focusOffset);
                } else if (indexOf > 0) {
                    string = end_select.substring(0, select.focusOffset) + select + first_select.substring(select.anchorOffset);
                }
            }
            var q = document.getElementById(dataSelect);
            var range = document.createRange();
            range.selectNode(q);
            select.addRange(range);
        });
        parent.p.find("#WordSpaceSpin").on("blur", function () {
            var WordSpaceSpin = parent.p.find("#WordSpaceSpin").val();
            document.execCommand("insertHTML", false, "<span style='word-spacing: " + WordSpaceSpin + ";'>" + document.getSelection() + "</span>");
        });
        parent.p.find('#toolbarLocation').find(".formatBlockText").unbind();
        parent.p.find('#toolbarLocation').find(".formatBlockText").click(function () {
            var formatBlockText = $(this).attr("data-type");
            tinymce.execCommand('FormatBlock', false, formatBlockText);
            parent.panels.changeContent();
        });

        parent.p.find('#colorPickerJsTextBG').colorpicker().on('changeColor.colorpicker', function (event) {
            var colorTextBg = event.color.toHex();
            parent.p.find('label[for="colorPickerJsTextBG"]').css("background", colorTextBg);
            parent.p.find('label[for="colorPickerJsTextBG"] i').css("background", colorTextBg);
        });
        parent.p.find('#colorPickerJsTextBG').colorpicker().on('hidePicker.colorpicker', function (event) {
            var backColor = event.color.toHex();

            tinymce.execCommand('backColor', false, backColor);
            parent.panels.changeContent();
        });

        parent.p.find('#contForColorText').colorpicker().on('changeColor.colorpicker', function (event) {
            var thisCol = parent.p.find('#contForColorText').val()
            parent.p.find('.mce-ico.mce-i-forecolor').css("color", thisCol)
        });
        parent.p.find('#contForColorText').colorpicker().on('hidePicker.colorpicker', function (event) {
            var foreColor = parent.p.find("input#contForColorText").val();
            tinymce.execCommand('foreColor', false, foreColor);
            parent.panels.changeContent();
        });
    };
    this.editElement = function () {
        var item = parent.grid.currentItem;
        var has = false;
        if (!item.hasClass('edit-item')) {
            parent.p.find('.item-editor').removeClass('selected-tool');
            parent.p.find('.tool-text').hide();
            //$('.edit-item').unbind('mouseup');
            if (tinyMCE.editors.length !== 0) {
                //remove all tinymce data
                tinymce.execCommand('mceRemoveControl', true, 'edit-text');
                tinymce.execCommand('removeControl', true, 'edit-text');
                tinyMCE.editors = [];
                //remove all attributes for ckeditor
                $('.edit-item').removeAttr('contenteditable');
                $('.edit-item').removeAttr('id');
            }
            $('.edit-item').removeClass('edit-item');
            $('.no-sortable').removeClass('no-sortable');
            //add class for start editing current content
            item.addClass('edit-item');
            $('#edit-text').unbind();
        } else {
            has = true;
        }
        parent.p.find('.item-editor').addClass('selected-tool');
        parent.panels.switchTools('tool-' + self.typeElement);
        //edit text item
        if (self.typeElement === 'text') {
            parent.panels.switchTools('tool-text');
            //add attributes for ckeditor
            item.attr({
                contenteditable: 'true',
                id: 'edit-text'
            });
            item.addClass('no-sortable');
            //init tinyMCE
            if (!has) {
                $('#edit-text').tinymce({
                    //selector: '#edit-text',
                    inline: true,
                    mode: "exact",
                    menubar: false,
                    plugins: "textcolor, insertdatetime, searchreplace",
                    insertdatetime_formats: ["%Y.%m.%d", "%H:%M"],
                    toolbar1: 'bold italic underline strikethrough subscript superscript',
                    toolbar2: 'fontselect fontsizeselect',
                    toolbar3: 'forecolor backcolor',
                    toolbar4: 'alignleft aligncenter alignright alignjustify',
                    toolbar5: 'cut copy paste bullist numlist blockquote',
                    toolbar6: 'bullist numlist outdent indent nonbreaking',
                    toolbar7: "insertdatetime, searchreplace",
                    toolbar8: "createLink",
                    theme_advanced_toolbar_location: 'external',
                    fontsize_formats: "6px 7px 8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 22px 24px 36px 48px 50px 72px",
                    theme_advanced_fonts: "Andale Mono=andale mono,times;" +
                    "Arial=arial,helvetica,sans-serif;" +
                    "Arial Black=arial black,avant garde;" +
                    "Book Antiqua=book antiqua,palatino;" +
                    "Comic Sans MS=comic sans ms,sans-serif;" +
                    "Courier New=courier new,courier;" +
                    "Georgia=georgia,palatino;" +
                    "Helvetica=helvetica;" +
                    "Impact=impact,chicago;" +
                    "Symbol=symbol;" +
                    "Tahoma=tahoma,arial,helvetica,sans-serif;" +
                    "Terminal=terminal,monaco;" +
                    "Times New Roman=times new roman,times;" +
                    "Trebuchet MS=trebuchet ms,geneva;" +
                    "Verdana=verdana,geneva;" +
                    "Webdings=webdings;" +
                    "Wingdings=wingdings,zapf dingbats",
                    forced_root_block: false,
                    fixed_toolbar_container: $("#hidenTolbar"),
                    setup: function (editor) {
                        //change size item and row by text content
                        editor.on('keyup', function (el) {
                            self.upEventsText();
                        });
                        editor.on('click', function (el) {
                            self.upEventsText();
                        });
                        editor.on('change', function () {
                            editor.save();
                            $('#edit-text').tinymce().save();
                            tinyMCE.triggerSave();
                            parent.panels.changeContent();
                        });
                        editor.on("mouseleave", function () {
                            //$('#edit-text').tinymce().save();
                            tinyMCE.triggerSave();
                        })
                    },
                    init_instance_callback: function (editor, el) {
                        editor.focus();
                        $('#edit-text').tinymce().save();
                        $('#edit-text').on("mouseleave", function () {
                        });
                        tinyMCE.triggerSave();
                    }
                });
            }
            parent.panels.changeHeightIframe();
        } else if (self.typeElement === 'form') {
            formEditor.edit(item);
        } else if (self.typeElement === 'image') {
            parent.galleryManager.addImgInEditRow();
        } else if (self.typeElement === 'map') {
            parent.mapTool.settings(item);
        } else if (self.typeElement === 'youtube') {
            parent.youtubeTool.settings(item);
//            parent.panels.switchTools('tool-youtube');
        } else if (self.typeElement === 'features') {
            parent.featuresTool.settings(item);
        } else if (self.typeElement === 'gallery') {
            //galleryTool.settings(item);
        } else if (self.typeElement === 'menu') {
            parent.menuTool.settings(item);
            parent.panels.switchTools('tool-menu');
        } else if (self.typeElement === 'row') {
            parent.panels.switchTools('tool-row');
            parent.panels.toolsInRow();
            parent.p.find('.tool-bg a').unbind();
            parent.p.find('.tool-bg a').on('click', function () {
                var eL = $(this);
                if (eL.attr('data-type')) {
                    var style = eL.attr('data-type');
                    self.removeBgRow($('.edit-item'));
                    $('.edit-item').removeAttr('style');
                    $('.edit-item').addClass(style);
                }
                $('.edit-item').removeAttr('style');
                $('.edit-item').removeAttr('data-img');
                var style = eL.attr('data-type');
                $('.edit-item').attr('data-src', style);
                $('.edit-item').addClass(style);

                parent.panels.changeContent();
            });

            parent.p.find('.tool-row-bg-remove').unbind();
            parent.p.find('.tool-row-bg-remove').on('click', function () {
                $('.edit-item').removeAttr('data-src');
                $('.edit-item').removeAttr('data-img');
                $('.edit-item').removeAttr('style');
                self.removeBgRow($('.edit-item'));
                parent.panels.changeContent();
            });

            parent.p.find('.tool-size a').unbind();
            parent.p.find('.tool-size a').on('click', function () {
                var eL = $(this);
                if (eL.attr('data-type') === 'none') {
                    $('.edit-item').removeClass('content-center');
                    $('.edit-item').attr('data-size', 'none');
                } else {
                    $('.edit-item').addClass('content-center');
                    $('.edit-item').attr('data-size', 'content-center');
                }
                parent.panels.changeContent();
            });
        }
    };
    this.removeElement = function () {
        var item = parent.grid.currentItem;
        var tmpItem;
        if (item.hasClass('column')) {
            tmpItem = item.parent();
        }
        $(item).remove();
        parent.panels.changeContent();
        parent.pages.recurseDomChildrenUpdate();
        parent.grid.currentItem = tmpItem;
        parent.getColumnSize(tmpItem);
        parent.panels.changeHeightIframe();
    };
    this.duplicateElement = function () {

    };
//remove row background
    this.removeBgRow = function (item) {
        var element = $(item);
        var classes = element.attr('class').split(/\s+/);
        var pattern = /^bg_/;
        for (var i = 0; i < classes.length; i++) {
            var className = classes[i];
            if (className.match(pattern)) {
                element.removeClass(className);
            }
        }
    };
};

var imageLinkTool = new function () {
    var self = this;
    this.modalId = '#imageLinkModal';
    this.el;
    this.settings = function (el) {
        var elLinkImage = $(el).find('.item-image a');
        this.el = elLinkImage;
        $('#imageLinkRemove').unbind();
        $('#imageLinkRemove').click(function (event) {
            $('#imageLinkTitle').val('');
            $('#imageLinkWeb').val('');
            $('#imageLinkPage option[value="0"]').attr('selected', 'selected');
            $('#imageLinkTarget').removeAttr('checked');
            $('#imageLinkLightbox').removeAttr('checked');
            $(self.modalId).modal('hide');
//            gridContentRow.save();
            parent.panels.changeContent();
        });
        var titleLink = elLinkImage.attr('title');
        var hrefLink = elLinkImage.attr('href');
        var targetLink = elLinkImage.attr('target');
        var lightBoxLink = elLinkImage.attr('data-lightbox');
        $('#imageLinkTitle').val(titleLink);
        if (lightBoxLink !== undefined) {
            $('#imageLinkLightbox').attr('checked', 'checked');
        } else if (hrefLink !== undefined) {
            var regExp = /([^])(template-editor\/)([0-9]+)/;
            var match = hrefLink.match(regExp);
            var pageId = 0;
            if (match && match[3]) {
                pageId = match[3];
                $('#imageLinkPage option[value="' + pageId + '"]').attr('selected', 'selected');
            } else {
                $('#imageLinkWeb').val(hrefLink);
            }
        }
        if (targetLink !== undefined) {
            $('#imageLinkTarget').attr('checked', 'checked');
        }
        $(this.modalId).modal('show');
        $('#imageLinkSave').unbind();
        $('#imageLinkSave').click(function (event) {
            self.update();
        });
    };
    this.update = function () {
        this.el.removeAttr('href');
        this.el.removeAttr('data-lightbox');
        this.el.removeAttr('target');
        this.el.attr('title', '');
        var titleLink = $('#imageLinkTitle').val();
        var hrefLink = $('#imageLinkWeb').val();
        var targetLink = $('#imageLinkTarget').attr('checked');
        var lightBoxLink = $('#imageLinkLightbox').attr('checked');
        var pageId = $('#imageLinkPage').val();
        this.el.attr('title', titleLink);
        var error = 0;
        if (lightBoxLink) {
            this.el.attr('data-lightbox', true);
        } else {
            if (pageId !== '0') {
                this.el.attr('href', 'http://template-editor/' + pageId);
            } else {
                if (hrefLink !== '') {
                    this.el.attr('href', hrefLink);
                } else {
                    error = 1;
                    alert('Enter the website link');
                }
            }
            if (targetLink) {
                this.el.attr('target', '_blank');
            }
        }
        if (error === 0) {
            $(this.modalId).modal('hide');
//            gridContentRow.save();
            parent.panels.changeContent();
        }
    };
};

//form item
var formEditor = new function () {
    var self = this;
    this.modalId = '#modalFormBuilder';
    this.allForms = [];
    this.currentForm;
    this.on = function () {
        $('#formSave').click(function () {
            self.save();
        });
    };
    this.edit = function (item) {
        $(this.modalId).modal();
        var idForm = item.find('.grid-stack-item-content').attr('id');
        this.currentForm = idForm;
        var formData = self.allForms[idForm];
        var formDataLength = formData.length;
        totalCountItem = 1;
        for (var i = 0; i < formDataLength; i++) {
            if (typeof formData[i] === 'object') {
                var formI = formData[i];
                var basic_arr = [formI.label, formI.required, formI.size, formI.defaultValue, formI.date_format,
                    formI.length, formI.formid, formI.fieldid, formI.uniqueCheck, formI.errorMessage,
                    formI.addAreaCode, formI.uniqueErrorMsg, formI.validErrorMsg, formI.contactAttribute,
                    formI.compareType, formI.attributeCompareErrorMsg];
                loadEachFieldToRightPanel(formI.type, basic_arr, formI, formI.likert);
            }
        }

    };
    this.save = function () {
        self.allForms[this.currentForm] = fields;
        $('#' + this.currentForm).html();
        var formDataLength = fields.length;
        totalCountItem = 1;
        var htmlItem = '';
        for (var i = 0; i < formDataLength; i++) {
            if (typeof fields[i] === 'object') {
                var formI = fields[i];
                var basic_arr = [formI.label, formI.required, formI.size, formI.defaultValue, formI.date_format,
                    formI.length, formI.formid, formI.fieldid, formI.uniqueCheck, formI.errorMessage,
                    formI.addAreaCode, formI.uniqueErrorMsg, formI.validErrorMsg, formI.contactAttribute,
                    formI.compareType, formI.attributeCompareErrorMsg];
                var newElForm = insertFormToItem(formI.type, basic_arr, formI, formI.likert);
                htmlItem += $(newElForm).html();
            }
        }
        $('#' + this.currentForm).html(htmlItem);
        $(this.modalId).modal('hide');
    };
};
$(document).ready(function () {
    $('.responsive a').click(function () {
        $(this).toggleClass('active');
    });
    // fadeIn css global editor
    bodySelector.on('click', '.body-css, .cssRowEdit, .grid-stack-item-css-edit', function () {
        $('.cssEditor').fadeIn(200);
        $('#content').css({'width': '80%', 'margin-left': '19%'}, 1000)

    });
    // cancel css global editor
    bodySelector.on('click', '#cancelCSS', function () {
        $('.cssEditor').fadeOut(200);
        $('#content').animate({'width': '100%', 'margin-left': '0%'}, 1000)

    });


    bodySelector.on('click', '.smallBtn', function () {
        $(this).parents('li').find('.smallBtn').removeClass('activeSmallBtn');
        $(this).addClass('activeSmallBtn');

    });

    bodySelector.on('click', '.middleBtn', function () {
        $(this).parents('li').find('.middleBtn').removeClass('activeMiddleBtn');
        $(this).addClass('activeMiddleBtn');

    });

    bodySelector.on('click', '.slider', function () {
//        var changedVal = $(this).find('.tooltip-inner').text()
//        $(this).parents('.panel-body').find('p strong span').text(changedVal)
        var borderWidth = $('#slider_border-width').parent('.slider').find('.tooltip-inner').text();
        $('#border-width').text(borderWidth);
        var padding = $('#slider_padding').parent('.slider').find('.tooltip-inner').text();
        $('#padding').text(padding);
        var margin = $('#slider_margin').parent('.slider').find('.tooltip-inner').text();
        $('#margin').text(margin);
        var fontSize = $('#slider_font-size').parent('.slider').find('.tooltip-inner').text();
        $('#font-size').text(fontSize);
        var popfontSize = $('#popslider_font-size').parent('.slider').find('.tooltip-inner').text();
        $('#popfont-size').text(popfontSize);
        var lineHeight = $('#slider_line-height').parent('.slider').find('.tooltip-inner').text();
        $('#line-height').text(lineHeight);
        var poplineHeight = $('#popslider_line-height').parent('.slider').find('.tooltip-inner').text();
        $('#popline-height').text(poplineHeight);
        var textIndent = $('#slider_text-indent').parent('.slider').find('.tooltip-inner').text();
        $('#text-indent').text(textIndent);
        var poptextIndent = $('#popslider_text-indent').parent('.slider').find('.tooltip-inner').text();
        $('#poptext-indent').text(poptextIndent);
        var letterSpacing = $('#slider_letter-spacing').parent('.slider').find('.tooltip-inner').text();
        $('#letter-spacing').text(letterSpacing);
        var popletterSpacing = $('#popslider_letter-spacing').parent('.slider').find('.tooltip-inner').text();
        $('#popletter-spacing').text(popletterSpacing);
        var wordSpacing = $('#slider_word-spacing').parent('.slider').find('.tooltip-inner').text();
        $('#word-spacing').text(wordSpacing);
        var popwordSpacing = $('#popslider_word-spacing').parent('.slider').find('.tooltip-inner').text();
        $('#popword-spacing').text(popwordSpacing);
        var top = $('#slider_top').parent('.slider').find('.tooltip-inner').text();
        $('#top').text(top);
        var left = $('#slider_left').parent('.slider').find('.tooltip-inner').text();
        $('#left').text(left);
        var right = $('#slider_right').parent('.slider').find('.tooltip-inner').text();
        $('#right').text(right);
        var bottom = $('#slider_bottom').parent('.slider').find('.tooltip-inner').text();
        $('#bottom').text(bottom);
        var borderRadius = $('#slider_border-radius').parent('.slider').find('.tooltip-inner').text();
        $('#border-radius').text(borderRadius);
        var textHlength = $('#slider_text-h-length').parent('.slider').find('.tooltip-inner').text();
        $('#text-h-length').text(textHlength);
        var textVlength = $('#slider_text-v-length').parent('.slider').find('.tooltip-inner').text();
        $('#text-v-length').text(textVlength);
        var textBlength = $('#slider_text-b-length').parent('.slider').find('.tooltip-inner').text();
        $('#text-b-length').text(textBlength);
        var boxHlength = $('#slider_box-h-length').parent('.slider').find('.tooltip-inner').text();
        $('#box-h-length').text(boxHlength);
        var boxVlength = $('#slider_box-v-length').parent('.slider').find('.tooltip-inner').text();
        $('#box-v-length').text(boxVlength);
        var boxBlength = $('#slider_box-b-length').parent('.slider').find('.tooltip-inner').text();
        $('#box-b-length').text(boxBlength);
        updateDisplay();
        updateDisplayPopUp();
    });

    bodySelector.on('click', '.slider-pop', function () {
        var popfontSize = $('#popslider_font-size').parent('.slider').find('.tooltip-inner').text();
        $('#popfont-size').text(popfontSize);
        var poplineHeight = $('#popslider_line-height').parent('.slider').find('.tooltip-inner').text();
        $('#popline-height').text(poplineHeight);
        var poptextIndent = $('#popslider_text-indent').parent('.slider').find('.tooltip-inner').text();
        $('#poptext-indent').text(poptextIndent);
        var popletterSpacing = $('#popslider_letter-spacing').parent('.slider').find('.tooltip-inner').text();
        $('#popletter-spacing').text(popletterSpacing);
        var popwordSpacing = $('#popslider_word-spacing').parent('.slider').find('.tooltip-inner').text();
        $('#popword-spacing').text(popwordSpacing);
        updateDisplayPopUp()
    });


    //start editor
    //globalSettings.on();
    //init gallery upload
    //galleryManager.on();
    formEditor.on();


});
//open close confirmation
window.onbeforeunload = function (e) {
    //check page changes
    if (parent.pageChanges === 1) {
        var msg = 'dont close please';
        if (typeof e == "undefined")
            e = window.event;
        if (e)
            e.returnValue = msg;
        return msg;
    }
};