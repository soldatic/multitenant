//Button function
var buttonFunction = new function(){
    var self = this;
    var item;
    this.init = function(e, eventElem){
        var buttonFunc = e.target.dataset.func;
        self.routeToFunc(buttonFunc, eventElem, e);
    };
    // route to func
    this.routeToFunc = function(buttonFunc, eventElem, e) {
        if (isNumber(buttonFunc)) {
            var elemForCopyOrCut = fr.find('*[data-e-id="' + buttonFunc + '"]');
            var cutOrCopy = e.target.parentNode.id;
            if (cutOrCopy === 'copyElem'){
                eventElem = elemForCopyOrCut[0];
                self.copyFunc(eventElem)
            } else {
                eventElem = elemForCopyOrCut;
                self.cutFunc(eventElem)
            }
        } else {
            switch (buttonFunc) {
                /*case 'copy':
                    self.copyFunc(eventElem);
                    break;
                case 'cut':
                    self.cutFunc(eventElem);
                    break;*/
                case 'save':
                    self.saveFrame();
                    break;
                case 'paste_inside':
                    self.pasteInsideFunc(eventElem);
                    break;
                case 'paste_inside_first':
                    self.pasteInsideFirstFunc(eventElem);
                    break;
                case 'dublicate':
                    self.dublicateFunc(eventElem);
                    break;
                case 'delete':
                    self.deleteFunc(eventElem);
                    break;
            }
        }
    };

    //Copy element function
    this.copyFunc = function (eventElem){
        var copyElem;
        var elemAttr = {};
        var nodeNameEl = eventElem.nodeName.toLowerCase();
        Array.prototype.slice.call(eventElem.attributes).forEach(function(item) {
            elemAttr[item.name] = item.value;
        });
        if (!eventElem.target){
            copyElem =  eventElem.outerHTML;
        } else {
            copyElem = eventElem.target.outerHTML;
        }
        localStorage.setItem('copy', copyElem); // set copy item in LocalStorage
        localStorage.setItem('nodeName', nodeNameEl); // set tag item in LocalStorage
        localStorage.setItem('attr', JSON.stringify(elemAttr)); // set attr item in LocalStorage
    };

    //Cut element function
    this.cutFunc = function (eventElem){
        var cutElem;
        var elemAttr = {};
        var nodeNameEl = eventElem[0].nodeName.toLowerCase();
        Array.prototype.slice.call(eventElem[0].attributes).forEach(function(item) {
            elemAttr[item.name] = item.value;
        });
        if (!eventElem.target){
            cutElem =  eventElem[0].outerHTML;
        } else {
            cutElem = eventElem[0].target.outerHTML;
        }
        localStorage.setItem('copy', cutElem); // set copy item in LocalStorage
        localStorage.setItem('nodeName', nodeNameEl); // set tag item in LocalStorage
        localStorage.setItem('attr', JSON.stringify(elemAttr)); // set attr item in LocalStorage
        eventElem.remove();
    };

    //Paste inside element from local storage function
    this.pasteInsideFunc = function (eventElem){
        var pasteTo =  eventElem.target || eventElem;
        var nodeElemName = localStorage.getItem('nodeName');
        var elemForPaste = $.parseHTML(localStorage.getItem('copy'))[0];
        elemForPaste.setAttribute('data-e-id', getRandomInt(1111111, 9999999));
        var elemPaste;
        if(nodeElemName === 'img'){
            elemPaste = document.createElement('a');
            elemPaste.setAttribute('href', '#');
            elemPaste.appendChild(elemForPaste);
        } else {
            elemPaste = elemForPaste;
        }

       // console.log(self.changeIdelems(elemPaste));
        var pasteIns = true;
        var elem = self.changeIdelems(elemPaste, pasteIns);


        pasteTo.appendChild(elem, pasteTo.nextSibling);
        item = $(eventElem.target);
        getColumnSize(item)
        panels.changeHeightIframe();
    };

    //Paste inside element first from local storage function
    this.pasteInsideFirstFunc = function (eventElem){
        var pasteTo =  eventElem.target || eventElem;
        var nodeElemName = localStorage.getItem('nodeName');
        var elemForPaste = $.parseHTML(localStorage.getItem('copy'))[0];
        elemForPaste.setAttribute('data-e-id', getRandomInt(1111111, 9999999));
        var elemPaste;
        if(nodeElemName === 'img'){
            elemPaste = document.createElement('a');
            elemPaste.setAttribute('href', '#');
            elemPaste.appendChild(elemForPaste);
        } else {
            elemPaste = elemForPaste;
        }
        pasteTo.insertBefore(elemPaste, pasteTo.firstChild);
        item = $(eventElem.target);
        getColumnSize(item)
        panels.changeHeightIframe();
    };

    //Dublicate
    this.dublicateFunc = function (eventElem){
        var itemForDublicate;
        var placeForPasteDublicate;
        if (!eventElem.target){
             itemForDublicate =  eventElem.cloneNode(true);
             placeForPasteDublicate =  eventElem
        } else {
            itemForDublicate = eventElem.target.cloneNode(true) ;
            placeForPasteDublicate =  eventElem.target;
        }
        var dubl = true;
        var pallaceForItem;
        if (itemForDublicate.nodeName == "IMG"){
            var elemchange = self.changeIdelems(itemForDublicate, dubl);
            var elem = document.createElement('a');
            elem.setAttribute('href', '#');
            elem.appendChild(elemchange);
            pallaceForItem = $(placeForPasteDublicate).parents('.column')
            pallaceForItem.append(elem)
            console.log(1);
        } else {
            var elem = self.changeIdelems(itemForDublicate, dubl);
            pallaceForItem = placeForPasteDublicate.parentNode;
            pallaceForItem.insertBefore(elem, placeForPasteDublicate.nextSibling);
            console.log(2);

        }

        item = $(eventElem.target);
        getColumnSize(item);
        panels.changeHeightIframe();
    };

    //dublicate helper
    this.changeIdelems = function (element, dubl, pasteIns, pasteInsFirst) {
        // console.log('id');
        // console.log(dubl);
        // console.log(pasteIns);
        // console.log(pasteInsFirst);
        if(element.nodeName != '#text' && element.getAttribute('data-e-id'))
            element.setAttribute('data-e-id', getRandomInt(1111111, 9999999));
        for (var i = 0; i < element.childNodes.length; i++) {
            if(dubl == true){
                self.changeIdelems(element.childNodes[i]);
                dubl = false;
            } else if (pasteIns == true){
                self.changeIdelems(element.childNodes[i]);
                pasteIns = false;
            } else if (pasteInsFirst == true){
                self.changeIdelems(element.childNodes[i]);
                pasteInsFirst = false;
            }
        }
        return element;
    };

    //Remove Item
    this.deleteFunc = function (eventElem){
        eventElem.target.remove();
        item = $(eventElem.target);
        getColumnSize(item)
        panels.changeHeightIframe();
    };
    //Save frame
    this.saveFrame = function (){
        $('#savePageChanges').click();
    };
};

var contextMenu = new function(){
    var self = this;
    var contMenuSel = document.querySelector('.contMenu');
    var secondContMenu = document.getElementById('secondContMenu');
    var copyElem = document.getElementById('copyElem');
    var cutElem = document.getElementById('cutElem');

    // change hotkey oc
    (function() {
        var hotKeytext = {};
        var oc = getOCUser();
        switch (oc) {
            case 'mac':
                hotKeytext = {
                    'copy': "<img src='http://" +  window.location.hostname + "/img/iconCmd/20.png'> + C",
                    'paste': "<img src='http://" +  window.location.hostname + "/img/iconCmd/20.png'> + V",
                    'dublicate': "<img src='http://" +  window.location.hostname + "/img/iconCmd/20.png'> + D",
                    'delete':'Delete'
                };
                break;
            default:
            case 'windows':
            case 'linux':
                hotKeytext = {
                    'copy':'CTRL + C',
                    'paste':'CTRL + V',
                    'dublicate':'CTRL + D',
                    'delete':'Backspace'
                };
              //  console.log(oc);
                break;
        }
        $("li[data-func='copy'] span").html(hotKeytext.copy);
        $("li[data-func='paste_inside'] span").html(hotKeytext.paste);
        $("li[data-func='dublicate'] span").html(hotKeytext.dublicate);
        $("li[data-func='delete'] span").html(hotKeytext.delete);
    })();

    // obj with tag name and icon
    this.tagObj = {
        p:{
            'name':'Paragrapgh',
            'icon':'fa fa-paragraph'
        },
        h1:{
            'name':'H1',
            'icon':'fa fa-header'
        },
        h2:{
            'name':'H2',
            'icon':'fa fa-header'
        },
        h3:{
            'name':'H3',
            'icon':'fa fa-header'
        },
        h4:{
            'name':'H4',
            'icon':'fa fa-header'
        },
        h5:{
            'name':'H5',
            'icon':'fa fa-header'
        },
        h6:{
            'name':'H6',
            'icon':'fa fa-header'
        },
        ul:{
            'name':'Unordered List',
            'icon':'fa fa-list-ul'
        },
        ol:{
            'name':'Ordered List',
            'icon':'fa fa-list-ol'
        },
        li:{
            'name':'List item',
            'icon':'fa fa-list'
        },
        blockquote:{
            'name':'Block Quote',
            'icon':'fa fa-quote-right'
        },
        a:{
            'name':'Text Link',
            'icon':'fa fa-link'
        },
        video:{
            'name':'Video',
            'icon':'fa fa-youtube-play'
        },
        audio:{
            'name':'Audio',
            'icon':'fa fa-music'
        },
        form:{
            'name':'Form',
            'icon':'fa fa-server'
        },
        input_text:{
            'name':'Text Field',
            'icon':'fa fa-keyboard-o'
        },
        input_checkbox:{
            'name':'Checkbox',
            'icon':'fa fa-check-square-o'
        },
        input_email:{
            'name':'Email field',
            'icon':'fa fa-envelope'
        },
        input_button:{
            'name':'Button',
            'icon':'fa fa-hand-o-up'
        },
        input_file:{
            'name':'File field',
            'icon':'fa fa-file'
        },
        input_password:{
            'name':'Password field',
            'icon':'fa fa-key'
        },
        input_radio:{
            'name':'Radio button',
            'icon':'fa fa-dot-circle-o'
        },
        input_number:{
            'name':'Number field',
            'icon':'fa fa-keyboard-o'
        },
        input_range:{
            'name':'Number field',
            'icon':'fa fa-keyboard-o'
        },
        input_tel:{
            'name':'Phone Number field',
            'icon':'fa fa-phone'
        },
        input_date:{
            'name':'Date field',
            'icon':'fa fa-calendar'
        },
        label:{
            'name':'Label',
            'icon':'fa fa-code-fork'
        },
        select: {
            'name':'Select',
            'icon':'fa fa-list-alt'
        },
        textarea: {
            'name':'Textarea',
            'icon':'fa fa-pencil-square-o'
        },
        button: {
            'name':'Button',
            'icon':'fa fa-hand-o-up'
        },
        img: {
            'name':'Image',
            'icon':'fa fa-picture-o'
        },
        div: {
            'name':'Container',
            'icon':'fa fa-square-o'
        },
        row: {
            'name':'row',
            'icon':'fa fa-square-o'
        },
        column: {
            'name':'column',
            'icon':'fa fa-square-o'
        },
        span: {
            'name':'Container',
            'icon':'fa fa-square-o'
        },
        header: {
            'name':'Header Container',
            'icon':'fa fa-square-o'
        },
        nav: {
            'name':'Navbar',
            'icon':'fa fa-graduation-cap'
        },
        canvas: {
            'name':'Canvas',
            'icon':'fa fa-object-group'
        },
        code: {
            'name':'Code',
            'icon':'fa fa-code'
        },
        iframe: {
            'name':'Iframe',
            'icon':'fa fa-table'
        },
        table: {
            'name':'Table',
            'icon':'fa fa-table'
        },
        th: {
            'name':'Table header',
            'icon':'fa fa-table'
        },
        tr: {
            'name':'Table row',
            'icon':'fa fa-table'
        },
        td: {
            'name':'Table cell',
            'icon':'fa fa-th'
        },
        tbody: {
            'name':'Table body',
            'icon':'fa fa-th'
        },
        tfoot: {
            'name':'Table footer',
            'icon':'fa fa-th'
        },
        thead: {
            'name':'Table head',
            'icon':'fa fa-th'
        },
        section: {
            'name':'Section',
            'icon':'fa fa-square-o'
        },
        plaintext: {
            'name':'Plain text',
            'icon':'fa fa-fa'
        },
        option: {
            'name':'Plain text',
            'icon':'fa fa-align-justify'
        },
        fieldset: {
            'name':'Field set',
            'icon':'fa fa-align-left'
        },
        body: {
            'name':'Page body',
            'icon':'fa fa-align-left'
        },
        html: {
            'name':'Page html',
            'icon':'fa fa-align-left'
        },
        small: {
            'name':'Small',
            'icon':'fa fa-square-o'
        }

    };

    this.run = function (e){
        var eventElem = e;
        self.checkClickedElement(e);
        document.getElementById('mainContMenu').onclick = function(e){
            e.preventDefault();
           // console.log(e.target)
            if(e.target && e.target.nodeName == "LI") {
                buttonFunction.init(e, eventElem);
                contMenuSel.style.display = "none";
            }
        };
        self.checkItemForPaste();
    };

    // Check copy obj on empty
    this.checkItemForPaste = function () {
        if (localStorage.getItem('copy') === null) {
            document.querySelector('#mainContMenu li[data-func="paste_inside"]').className = 'emptyCopy';
            document.querySelector('#mainContMenu li[data-func="paste_inside_first"]').className = 'emptyCopy';
        } else {
            document.querySelector('#mainContMenu li[data-func="paste_inside"]').className = '';
            document.querySelector('#mainContMenu li[data-func="paste_inside_first"]').className = '';
        }
    };

    // check element tag, id, data-id
    this.checkClickedElement = function(e){
        var elemTagName = e.target.nodeName;
        self.setContMenuStructure(elemTagName, e);
    };

    // check coordinates mouse event
    this.checkMouseCoordinates = function(e){
        // X position
        var conteWidthIframe = $('#xgate-template-editor-body').width();
        var WidthIframe = $('#editor-iframe').width();
        var marginIframe = (conteWidthIframe - WidthIframe) / 2;
        var eClientX = e.clientX + 57;
        leftMousePosition = eClientX + marginIframe;
        // Y position
        var HeightIframe = $('#header').height();
        var sizeYwind = window.innerHeight || document.body.clientHeight;
        var differenceSize = sizeYwind - (e.screenY - HeightIframe);
        var topMousePosition;
        if  (differenceSize < 200){
            topMousePosition = (e.screenY - HeightIframe) - 270;
        } else {
            topMousePosition = e.screenY - HeightIframe - 20;
        }
        self.setPositionContextMenu(topMousePosition, leftMousePosition, e);
        self.showPrevisElemHover();
    };

    // set position context menu
    this.setPositionContextMenu = function(topMousePosition, leftMousePosition, e) {
        contMenuSel.style.left = leftMousePosition + 'px';
        contMenuSel.style.top = topMousePosition + 'px';
        self.showContMenu();
    };

    //Hide contextMenu
    this.HideContMenu = function(e){
        if (e.target.nodeName != 'HTML') {
            if (e.target.offsetParent != null) {
                var elemTarget = e.target.offsetParent.getAttribute('class');
            } else {
                contMenuSel.style.display = "none";
            }
        }
        if (!elemTarget || elemTarget === "BODY") {
            contMenuSel.style.display = "none";
        } else if(elemTarget != 'contMenu'){
            contMenuSel.style.display = "none";
        }
    };

    //Show contextMenu
    this.showContMenu = function(){
        contMenuSel.style.display = "block";
    };

    // Hover elem
    this.showPrevisElemHover = function(){
        $('#secondContMenu li, .copyElem li').hover( function(){
            var idLi = $(this).attr('data-func');
            if (idLi > 0 && idLi != null) {
                fr.find('*[data-e-id="' + idLi + '"]').mouseover();
            }
        }).on('mouseout', function () {
           fr.find('.hover').mouseout();
        });
        $('#secondContMenu li').click( function(){
            var idLi = $(this).attr('data-func');
            if (idLi > 0 && idLi != null) {
                fr.find('*[data-e-id="' + idLi + '"]').click();
            }
            return false;
        })
    };

    //Checked element tag name
    this.setContMenuStructure = function(elemTagName, e){
        elemTagName = elemTagName.toLowerCase();
        var elemFromObj = self.tagObj[elemTagName];
        if (elemTagName === 'html' || elemTagName === 'body') {
            elemFromObj = 'false';
            document.querySelector('#SelParentElem').style.display = 'none';
        }
        self.buildStructureMenu(elemFromObj, e);

    };

    //build structure context menu
    this.buildStructureMenu = function(elemFromObj, e){
        if (elemFromObj != 'false') {
            secondContMenu.innerHTML = '';
            cutElem.innerHTML = '';
            copyElem.innerHTML = '';
            var subMenuActive = document.createElement('li');
            var iconList = document.createElement('i');
            var textList = document.createElement('p');
            var pathToElem = [];
            var NameElem = [];
            var nodes = e.target;
            while(nodes != document.body || NameElem != 'body') {
                NameElem = nodes.nodeName.toLowerCase();
                if (NameElem === 'body'){
                    break
                }
                if (NameElem === 'input'){
                    var tests = 'input_' + nodes.type;
                    elemFromObj = self.tagObj[tests];
                } else if (nodes.classList.contains('column')){
                    elemFromObj = self.tagObj['column'];
                } else if (nodes.classList.contains('row')){
                    elemFromObj = self.tagObj['row'];
                } else if (NameElem != 'input' && !nodes.classList.contains('column') && !nodes.classList.contains('row')){
                    elemFromObj = self.tagObj[NameElem];
                }
                subMenuActive = document.createElement('li');
                var dataEd = nodes.getAttribute('data-e-id');
                subMenuActive.setAttribute('data-func', nodes.getAttribute('data-e-id'));
                if (dataEd === null || dataEd === '1'){
                    subMenuActive.className = 'cantCopy';
                }
                pathToElem.push(nodes);
                nodes = nodes.parentNode;
                iconList = document.createElement('i');
                textList = document.createElement('p');
                textList.innerHTML = elemFromObj.name;
                iconList.className = elemFromObj.icon;
                subMenuActive.appendChild(iconList); // append icon in list
                subMenuActive.appendChild(textList); // append text in list

                secondContMenu.appendChild(subMenuActive.cloneNode(true));
                copyElem.appendChild(subMenuActive.cloneNode(true));
                cutElem.appendChild(subMenuActive.cloneNode(true));
            }
            document.querySelector('#SelParentElem').style.display = 'block';
        }
        self.checkMouseCoordinates(e);
    }
};
