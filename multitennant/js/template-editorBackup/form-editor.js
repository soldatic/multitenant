function insertFormToItem(type, basic_arr, option_arr, likert_arr)
{
    var count = totalCountItem;
    totalCountItem++;
    var id = "foli" + count;
    var divId = "itemOnPage" + count;
    var field_html = "";
    var loadClass = false;
    var isLikert = false;
    var isAddress = false;
    var fieldDiv = document.createElement("div");
    fieldDiv.id = divId;
    fieldDiv.className = 'form-group-edit col-sm-12 col-xs-12 nopad';
    fieldDiv.style.display = 'block';
    var fieldUl = document.createElement("ul");
    var field = document.createElement("li");
    var labelInnerHtml = '';
    fieldLabel = document.createElement("label");
    fieldLabel.id = 'title' + count;
    fieldLabel.for = 'Field' + count;
    if (type == 'likert')
    {
        fieldLabel.className = 'cus_title';
    } else {
        fieldLabel.className = 'frmquestion cstm_frmquestion desc-edit desc';
    }
    labelInnerHtml += basic_arr[0];
    if (basic_arr[1] == 1) {
        labelInnerHtml += '<span class="req" id="req_' + count + '">*</span>';
    }
    else {
        labelInnerHtml += '<span class="req" id="req_' + count + '"></span>';
    }
//	if(type!='pagebreak'){
    if ($.inArray(type, ['pagebreak', 'plaintext']) < 0) {
        fieldLabel.innerHTML = labelInnerHtml;
    }
    field.id = id;
    field.className = ' form-group-edit col-sm-12 col-xs-12 nopad ';
    field.style.cursor = 'pointer';
    field.style.top = '0px';
    field.style.left = '0px';
    field.style.left = '0px';
//    field.title = 'Click to edit. Drag to reorder.';
    field.title = '';
    field.zIndex = '100';
    var style = "";
//    field_html += '<div class="floatDiv" onclick="settingFieldOnLeftPanel(\'' + count + '\');">';
    field_html += '<div class="floatDiv">';
    field_html += '</div>';
    if (type == "text")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'medium';
        }
        // basic_arr[3] = basic_arr[3].replace(/\"/g, "\\\"");
        // alert(basic_arr[3].replace(/\"/g, "\\\""));
        // var show_val = basic_arr[3]
        //var show_val = basic_arr[3].replace(/\"/g, "\\\"");
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
//		field_html += '<div >' +
//		field_html += '</div>' +
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="" class="field text ' + style + '" name="Field' + count + '" id="Field' + count + '">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1],
//            size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}
    //            , length: basic_arr[5], formid: basic_arr[6], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "membergate")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'medium';
        }
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="" class="field text ' + style + '" name="Field' + count + '" id="Field' + count + '">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1],
//            size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}
//            , length: basic_arr[5], formid: basic_arr[6], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "redeemcode")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'medium';
        }
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="" class="field text ' + style + '" name="Field' + count + '" id="Field' + count + '">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1],
//            size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}
//            , length: basic_arr[5], formid: basic_arr[6], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "number")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'medium';
        }

        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="' + basic_arr[3] + '" class="field text ' + style + '" name="Field' + count + '" id="Field' + count + '">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}
//            , length: basic_arr[5], formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "textarea")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'small';
        }
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
console.log(basic_arr)
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<div class="handle"></div>';
        field_html += '<textarea disabled="disabled" readonly="readonly" cols="50" rows="10" class="field textarea ' + style + '" name="Field' + count + '" id="Field' + count + '">' + basic_arr[3] + '</textarea>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}
//            , length: basic_arr[5], formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "sketch")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'small';
        }
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<div class="handle"></div>';
        field_html += '<canvas id="simple_sketch" style="width:380px;background-color:white;"></canvas>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}
//            , length: basic_arr[5], formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "checkbox")
    {
//        alert('hmm');
        if (basic_arr[2] == "" || basic_arr[2] == "oneColumn")
        {
            style = "";
        }
        else
        {
            loadClass = true;
            style = basic_arr[2];
        }
        if (basic_arr[3] != "")
        {
            var str = "," + basic_arr[3] + ",";
        }
        var choice_list = new Array();
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        var otherIndex = 100;
        for (var i = 0; i < option_arr['choices'].length; i++)
        {
            var checked = 0;
            if (option_arr['selected'][i] == 1) {
                checked = 1;
            }
            field_html += '<span>';
            if (option_arr['choices'][i].indexOf("##_##") != -1) {
                field_html += '<input style="display:inline-block;" type="checkbox" id="Field' + count + '-' + otherIndex + '" name="Field' + count + '-' + (i + 1) + '"';
                otherIndex++;
            }
            else {
                field_html += '<input style="display:inline-block;" type="checkbox" id="Field' + count + '-' + (i + 1) + '" name="Field' + count + '-' + (i + 1) + '"';
            }
//			field_html += '<input style="display:inline-block;" type="checkbox" id="Field'+count+'-' + (i+1) + '" name="Field'+count+'-' + (i+1) + '"';
            if (checked == 1)
            {
                field_html += ' checked="checked"';
            }
            field_html += ' class="field checkbox" value="' + (i + 1) + '" readonly="readonly" disabled="disabled">';
            if (option_arr['choices'][i].indexOf("##_##") != -1) {
                var labelArr = new Array();
                labelArr = option_arr['choices'][i].split("##_##");
                field_html += '<label style="display:inline;margin-left: 10px;" class="frmchoice" for="Field' + count + '-100">' + labelArr[0] + '</label>';
                field_html += '<input style="display:inline-block;;width:40%;height:16px;" type="text" value="" id="Field' + count + '_otherContent' + (i + 1) + '" name="Field' + field.count + '_otherContent"></input>';
                field_html += '<span id="otherReq' + count + '" class="req" style="float:none;width:5px;margin:0px;">';
                if (option_arr['otherRequired'][i] == 1) {
                    field_html += '&nbsp;*';
                }
                field_html += '</span>';
                field_html += '</span>';
            }
            else {
                field_html += '<label class="frmchoice" for="Field' + count + '-' + (i + 1) + '">' + option_arr['choices'][i] + '</label>';
            }
//			field_html += '<label style="display:inline-block;" class="choice" for="Field'+count+'-' + (i+1) + '">' + option_arr['choices'][i] + '</label>';
            field_html += '</span>';
            choice_list[i] = {id: (i + 1), label: option_arr['choices'][i], selected: checked, valueid: option_arr['id'][i], choiceid: option_arr['choiceid'][i], otherRequired: option_arr['otherRequired'][i]};
        }
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'small', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: style, likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "radio")
    {
        if (basic_arr[2] == "" || basic_arr[2] == "oneColumn")
        {
            style = "";
        }
        else
        {
            loadClass = true;
            style = basic_arr[2];
        }
        var choice_list = new Array();
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        for (var i = 0; i < option_arr['choices'].length; i++)
        {
            var checked = 0;
            if (option_arr['selected'][i] == 1) {
                checked = 1;
            }
            field_html += '<span>';
            if (option_arr['choices'][i].indexOf("##_##") != -1) {
                field_html += '<input style="display:inline-block;" type="radio" id="Field' + count + '-100" name="Field' + count + '-' + (i + 1) + '"';
            }
            else {
                field_html += '<input style="display:inline-block;" type="radio" id="Field' + count + '-' + (i + 1) + '" name="Field' + count + '-' + (i + 1) + '"';
            }
//			field_html += '<input type="radio" id="Field'+count+'-' + (i+1) + '" name="Field'+count+'-' + (i+1) + '"';
            if (checked == 1)
            {
                field_html += ' checked="checked"';
            }
            field_html += ' class="field radio" value="' + (i + 1) + '" readonly="readonly" disabled="disabled">';
            if (option_arr['choices'][i].indexOf("##_##") != -1) {
                var labelArr = new Array();
                labelArr = option_arr['choices'][i].split("##_##");
                field_html += '<label style="display:inline;margin-left: 10px;" class="frmchoice" for="Field' + count + '-100">' + labelArr[0] + '</label>';
                field_html += '<input style="display:inline-block;;width:40%;height:16px;" type="text" value="" id="Field' + count + '_otherContent" name="Field' + field.count + '_otherContent"></input>';
                field_html += '<span id="otherReq' + count + '" class="req" style="float:none;width:5px;margin:0px;">';
                if (option_arr['otherRequired'][i] == 1) {
                    field_html += '&nbsp;*';
                }
                field_html += '</span>';
//				field_html += '</span>';
            }
            else {
                field_html += '<label class="frmchoice" for="Field' + count + '-' + (i + 1) + '">' + option_arr['choices'][i] + '</label>';
            }
//			field_html += '<label class="frmchoice" for="Field'+count+'-' + (i+1) + '">' + option_arr['choices'][i] + '</label>';
            field_html += '</span>';
            choice_list[i] = {id: (i + 1), label: option_arr['choices'][i], selected: checked, valueid: option_arr['id'][i], choiceid: option_arr['choiceid'][i], otherRequired: option_arr['otherRequired'][i]};
        }
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'small', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: style, likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "agerange")
    {
        if (basic_arr[2] == "" || basic_arr[2] == "oneColumn")
        {
            style = "";
        }
        else
        {
            loadClass = true;
            style = basic_arr[2];
        }
        var choice_list = new Array();
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        for (var i = 0; i < option_arr['choices'].length; i++)
        {
            var checked = 0;
            if (option_arr['selected'][i] == 1) {
                checked = 1;
            }
            field_html += '<span>';
            field_html += '<input style="display:inline-block;" type="radio" id="Field' + count + '-' + (i + 1) + '" name="Field' + count + '-' + (i + 1) + '"';
//			field_html += '<input type="radio" id="Field'+count+'-' + (i+1) + '" name="Field'+count+'-' + (i+1) + '"';
            if (checked == 1)
            {
                field_html += ' checked="checked"';
            }
            field_html += ' class="field radio" value="' + (i + 1) + '" readonly="readonly" disabled="disabled">';
            field_html += '<label class="frmchoice" for="Field' + count + '-' + (i + 1) + '">' + option_arr['choices'][i] + '</label>';
            field_html += '</span>';
            choice_list[i] = {id: (i + 1), label: option_arr['choices'][i], selected: checked, valueid: option_arr['id'][i], choiceid: option_arr['choiceid'][i], otherRequired: option_arr['otherRequired'][i]};
        }
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'small', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: style, likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "select")
    {
//        alert('hmm');

        var choice_list = new Array();
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<select id="Field' + count + '" name="Field' + count + '" class="field select medium" readonly="readonly" disabled="disabled">';
        for (var i = 0; i < option_arr['choices'].length; i++)
        {
            var checked = 0;
            if (option_arr['selected'][i] == 1) {
                checked = 1;
            }
            field_html += '<option id="Field' + count + '-' + (i + 1) + '" value="' + (i + 1) + '"';
            if (checked == 1)
            {
                field_html += ' selected="selected"';
            }
            field_html += '>' + option_arr['choices'][i] + '</option>';
            choice_list[i] = {id: (i + 1), label: option_arr['choices'][i], selected: checked, valueid: option_arr['id'][i], choiceid: option_arr['choiceid'][i], otherRequired: option_arr['otherRequired'][i]};
        }
        field_html += '</select>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "limit")
    {
//        alert('hmm');

        var choice_list = new Array();
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<select id="Field' + count + '" name="Field' + count + '" class="field select medium" readonly="readonly" disabled="disabled">';
        for (var i = 0; i < option_arr['choices'].length; i++)
        {
            var checked = 0;
            if (option_arr['selected'][i] == 1) {
                checked = 1;
            }
            field_html += '<option id="Field' + count + '-' + (i + 1) + '" value="' + (i + 1) + '"';
            if (checked == 1)
            {
                field_html += ' selected="selected"';
            }
            field_html += '>' + option_arr['choices'][i] + '</option>';
            choice_list[i] = {id: (i + 1), label: option_arr['choices'][i], selected: checked, quota: option_arr['quota'][i], maxquota: option_arr['maxquota'][i], valueid: option_arr['id'][i], choiceid: option_arr['choiceid'][i], otherRequired: option_arr['otherRequired'][i]};
        }
        field_html += '</select>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    /*
     else if(type == "section")
     {
     //        alert('hmm');
     field.className = 'section';
     field_html = '<h3 id="title'+count+'">' +
     'Section Break' +
     '</h3>' +
     '<div class="" id="instruct'+count+'">' +
     'A description of the section goes here.' +
     '</div>' +
     '<div class="fieldActions" id="fa'+count+'">' +
     '<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="/img/template-editor/add.png" class="faDup">' +
     '<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="/img/template-editor/delete.png" class="faDel">' +
     '</div>';
     fields.push({id: id, count: count, type: type, label: 'Section Break', required: '', size: 'medium', duplicate: 0, defaultValue: 'A description of the section goes here.', choices: [], date_format: basic_arr[4], columns: '', likert: {} });
     }
     */

    else if (type == "birth")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        if (basic_arr[4] == 'YYYY-MM-DD') {
            field_html += '<span class="time birthThreeColumn">';
            field_html += '<select id="Field' + count + '-3" name="Field' + count + '-3" class="field select birthSelect" readonly="readonly" disabled="disabled">';
            field_html += '<option value="">YYYY</option>';
            for (var i = 2013; i > 1900; i--)
            {
                field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + 'YYYY</option>';
            }
            field_html += '</select>';
//			field_html += '<label for="Field'+count+'-3">YYYY-</label>';
            field_html += '</span>';
            field_html += '<span class="time birthThreeColumn">';
            field_html += '<select id="Field' + count + '-1" name="Field' + count + '-1" class="field select birthSelect" readonly="readonly" disabled="disabled">';
            field_html += '<option value="">DD</option>';
            for (var i = 1; i < 32; i++)
            {
                field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
            }
            field_html += '</select>';
//			field_html += '<label for="Field'+count+'-1">MM-</label>';
            field_html += '</span>';
            field_html += '<span class="time birthThreeColumn">';
            field_html += '<select id="Field' + count + '-2" name="Field' + count + '-2" class="field select birthSelect" readonly="readonly" disabled="disabled">';
            field_html += '<option value="">MM</option>';
            for (var i = 1; i < 13; i++)
            {
                field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
            }
            field_html += '</select>';
//			field_html += '<label for="Field'+count+'-2">DD</label>';
            field_html += '</span>';
        }
        else {
            field_html += '<span class="time birthTwoColumn">';
            field_html += '<select id="Field' + count + '-1" name="Field' + count + '-1" class="field select birthSelect" readonly="readonly" disabled="disabled">';
            field_html += '<option value="">DD</option>';
            for (var i = 1; i < 32; i++)
            {
                field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
            }
            field_html += '</select>';
            //		field_html += '<label for="Field'+count+'-1">MM-</label>';
            field_html += '</span>';
            field_html += '<span class="time birthTwoColumn">';
            field_html += '<select id="Field' + count + '-2" name="Field' + count + '-2" class="field select mmSelect" readonly="readonly" disabled="disabled">';
            field_html += '<option value="">MM</option>';
            for (var i = 1; i < 13; i++)
            {
                field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
            }
            field_html += '</select>';
            //		field_html += '<label for="Field'+count+'-2">DD</label>';
            field_html += '</span>';
        }
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: '', likert: {}, fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "birthdate")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-1" name="Field' + count + '-1" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 1; i < 32; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '</span>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: '', likert: {}, fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "birthmonth")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-2" name="Field' + count + '-2" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 1; i < 13; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '<label for="Field' + count + '-2"></label>';
        field_html += '</span>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: '', likert: {}, fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "shortname")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
        field_html += '</label>';
        field_html += '<div class="frmquestion cstm_frmquestion">';
        field_html += '<span class="shortname" style="width: 46%; min-width: 85px;">';
        field_html += '<input style="width:80%;" type="text" disabled="disabled" placeholder="First" readonly="readonly" size="14" value="" class="field text fn" name="Field' + count + '-1" id="Field' + count + '-1">';
        field_html += '<label for="Field' + count + '-1" style="display:none;">First</label>';
        field_html += '</span>';
        field_html += '<span  class="symbol"  style="width:1%;height:1px;"></span>';
        field_html += '<span  class="shortname"  style="width: 47%; min-width: 85px;">';
        field_html += '<input type="text" disabled="disabled" placeholder="Last" readonly="readonly" size="14" value="" class="field text ln" name="Field' + count + '-2" id="Field' + count + '-2">';
        field_html += '<label for="Field' + count + '-2" style="display:none;">Last</label>';
        field_html += '</span>';
        field_html += '</div>';
//		field_html += '<br><br>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "firstname")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" size="14" value="" class="field text fn" name="Field' + count + '-1" id="Field' + count + '-1">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "lastname")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" size="14" value="" class="field text fn" name="Field' + count + '-1" id="Field' + count + '-1">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "date")
    {
//		var tmp_arr = basic_arr[4].split("/");
//		var date_format = 0;
//		if(basic_arr[4] == "DD/MM/YYYY")
//		{
//			date_format = 1;
//		}
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		field_html +='<span class="req" id="req_'+count+'">' + basic_arr[1] + '</span>';
        field_html += '</label>';
        field_html += '<span class="date">';
        field_html += '<input id="Field' + count + '" name="Field' + count + '" class="field text" value="" size="14" maxlength="10" readonly="readonly" disabled="disabled" type="text">';
        field_html += '<label for="Field' + count + '">' + basic_arr[4] + '</label>';
        field_html += '</span>';
        field_html += '<span id="cal' + count + '">';
        field_html += '<img id="pick' + count + '" class="datepicker" src="' + imgpath + '/img/template-editor/calendar.png" alt="Pick a date.">';
        field_html += '</span>';
        field_html += '<br><br>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "email")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'medium';
        }
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0]
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="' + basic_arr[3] + '" class="field text ' + style + '" name="Field' + count + '" id="Field' + count + '">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "url")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'medium';
        }
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="' + basic_arr[3] + '" class="field text ' + style + '" name="Field' + count + '" id="Field' + count + '">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "time")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-1" name="Field' + count + '-1" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 1; i < 13; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '<label for="Field' + count + '-1">HH</label>';
        field_html += '</span>';
        field_html += '<span class="symbol">:</span>';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-2" name="Field' + count + '-2" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 0; i < 60; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '<label for="Field' + count + '-2">MM</label>';
        field_html += '</span>';
        field_html += '<span class="symbol">:</span>';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-3" name="Field' + count + '-3" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 0; i < 60; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '<label for="Field' + count + '-3">SS</label>';
        field_html += '</span>';
        field_html += '<span class="ampm">';
        field_html += '<select id="Field' + count + '-4" name="Field' + count + '-4" class="field select" style="width: 4em;" readonly="readonly" disabled="disabled">';
        field_html += '<option value="AM" selected="selected">AM</option>';
        field_html += '<option value="PM">PM</option>';
        field_html += '</select>';
        field_html += '<label for="Field' + count + '-4">AM/PM</label>';
        field_html += '</span>';
        field_html += '<br><br>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "phone")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        var mobileWidth = 'style="width:90%;"';
        if (basic_arr[11] == "1") {
            field_html += '<select disabled="disabled" readonly="readonly" id="areacode' + count + '" name="areacode' + count + '" style="margin-right:5px;"><option>Area Code</option></select>';
            mobileWidth = 'style="width:70%;"';
        }
        field_html += '<input ' + mobileWidth + ' type="text" disabled="disabled" readonly="readonly" maxlength="255" value="' + basic_arr[3] + '" ';
        field_html += 'class="field text medium" name="Field' + count + '" id="Field' + count + '">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        //areacode
        var choice_list = new Array();
        for (var i = 0; i < option_arr['choices'].length; i++)
        {
            var checked = 0;
            if (option_arr['selected'][i] == 1) {
                checked = 1;
            }
            choice_list[i] = {id: (i + 1), label: option_arr['choices'][i], selected: checked, valueid: option_arr['id'][i], choiceid: option_arr['choiceid'][i], otherRequired: option_arr['otherRequired'][i]};
        }
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: basic_arr[3], choices: choice_list, date_format: basic_arr[4], columns: '', likert: {}
//            , length: basic_arr[5], formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "money")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<span class="symbol">$</span>';
        field_html += '<span class="shortname">';
        field_html += '<input type="text" id="Field' + count + '-1" name="Field' + count + '-1" class="field text currency" value="' + basic_arr[3] + '" size="10" readonly="readonly" disabled="disabled">';
        field_html += '<label for="Field' + count + '-1">Dollars</label>';
        field_html += '</span>';
        field_html += '<span class="symbol">.</span>';
        field_html += '<span class="shortname">';
        field_html += '<input type="text" id="Field' + count + '-2" name="Field' + count + '-2" class="field text" value="" size="2" maxlength="2" readonly="readonly" disabled="disabled">';
        field_html += '<label for="Field' + count + '-2">Cents</label>';
        field_html += '</span>';
        field_html += '<br><br>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "address")
    {
        if (basic_arr[3] != "")
        {
            isAddress = true;
        }
        field.className += " complex";
        field.style.display = "block";
        field.style.height = "240px";
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<span class="full left">';
        field_html += '<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">';
        field_html += '<label for="Field">Street Address</label>';
        field_html += '</span>';
        field_html += '<span class="full left">';
        field_html += '<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">';
        field_html += '<label for="Field">Address Line 2</label>';
        field_html += '</span>';
        field_html += '<span class="full left">';
        field_html += '<span class="left">';
        field_html += '<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">';
        field_html += '<label for="Field">City</label>';
        field_html += '</span>';
        field_html += '<span class="right">';
        field_html += '<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">';
        field_html += '<label for="Field">State / Province / Region</label>';
        field_html += '</span>';
        field_html += '</span>';
        field_html += '<span class="full left">';
        field_html += '<span class="left">';
        field_html += '<input id="Field" name="Field" class="field text addr" value="" maxlength="15" readonly="readonly" disabled="disabled" type="text">';
        field_html += '<label for="Field">Postal / Zip Code</label>';
        field_html += '</span>';
        field_html += '<span class="right">';
        field_html += '<select id="Field" name="Field" class="field select addr" readonly="readonly" disabled="disabled">';
        field_html += '<option value="" selected="selected"></option>';
        field_html += option_countries();
        field_html += '</select>';
        field_html += '<label for="Field">Country</label>';
        field_html += '</span>';
        field_html += '</span>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "pagebreak") {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion">';
        //field_html += basic_arr[0];
        //field_html += '<span class="req" id="req_'+count+'">' + basic_arr[1] + '</span>';
        field_html += '---------Page x of y---------';
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="button" id="backpage" name="backpage" value="Back">&nbsp;<input type="button" id="nextpage" name="nextpage" value="Next">';
        field_html += '</div>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "likert")
    {
        var statement_list = new Array();
        //selected
        if (basic_arr[3] == "")
        {
            basic_arr[3] = -1;
        }

        if (basic_arr[3] > -1)
        {
            isLikert = true;
        }

        field.className += " likert col" + option_arr['choices'].length;

        field_html += '<label for="Field' + count + '" id="title' + count + '" class="cus_title">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label></br></br>';

        field_html += '<table cellspacing="0">';
        // field_html += '<caption id="title'+count+'">';
        // field_html += basic_arr[0];
        // field_html += '<span class="req" id="req_'+count+'">' + basic_arr[1] + '</span>';
        // field_html += '</caption>';
        field_html += '<thead>';
        field_html += '<tr>';
        field_html += '<th>&nbsp;</th>';
        var originalColumns = new Array();
        for (var i = 0; i < option_arr['choices'].length; i = i + likert_arr.length)
        {
            originalColumns.push(option_arr['choices'][i]);
        }
        for (var i = 0; i < originalColumns.length; i++)
        {
            field_html += '<td>' + originalColumns[i] + '</td>';
        }
        field_html += '</tr>';
        field_html += '</thead>';
        field_html += '<tbody>';
        var x_index = 0;
        var likert_num = likert_arr.length;
        for (var i = 0; i < likert_arr.length; i++)
        {
            if (i % 2 == 0)
            {
                field_html += '<tr class="">';
            }
            else
            {
                field_html += '<tr class="alt">';
            }
            field_html += '<th><label for="Field' + (i + 1) + '">' + likert_arr[i]['label'] + '</label></th>';
            for (var j = 0; j < originalColumns.length; j++)
            {
                field_html += '<td title="' + originalColumns[j] + '">';
                if (option_arr['selected'][x_index] == "1")
                {
                    field_html += '<input type="radio" name="Field' + (j + 1) + '" id="Field' + count + '-' + (j + 1) + ' checked="checked" readonly="readonly" disabled="disabled">';
                } else {
                    field_html += '<input type="radio" name="Field' + (j + 1) + '" id="Field' + count + '-' + (j + 1) + ' readonly="readonly" disabled="disabled">';
                }
                field_html += '<label for="Field' + count + '-' + (j + 1) + '">' + (j + 1) + '</label>';
                field_html += '</td>';
                x_index++;
            }
            field_html += '</tr>';
            statement_list[i] = {likertid: likert_arr[i]['id'], label: likert_arr[i]['label'], valueid: likert_arr[i]['vid']};
        }
        choice_list = new Array();
        for (var i = 0; i < option_arr['choices'].length; i++)
        {
            var checked = 0;
            if (option_arr['selected'][i] == 1) {
                checked = 1;
            }
            choice_list[i] = {id: (i + 1), label: option_arr['choices'][i], selected: checked, valueid: option_arr['id'][i], choiceid: option_arr['choiceid'][i], otherRequired: option_arr['otherRequired'][i], likertid: option_arr['likertid'][i]};
        }
        field_html += '</tbody>';
        field_html += '</table>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//		option_obj = toObject(option_arr);
//		console.log(option_obj);
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: '', likert: {statements: statement_list, columns: originalColumns, selected: basic_arr[3], originalStatementCount: statement_list.length}, formid: basic_arr[6], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16], });
    }
    // Modify By Wood
    // Draw the plain text interface for load up web form
    else if (type == "plaintext")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'medium';
        }
        field_html += '<div id="Field' + count + '" class="' + style + '" style="font-size:15px;">';
        field_html += basic_arr[0];
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: '', size: style, duplicate: 0, defaultValue: '', choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    // End Modify By Wood
    else
    {
        return;
    }
    field.innerHTML = field_html;
    fieldUl.appendChild(field);
    fieldDiv.appendChild(fieldLabel);
    fieldDiv.appendChild(fieldUl);
//    var formFields = $(item);
//    formFields.append(fieldDiv);
//    var formFields = document.getElementById('formFields');
//    formFields.appendChild(fieldDiv);
//	formFields.appendChild(field);
    if ((type == "text" || type == "number" || type == "textarea" || type == "email" || type == "url") && basic_arr[3] != "")
    {
        document.getElementById('Field' + count).value = basic_arr[3];
    }
//    $('#' + id).click(makeEditable);
//	$('#Field'+count).mousedown(function(){
//		alert('123');
//	});
    if (loadClass)
    {
        $('#' + id).addClass(style);
    }
//    bindDragEvents(divId);

    if (isLikert)
    {
        generateLikert(id)
    }
    else if (isAddress)
    {
        all_options = document.getElementById(id).getElementsByTagName('option');
        for (var i = 0; i < all_options.length; i++)
        {
            if (all_options[i].value == basic_arr[3])
            {
                all_options[i].selected = true;
                break;
            }
        }
    }
    
    return fieldDiv;
}
