//flag changes on page (0 - not changes, 1 - page changed)
var pageChanges = 0;
var responseDuplicate = false;
var fr, gridContentRow, gridContent, pages, p;
var valueShadow = "";
var r = 0;
var g = 0;
var b = 0;
var a = 1;

//global ajax object
var ajaxData = new function () {
    var self = this;
    this.request = function (res) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseUrl + "/admin/templates/ajax/" + res.url,
            data: res.data,
            beforeSend: function () {
                loader.start();
            },
            success: function (data) {
                if (data.error !== undefined) {
                    alert(data.error);
                } else {
                    if (res.success)
                        res.success(data);
                }
                loader.stop();
            },
            error: function () {
                loader.stop();
                alert('wrong request!');
            }
        });
    };
};

//animation object for loading
var loader = new function () {
    this.startClass = 'start';
    this.loaderClass = '.loader';
    this.start = function () {
        $(this.loaderClass).addClass(this.startClass);
    };
    this.stop = function () {
        $(this.loaderClass).removeClass(this.startClass);
    };
};

var panels = new function () {
    "use strict";
    var self = this;
    this.currentEditPageId;
    this.typeAction = 'add'; // add or edit
    this.typeActionMenu = 'add'; // add or edit
    //scroll in panels on resize
    $(window).resize(function () {
        var windHeigh = $(window).height();
        var windWidthq = $(window).width();
        var iframeHeigh = fr.height();
        var iframeWidth = fr.width();
        $('#iframeSizeWidth').text(iframeWidth + 'px');
        $('#iframeSizeHeight').text(iframeHeigh + 'px');
        $('#pages-panel-np .panel-body').height(windHeigh - 100);
        $('#pages-panel .panel-body').height(windHeigh - 100);
        $('#insert-panel .panel-body').height(windHeigh - 100);
        $('#menu-panel-np .panel-body').height(windHeigh - 100);
        $('#item-editor-tab-s1').height(windHeigh - 260);
        $('#item-editor-tab-s2').height(windHeigh - 260);
        $('#item-editor-tab-s3').height(windHeigh - 260);
        $('#item-editor-tab-s4').height(windHeigh - 270); //270
        $('#item-editor-tab-s5').height(windHeigh - 225);
        $('.widget-body.no-padding.greyBackground').height(windHeigh - 122).mCustomScrollbar();
        $('.modalWithBgImg').mCustomScrollbar();
        //$('#editor-iframe').width($(window).width() - 290);

        if (windWidthq < "1288" && windWidthq > "1065" && iframeWidth < "991" && iframeWidth > "768") {
            $('.device-type a').removeClass('active');
            $('.device-type .tabletportrait').addClass('active');
        } else if (windWidthq > "1288" && iframeWidth > "991") {
            $('.device-type a').removeClass('active');
            $('.device-type .desktop').addClass('active');
        } else if (iframeWidth < "768") {
            $('.device-type a').removeClass('active');
            $('.device-type .mobileportrait').addClass('active');
        }
    });




    //scroll in panels init
    $('#pages-panel-np .panel-body').height($(window).height() - 100).mCustomScrollbar();
    $('#pages-panel .panel-body').height($(window).height() - 100).mCustomScrollbar();
    $('#insert-panel .panel-body').height($(window).height() - 100).mCustomScrollbar();
    $('#menu-panel-np .panel-body').height($(window).height() - 100).mCustomScrollbar();
    $('.modalWithBgImg').mCustomScrollbar();
    $('#item-editor-tab-s1').height($(window).height() - 225).mCustomScrollbar();
    $('#item-editor-tab-s4').height($(window).height() - 270).mCustomScrollbar(); //270
    $('#item-editor-tab-s3').height($(window).height() - 225).mCustomScrollbar();
    $('#item-editor-tab-s2').height($(window).height() - 260).mCustomScrollbar();
    $('.widget-body.no-padding.greyBackground').height($(window).height() - 122).mCustomScrollbar();
    //$('#editor-iframe').height($(window).height() - 105);



    this.changeContent = function () {
        pageChanges = 1;
        $('#savePageChanges').removeAttr('disabled');
    };
    this.toolsInRow = function () {
    };
    this.run = function () {
        var windWidthq = $(window).width();
        var iframeHeigh = fr.find('html').height();
        var iframeWidth = fr.find('html').width();
        $('#iframeSizeWidth').text(iframeHeigh + 'px');
        $('#iframeSizeHeight').text(iframeWidth + 'px');
        if (windWidthq < "1288" && windWidthq > "1065" && iframeWidth < "991" && iframeWidth > "768") {
            $('.device-type a').removeClass('active');
            $('.device-type .tabletportrait').addClass('active');
        } else if (windWidthq > "1288" && iframeWidth > "991") {
            $('.device-type a').removeClass('active');
            $('.device-type .desktop').addClass('active');
        } else if (iframeWidth < "768") {
            $('.device-type a').removeClass('active');
            $('.device-type .mobileportrait').addClass('active');
        }
        //save page changes or new page
        $('#page-np-save').click(function () {
            var block = $('#pages-panel-np');
            //validate form
            if (block.find('input[name=pageName]').val() !== '') {
                //get meta data from page panel
                var settings = JSON.stringify({
                    pageOGTitle: block.find('input[name=pageOGTitle]').val(),
                    pageOGDescription: block.find('textarea[name=pageOGDescription]').val(),
                    pageOGImage: block.find('input[name=pageOGImage]').val(),
                    pageHeadCode: block.find('textarea[name=pageHeadCode]').val(),
                    pageBodyCode: block.find('textarea[name=pageBodyCode]').val()
                });
                //set data to the server
                ajaxData.request({
                    url: (self.typeAction === 'add') ? 'setPage' : 'updatePage',
                    data: {
                        tplId: templateId,
                        dId: domainId,
                        pageId: (self.typeAction === 'add') ? false : self.currentEditPageId,
                        pageName: block.find('input[name=pageName]').val(),
                        pageTitle: block.find('input[name=pageTitle]').val(),
                        setHomePage: block.find('input[name=setHomePage]:checked').length !== 0 ? 1 : 0,
                        pageDescription: block.find('textarea[name=pageDescription]').val(),
                        pageKeywords: block.find('input[name=pageKeywords]').val(),
                        pageSettings: settings
                    },
                    success: function (data) {
                        //reload pages list
                        $('#pages-panel').find('.panel-body ul').html(data.html);
                        $('#pages-panel-np').hide();
                    }
                });
            } else {
                alert('Field "Page Name" is empty!')
            }
        });
        //confirm page remove
        $('#removePageButton').click(function () {
            ajaxData.request({
                url: 'removePage',
                data: {
                    pageId: self.currentEditPageId,
                    tplId: templateId,
                    dId: domainId
                },
                success: function (data) {
                    //remove page item from pages list
                    $('#pages-panel li[data-id="' + self.currentEditPageId + '"]').remove();
                    //if removed pageId == current pageId - reload on other page
                    if (self.currentEditPageId === pageId) {
                        //get first pageId in pages list
                        pageId = $('#pages-panel li:first').attr('data-id');
                        //load page
                        pages.load();
                    }
                }
            });
            $('#modalRemove').modal('hide');
        });
        //open page by click
        $('#pages-panel').on('click', 'a.pageJS', function () {
            if (pageChanges == 1) {
                var isAdmin = confirm("You entered data can not be saved!");
                if (isAdmin == true) {
                    if ($(this).parent().attr('data-id')) {
                        //get page id
                        var id = $(this).parent().attr('data-id');
                        //change global pageId
                        pageId = parent.pageId = id;
                        //reset page data
                        //globalSettings.removeAllBgData();
                        //load new page
                        pages.load();
                    }
                }
            } else {
                if ($(this).parent().attr('data-id')) {
                    //get page id
                    var id = $(this).parent().attr('data-id');
                    //change global pageId
                    pageId = parent.pageId = id;
                    //reset page data
                    //globalSettings.removeAllBgData();
                    //load new page
                    pages.load();
                }
            }
        });
        //close page add(edit) panel
        $('#page-np-close').click(function () {
            $('#pages-panel-np').hide();
        });
        //open add page panel
        $('#page-add').click(function () {
            //change panel type
            self.typeAction = 'add';
            $('#pages-panel-np').show();
            $('#pages-panel-np .panel-name').html('Add new page');
            //reset all fields
            $('#pages-panel-np').find('input, textarea').val('');
            $('#pages-panel-np').find('input[name=setHomePage]').prop('checked', false);
        });
        //open edit panel
        $('#pages-panel').on('click', '.page-settings', function () {
            $('#pages-panel-np').find('input, textarea').val('');
            //set page id for edit
            self.currentEditPageId = $(this).parents('li').attr('data-id');
            //change panel type
            self.typeAction = 'edit';
            ajaxData.request({
                url: 'getPage',
                data: {
                    pageId: self.currentEditPageId,
                    tplId: templateId,
                    dId: domainId
                },
                success: function (data) {
                    var settings = JSON.parse(data.html.tpl_page_meta_settings);
                    var pagesPanelNp = $('#pages-panel-np');
                    pagesPanelNp.show();
                    $('#pages-panel-np .panel-name').html('Edit page');
                    pagesPanelNp.find('input[name="pageName"]').val(data.html.tpl_page_name);
                    pagesPanelNp.find('input[name="pageTitle"]').val(data.html.tpl_page_title);
                    pagesPanelNp.find('textarea[name="pageDescription"]').val(data.html.tpl_page_description);
                    pagesPanelNp.find('input[name="pageKeywords"]').val(data.html.tpl_page_keywords);
                    pagesPanelNp.find('input[name="pageOGTitle"]').val(settings.pageOGTitle);
                    pagesPanelNp.find('textarea[name="pageOGDescription"]').val(settings.pageOGDescription);
                    pagesPanelNp.find('input[name="pageOGImage"]').val(settings.pageOGImage);
                    pagesPanelNp.find('textarea[name="pageHeadCode"]').val(settings.pageHeadCode);
                    pagesPanelNp.find('textarea[name="pageBodyCode"]').val(settings.pageBodyCode);
                    if (data.html.tpl_page_default === '1') {
                        $('#pages-panel-np').find('input[name=setHomePage]').prop('checked', true);
                    } else {
                        $('#pages-panel-np').find('input[name=setHomePage]').prop('checked', false);
                    }
                }
            });
        });
        //remove page link in pages panel
        $('#pages-panel').on('click', '.page-remove', function () {
            //set page id for remove
            self.currentEditPageId = $(this).parents('li').attr('data-id');
            $('#modalRemove').modal();
            //set page name on modal
            var namePage = $('#pages-panel li[data-id="' + self.currentEditPageId + '"] a').html();
            $('#modalRemove .modal-body strong').html(namePage);
        });
        //duplicate page
        $('#pages-panel').on('click', '.page-duplicate', function () {
            //set page for copy
            self.currentEditPageId = $(this).parents('li').attr('data-id');
            ajaxData.request({
                url: 'duplicatePage',
                data: {
                    pageId: self.currentEditPageId,
                    tplId: templateId,
                    dId: domainId
                },
                success: function (data) {
                    //reload pages list
                    $('#pages-panel').find('.panel-body ul').html(data.html);
                }
            });
        });
        //close page panel
        $('.panel-close').click(function () {
            $(this).parents('.panel').hide();
            $('#pages-panel-np').hide();
            $('#pages-pane').parents('li').removeClass('active');
            $('#insert-pane').parents('li').removeClass('active');
        });
        //left menu
        $('#left-panel nav li').click(function () {
            if ($(this).attr('data-link') !== undefined) {
                var activePanelId = $('#left-panel nav li[data-link="panel"].active').attr('data-panel');
                $('#' + activePanelId).find('.panel-close').click();
                $('#left-panel nav li[data-link="panel"]').removeClass('active');
                $(this).addClass('active');
                var panelId = $(this).attr('data-panel');
                $('#' + panelId).show();
            } else {
                $(this).toggleClass('active');
            }
        });
        $('#savePageChanges').click(function () {
            if (pageChanges === 1) {
                pages.save();
                pageChanges = 0;
                $(this).attr('disabled', 'disabled');
            }
        });
        //Collabse rightbar
        var triggerVisibleSidebar = $('.triggerVisibleSidebar');

        bodySelector.on('click', '.triggerVisibleSidebar_visible', function () {
            $('#right-panel').animate({
                right: "-241px"
            }, 700);
            $('.xgate-template-editor #main').animate({
                width: "100%"
            }, 700);
            setTimeout(function () {
                $('.xgate-template-editor #main').css('width', 'calc(100% - 56px) ');
            }, 700);
            triggerVisibleSidebar.animate({
                right: "32px"
            }, 700);
            triggerVisibleSidebar.removeClass('triggerVisibleSidebar_visible');
            triggerVisibleSidebar.addClass('triggerVisibleSidebar_hidden');
        });
        bodySelector.on('click', '.triggerVisibleSidebar_hidden', function () {
            $('#right-panel').animate({
                right: "0"
            }, 700);
            triggerVisibleSidebar.animate({
                right: "0"
            }, 700);
            setTimeout(function () {
                $('.xgate-template-editor #main').attr('style', ' ');
            }, 700);
            triggerVisibleSidebar.removeClass('triggerVisibleSidebar_hidden');
            triggerVisibleSidebar.addClass('triggerVisibleSidebar_visible');
        });

        var collapseLeftBar = $('#collapseLeftBar');
        bodySelector.on('click', '.triggerVisibleSidebarLeft_visible', function () {
            $('#left-panel').animate({
                left: "-50px"
            }, 300);
            $('#collapseLeftBar').animate({
                left: "30px"
            }, 300);
            $('#collapseLeftBar i').animate({
                marginLeft: "25px"
            }, 300);
            collapseLeftBar.removeClass('triggerVisibleSidebarLeft_visible');
            collapseLeftBar.addClass('triggerVisibleSidebarLeft_hidden');
        });
        bodySelector.on('click', '.triggerVisibleSidebarLeft_hidden', function () {
            $('#left-panel').animate({
                left: "0px"
            }, 300);
            $('#collapseLeftBar').animate({
                left: "0px"
            }, 300);
            $('#collapseLeftBar i').animate({
                marginLeft: "14px"
            }, 300);
            collapseLeftBar.removeClass('triggerVisibleSidebarLeft_hidden');
            collapseLeftBar.addClass('triggerVisibleSidebarLeft_visible');
        });

    };
    this.switchTools = function (tool) {
        $('.item-editor').removeClass('selected-tool');
        $('.tool-in-panel').hide();
        $('.item-editor').addClass('selected-tool');
        $("#activePrewiewImage").attr("id", " ");
        if (tool === 'tool-row') {
            $('a[href="#item-editor-tab-s1"]').click();
            $('.' + tool).show();
        } else if (tool === 'tool-image') {
            $('a[href="#item-editor-tab-s1"]').click();
            $('.' + tool).show();
        } else if (tool === 'tool-text') {
            $('a[href="#item-editor-tab-s1"]').click();
            $('.' + tool).show();
        } else {
            $('.' + tool).show();
        }
    };
    this.changeHeightIframe = function () {
        //setTimeout(function () {
        if (Number(fr.find('body').height()) + 105 < $(window).height()) {
            $('#editor-iframe').height($(window).height() - 105);
            fr.find('html').height($(window).height() - 105);
            fr.find('body').css('min-height', $(window).height() - 105 + 'px');
            fr.find('#grid').css('min-height', $(window).height() - 105 + 'px');
        } else {
            $('#editor-iframe').height(Number(fr.find('body').height()) + 50);
            fr.find('html').height(Number(fr.find('body').height()) + 50);
            fr.find('body').css('min-height', $(window).height() - 105 + 'px');
            fr.find('#grid').css('min-height', $(window).height() - 105 + 'px');
        }
        changeIdFr();
        //}, 200);
    };
};

var grid = new function () {
    var self = this;
    this.typeWidget;
    this.currentItem;
    this.deviceDecstope = $(".responsive .desktop");
    this.deviceTable = $(".responsive .tabletportrait");
    this.deviceMobile = $(".responsive .mobileportrait");
    this.editElements = 'img,p,h1,h2,h3,h4,h5,h6,a.link,button,.menuJS,.item-map-over,.item-youtube-over';
    this.editElements1 = 'img,p,h1,h2,h3,h4,h5,h6,a.link,button,nav,.item-map,.item-youtube';
    this.dropFunc = function (dropObj) {
        if (fr.find('.dragTool').length !== 0 || fr.find('.dragToolRow').length !== 0) {
            fr.find('.dragTool').remove();
            ajaxData.request({
                url: 'getElements',
                data: {
                    typeWidget: self.typeWidget,
                    pageId: pageId
                },
                success: function (data) {
                    gridContent.addNewElement(data, dropObj, function () {
                        self.initElements();
                        parent.panels.changeContent();
                        //  panels.changeHeightIframe();
                        pages.recurseDomChildrenUpdate();
                        panels.changeHeightIframe();
                        console.log('dropFunc')
                    });
                }
            });
        } else {
            pages.recurseDomChildrenUpdate();
        }
    };
    this.remowsActiveDevice = function () {
        $(".devMod").hide();
        self.deviceTable.removeClass("active");
        self.deviceMobile.removeClass("active");
        self.deviceDecstope.removeClass("active");
    };
    this.dataSihiIcon = function () {
        var selfs = self.currentItem;
        //var dataSizeHidden = selfs.attr('data-sizehidden');
        var dataSizeIcon = selfs.attr('data-sizeicon');
        var deviceTextP = $("p.visibleOn");
        switch (dataSizeIcon) {
            case "1":
                self.remowsActiveDevice();
                self.deviceDecstope.addClass("active");
                deviceTextP.find("span[data-sizeicon='" + dataSizeIcon + "']").show();
                break;
            case "2":
                self.remowsActiveDevice();
                self.deviceDecstope.addClass("active");
                self.deviceTable.addClass("active");
                deviceTextP.find("span[data-sizeicon='" + dataSizeIcon + "']").show();
                break;
            case "3":
                self.remowsActiveDevice();
                self.deviceDecstope.addClass("active");
                self.deviceMobile.addClass("active");
                deviceTextP.find("span[data-sizeicon='" + dataSizeIcon + "']").show();
                break;
            case "4":
                self.remowsActiveDevice();
                self.deviceTable.addClass("active");
                deviceTextP.find("span[data-sizeicon='" + dataSizeIcon + "']").show();
                break;
            case "5":
                self.remowsActiveDevice();
                self.deviceTable.addClass("active");
                self.deviceMobile.addClass("active");
                deviceTextP.find("span[data-sizeicon='" + dataSizeIcon + "']").show();
                break;
            case "6":
                self.remowsActiveDevice();
                self.deviceMobile.addClass("active");
                deviceTextP.find("span[data-sizeicon='" + dataSizeIcon + "']").show();
                break;
            case "7":
                self.remowsActiveDevice();
                self.deviceTable.addClass("active");
                self.deviceMobile.addClass("active");
                deviceTextP.find("span[data-sizeicon='" + dataSizeIcon + "']").show();
                break;
            case "8":
                self.remowsActiveDevice();
                self.deviceTable.addClass("active");
                self.deviceMobile.addClass("active");
                self.deviceDecstope.addClass("active");
                deviceTextP.find("span[data-sizeicon='" + dataSizeIcon + "']").show();
                break;
            case "9":
                self.remowsActiveDevice();
                deviceTextP.find("span[data-sizeicon='" + dataSizeIcon + "']").show();
                break;
            case undefined:
                self.deviceTable.addClass("active");
                self.deviceMobile.addClass("active");
                self.deviceDecstope.addClass("active");
                break;
        }
    };
    this.changeDevices = function () {
        var changeDevVer = $(".changeDevVersion .responsive a");
        var selfs = self.currentItem;
        self.dataSihiIcon();
        changeDevVer.unbind();
        changeDevVer.click(function () {
            $(this).toggleClass('active');
            panels.changeContent();
            var firInp = (self.deviceDecstope.hasClass('active')) ? true : false;
            var secInp = (self.deviceTable.hasClass('active')) ? true : false;
            var thirtInp = (self.deviceMobile.hasClass('active')) ? true : false;
            // Desktop only
            if (firInp == true && secInp == false && thirtInp == false) {
                $(".devMod").hide();
                $("#deskOnly").show();
                selfs.removeClass("visible-xs visible-sm visible-md visible-lg hidden-sm hidden-xs hidden-lg hidden-md");
                selfs.addClass("hidden-sm hidden-xs");
                selfs.attr("data-sizeicon", "1");
            } else {
                $("#deskOnly").hide();
            }
            // Tablet only
            if (firInp == false && secInp == true && thirtInp == false) {
                $(".devMod").hide();
                $("#tabOnly").show();
                selfs.removeClass("visible-xs visible-sm visible-md visible-lg hidden-sm hidden-xs hidden-lg hidden-md");
                selfs.addClass("hidden-xs hidden-md");
                // selfs.addClass("hidden-xs hidden-lg hidden-md");
                selfs.attr("data-sizeicon", "4");
            } else {
                $("#tabOnly").hide();
            }
            // Mobile only
            if (firInp == false && secInp == false && thirtInp == true) {
                $(".devMod").hide();
                $("#mobOnly").show();
                selfs.removeClass("visible-xs visible-sm visible-md visible-lg hidden-sm hidden-xs hidden-lg hidden-md");
                selfs.addClass("hidden-sm hidden-md hidden-lg");
                selfs.attr("data-sizeicon", "6");
            } else {
                $("#mobOnly").hide();
            }
            // All Device
            if (firInp == true && secInp == true && thirtInp == true) {
                $(".devMod").hide();
                $("#allDevice").show();
                selfs.removeClass("visible-xs visible-sm visible-md visible-lg hidden-sm hidden-xs hidden-lg hidden-md");
                selfs.addClass("visible-xs visible-sm visible-md visible-lg");
                selfs.attr("data-sizeicon", "8");
            } else {
                $("#allDevice").hide();
            }
            // Your imagination : )
            if (firInp == false && secInp == false && thirtInp == false) {
                $(".devMod").hide();
                $("#nullDev").show();
                selfs.removeClass("visible-xs visible-sm visible-md visible-lg hidden-sm hidden-xs hidden-lg hidden-md");
                selfs.addClass("hidden-sm hidden-md hidden-lg hidden-xs");
                selfs.attr("data-sizeicon", "9");
            } else {
                $("#nullDev").hide();
            }
            // Desktop and Tablet
            if (firInp == true && secInp == true && thirtInp == false) {
                $(".devMod").hide();
                $("#deskAndTab").show();
                selfs.removeClass("visible-xs visible-sm visible-md visible-lg hidden-sm hidden-xs hidden-lg hidden-md");
                selfs.addClass("hidden-xs");
                selfs.attr("data-sizeicon", "2");
            } else {
                $("#deskAndTab").hide();
            }
            // Desktop and Mobile
            if (firInp == true && secInp == false && thirtInp == true) {
                $(".devMod").hide();
                $("#deskAndMob").show();
                selfs.removeClass("visible-xs visible-sm visible-md visible-lg hidden-sm hidden-xs hidden-lg hidden-md");
                selfs.addClass("hidden-sm");
                selfs.attr("data-sizeicon", "3");
            } else {
                $("#deskAndMob").hide();
            }
            // Tablet and Mobile
            if (firInp == false && secInp == true && thirtInp == true) {
                $(".devMod").hide();
                $("#tabAndMob").show();
                selfs.removeClass("visible-xs visible-sm visible-md visible-lg hidden-sm hidden-xs hidden-lg hidden-md");
                selfs.addClass("hidden-md hidden-lg");
                selfs.attr("data-sizeicon", "5");
            } else {
                $("#tabAndMob").hide();
            }
            self.dataSihiIcon();
        });
    };
    this.overElement = function (item, type, e) {
        item.addClass('hover');
        fr.find('.info-item').html(type);
        fr.find('.info-item').css({
            display: 'block',
            top: $(e.target).offset().top,
            left: $(e.target).offset().left,
            'margin-top': '-16px'
        });
    };
    this.outElement = function (item) {
        item.removeClass('hover');
        fr.find('.info-item').html('');
        fr.find('.info-item').hide();
    };
    this.hotKeysEvent = function (event) {
        var eventElem = fr.find('.edit-item');
        if (event.ctrlKey || event.metaKey) {
            switch (String.fromCharCode(event.which).toLowerCase()) {
                case 's': //ctrl + s - save page content
                    event.preventDefault();
                    $('#savePageChanges').click();
                    break;
                case 'c': // ctrl + c - copy element in buffer
                    buttonFunction.copyFunc(eventElem[0]);
                    break;
                case 'v': // ctrl + v - paste
                    buttonFunction.pasteInsideFunc(eventElem[0]);
                    break;
                case 'x': // ctrl + x - cut
                    buttonFunction.cutFunc(eventElem[0]);
                    break;
                case 'd': // ctrl + d - duplicate
                    event.preventDefault();
                    buttonFunction.dublicateFunc(eventElem[0]);
                    break;
            }
        } else {
            switch (event.keyCode) {
                case 8: //backspace(delete) - remove element
                case 46:
                    var nonEventEl = ['INPUT', 'TEXTAREA'];
                    if (!$(event.target).hasClass('mce-edit-focus') && fr.find('.edit-item') &&
                        $.inArray(event.target.tagName, nonEventEl) === -1) {
                        event.preventDefault();
                        gridContent.removeElement();
                    }
                    break;
            }
        }
    };
    this.hotKeysRun = function () {
        $(window).on('keydown', function (event, e) {
            self.hotKeysEvent(event);
        });
        fr.find('body').on('keydown', function (event, e) {
            self.hotKeysEvent(event);
        });
    };
    this.run = function () {
        self.hotKeysRun();

        fr.find('#grid').on('mouseover', self.editElements, function (e) {
            var tagName = e.target.tagName;
            var textItems = ['P', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6'];
            if ($.inArray(tagName, textItems) !== -1) {
                self.overElement($(this), 'text', e);
            } else if (tagName === 'IMG') {
                self.overElement($(this), 'image', e);
            } else if (tagName === 'A') {
                if ($(this).hasClass('.link')) {
                    self.overElement($(this), 'link', e);
                }
            } else if (tagName === 'BUTTON') {
                self.overElement($(this), 'button', e);
            } else if (tagName === 'NAV') {
                self.overElement($(this), 'navbar', e);
            }
        }).on('mouseout', self.editElements, function (e) {
            self.outElement($(this));
        });

        $('#DOM-navigator').on('mouseover', 'span', function () {
            var eId = $(this).attr('data-id');
            fr.find('*[data-e-id=' + eId + ']').mouseover();
        }).on('mouseout', 'span', function () {
            var eId = $(this).attr('data-id');
            fr.find('*[data-e-id=' + eId + ']').mouseout();
        });
        $('#DOM-navigator').on('mouseup', 'span', function (e) {
            var eId = $(this).attr('data-id');
            fr.find('*[data-e-id=' + eId + ']').click();
        });
        var elemInDomList;
        // show context menu
        fr.find('#grid').on('contextmenu', function (e) {
            e.preventDefault();
            contextMenu.run(e);
        });
        fr.find('#grid').on('click', self.editElements, function (e) {
            var tagName = e.target.tagName;
            var textItems = ['P', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6'];
            var item = $(this);
            self.currentItem = item;
            self.changeDevices();
            var edit = 0;
            //var dataEID = $(this).attr('data-e-id');
            //$('#DOM-navigator-main').find('*[data-id="' + dataEID + '"]').trigger('click');
            if ($.inArray(tagName, textItems) !== -1) {
                edit = 1;
                gridContent.typeElement = 'text';
            } else if (tagName === 'IMG') {
                edit = 1;
                gridContent.typeElement = 'image';
            } else if (tagName === 'A') {
                if ($(this).hasClass('.link')) {
                    edit = 1;
                    gridContent.typeElement = 'link';
                } else {
                    edit = 0;
                }
            } else if (tagName === 'BUTTON') {
                edit = 1;
                gridContent.typeElement = 'button';
            } else if (tagName === 'DIV') {
                if ($(this).hasClass('menuJS')) {
                    self.currentItem = $(this).parent();
                    edit = 1;
                    gridContent.typeElement = 'menu';
                } else if ($(this).hasClass('item-map-over')) {
                    edit = 1;
                    self.currentItem = $(this).parent();
                    gridContent.typeElement = 'map';
                } else if ($(this).hasClass('item-youtube-over')) {
                    edit = 1;
                    self.currentItem = $(this).parent();
                    gridContent.typeElement = 'youtube';
                } else {
                    edit = 0;
                }
            } else {
                edit = 0;
            }
            if (edit === 1) {
                globalCssEditor.initEditor(item); //def val
                globalCssEditor.posTLBR(item); //def val
                globalCssEditor.positionCssEditor(item); // Position - display, float, clear, overflow, position - button;
                globalCssEditor.tabBtnShadowEditor(item);
                globalCssEditor.bgEditor(item);
                globalCssEditor.borderEditor(item);
                globalCssEditor.margPadding(item); // Margin and padding
                globalCssEditor.getStyleItem(item);
                gridContent.editElement();
            }
        });

        fr.find('#grid').on('click', '.row', function (e) {
            if ($(e.target).hasClass('row') || $(e.target.firstElementChild).hasClass('row')) {
                gridContent.typeElement = 'row';
                var item = $(e.target);
                self.currentItem = item;
                self.changeDevices();
                gridContent.editElement();
                globalCssEditor.initEditor(item); //def val
                globalCssEditor.positionCssEditor(item); // Position - display, float, clear, overflow, position - button;
                globalCssEditor.bgEditor(item);
                globalCssEditor.borderEditor(item);
                //getColumnSize(item);
            }
        });

        fr.find('#grid').on('mouseover', '.row', function (e) {
            if ($(e.target).hasClass('row') || $(e.target.firstElementChild).hasClass('row')) {
                self.overElement($(this), 'row', e);
            }
        }).on('mouseout', '.row', function (e) {
            self.outElement($(this));
        });

        fr.find('#grid').on('click', '.column', function (e) {
            //var event = e;
            // var dataEID = $(this).attr('data-e-id');
            if ($(e.target).hasClass('column') || $(e.target.firstElementChild).hasClass('column')) {
                // $('#DOM-navigator-main').find('*[data-id="' + dataEID + '"]').trigger('click');
                gridContent.typeElement = 'column';
                var item = $(e.target);
                self.currentItem = item;
                self.changeDevices();
                gridContent.editElement();
                globalCssEditor.bgEditor(item);
                globalCssEditor.borderEditor(item);
                // getColumnSize(item);

            }
        });

        // hide context menu
        fr.find('#grid').on('click', function (e) {
            contextMenu.HideContMenu(e);
            if ($(e.target).hasClass('column') || $(e.target.firstElementChild).hasClass('column')) {
                var item = $(e.target);
                var flagGet = true;
                getColumnSize(item, e, flagGet);
                //setTimeout(function () {
                //    self.chekckcc(e);
                //}, 1000)
            } else {
                self.chekckcc(e);
            }


            //var idElemForActive = e.target.getAttribute('data-e-id');
            //elemInDomList = $('#DOM-navigator-main').find('*[data-id="' + idElemForActive + '"]');
            //$('#DOM-navigator-main').find('.treeHover').removeClass("treeHover");
            //elemInDomList.addClass("treeHover");
        });

        fr.find('#grid').on('mouseover', '.column', function (e) {
            if ($(e.target).hasClass('column') || $(e.target.firstElementChild).hasClass('column')) {
                self.overElement($(this), 'column', e);
            }
        }).on('mouseout', '.column', function (e) {
            self.outElement($(this));
        });
    };

    this.chekckcc = function (e) {
        var idElemForActive = e.target.getAttribute('data-e-id');
        //console.log(idElemForActive);
        elemInDomList = $('#DOM-navigator-main').find('*[data-id="' + idElemForActive + '"]');
        $('#DOM-navigator-main').find('.treeHover').removeClass("treeHover");
        elemInDomList.addClass("treeHover");
    };

    //------
    this.initElements = function () {
        fr.find('.column').sortable({
            items: self.editElements1,
            connectWith: ".column",
            opacity: .35,
            cancel: '.no-sortable',
            dropOnEmpty: true,
            scroll: true,
            scrollSensitivity: 100,
            iframefix: true,
            sort: function (event, ui) {
                $(ui.helper).css({
                    overflow: 'hidden',
                    width: '0px',
                    height: '0px'
                });
            },
            update: function () {
                panels.changeContent();
            },
            placeholder: {
                element: function () {
                    return $('<div class="ui-state-highlight"></div>')[0];
                },
                update: function () {
                    return;
                }
            },
            receive: function (event, ui) {
                var item = $(ui.helper);
                self.dropFunc(item);
            }
        });

        $('.xgate-template-editor-tools').draggable({
            cursor: "crosshair",
            connectToSortable: fr.find('#grid, .column'),
            scroll: 'true',
            helper: 'clone',
            scrollSensitivity: 100,
            iframeFix: true,
            refreshPositions: true,
            start: function (event, ui) {
                self.typeWidget = $(this).attr('data-type');
                fr.find('body').append('<div class="dragTool"><a class="' + $(this).context.className + '">' + $(this).html() + '</a></div>');
                fr.find('.dragTool').css({
                    position: 'absolute',
                    'z-index': 999
                });
                $('#insert-panel').hide();
                $('#insert-pane').parents('li').removeClass('active');
            },
            stop: function () {
                $(this).css({
                    top: 0,
                    left: 0,
                    position: 'relative'
                });
                fr.find('.dragTool').remove();
            },
            drag: function (event, ui) {
                fr.find('.dragTool').css({
                    left: event.offsetX - fr.find('.dragTool').width() / 2,
                    top: event.offsetY,
                    position: 'absolute'
                });
            }
        });

        $('.xgate-template-editor-tools-i').draggable({
            cursor: "crosshair",
            connectToSortable: fr.find('.column'),
            scroll: 'true',
            helper: 'clone',
            scrollSensitivity: 100,
            iframeFix: true,
            refreshPositions: true,
            start: function (event, ui) {
                self.typeWidget = $(this).attr('data-type');
                fr.find('body').append('<div class="dragToolRow"><a class="' + $(this).context.className + '">' + $(this).html() + '</a></div>');
                fr.find('.dragToolRow').css({
                    position: 'absolute',
                    'z-index': 22
                });
                $('#insert-panel').hide();
                $('#insert-pane').parents('li').removeClass('active');
            },
            stop: function () {
                $(this).css({
                    top: 0,
                    left: 0,
                    position: 'relative'
                });
                fr.find('.dragToolRow').remove();
            },
            drag: function (event, ui) {
                fr.find('.dragToolRow').css({
                    left: event.pageX - fr.find('.dragToolRow').width() / 2,
                    top: ui.offset.top
                });
            }
        });
    };
};

//main tools panel
var tools = new function () {
    var self = this;
    this.run = function () {
        $('.tools-select').click(function () {
            $('.tools-s').hide();
            $('#' + $(this).attr('data-tools')).show();
        });
        $('.tools-s-close').click(function () {
            $('.tools-s').hide();
        });
        $('#menu-editor button.navbar-toggle').click(function () {
            $('.tools-s').hide();
        });
        $('#menu-editor li:not(.tools-menu,.tools-select)').click(function () {
            $('.tools-s').hide();
        });
        this.drag();
    };
    this.drag = function () {
        fr.find('#grid').sortable({
            items: '.row',
            connectWith: ".column, .row",
            opacity: .35,
            cancel: '.no-sortable',
            dropOnEmpty: true,
            scroll: true,
            scrollSensitivity: 100,
            iframefix: true,
            sort: function (event, ui) {
                //$(ui.helper).width('0px');
                //$(ui.helper).height('0px');
            },
            placeholder: {
                element: function (currentItem) {
                    return $('<div class="ui-state-highlight"></div>')[0];
                },
                update: function (container, p) {
                    return;
                }
            },
            update: function () {
                panels.changeContent();
            },
            receive: function (event, ui) {
                var item = $(ui.helper);
                var data = grid.dropFunc(item);
            }
        });

    };
};
//image manager
var galleryManager = new function () {
    var self = this;
    this.typeModal = 'header';
    this.modalId = '#changePicture';
    this.usedImages = [];
    this.bgRowItem;
    this.selectImage = 0;
    this.removeImageIn = function () {
        //remove image
        imageId = $("#activePrewiewImage img").attr("data-id");
        var imgforRemowInRow = fr.find("img.edit-item[data-imgremove=" + imageId + "]");
        var imgforRemowInProp = $("#imgPropThumb[data-imgremove=" + imageId + "]");
        var imgforRemowInBG = $(".contWithprevBg[data-id=" + imageId + "]");
        ajaxData.request({
            url: baseUrl + "RemoveFiles",
            data: {
                imageId: imageId,
                dId: domainId,
                tplId: templateId
            },
            success: function () {
                $("#activePrewiewImage").remove();
                $(".contWithprevBg #activeBgImg").remove();
                $('#previewPopUp').fadeOut(200);
                $("#imgRemowe").addClass("disabled");
                $('#imgPrevId').attr('src', '');
                $('#imgPrevId').attr('alt', '');
                $('#imgPrevId').attr('title', '');
                $('.preview').attr('id', '');
                $('#imgPopup').fadeOut(100);
                $("#imgRemowe").addClass("disabled");
                imgforRemowInRow.removeAttr("data-imgremove");
                imgforRemowInRow.attr('src', '../../img/uploadImg.png');
                imgforRemowInProp.removeAttr("data-imgremove");
                imgforRemowInBG.removeAttr("data-imgremove");
                imgforRemowInProp.attr("src", "");
                imgforRemowInBG.attr("src", "");
                $(".imgWidthForPreviewProp").text(" ");
                $(".imgHeightForPreviewProp").text(" ");
                gridContentRow.save();
                // pages.changeContent();
            }
        });
    };
    // add img in edit panel
    this.addImgInEditRow = function () {
        var imgContainer = fr.find("div[data-type='image']");
        // if ($("div[data-type='image']").is(".edit-item")) {
        var source = fr.find('.edit-item').parent();
        var panel = $('#linktabImg');
        var listPages = $('#pages-panel .panel-body nav li');
        $('select[name=imagePageUrl]').html('');
        $('select[name=imagePageUrl]').append('<option value="0" selected="selected">Choose Page</option>');
        $.each(listPages, function () {
            $('select[name=imagePageUrl]').append('<option value="' + $(this).attr('data-id') + '">' + $(this).find('a').html() + '</option>');
        });
        self.selectImage = 1;
        panel.find('input[name=imageLink]').removeAttr('checked');
        panel.find('input[name=imageUrl]').val('');
        panel.find('select[name=imagePageUrl]').val(0).change();
        panel.find('input[value=page]').removeAttr('checked');
        panel.find('input[value=url]').attr('checked', 'checked');
        panel.find('input[name=imageLightbox]').removeAttr('checked');
        panel.find('input[name=imageTargetUrl]').removeAttr('checked');
        if (source.attr('href') && !source.attr('data-lightbox')) {
            panel.find('input[name=imageLink]').attr('checked', 'checked');
            if (source.attr('data-page')) {
                panel.find('input[value=page]').attr('checked', 'checked');
                panel.find('select[name=imagePageUrl]').val(source.attr('data-page')).change();
            } else {
                panel.find('input[value=url]').attr('checked', 'checked');
                panel.find('input[name=imageUrl]').val(source.attr('href'));
            }
            if (source.attr('target')) {
                panel.find('input[name=imageTargetUrl]').attr('checked', 'checked');
            }
        }
        if (source.attr('data-lightbox')) {
            panel.find('input[name=imageLightbox]').attr('checked', 'checked');
        }
        self.selectImage = 0;
        var imgActiveInRow = fr.find('.edit-item');
        var imgActiveInRowProp = fr.find('.edit-item');
        var imgThumbProp = imgActiveInRow.attr("data-srcthumb");
        var idForRemoveProp = imgActiveInRow.attr("data-imgremove");
        $("#imgPropThumb").attr("src", " ");
        $("#imgPropThumb").removeAttr("data-imgremove");
        $("#imgPropThumb").attr("data-imgremove", "" + idForRemoveProp + "");
        $(".imgWidthForPreviewProp").text(fr.find("div.edit-item[data-type='image'] .item-image img").css("width"));
        $(".imgHeightForPreviewProp").text(imgActiveInRowProp.css("height"));
        if (imgThumbProp === undefined) {
            $("#imgPropThumb").attr("src", "http://" + window.location.hostname + "/upload/0/be152cff2d95e47fcc5ed1672e0d3501.jpg")
        } else {
            $("#imgPropThumb").attr("src", "" + imgThumbProp + "")
        }
    };
    this.insertImage = function (imgSrcForPreview, srcForThumb, imgTitileForPreview, imgAltForPreview, callback) {
        fr.find('.edit-item .item-image').addClass('gallery-image');
        fr.find('.edit-item .item-image').html("<a title=''><img src='" + imgSrcForPreview + "' data-srcthumb='" + srcForThumb + "'  title='" + imgTitileForPreview + "' alt='" + imgAltForPreview + "' /></a>");
        setTimeout(callback, 300);
    };
    this.on = function () {
        $('#linktabImg').change(function (e) {
            if (self.selectImage === 0) {
                var panel = $('#linktabImg');
                var source = fr.find('.edit-item').parent();
                if (e.target.name === 'imageLightbox') {
                    panel.find('input[name=imageLink]').removeAttr('checked');
                }
                if (e.target.name === 'imageLink') {
                    panel.find('input[name=imageLightbox]').removeAttr('checked');
                }
                if (panel.find('input[name=imageLink]:checked').length !== 0) {
                    source.removeAttr('data-lightbox');
                    source.removeAttr('class');
                    if (panel.find('input[name=typeImageHref]:checked').val() === 'url') {
                        source.removeAttr('href');
                        if (panel.find('input[name=imageUrl]').val() !== '') {
                            source.attr('href', panel.find('input[name=imageUrl]').val());
                            source.removeAttr('data-page');
                        }
                    } else {
                        source.removeAttr('href');
                        if (panel.find('select[name=imagePageUrl]').val() !== '0') {
                            source.attr('href', '/site/view/' + panel.find('select[name=imagePageUrl]').val());
                            source.attr('data-page', panel.find('select[name=imagePageUrl]').val());
                        }
                    }
                    if (panel.find('input[name=imageTargetUrl]:checked').length !== 0) {
                        source.attr('target', '_blank');
                    } else {
                        source.removeAttr('target');

                    }
                } else {
                    source.removeAttr('href');
                    source.removeAttr('class');
                    source.removeAttr('alt');
                    source.removeAttr('data-lightbox');
                    source.removeAttr('data-page');
                    source.removeAttr('target');
                }
                if (panel.find('input[name=imageLightbox]:checked').length !== 0) {
                    var imgSrcForPreview = source.find("img").attr("src");
                    source.attr('data-lightbox', true);
                    source.addClass("example-image-link");
                    source.attr('href', imgSrcForPreview);
                }
                panels.changeContent();
            }
        });
        /*fr.find("div[data-type='image']").on("click",function () {

         })*/

        $('#contWithUploadImg').on('click', '.showPreview', function () {
            var selfImage = fr.find("img.edit-item");
            $("#activePrewiewImage").attr("id", " ");
            var selectedImgForPrew = $(this).parent().find("img");
            $(this).parent(".preview").attr('id', 'activePrewiewImage');
            var imgSrcForPreview = $(this).attr("data-img");
            var imgAltForPreview = $(this).attr("alt");
            var srcForThumb = $(this).attr("src");
            var imgTitileForPreview = $(this).attr("title");
            var idForRemove = $(this).attr("data-id");
            $("#imgPropThumb").attr("src", " ");
            $("#imgPropThumb").removeAttr("data-imgremove");
            var imgActiveInRow = selfImage;
            var idForRemoveProp = imgActiveInRow.attr("data-imgremove");
            var imgActiveInRowProp = fr.find("div.edit-item[data-type='image'] .item-image img");
            imgActiveInRow.removeAttr("data-imgremove");
            imgActiveInRow.attr("data-imgremove", "" + idForRemove + "");
            self.insertImage(imgSrcForPreview, srcForThumb, imgTitileForPreview, imgAltForPreview, function () {
                imgActiveInRow.attr({
                    src: imgSrcForPreview,
                    alt: imgAltForPreview,
                    'data-srcthumb': srcForThumb
                });
            });
            var imgThumbProp = imgActiveInRow.attr("data-srcthumb");
            $("#imgPropThumb").attr("data-imgremove", "" + idForRemove + "");
            $(".imgWidthForPreviewProp").text(fr.find("div.edit-item[data-type='image'] .item-image img").css("width"));
            $(".imgHeightForPreviewProp").text(fr.find("div.edit-item[data-type='image'] .item-image img").css("height"));
            $("#imgPropThumb").attr("src", srcForThumb);
            panels.changeContent();
        });

        fr.find('img.edit-item').click(function () {
            var idForRemove = $(this).attr("data-id");
            var selfImage = fr.find("img.edit-item");
            var srcForThumb = $(this).attr("src");
            var imgThumbProp = imgActiveInRow.attr("data-srcthumb");
            $("#imgPropThumb").attr("data-imgremove", "" + idForRemove + "");
            $(".imgWidthForPreviewProp").text($(this).css("width"));
            $(".imgHeightForPreviewProp").text($(this).css("height"));
            $("#imgPropThumb").attr("src", selfImage);
        });


        bodySelector.on('click', '#dropPicture, #showDropZone', function () {
            $('#wid-id-uploadPicture').hide(300);
            $('#wid-id-uploadPicture-Img').hide(300);
            $('#wid-id-dropzone').show(300);
            $('#wid-id-dropzone-Img').show(300);
        });
        bodySelector.on('click', '#showDropZone', function () {
            $('#wid-id-dropzoneImg').show(300);
        });
        bodySelector.on('click', '#backGallery', function () {
            $('#wid-id-uploadPicture').show(300);
            $('#wid-id-uploadPicture-Img').show(300);
            $('#wid-id-dropzone').hide(300);
            $('#wid-id-dropzone-Img').hide(300);
        });
        $(this.modalId).on('hidden.bs.modal', function () {
            if (self.typeModal === 'image') {
//                $('.edit-item .grid-stack-item-save').click();
                panels.changeContent();
            }
        });
        $(this.modalId).on('show.bs.modal', function () {
            $(".superbox-list").removeClass('active');
            $(".superbox-img").removeClass('selected');
            $(".superbox-show").hide();
        });
        $('#wid-id-uploadPicture').show(300);
        $('#wid-id-uploadPicture-Img').show(300);
        $('#wid-id-dropzone').hide(300);
        $('#wid-id-dropzone-Img').hide(300);
        //selectPicture
        bodySelector.on('click', '.selectPicture img', function (event) {
            $('.selectPicture img').removeClass('selected');
            $(this).addClass('selected');
            var src = $('.superbox-current-img').attr('src');
            if (self.typeModal === 'image') {
                $('.image-settings-links').addClass('show');
            }
        });
        // gallery superbox plugin
        $('.selectPicture').SuperBox();
        //use selected image
        bodySelector.on('click', '.useImageGallery', function (event) {
            //selected image
            var img = $('.superbox-current-img').attr('src');
            //image in content
            //image for row background
            if (self.typeModal === 'bgrow') {
                parent.gridContentRow.removeBgRow(self.bgRowItem);
                $(self.bgRowItem).find('.buttons-block.out .bg-menu li').removeClass('active');
                $(self.bgRowItem).addClass('bg_custom_img');
                $(self.bgRowItem).attr('data-img', img);
                $(self.bgRowItem).attr('data-src', 'bg_custom_img');
                $(self.bgRowItem).css('background-image', 'url(' + img + ')');
//                gridContentRow.save();
                panels.changeContent();
            }
            //image for body background
            if (self.typeModal === 'bgbody') {
                globalSettings.removeAllBgData();
                globalSettings.globalItem.find('.bg-menu li').removeClass('active');
                globalSettings.globalItem.addClass('bg_custom_img');
                globalSettings.globalItem.attr('data-img', img);
                globalSettings.globalItem.attr('data-src', 'bg_custom_img');
                globalSettings.globalItem.css('background-image', 'url(' + img + ')');
//                globalSettings.save();
                panels.changeContent();
            }
        });

        //download image by url
        bodySelector.on('click', '.downloadImgWithUrl', function () {
            var urlImage = $('#inputWithUrlImg').val();
            ajaxData.request({
                url: 'downloadImage',
                data: {
                    imageUrl: urlImage,
                    dId: domainId,
                    tplId: templateId
                },
                success: function (data) {
                    $('#contWithUploadImg, .contWithprevBg').append('<div class="preview">\n\
                            <span class="iconOnPrew iconOnPrewImgZoom"><span class="glyphicon glyphicon-zoom-in"></span></span> \n\
                            <span class="iconOnPrew iconOnPrewImgTrash"><span class="glyphicon glyphicon-trash"></span></span> \n\
                            <img class="img-responsive showPreview" src="/upload/' + domainId + '/thumb_' + data.fileName + '" \n\
                            data-img="/upload/' + domainId + '/' + data.fileName + '" \n\
                             data-id="' + data.imageId + '">\n\
                            </div>');
                    setTimeout(function () {
                        $('.modalWithBgImg').mCustomScrollbar('update');
                    }, 10);
                }
            });
            return false;
        });
        //dropzone init
        // Upload image with DporZone

        var myDropzone = new Dropzone('#mydropzone1', {
            url: baseUrl + "/admin/templates/ajax/uploadFiles",
            addRemoveLinks: true,
            maxFilesize: 5,
            autoProcessQueue: false,
            //uploadprogress:false,
            parallelUploads: 20,
            uploadMultiple: true,
            init: function () {
                setTimeout(function () {
                    $("#mydropzone1").addClass("dropzone");
                }, 300);
            },
            dictResponseError: 'Error uploading file!',
            success: function (data, response, xhr) {
                if (responseDuplicate) {
                    return;
                }
                responseDuplicate = true;
                var responseImgJson = jQuery.parseJSON(data.xhr.response);
                var responseImg = responseImgJson.images;
                var countResponse = responseImg.length;
                for (var i = 0; i < countResponse; i++) {
                    $('#contWithUploadImg, .contWithprevBg').append('<div class="preview">\n\
                                <span class="iconOnPrew iconOnPrewImgZoom"><span class="glyphicon glyphicon-zoom-in"></span></span> \n\
                                <span class="iconOnPrew iconOnPrewImgTrash"><span class="glyphicon glyphicon-trash"></span></span> \n\
                                <img class="img-responsive showPreview" src="/upload/' + domainId + '/thumb_' + responseImg[i].fileName + '" \n\
                                data-img="/upload/' + domainId + '/' + responseImg[i].fileName + '" \n\
                                 data-id="' + responseImg[i].imageId + '">\n\
                                </div>');
                    setTimeout(function () {
                        $('.modalWithBgImg').mCustomScrollbar('update');
                    }, 10);
                }
            }
        });

        bodySelector.on('click', '#menu-img-save', function () {
            myDropzone.processQueue();
            myDropzone.on("complete", function (file) {
                myDropzone.removeFile(file);

            });
            responseDuplicate = false;
        });
        myDropzone.autoDiscover = false;
    };
};
bodySelector.on('click', '#uploadImg, #uploadImgFloat', function () {
    $('#menu-panel-img').fadeIn(200);
    $('#previewPopUp').fadeOut(200);
    $("#imgRemowe").addClass("disabled");
});
bodySelector.on('click', '#menu-img-close', '#menu-img-save', function () {
    $('#menu-panel-img').fadeOut(200);
});
bodySelector.on("click", ".iconOnPrewImgTrash", function () {
    $('.preview').attr('id', '');
    $(this).parent(".preview").attr('id', 'activePrewiewImage');
    galleryManager.removeImageIn()
});
//Img preview in edit templates
bodySelector.on('click', '.iconOnPrewImgZoom', function () {
    //galleryManager.requestRemoveImage();
    var selectedImgForPrew = $(this).parent().find("img");
    $("#menu-panel-img").fadeOut(200);
    //$('#imgPopup').fadeIn(200);
    $('.preview').attr('id', '');
    selectedImgForPrew.parent().attr('id', 'activePrewiewImage');
    //$("#imgRemowe").removeClass("disabled");
    var imgSrcForPreview = selectedImgForPrew.attr("data-img");
    var imgAltForPreview = selectedImgForPrew.attr("alt");
    var imgTitileForPreview = selectedImgForPrew.attr("title");
    $('#imgPrevId').attr('src', imgSrcForPreview);
    $('#imgPrevId').attr('alt', imgAltForPreview);
    $('#imgPrevId').attr('title', imgTitileForPreview);
    $(".imgTitileForPreview").attr('value', imgAltForPreview);
    // We get the size of a picture and write in preview
    var imgHeightForPreview = $('#imgPrevId').height();
    var imgWidthForPreview = $('#imgPrevId').width();
    $("#imgWidthForPreview").text(imgWidthForPreview);
    var src = imgSrcForPreview,
        imgq = new Image();
    imgq.src = src;
    imgq.onload = function () {
        $("#imgWidthForPreview").text(this.naturalWidth);
        $("#imgHeightForPreview").text(this.naturalHeight);
    };
});

var changeIdFr = function () {
    // set or change id for Column
    fr.find('.column').on('click', function () {
        $("div.anker-page input#anker-id").css({
            border: "none"
        });
        var self = $(this);
        var idColumn = self.attr('id');
        var inputWithId = $("div.anker-page input#anker-id");
        inputWithId.val(idColumn);
        inputWithId.blur(function () {
            var idAnker = $(this).val();
            var regularId = /(^[0-9]|(#))/i;
            //var regularIdHash = /(#)/i;
            if (regularId.test(idAnker)) {
                $("div.anker-page input#anker-id").css({
                    borderColor: "#f00",
                    borderWidth: "1px",
                    borderStyle: "solid"
                });
                setTimeout(function () {
                    $("div.anker-page input#anker-id").unbind('blur');
                }, 500)
            } else {
                $("div.anker-page input#anker-id").css({
                    borderColor: "#5cb85c",
                    borderWidth: "1px",
                    borderStyle: "solid"
                });
                self.attr('id', idAnker);
                setTimeout(function () {
                    $("div.anker-page input#anker-id").unbind('blur');
                }, 500)
            }
        });
    });

};
bodySelector.on('click', '.iconOnPrewImgZoom', function () {
    $('#previewPopUp').fadeIn(200);
});
bodySelector.on('click', '.imgPopupClose, #item-editor-tab a', function () {
    $('#imgPopup').fadeOut(200);
    $('#previewPopUp').fadeOut(200);
    $("#imgRemowe").addClass("disabled");
    $('#imgPrevId').attr('src', '');
    $('#imgPrevId').attr('alt', '');
    $('#imgPrevId').attr('title', '');
    $('.preview').attr('id', '');
});
bodySelector.on('click', '#previewClose', function () {
    $('#previewPopUp').fadeOut(200);
    $("#imgRemowe").addClass("disabled");
    $('#activePrewiewImage').attr('id', '')
});
bodySelector.on("click", ".buttonForGallery", function () {
    $("#item-editor-tab li a[href='#item-editor-tab-s4']").click()
});

$(".device-type .mobileportrait").on("click", function () {
    $(".device-type .desktop").removeClass("active");
    $(".device-type .tabletportrait").removeClass("active");
    $("#template-content").css({
        width: "324px"
        // minWidth: "324px",
        // border: "2px solid"
    });
    $(this).addClass("active");
    panels.changeHeightIframe();

});
$(".device-type .tabletportrait").on("click", function () {
    $(".device-type .mobileportrait").removeClass("active");
    $(".device-type .desktop").removeClass("active");
    $("#template-content").css({
        width: "772px"
        //minWidth: "772px",
        //border: "2px solid"
    });
    $(this).addClass("active");
    panels.changeHeightIframe();
});
$(".device-type .desktop").on("click", function () {
    $(".device-type .mobileportrait").removeClass("active");
    $(".device-type .tabletportrait").removeClass("active");
    $("#template-content").css({
        width: "100%"
        // minWidth: "1280px",
        //border: "none"
    });
    $(this).addClass("active");
    panels.changeHeightIframe();
});


$("#imgRemowe").on("click", function () {
    galleryManager.removeImageIn()
});


//menu item
var menuTool = new function () {
    var self = this;
    this.listMenu = {};
    this.modalId = '';
    //current menu id
    this.menuGroupId;
    //current item id in menu
    this.currentMenuId;
    this.init = function (data) {
        self.listMenu[data.id_menu] = data.data_menu.menu_items;
        //self.reloadNavBar(data.html.id_menu);
    };
    this.settings = function (item) {
        var idMenu = item.find('.menuJS').attr('id');
        if (idMenu) {
            self.viewListMenu(idMenu);
            self.menuGroupId = idMenu;
        }
        //self.reloadNavBar();
    };
    //reload html in current menu element
    this.reloadNavBar = function (idMenu) {
        if (!idMenu) {
            idMenu = self.menuGroupId;
        }
        var html = '';
        var countItemsInMenu = Object.keys(self.listMenu[idMenu]).length;
        if (countItemsInMenu > 0) {
            for (var k in self.listMenu[idMenu]) {
                var data = self.listMenu[idMenu][k];
                if (data.menu_display !== 0) {
                    if (data.children && Object.keys(data.children).length > 0) {
                        html += '<li class="dropdown">' +
                            '<a data-toggle="dropdown" class="dropdown-toggle"><span class="' + data.menu_image_url + '"></span>' + data.menu_name +
                            '<b class="caret"></b>' +
                            '</a>' +
                            '<ul class="dropdown-menu editorUL">';
                        for (var kk in self.listMenu[idMenu][k].children) {
                            var dataC = self.listMenu[idMenu][k].children[kk];
                            if (dataC.menu_display !== 0) {
                                var attrHref = 'data-id="' + dataC.menu_page_id + '"';
                                var attrTarget = 'target="_blank"';
                                if (dataC.menu_link_type === 'url') {
                                    attrHref = 'data-href="' + dataC.menu_url + '"';
                                }
                                if (dataC.menu_target === 0) {
                                    attrTarget = '';
                                }
                                html += '<li><a ' + attrHref + ' ' + attrTarget + '><span class="' + data.menu_image_url + '"></span> ' + dataC.menu_name + '</a></li>';
                            }
                        }
                        html += '</ul></li>';
                    } else {
                        var attrHref = 'data-id="' + data.menu_page_id + '"';
                        var attrTarget = 'target="_blank"';
                        if (data.menu_link_type === 'url') {
                            attrHref = 'data-href="' + data.menu_url + '"';
                        }
                        if (data.menu_target === 0) {
                            attrTarget = '';
                        }
                        html += '<li><a ' + attrHref + ' ' + attrTarget + '><span class="' + data.menu_image_url + '"></span> ' + data.menu_name + '</a></li>';
                    }
                }
            }
        }
        fr.find('.menuJS[id=' + idMenu + ']').find('ul.nav').html(html);
    };
    //open menu item list on panel
    this.viewListMenu = function (idMenu) {
        var menuHtml = '';
        var countItemsInMenu = Object.keys(self.listMenu[idMenu]).length;
        //view list item menu
        if (countItemsInMenu > 0) {
            for (var k in self.listMenu[idMenu]) {
                var menuData = self.listMenu[idMenu][k];
                var buttonCollapse = '';
                var menuHtmlC = '';
                if (menuData.children) {
                    var lC = Object.keys(menuData.children).length;
                    if (lC > 0) {
                        buttonCollapse = '<button data-action="collapse" type="button">Collapse</button>' +
                            '<button data-action="expand" type="button" style="display: none;">Expand</button>';
                        menuHtmlC = '<ol class="dd-list">';
                        for (var kk in menuData.children) {
                            var menuDataC = menuData.children[kk];
                            menuHtmlC += '<li class="dd-item dd3-item" data-id="' + kk + '" id="menuItem' + kk + '" data-id-p="' + k + '">' +
                                '<div class="dd-handle dd3-handle">' +
                                'Drag' +
                                '</div>' +
                                '<div class="dd3-content">' +
                                '<span class="menu-name"> ' + menuDataC.menu_name + ' </span>' +
                                '<span class="pull-right"> ' +
                                '<span>' +
                                '<a class="deleteMenu" rel="tooltip" title="" data-placement="bottom">' +
                                '<i class="glyphicon glyphicon-trash page-remove"></i></a>' +
                                '</span>' +
                                '</span>' +
                                '<span id="iconMenuItems" class="iconForMenuList ' + menuData.menu_image_url + '"></span>' +
                                '</div>' +
                                '</li>';
                        }
                        menuHtmlC += '</ol>';
                    }
                }
                menuHtml += '<li class="dd-item dd3-item" data-id="' + k + '" id="menuItem' + k + '">' +
                    buttonCollapse +
                    '<div class="dd-handle dd3-handle">' +
                    'Drag' +
                    '</div>' +
                    '<div class="dd3-content">' +
                    '<span class="menu-name"> ' + menuData.menu_name + ' </span>' +
                    '<span class="pull-right"> ' +
                    '<span>' +
                    '<a class="deleteMenu" rel="tooltip" title="" data-placement="bottom">' +
                    '<i class="glyphicon glyphicon-trash page-remove"></i></a>' +
                    '</span>' +
                    '</span>' +
                    '<span id="iconMenuItems" class="iconForMenuList ' + menuData.menu_image_url + '"></span>' +
                    '</div>' +
                    menuHtmlC +
                    '</li>';
            }
        }
        $('#nestable-menu ol').html(menuHtml);
    };
    //get menu item data
    this.update = function () {
        self.typeActionMenu = 'edit';
        var menuData = self.listMenu[self.menuGroupId][self.currentMenuId];
        if (!menuData) {
            var idParent = $('.dd3-item[data-id=' + self.currentMenuId + ']').parents('.dd3-item').attr('data-id');
            menuData = self.listMenu[self.menuGroupId][idParent].children[self.currentMenuId];
        }
        var panel = $('#menu-panel-np');
        var listPages = $('#pages-panel .panel-body nav li');
        $('select.linkToSitePage').html('');
        $('select.linkToSitePage').append('<option value="0" selected="selected">Choose Page</option>');
        $.each(listPages, function () {
            $('select.linkToSitePage').append('<option value="' + $(this).attr('data-id') + '">' + $(this).find('a').html() + '</option>');
        });
        self.resetPanel();
        $('#menu-panel-np .panel-name').html('Edit Menu Item');
        panel.find('input[name="menuName"]').val(menuData.menu_name);
        panel.find('select[name="menuTypeMenu"]').val(menuData.menu_menu_type).change();
        panel.find('input[name="menuLinkUrl"]').val(menuData.menu_url);
        if (menuData.menu_link_type === 'page') {
            panel.find('input[value=url]').removeAttr('checked');
            panel.find('input[value=page]').attr('checked', 'checked');
            panel.find('select[name=pageList]').val(menuData.menu_page_id).change();
        } else {
            panel.find('input[value=url]').attr('checked', 'checked');
            panel.find('input[value=page]').removeAttr('checked');
        }
        if (menuData.menu_image_url !== '') {
            panel.find('.insertImgInMenu').html('<span class="' + menuData.menu_image_url + '"></span>');
        } else {
            panel.find('.insertImgInMenu').html('Click to select picture');
        }

        panel.find('.menu-' + menuData.menu_image_position + '-image').addClass('activeImgInsertInList');
        panel.find('input#separateWindow').attr('checked', (menuData.menu_target === 1) ? 'checked' : false);
        panel.find('input#dontShowInMenu').attr('checked', (menuData.menu_display === 0) ? 'checked' : false);
        panel.show();
    };
    //remove item
    this.removeMenuItem = function () {
        if (self.listMenu[self.menuGroupId][self.currentMenuId]) {
            delete self.listMenu[self.menuGroupId][self.currentMenuId];
        } else {
            var idParent = $('.dd3-item[data-id=' + self.currentMenuId + ']').parents('.dd3-item').attr('data-id');
            delete self.listMenu[self.menuGroupId][idParent].children[self.currentMenuId];
        }
        self.reloadNavBar();
        $('#menu-panel-np').hide();
        $('#nestable-menu li[data-id=' + self.currentMenuId + ']').remove();
        $('#modalRemoveMenu').modal('hide');

        panels.changeContent();
    };
    //save all items
    this.save = function () {
        panels.changeContent();
        var panel = $('#menu-panel-np');
        var newMenuData = {
            'menu_name': panel.find('input[name=menuName]').val(),
            'menu_menu_type': panel.find('select[name="menuTypeMenu"]').val(),
            'menu_link_type': panel.find('input[name=menuLinkType]:checked').val(),
            'menu_page_id': panel.find('select[name=pageList]').val(),
            'menu_url': panel.find('input[name=menuLinkUrl]').val(),
            'menu_display': panel.find('input#dontShowInMenu').attr('checked') ? 0 : 1,
            'menu_image_position': (panel.find('.activeImgInsertInList')) ? panel.find('.activeImgInsertInList').attr('data-position') : '',
            'menu_image_url': (panel.find('.insertImgInMenu span').attr('class') ? panel.find('.insertImgInMenu span').attr('class') : ''),
            'menu_target': panel.find('input#separateWindow').attr('checked') ? 1 : 0,
            'children': undefined
        };
        if (panel.find('input[name=menuName]').val() !== '' && panel.find('select[name=menuTypeMenu]').val() !== 0) {
            if (self.typeActionMenu === 'add') {
                var l = Object.keys(self.listMenu[self.menuGroupId]).length;
                var ll = 0;
                for (var k in self.listMenu[self.menuGroupId]) {
                    if (self.listMenu[self.menuGroupId][k].children) {
                        ll += Object.keys(self.listMenu[self.menuGroupId][k].children).length;
                    }
                }
                var length = l + ll;
                self.listMenu[self.menuGroupId]['it' + length] = newMenuData;
            } else {
                if (self.listMenu[self.menuGroupId][self.currentMenuId]) {
                    if (self.listMenu[self.menuGroupId][self.currentMenuId].children) {
                        newMenuData.children = self.listMenu[self.menuGroupId][self.currentMenuId].children;
                    }
                    self.listMenu[self.menuGroupId][self.currentMenuId] = newMenuData;
                } else {
                    var idParent = $('.dd3-item[data-id=' + self.currentMenuId + ']').parents('.dd3-item').attr('data-id');
                    self.listMenu[self.menuGroupId][idParent].children[self.currentMenuId] = newMenuData;
                }
            }
            self.viewListMenu(self.menuGroupId);
            self.reloadNavBar();
            $('#menu-panel-np').hide();
        } else {
            alert('fields "Item Name" and "Type Of Menu" is empty!');
        }
    };
    //reset all fields in menu panel
    this.resetPanel = function () {
        var panel = $('#menu-panel-np');
        panel.find('input[name=menuName]').val('');
        panel.find('select[name=menuTypeMenu]').val('0').change();
        panel.find('input[name=menuLinkUrl]').val('');
        panel.find('select[name=pageList]').val(0).change();
        panel.find('.activeImgInsertInList').removeClass('activeImgInsertInList');
        panel.find('input#separateWindow').removeAttr('checked');
        panel.find('input#dontShowInMenu').removeAttr('checked');
        panel.find('input[value=url]').attr('checked', 'checked');
        panel.find('input[value=page]').removeAttr('checked');
        panel.find(".insertImgInMenu").html("Click to select picture");
    };

    //events in panel
    this.panel = function () {
        // activate Nestable for list 1
        $('#nestable-menu').nestable({
            maxDepth: 2
        });
        bodySelector.on('moveend', '.dd-handle.dd3-handle', function (e) {
                panels.changeContent();
                var eP = $('#nestable-menu');
                var e = eP.nestable('serialize');
                var newE = {};
                var l = e.length;
                for (var i = 0; i < l; i++) {
                    var child = {};
                    if (e[i].children) {
                        var lc = e[i].children.length;
                        for (var j = 0; j < lc; j++) {
                            if (self.listMenu[self.menuGroupId][e[i].children[j].id]) {
                                child[e[i].children[j].id] = self.listMenu[self.menuGroupId][e[i].children[j].id];
                                $('.dd3-item[data-id=' + e[i].children[j].id + ']').attr('data-id-p', e[i].id);
                            } else {
                                if (self.listMenu[self.menuGroupId][e[i].id].children[e[i].children[j].id]) {
                                    child[e[i].children[j].id] = self.listMenu[self.menuGroupId][e[i].id].children[e[i].children[j].id];
                                    $('.dd3-item[data-id=' + e[i].children[j].id + ']').attr('data-id-p', e[i].id);
                                } else {
                                    for (var k in self.listMenu[self.menuGroupId]) {
                                        for (var kk in self.listMenu[self.menuGroupId][k].children) {
                                            if (e[i].children[j].id === kk) {
                                                child[e[i].children[j].id] = self.listMenu[self.menuGroupId][k].children[e[i].children[j].id];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    var tmp;
                    if (self.listMenu[self.menuGroupId][e[i].id]) {
                        tmp = self.listMenu[self.menuGroupId][e[i].id];
                    } else {
                        for (var k in self.listMenu[self.menuGroupId]) {
                            for (var kk in self.listMenu[self.menuGroupId][k].children) {
                                if (e[i].id === kk) {
                                    tmp = self.listMenu[self.menuGroupId][k].children[e[i].id];
                                }
                            }
                        }

                    }
                    var newMenuData = {
                        'menu_name': tmp.menu_name,
                        'menu_menu_type': tmp.menu_menu_type,
                        'menu_link_type': tmp.menu_link_type,
                        'menu_page_id': tmp.menu_page_id,
                        'menu_url': tmp.menu_url,
                        'menu_display': tmp.menu_display,
                        'menu_image_position': tmp.menu_image_position,
                        'menu_image_url': tmp.menu_image_url,
                        'menu_target': tmp.menu_target,
                        'children': child
                    };
                    newE[e[i].id] = newMenuData;
                }
                delete self.listMenu[self.menuGroupId];
                self.listMenu[self.menuGroupId] = newE;
                self.reloadNavBar();
            }
        );
        // change menu item
        $('#nestable-menu').on('click', '.menu-name', function () {
            $('.dd3-content').removeClass('activeImgInsertInList');
            $(this).parents('.dd3-content').addClass('activeImgInsertInList');
            var menuId = $(this).parents('.dd3-item').attr('data-id');
            self.currentMenuId = menuId;
            self.update();
        });
        //remove menu item
        $('#nestable-menu').on('click', '.deleteMenu', function () {
            var idMenuItem = $(this).parents('.dd3-item').attr('data-id');
            //set menu id for remove
            self.currentMenuId = idMenuItem;
            $('#modalRemoveMenu').modal();
            //set name menu item on modal
            var nameMenu = $(this).parents('.dd3-content').find('.menu-name').html();
            $('#modalRemoveMenu .modal-body strong').html(nameMenu);
        });
        //confirm remove menu item
        bodySelector.on('click', '#removeMenuButton', function () {
            self.removeMenuItem();
        });
        //open menu panel for add new item
        $('#menu-add').click(function () {
            self.typeActionMenu = 'add';
            var panel = $('#menu-panel-np');
            panel.hide();
            $('#menu-panel-np .panel-name').html('Add New Menu Item');
            self.resetPanel();
            $('div.dd3-content').removeClass('activeImgInsertInList');
            panel.show();
        });
        // select  Choose Link Type
        $('select[name=menuTypeMenu]').change(function () {
            if ($(this).val() === 'content') {
                $('#contenttab').show();
                $('#linktab').hide();
            } else if ($(this).val() === 'link') {
                $('#linktab').show();
                $('#contenttab').hide();
            } else {
                $('#linktab').hide();
                $('#contenttab').hide();
            }
        });
        //change image position for link
        $('.menu-bottom-image, .menu-top-image, .menu-left-image, .menu-right-image').click(function () {
            $('.menu-bottom-image, .menu-top-image, .menu-left-image, .menu-right-image').not(this).removeClass('activeImgInsertInList');
            $(this).toggleClass('activeImgInsertInList');
        });
        // Choose icon menu
        bodySelector.on("click", ".insertImgInMenu", function () {
            $("#choseIconForMenu").fadeIn(200);
        });
        bodySelector.on("click", "#resetIconMenu", function () {
            $(".insertImgInMenu").html("Click to select picture");
        });
        bodySelector.on("click", "#menuIconClose", function () {
            $("#choseIconForMenu").fadeOut(200);
        });
        bodySelector.on("click", ".listIcon li", function () {
            var changeIconMenu = $(this).html()
            $(".insertImgInMenu").html(changeIconMenu);
            var iconInCont = $(".insertImgInMenu").html();
        });
        $('.menu-right-image[data-position="right"]').click(function () {
            $(".insertImgInMenu span").removeClass("iconFloatLeft");
            $(".insertImgInMenu span").removeClass("iconFloatTop");
            $(".insertImgInMenu span").removeClass("iconFloatBottom");
            $(".insertImgInMenu span").addClass("iconFloatRight");
        });
        $('.menu-left-image[data-position="left"]').click(function () {
            $(".insertImgInMenu span").removeClass("iconFloatRight");
            $(".insertImgInMenu span").removeClass("iconFloatTop");
            $(".insertImgInMenu span").removeClass("iconFloatBottom");
            $(".insertImgInMenu span").addClass("iconFloatLeft");
        });
        $('.menu-top-image[data-position="top"]').click(function () {
            $(".insertImgInMenu span").removeClass("iconFloatLeft");
            $(".insertImgInMenu span").removeClass("iconFloatRight");
            $(".insertImgInMenu span").removeClass("iconFloatBottom");
            $(".insertImgInMenu span").addClass("iconFloatTop");
        });
        $('.menu-bottom-image[data-position="bottom"]').click(function () {
            $(".insertImgInMenu span").removeClass("iconFloatLeft");
            $(".insertImgInMenu span").removeClass("iconFloatRight");
            $(".insertImgInMenu span").removeClass("iconFloatTop");
            $(".insertImgInMenu span").addClass("iconFloatBottom");
        });
        //save menu changes or new menu item
        $('#menu-np-save').click(function () {
            $("#choseIconForMenu").fadeOut(200);
            self.save();
        });
        //close panel menu
        $('#menu-np-close').click(function () {
            $('#menu-panel-np').fadeOut(200);
            $("#choseIconForMenu").fadeOut(200);
        });
    };
};

//map item
var mapTool = new function () {
    var self = this;
    this.listMaps = {};
    this.mapId;
    this.init = function (data) {
        var el = fr.find('#' + data.id);
        var address = data.options.address;
        var latitude = data.options.lat;
        var longitude = data.options.lng;
        //init map
        var myLatLng = this.geo(latitude, longitude, address, function (center, data, callback) {
            var mapOptions = {
                zoom: data.options.zoom,
                center: center,
                panControl: data.options.panControl,
                scaleControl: data.options.scaleControl,
                streetViewControl: data.options.streetViewControl,
                overviewMapControl: data.options.overviewMapControl,
                mapTypeControl: data.options.mapTypeControl,
                zoomControl: data.options.zoomControl
            };
            //set map data
            self.listMaps[data.id] = {
                'options': mapOptions,
                'id': data.id,
                'title': data.title
            };
            self.listMaps[data.id]['options']['lat'] = data.options.lat;
            self.listMaps[data.id]['options']['lng'] = data.options.lng;
            self.listMaps[data.id]['options']['address'] = data.options.address;
            //create map
            var maps = new google.maps.Map(fr.find('#' + data.id)[0], mapOptions);
            google.maps.event.addDomListener(window, "resize", function () {
                var center = maps.getCenter();
                google.maps.event.trigger(maps, "resize");
                maps.setCenter(center);
            });
            parent.p.on("resize", function () {
                var center = maps.getCenter();
                google.maps.event.trigger(maps, "resize");
                maps.setCenter(center);
            });
            //create marker on map
            var marker = new google.maps.Marker({
                position: center,
                map: maps,
                title: data.title
            });

            callback(el);
        }, data);
    };
    this.over = function (el) {
        setTimeout(function () {
            el.append('<div class="item-map-over item-over"></div>');
        }, 200);
    };
    //get map center by address or lat-lng
    this.geo = function (lat, lng, address, callback, data) {
        var myLatLng = new google.maps.LatLng(lat, lng);
        if (myLatLng.lat && myLatLng.lng) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': address}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    myLatLng = new google.maps.LatLng(latitude, longitude);
                    callback(myLatLng, data, self.over);
                }
            });
        } else {
            callback(myLatLng, data, self.over);
        }
    };
    this.update = function () {
        panels.changeContent();
        var dataMap = self.listMaps[this.mapId];
        var data = {
            id: this.mapId,
            title: $('#mapTitle').val(),
            options: {
                center: '',
                lat: $('#mapLocationsLat').val(),
                lng: $('#mapLocationsLng').val(),
                address: $('#mapAddress').val(),
                zoom: Number($('#mapOptionsZoom').val()),
                panControl: ($('#mapPanControl:checked').length !== 0) ? true : false,
                scaleControl: ($('#mapScaleControl:checked').length !== 0) ? true : false,
                streetViewControl: ($('#mapStreetViewControl:checked').length !== 0) ? true : false,
                overviewMapControl: ($('#mapOverviewMapControl:checked').length !== 0) ? true : false,
                mapTypeControl: ($('#mapMapTypeControl:checked').length !== 0) ? true : false,
                zoomControl: ($('#mapZoomControl:checked').length !== 0) ? true : false
            }
        };
        this.init(data);
    };
    this.settings = function (el) {
        this.mapId = el.attr('id');
        var dataMap = self.listMaps[this.mapId];
        var mOpt = dataMap.options;
        $('#mapTitle').val(dataMap.title);
        $('#mapAddress').val(mOpt.address);
        $('#mapLocationsLat').val(mOpt.lat);
        $('#mapLocationsLng').val(mOpt.lng);
        $('#mapOptionsZoom').val(mOpt.zoom);
        $('#mapOptions input[type=checkbox]').removeAttr('checked');
        if (mOpt.panControl) {
            $('#mapPanControl').prop('checked', true);
        }
        if (mOpt.scaleControl) {
            $('#mapScaleControl').prop('checked', true);
        }
        if (mOpt.streetViewControl) {
            $('#mapStreetViewControl').prop('checked', true);
        }
        if (mOpt.overviewMapControl) {
            $('#mapOverviewMapControl').prop('checked', true);
        }
        if (mOpt.mapTypeControl) {
            $('#mapMapTypeControl').prop('checked', true);
        }
        if (mOpt.zoomControl) {
            $('#mapZoomControl').prop('checked', true);
        }
        $('.tool-map').unbind();
        $('.tool-map').on('change', function () {
            self.update();
        });
    };
};
//youtube item
var youtubeTool = new function () {
    var self = this;
    this.modalId = '#youtubeSettings';
    this.el;
    //get video id
    this.parseURL = function (url) {
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        if (match && match[7].length == 11) {
            return match[7];
        } else {
            return false;
        }
    };
    this.settings = function (el) {
        this.el = el;
        $('.tool-youtube').unbind();
        $('.tool-youtube').on('change', function () {
            self.update();
        });
    };
    this.update = function () {
        var url = $('#youtubeSettings').val();
        if (url !== '') {
            var videoId = this.parseURL(url);
            if (videoId) {
                var html = '<iframe src="https://www.youtube.com/embed/' + videoId + '" frameborder="0" allowfullscreen></iframe><div class="item-youtube-over item-over"></div>';
                this.el.html(html);
                panels.changeContent();
            } else {
                alert('wrong url!');
            }
        } else {
            alert('wrong url!');
        }
    };
};
//features item
var featuresTool = new function () {
    var self = this;
    this.el;
    this.settings = function (el) {
        this.el = el.find('.item-image');
        $('.tool-grid-options').unbind();
        $('.tool-grid-options').on('change', function () {
            self.update();
        });
    };

};
$(document).ready(function () {
    p = $(this);
    function callIframe(callback) {
        $('#editor-iframe').attr('src', '/themes/grid.html');
        $('#editor-iframe').load(function () {
            pages.init();
            callback(this);
        });
    }

    callIframe(iframeInit);
    function iframeInit(d) {
        fr = $('#editor-iframe').contents();
        menuTool.panel();
        tools.run();
        panels.run();
        grid.run();
        //panels.editor();
        galleryManager.on();
        galleryManager.addImgInEditRow();
        // panels.borderEditor();
        changeIdFr();
        setTimeout(function () {
            panels.changeHeightIframe();
            grid.initElements();
            gridContent.tinyToolbar();
            var iframeHeigh = fr.height();
            var iframeWidth = fr.width();
            $('#iframeSizeWidth').text(iframeWidth + 'px');
            $('#iframeSizeHeight').text(iframeHeigh + 'px');
        }, 200)
    }
});