function formFieldsSettings()
{
	$('#surveyForm').addClass("editFrmTitle");
	var allProps = document.getElementById('allProps');
	allProps.innerHTML = '';
	currently_editing = "";
	
	var li;
	var i;
	var html = "";
	
	// Field Setting - Label
	li = document.createElement('li');
	li.id = "listForm";
	li.style.display = "block";
	html = "";
	html += '<label class="desc">Form Name</label>';
	html += '<div>';
	html += '<textarea onblur=\"updateForm(this.value, \'Name\');\" onkeyup=\"updateForm(this.value, \'Name\');\" id=\"formName\" cols=\"50\" rows=\"2\" class=\"textarea\" style=\"height: 2em;\"></textarea>';
	html += '</div>';
	li.innerHTML = html;
	allProps.appendChild(li);
	// End Field Setting - Label
	document.getElementById('formName').innerHTML = document.getElementById("fname").innerHTML;
	
	
	li = document.createElement('li');
	li.id = "listDescription";
	li.style.display = "block";
	html = "";
	html += '<label class="desc">Description</label>';
	html += '<div>';
	html += '<textarea onblur=\"updateForm(this.value, \'Description\');\" onkeyup=\"updateForm(this.value, \'Description\');\" id=\"formDescription\" cols=\"50\" rows=\"10\" class=\"textarea\"></textarea>';
	html += '</div>';
	li.innerHTML = html;
	allProps.appendChild(li);
	document.getElementById('formDescription').innerHTML = replaceNewline(document.getElementById("fdescription").innerHTML, "display");
}



//filter the field setting options
function configureFieldSettingsOnLeftPanel()
{    
	var allProps = document.getElementById('allProps');
	allProps.innerHTML = '';;
	var cur = fieldInFields(currently_editing);
	var loadDefault = false;
	if(cur == -1)
	{
		currently_editing = "";
		return;
	}
	var field = fields[cur];
        console.log(field.type)
	var li;
	var i;
	var html = "";
	
	// Field Setting - Label
	li = document.createElement('li');
	li.id = "listTitle";
	li.style.display = "block";
	html = "";
	// Modify By Wood
	// Draw the plain text setting
	if(field.type == "plaintext")
	{
		html += '<label class="desc">Content</label>';
		html += '<div>';
		//html += '<textarea onmouseup="updateProperties(this.value, \'PlainText\')" onkeyup="updateProperties(this.value, \'PlainText\')" cols="35" rows="10" class="textarea" style="height: 100px;" id="fieldTitle">' + replaceHTML(replaceNewline(field.label, "display"), "display") + '</textarea>';
		html += '<textarea onmouseup="updateProperties(this.value, \'PlainText\')" onkeyup="updateProperties(this.value, \'PlainText\')" cols="35" rows="10" class="textarea" style="height: 100px" id="fieldTitle">' + replaceNewline(field.label, "display") + '</textarea>';
		html += '</div>';
	}
	else
	{
		html += '<label class="desc">Field Label</label>';
		html += '<div>';
		html += '<textarea onmouseup="updateProperties(this.value, \'Title\')" onkeyup="updateProperties(this.value, \'Title\')" cols="35" rows="10" class="textarea" id="fieldTitle">' + replaceNewline(field.label, "display") + '</textarea>';
		html += '</div>';
	}
	// html += '<label class="desc">Field Label</label>';
	// html += '<div>';
	// html += '<textarea onmouseup="updateProperties(this.value, \'Title\')" onkeyup="updateProperties(this.value, \'Title\')" cols="35" rows="10" class="textarea" id="fieldTitle">' + replaceNewline(field.label, "display") + '</textarea>';
	// html += '</div>';
	// End Modify By Wood

	// add by David NG - length check - 2011-0523 - start
	if(field.type == "text" || field.type == "textarea" || field.type == "number" || field.type == "phone"|| field.type == "hiddentext" )
	{
		if( field.length+'' == 'undefined' || field.length == '0')
			field.length = '';
		html += '<label class="desc">Field Length</label>';
		html += '<div>';
		//html += '<select id="fieldLengthType" name="length_type" class="select"> <option value="="> Equal To </option> <option value="min"> Minimum Length </option> <option value="max"> Maximum Length </option> <option value="bet"> Between (separate by |   eg:  5|7 means betwen 5 and 7 </option> </select>';
		html += '<input type="text" id="fieldLength" name="length" maxlength="10" class="text large" onmouseup="updateProperties(this.value, \'Length\',this)" onkeyup="updateProperties(this.value, \'Length\',this)" value="'+ (field.length) +'">';
		html += '</div>';
	}
	// add by David NG - length check - 2011-0523 - end
	
	li.innerHTML = html;
	allProps.appendChild(li);
	// End Field Setting - Label
	
	
	
	// Field Setting - Field Size
//	if(field.type == "text" || field.type == "textarea" || field.type == "number" || field.type == "email" || field.type == "url")
//	{
//		var size_arr = new Array("Small", "Medium", "Large");
//		li = document.createElement('li');
//		li.id = "listSize";
//		li.className = "half";
//		li.style.display = "block";
//		html = "";
//		html += '<label class="desc">Field Size</label>';
//		html += '<select onchange="updateProperties(this.value, \'Size\')" autocomplete="off" id="fieldSize" class="select full">';
//		for(var i=0; i<size_arr.length; i++)
//		{
//			html += '  <option value="' + size_arr[i].toLowerCase() + '"';
//			if(size_arr[i].toLowerCase() == field.size)
//			{
//				html += ' selected="selected"';
//			}
//			html += '>&nbsp;' + size_arr[i] + '</option>';
//		}
//		html += '</select>';
//		li.innerHTML = html;
//		allProps.appendChild(li);
//	}
	// End Field Setting - Field Size
	
	// Field Setting - Date
	if(field.type == 'date')
	{
		li = document.createElement('li');
		li.id = "listDateFormat";
		li.className = "right half";
		li.style.display = "block";
		html = "";
		html += '<label class="desc">Date Format</label>';
		html += '<select onchange="changeDateFieldType(this.value);" autocomplete="off" id="fieldSize" class="select full">';
		if(field.date_format == 0)
		{
			html += '<option selected="selected" value="date" id="fieldDateAmerican">&nbsp;MM / DD / YYYY</option>';
			html += '<option value="eurodate" id="fieldDateEuro">&nbsp;DD / MM / YYYY</option>';
		}
		else
		{
			html += '<option value="date" id="fieldDateAmerican">&nbsp;MM / DD / YYYY</option>';
			html += '<option selected="selected" value="eurodate" id="fieldDateEuro">&nbsp;DD / MM / YYYY</option>';
		}
		html += '</select>';
		
		li.innerHTML = html;
		allProps.appendChild(li);
	}
	// End Field Setting - Date
	//Field Layout (onecolumn, twocolumn, etc)
	
	
	// Field Setting - Layout
	if(field.type == 'checkbox' || field.type == 'radio' || field.type == 'agerange')
	{
		var layout_arr = new Array("", "twoColumns", "threeColumns", "notStacked");
		var dislayout_arr = new Array("One Column", "Two Columns", "Three Columns", "Side by Side");
		li = document.createElement('li');
		li.id = "listLayout";
		li.className = "clear";
		li.style.display = "block";
		html = "";
		html += '<label class="desc">Field Layout</label>';
		html += '<select onchange="toggleChoicesLayout(this.value);" autocomplete="off" id="fieldLayout" class="select full">';
		for(var i=0; i<layout_arr.length; i++)
		{
			html += '  <option value="' + layout_arr[i] + '"';
			if((field.columns).toLowerCase() == layout_arr[i].toLowerCase())
			{
				html += ' selected="selected"';
			}
			html += '>&nbsp;' + dislayout_arr[i] + '</option>';
		}
		html += '</select>';
		li.innerHTML = html;
		allProps.appendChild(li);
	}
	// End Field Setting - Layout
	
	// Field Setting - Space
	li = document.createElement('li');
	li.id = "listDateFormat";
	li.className = "clear noheight";
	allProps.appendChild(li);
	// Field Setting - Space
	
	// Field Setting - choices for checkbox
	if(field.type == 'checkbox')
	{
		li = document.createElement('li');
		li.id = "listChoices";
		li.className = "clear";
		li.style.display = "block";
		html = "";
		html += '<fieldset class="choices">';
		html += '<legend>Choices</legend>';
		html += '<ul id="fieldChoices">';
		var otherIndex = 100;
		for(var j=0;j<field.choices.length;j++){
			if(field.choices[j].label.indexOf("##_##")!=-1){
				field.choices[j].id = otherIndex;
				otherIndex++;
			}
		}
		
		for(i=0; i<field.choices.length; i++)
		{
			if(field.choices[i].id<100){
				html += '<li>';
				html += '<input type="checkbox" name="allChoices" title="Make this choice pre-selected."';
				if(field.choices[i].selected)
				{
					html += ' checked="checked"';
				}
				html += ' class="" onclick="changeCheckboxDefault(\''+field.choices[i].id+'\')"> ';
				html += '<input type="text" id="xchoices' + i + '" onkeyup="updateChoice(\''+field.choices[i].id+'\', this.value);" value="" autocomplete="off" maxlength="150" class="text"> ';
				html += '<img title="Add another choice." alt="Add" onclick="addCheckbox(\''+field.choices[i].id+'\',this)" src="' + imgpath + '/img/template-editor/add.png">';
				if(i > 0)
				{
					html += '<img title="Delete this choice." alt="Delete" onclick="deleteCheckbox(\''+field.choices[i].id+'\',this)" src="' + imgpath + '/img/template-editor/delete.png">';
				}
				html += '</li>';
			}
		}
		
		//scott add for other Option
		var otherCount = 100;
		html += '<li>';
		html += '<input type="checkbox" name="allChoices" title="Make this choice pre-selected."';
		var otherDisabled = "disabled=\"disabled\"";
		var addVisibility = "style='visibility:hidden;' ";
		if((field.choices[field.choices.length-1].id>=100)||field.choices[field.choices.length-1].label.indexOf("##_##")!=-1)
		{
			var otherRequireFlag = field.choices[field.choices.length-1].otherRequired;
			html += ' checked="checked"';
			otherDisabled = "";
			addVisibility = "style=\"visibility:visible;\" ";
		}
		html += ' class="" onclick="editOtherCheckbox(\'99\',this)" id="xchoices99"> ';
		html += '<input type="text" id="xchoices100" onkeyup="updateChoice(\'100\', this.value);" value="Others:" autocomplete="off" maxlength="150" class="text" '+otherDisabled+'> ';
//		html += '<img title="Add another choice." '+addVisibility+' alt="Add" onclick="addOtherCheckbox(\'100\',this)" src="' + imgpath + '/img/template-editor/add.png">';
		html += '</li>';
		for(var i=0;i<field.choices.length;i++){
			if(field.choices[i].id>100){
				var tmpArr = new Array();
				tmpArr = field.choices[i].label.split("##_##");
				var otherText = tmpArr[0];
				html += '<li>';
				html += '<input type="checkbox" style="visibility:hidden;"> ';
				html += '<input type="text" id="xchoices'+field.choices[i].id+'" onkeyup="updateChoice(\''+field.choices[i].id+'\', this.value);" value="'+otherText+'" autocomplete="off" maxlength="150" class="text" > ';
//				html += '<img title="Add another choice."  alt="Add" onclick="addOtherCheckbox(\''+field.choices[i].id+'\',this)" src="' + imgpath + '/img/template-editor/add.png">';
				html += '<img title="Delete this choice." alt="Delete" onclick="deleteOtherCheckbox(\''+field.choices[i].id+'\',this)" src="' + imgpath + '/img/template-editor/delete.png">';
				html += '</li>';
			}
		}
		html += '</ul>';
		html += '</fieldset>';
		li.innerHTML = html;
		allProps.appendChild(li);
//		console.log(field);
		for(i=0; i<field.choices.length; i++)
		{
			if(field.choices[i].id<100){
				document.getElementById("xchoices" + i).value = field.choices[i].label;
			}
			else{
				var labelArr = new Array();
				labelArr = field.choices[i].label.split("##_##");
				document.getElementById("xchoices"+field.choices[i].id).value = labelArr[0];
			}
//			document.getElementById("xchoices" + i).value = field.choices[i].label;
		}
		
		// Field Setting - Space
		li = document.createElement('li');
		li.id = "listDateFormat";
		li.className = "clear noheight";
		allProps.appendChild(li);
		// Field Setting - Space
	}
	// End Field Setting -  choices for checkbox
	
	// Field Setting - choices for radio
	if(field.type == 'radio')
	{
		li = document.createElement('li');
		li.id = "listChoices";
		li.className = "clear";
		li.style.display = "block";
		html = "";
		html += '<fieldset class="choices">';
		html += '<legend>Choices</legend>';
		html += '<ul id="fieldChoices">';
		//scott add		
		if(field.choices[field.choices.length-1].label.indexOf("##_##")!=-1){
			field.choices[field.choices.length-1].id = 100;
		}
		for(i=0; i<field.choices.length; i++)
		{
			if(field.choices[i].id<100){
				html += '<li>';
				html += '<input type="radio" name="allChoices" title="Make this choice pre-selected."';
				if(field.choices[i].selected)
				{
					html += ' checked="checked"';
				}
				html += ' class="" onclick="changeRadioDefault(\''+field.choices[i].id+'\')"> ';
				html += '<input type="text" id="xradio' + i + '" onkeyup="updateChoice(\''+field.choices[i].id+'\', this.value);" value="" autocomplete="off" maxlength="150" class="text"> ';
				html += '<img title="Add another choice." alt="Add" onclick="addRadio(\''+field.choices[i].id+'\',this)" src="' + imgpath + '/img/template-editor/add.png"> ';
				if(i > 0)
				{
					html += '<img title="Delete this choice." alt="Delete" onclick="deleteRadio(\''+field.choices[i].id+'\',this)" src="' + imgpath + '/img/template-editor/delete.png">';
				}
				html += '</li>';
			}
		}
		//scott add for other Option
		var otherCount = 100;
		html += '<li>';
		html += '<input type="checkbox" name="allChoices" title="Make this choice pre-selected."';
		var otherDisabled = "disabled=\"disabled\"";
		var addVisibility = "style='visibility:hidden;' ";
		if((field.choices[field.choices.length-1].id==100)||field.choices[field.choices.length-1].label.indexOf("##_##")!=-1)
		{
			var otherRequireFlag = field.choices[field.choices.length-1].otherRequired;
			html += ' checked="checked"';
			otherDisabled = "";
			addVisibility = "style='visibility:visible;' ";
		}
		html += ' class="" onclick="editOtherRadio(\'99\',this)" id="xradio99"> ';
		html += '<input type="text" id="xradio100" onkeyup="updateChoice(\'100\', this.value);" value="Others:" autocomplete="off" maxlength="150" class="text" '+otherDisabled+'> ';
		html += '</li>';
		html += '</ul>';
		html += '</fieldset>';
		li.innerHTML = html;
		allProps.appendChild(li);
		if(field.choices[field.choices.length-1].id==100){
			for(i=0; i<(field.choices.length-1); i++)
			{
				document.getElementById("xradio" + i).value = field.choices[i].label;
			}

			var labelArr = new Array();
			labelArr = field.choices[field.choices.length-1].label.split("##_##");
			document.getElementById("xradio100").value = labelArr[0];
		}
		else{
			for(i=0; i<(field.choices.length); i++)
			{
				document.getElementById("xradio" + i).value = field.choices[i].label;
			}
		}
//		for(i=0; i<field.choices.length; i++)
//		{
//			document.getElementById("xradio" + i).value = field.choices[i].label;
//		}
		
		// Field Setting - Space
		li = document.createElement('li');
		li.id = "listDateFormat";
		li.className = "clear noheight";
		allProps.appendChild(li);
		// Field Setting - Space
	}
	if(field.type == 'agerange')
	{
		li = document.createElement('li');
		li.id = "listChoices";
		li.className = "clear";
		li.style.display = "block";
		html = "";
		html += '<fieldset class="choices">';
		html += '<legend>Choices</legend>';
		html += '<ul id="fieldChoices">';
		//scott add		
		if(field.choices[field.choices.length-1].label.indexOf("##_##")!=-1){
			field.choices[field.choices.length-1].id = 100;
		}
		for(i=0; i<field.choices.length; i++)
		{
			if(field.choices[i].id<100){
				html += '<li>';
				html += '<input type="radio" name="allChoices" title="Make this choice pre-selected."';
				if(field.choices[i].selected)
				{
					html += ' checked="checked"';
				}
				html += ' class="" onclick="changeRadioDefault(\''+field.choices[i].id+'\')"> ';
				html += '<input type="text" id="xradio' + i + '" onkeyup="updateChoice(\''+field.choices[i].id+'\', this.value);" value="" autocomplete="off" maxlength="150" class="text"> ';
				html += '<img title="Add another choice." alt="Add" onclick="addRadio(\''+field.choices[i].id+'\',this)" src="' + imgpath + '/img/template-editor/add.png"> ';
				if(i > 0)
				{
					html += '<img title="Delete this choice." alt="Delete" onclick="deleteRadio(\''+field.choices[i].id+'\',this)" src="' + imgpath + '/img/template-editor/delete.png">';
				}
				html += '</li>';
			}
		}
		//scott add for other Option
		var otherCount = 100;
		
		html += '</ul>';
		html += '</fieldset>';
		li.innerHTML = html;
		allProps.appendChild(li);
		for(i=0; i<field.choices.length; i++)
		{
			document.getElementById("xradio" + i).value = field.choices[i].label;
		}
		
		// Field Setting - Space
		li = document.createElement('li');
		li.id = "listDateFormat";
		li.className = "clear noheight";
		allProps.appendChild(li);
		// Field Setting - Space
	}
	// End Field Setting - choices for radio
	
	
	// Field Setting - choices for select
	if(field.type == 'select')
	{
		li = document.createElement('li');
		li.id = "listChoices";
		li.className = "clear";
		li.style.display = "block";
		html = "";
		html += '<fieldset class="choices">';
		html += '<legend>Choices</legend>';
		html += '<ul id="fieldChoices">';
		for(i=0; i<field.choices.length; i++)
		{
			html += '<li>';
			html += '<input type="radio" name="allChoices" title="Make this choice pre-selected."';
			if(field.choices[i].selected)
			{
				html += ' checked="checked"';
			}
			html += ' class="" onclick="changeSelectDefault(\''+field.choices[i].id+'\')"> ';
			html += '<input type="text" id="xselect' + i+ '" onkeyup="updateChoice(\''+field.choices[i].id+'\', this.value);" value="'+field.choices[i].label+'" autocomplete="off" maxlength="150" class="text"> ';
			if(i > 0)
			{
				html += '<img title="Add another choice." alt="Add" onclick="addSelect(\''+field.choices[i].id+'\',this)" src="' + imgpath + '/img/template-editor/add.png"> ';
				if(i > 1)
				{
					html += '<img title="Delete this choice." alt="Delete" onclick="deleteSelect(\''+field.choices[i].id+'\',this)" src="' + imgpath + '/img/template-editor/delete.png">';
				}
			}
			html += '</li>';
		}
		html += '</ul>';
		html += '</fieldset>';
		li.innerHTML = html;
		allProps.appendChild(li);
		for(i=0; i<field.choices.length; i++)
		{
			document.getElementById("xselect" + i).value = field.choices[i].label;
		}
		
		// Field Setting - Space
		li = document.createElement('li');
		li.id = "listDateFormat";
		li.className = "clear noheight";
		allProps.appendChild(li);
		// Field Setting - Space
	}
	// End Field Setting - choices for select
	
	// Field Setting - choices for limit
	if(field.type == 'limit')
	{
		li = document.createElement('li');
		li.id = "listChoices";
		li.className = "clear";
		li.style.display = "block";
		html = "";
		html += '<fieldset class="limitChoices">';
		html += '<legend>Choices</legend>';
		html += '<div style="line-heigth:25px;"><div style="float:right;margin-right:30px;">Max-Quota</div><div style="float:right;margin-right:15px;">Quota</div></div>';
		html += '<ul id="fieldChoices">';
		//console.log(field);
		for(i=0; i<field.choices.length; i++)
		{
			html += '<li>';
			html += '<input type="radio" name="allChoices" title="Make this choice pre-selected."';
			if(field.choices[i].selected)
			{
				html += ' checked="checked"';
			}
			html += ' class="" onclick="changeSelectDefault(\''+field.choices[i].id+'\')"> ';
			html += '<input type="text" id="xselect' + i+ '" onkeyup="updateChoice(\''+field.choices[i].id+'\', this.value);" value="'+field.choices[i].label+'" autocomplete="off" maxlength="150" class="text" style="width:135px;"> ';
			if(i > 0)
			{
				if(field.choices[i].quota == ''){
					field.choices[i].quota = 0;
				}
				if(field.choices[i].maxquota == ''){
					field.choices[i].maxquota = 0;
				}
				html += '<input type="text" id="xselect_quota' + i+ '" onkeyup="updateQuota(\''+field.choices[i].id+'\', this.value);" value="'+field.choices[i].quota+'" autocomplete="off" maxlength="5" class="text" style="width:30px;"> ';
				html += '<input placeholder="Max quota" type="text" id="xselect_maxquota' + i+ '" onkeyup="updateMaxQuota(\''+field.choices[i].id+'\', this.value);" value="'+field.choices[i].maxquota+'" autocomplete="off" maxlength="5" class="text" style="width:30px;"> ';
				html += '<img title="Add another choice." alt="Add" onclick="addLimitSelect(\''+field.choices[i].id+'\',this)" src="' + imgpath + '/img/template-editor/add.png"> ';
				if(i > 1)
				{
					html += '<img title="Delete this choice." alt="Delete" onclick="deleteSelect(\''+field.choices[i].id+'\',this)" src="' + imgpath + '/img/template-editor/delete.png">';
				}
			}
			html += '</li>';
		}
		html += '<li id="errorquota" style="display:none;"></li>';
		html += '</ul>';
		html += '</fieldset>';
		li.innerHTML = html;
		allProps.appendChild(li);
		for(i=0; i<field.choices.length; i++)
		{
			document.getElementById("xselect" + i).value = field.choices[i].label;
		}
		
		// Field Setting - Space
		li = document.createElement('li');
		li.id = "listDateFormat";
		li.className = "clear noheight";
		allProps.appendChild(li);
		// Field Setting - Space
	}
	// End Field Setting - choices for limit
	
	// Field Setting - choices for likert
	if(field.type == 'likert')
	{
		li = document.createElement('li');
		li.id = "listLikert";
		li.className = "clear";
		li.style.display = "block";
		var html2 = "";
		html = "";
		html += '<fieldset class="choices">';
		html += '<legend>Statements</legend>';
		html += '<ul id="likertStatements">';
		for(i=0; i<field.likert.statements.length; i++)
		{
			html += '<li>';
			html += '<input type="text" id="xstatement' + i + '" value="" class="text statement" onkeyup="updateLikertStatement(this);"> ';
			html += '<img title="Add another choice." onclick="addLikertStatement(this);" alt="Add" src="' + imgpath + '/img/template-editor/add.png"> ';
			if(i > 0)
			{
				html += '<img title="Delete this choice." onclick="deleteLikertStatement(this)" alt="Delete" src="' + imgpath + '/img/template-editor/delete.png">';
			}
			html += '</li>';
		}
		html += '</ul>';
		html += '</fieldset>';
		html += '<br>';
		html += '<fieldset class="choices">';
		html += '<legend>Columns</legend>';
		html += '<ul id="likertColumns">';
		for(i=0; i<field.likert.columns.length; i++)
		{
			html += '<li>';
			html += '<input type="radio" name="allChoices" title="Make this column pre-selected."';
			if(field.likert.selected == i)
			{
				html += ' checked="checked"';
			}
			html += ' onclick="changeLikertDefault(this);"> ';
			html += '<input type="text" id="xlikert' + i + '" onkeyup="updateLikertColumn(this);" value="" class="text"> ';
			html += '<img title="Add another choice." onclick="addLikertChoice(this);" alt="Add" src="' + imgpath + '/img/template-editor/add.png"> ';
			if(i > 0)
			{
				html += '<img title="Delete this column." onclick="deleteLikertChoice(this);" alt="Delete" src="' + imgpath + '/img/template-editor/delete.png">';
			}
			html += '</li>';
		}
		html += '</ul>';
		html += '</fieldset>';
		li.innerHTML = html;
		allProps.appendChild(li);
		for(i=0; i<field.likert.statements.length; i++)
		{
			document.getElementById("xstatement" + i).value = field.likert.statements[i]['label'];
		}
		for(i=0; i<field.likert.columns.length; i++)
		{
			document.getElementById("xlikert" + i).value = field.likert.columns[i];
		}
		
		// Field Setting - Space
		li = document.createElement('li');
		li.id = "listDateFormat";
		li.className = "clear noheight";
		allProps.appendChild(li);
		// Field Setting - Space
	}
	// End Field Setting - choices for likert
	
	// Field Setting - Required field
	// Modify By Wood
	// Set the plain text is not required field
	if(field.type != 'section' && field.type != 'plaintext')
	// if(field.type != 'section')
	// End Modify By Wood
	// add Customize error message, email/mobile/text unique checking and Predefined error message	
	{
		li = document.createElement('li');
		li.id = "listOptions";
		li.className = "clear";
		li.style.display = "block";
		html = "";
		html += '<fieldset>';
		html += '<legend>Options</legend>';
		
		html += '<input type="checkbox" id="fieldRequired" value=""';
		if(field.required==1)
		{
			html += ' checked="checked"';
		}
		html += ' class="checkbox" onclick="(this.checked) ? checkVal = \'1\' : checkVal = \'0\';updateProperties(checkVal, \'IsRequired\')">';
		html += '<label for="fieldRequired" class="choice">&nbsp;&nbsp;Required</label>';
		// Field Setting - customize empty error message 
		if(field.errorMessage==null){
			field.errorMessage = '';
		}
		li = document.createElement('li');
		li.id = "listTextDefault";
		li.className = "clear";
		li.style.display = "block";
		if(field.required==1){
			html += '<label class="desc cstmErrorMsg" for="errorMessage">Customize Empty Input Error Message</label>';
			html += '<div>';
			html += '<input type="text" id="errorMessage" name="errorMessage" maxlength="255" class="text cstmErrorMsg" onmouseup="updateProperties(this.value, \'errorMessage\',this)"';
			html += '	onkeyup="updateProperties(this.value, \'errorMessage\',this)" value="'+ (field.errorMessage) +'">';	
			html += '</div>';
		}
			
		if(field.type == "radio"&&field.choices[field.choices.length-1].id>=100){
			var otherRequire_html = '<input type="checkbox" id="radioOtherRequire" value=""';
			if(otherRequireFlag==1)
			{
				otherRequire_html += ' checked="checked"';
			}
			otherRequire_html += ' class="checkbox" onclick="(this.checked) ? checkVal = \'1\' : checkVal = \'0\';updateOtherRequire(checkVal)">';
			otherRequire_html += '<label for="radioOtherRequire" class="choice">&nbsp;&nbsp;Require for other Option</label>';
			html += otherRequire_html;
		}
		if(field.type == "checkbox"&&field.choices[field.choices.length-1].id>=100){
			var otherRequire_html = '<input type="checkbox" id="checkboxOtherRequire" value=""';
			if(otherRequireFlag==1)
			{
				otherRequire_html += ' checked="checked"';
			}
			otherRequire_html += ' class="checkbox" onclick="(this.checked) ? checkVal = \'1\' : checkVal = \'0\';updateOtherRequire(checkVal)">';
			otherRequire_html += '<label for="checkboxOtherRequire" class="choice">&nbsp;&nbsp;Require for other Option</label>';
			html += otherRequire_html;
		}
		//unique check for email/mobile/text
		if(field.type == "email" || field.type == "phone" || field.type == "text"|| field.type == "membergate"|| field.type == "hiddentext"){
			html += '<input type="checkbox" id="uniqueCheck" value=""';
			if(field.uniqueCheck==1)
			{
				html += ' checked="checked"';
			}
			html += ' class="checkbox" onclick="(this.checked) ? checkVal = \'1\' : checkVal = \'0\';updateProperties(checkVal, \'UniqueCheck\')">';
			html += '<label for="uniqueCheck" class="choice">&nbsp;&nbsp;Unique Submit Check</label>';
			// Field Setting - customize unique check error message
			if(field.errorMessage==null){
				field.errorMessage = '';
			}
			li = document.createElement('li');
			li.id = "listTextDefault";
			li.className = "clear";
			li.style.display = "block";
			if(field.uniqueCheck==1){
				html += '<label class="desc cstmErrorMsg" for="uniqueErrorMsg">Customize Duplicate Input Error Message</label>';
				html += '<div>';
				html += '<input type="text" id="uniqueErrorMsg" name="uniqueErrorMsg" maxlength="255" class="text cstmErrorMsg" onmouseup="updateProperties(this.value, \'uniqueErrorMsg\',this)"';
				html += '	onkeyup="updateProperties(this.value, \'uniqueErrorMsg\',this)" value="'+ (field.uniqueErrorMsg) +'">';	
				html += '</div>';
			}
		}
		//contact match field for membergate
		if(field.type == "membergate"){
			html += '<label style="width:35%;margin-left:10px;" for="memberField" class="choice">Member Field</label>';
			html += '<select onchange="updateProperties(this.value, \'contactAttribute\',this)">';
			$.each(customizeFieldArr,function(index,value){
				html += '<option ';
				if(field.contactAttribute==value){
					html += ' selected="selected" ';
				}
				html += 'value="'+value+'">'+value+'</option>';
			});
			html += '</select>';
			html += '<label style="width:35%;margin-left:10px;" for="compareType" class="choice">Compare Type</label>';
			html += '<select onchange="updateProperties(this.value, \'compareType\',this)">';
			var matchConditionArr = {'contains':'contains','equal':'equal','notequal':'not equal','startwith':'start with','endwith':'end with'};
			$.each(matchConditionArr,function(index,value){
				html += '<option ';
				if(field.compareType==index){
					html += ' selected="selected" ';
				}
				html += 'value="'+index+'">'+value+'</option>';
			});
			html += '</select>';
			// Field Setting - customize unique check error message
			if(field.errorMessage==null){
				field.errorMessage = '';
			}
			li = document.createElement('li');
			li.id = "listTextDefault";
			li.className = "clear";
			li.style.display = "block";
			if(field.uniqueCheck==1){
				html += '<label class="desc cstmErrorMsg" for="uniqueErrorMsg">Customize Duplicate Input Error Message</label>';
				html += '<div>';
				html += '<input type="text" id="uniqueErrorMsg" name="uniqueErrorMsg" maxlength="255" class="text cstmErrorMsg" onmouseup="updateProperties(this.value, \'uniqueErrorMsg\',this)"';
				html += '	onkeyup="updateProperties(this.value, \'uniqueErrorMsg\',this)" value="'+ (field.uniqueErrorMsg) +'">';	
				html += '</div>';
			}
		}
		if(field.type == "phone"){
			html += '<input type="checkbox" id="addAreaCode" value=""';
			if(field.addAreaCode==1)
			{
				html += ' checked="checked"';
			}
			html += ' class="checkbox" onclick="(this.checked) ? checkVal = \'1\' : checkVal = \'0\';updateMobileAreaCode(checkVal)">';
			html += '<label for="addAreaCode" class="choice">&nbsp;&nbsp;Add Area Code</label>';
		}
		if(field.type == "birth"){
			html += '<input type="checkbox" id="date_format" value=""';
			if(field.date_format=='YYYY-MM-DD')
			{
				html += ' checked="checked"';
			}
			html += ' class="checkbox" onclick="(this.checked) ? checkVal = \'1\' : checkVal = \'0\';addYearOptionInBirth(checkVal)">';
			html += '<label for="date_format" class="choice">&nbsp;&nbsp;Add Year Option</label>';
		}
		html += '</fieldset>';
		li.innerHTML = html;
		allProps.appendChild(li);
	}
	// End Field Setting - Required field
	if(field.type == "hiddentext")
	{
		li = document.createElement('li');
		li.id = "listHiddenText";
		li.className = "clear";
		li.style.display = "block";
		var labelvalue='Param';
		if(field.hiddentype==1)
			{
			$radiocheck1=' checked="checked"';
			$radiocheck2='';
			var labelvalue='Param';
			}
		else
			{
			$radiocheck2=' checked="checked"';
			$radiocheck1='';
			var labelvalue='Default Value';
			}
		html = "";
		html += '<label for="fieldDefault" class="desc">Hidden Type</label>';
		html += '<div style="display:block;overflow:hidden;clear:both;padding-top:6px;">';
		html += '<input type="radio" id="fieldhiddentype1" '+$radiocheck2+' class="checkbox" name="hiddentype" value="0" maxlength="255" onmouseup="" onclick="changeHiddenRadioDefault(\'0\')"><label class="choice">Use Default Value</label>';
		html += '<input type="radio" id="fieldhiddentype2" '+$radiocheck1+' name="hiddentype" value="1" maxlength="255" class="checkbox" onmouseup="" onclick="changeHiddenRadioDefault(\'1\')"><label class="choice">Get From Param</label>';
		html += '</div>';
		li.innerHTML = html;
		allProps.appendChild(li);
		
		li = document.createElement('li');
		li.id = "listTextDefault";
		li.className = "clear";
		li.style.display = "block";
		
		html = "";
		html += '<label id="labelhiddentext" for="fieldDefault" class="desc">'+labelvalue+'</label>';
		html += '<div>';
		html += '<input type="text" id="fieldDefault" name="text" value="" maxlength="255" class="text large" onmouseup="updateProperties(this.value, \'DefaultVal\')" onkeyup="updateProperties(this.value, \'DefaultVal\')">';
		html += '</div>';
		li.innerHTML = html;
		allProps.appendChild(li);
		var field_id = "Field" + currently_editing.substring(4);
		document.getElementById("fieldDefault").value = document.getElementById(field_id).value;
	
	}
	// Field Setting - Predefined field
	if(field.type == "text" || field.type == "number" || field.type == "textarea" || field.type == "email" || field.type == "url" || field.type == "phone")
	{
		li = document.createElement('li');
		li.id = "listTextDefault";
		li.className = "clear";
		li.style.display = "block";
		html = "";
		html += '<label for="fieldDefault" class="desc">Predefined Value</label>';
		html += '<div>';
		html += '<input type="text" id="fieldDefault" name="text" value="" maxlength="255" class="text large" onmouseup="updateProperties(this.value, \'DefaultVal\')" onkeyup="updateProperties(this.value, \'DefaultVal\')">';
		html += '</div>';
		li.innerHTML = html;
		allProps.appendChild(li);
		var field_id = "Field" + currently_editing.substring(4);
		document.getElementById("fieldDefault").value = document.getElementById(field_id).value;
		//console.log(document.getElementById(field_id).value);
	}
	
	// End Field Setting - Predefined field

	// Field Setting - customize valid check error message 	
	if(field.type=='email'||field.type=='phone'||field.type=='url'||field.type=='number'||field.type=='address'||field.type=='date'||field.type=='birth'||field.type=='time'||field.type=='likert'||field.type=='money'||field.type=='membergate')
	{
		if(field.validErrorMsg==null){
			field.validErrorMsg = '';
		}
		li = document.createElement('li');
		li.id = "listTextDefault";
		li.className = "clear";
		li.style.display = "block";
		html = "";
		html += '<label class="desc" for="validErrorMsg">Customize Invalid Input Error Message</label>';
		html += '<div>';
		html += '<input type="text" id="validErrorMsg" name="validErrorMsg" maxlength="255" class="text medium" onmouseup="updateProperties(this.value, \'validErrorMsg\',this)"';
		html += '	onkeyup="updateProperties(this.value, \'validErrorMsg\',this)" value="'+ (field.validErrorMsg) +'">';	
		html += '</div>';
		li.innerHTML = html;
		allProps.appendChild(li);
		var field_id = "Field" + currently_editing.substring(4);
	}
	// Field Setting - customize customizeField match error message 	
	if(field.type=='membergate')
	{
		if(field.validErrorMsg==null){
			field.validErrorMsg = '';
		}
		li = document.createElement('li');
		li.id = "listTextDefault";
		li.className = "clear";
		li.style.display = "block";
		html = "";
		html += '<label class="desc" for="attributeCompareErrorMsg">Member Field Match Error Message</label>';
		html += '<div>';
		html += '<input type="text" id="attributeCompareErrorMsg" name="attributeCompareErrorMsg" maxlength="255" class="text medium" onmouseup="updateProperties(this.value, \'attributeCompareErrorMsg\',this)"';
		html += '	onkeyup="updateProperties(this.value, \'attributeCompareErrorMsg\',this)" value="'+ (field.attributeCompareErrorMsg) +'">';	
		html += '</div>';
		li.innerHTML = html;
		allProps.appendChild(li);
		var field_id = "Field" + currently_editing.substring(4);
	}	
	if(field.type=='redeemcode')
	{
		if(field.validErrorMsg==null){
			field.validErrorMsg = '';
		}
		li = document.createElement('li');
		li.id = "listTextDefault";
		li.className = "clear";
		li.style.display = "block";
		html = "";
		html += '<label class="desc" for="attributeCompareErrorMsg">Redeem Code Error Message</label>';
		html += '<div>';
		html += '<input type="text" id="attributeCompareErrorMsg" name="attributeCompareErrorMsg" maxlength="255" class="text medium" onmouseup="updateProperties(this.value, \'attributeCompareErrorMsg\',this)"';
		html += '	onkeyup="updateProperties(this.value, \'attributeCompareErrorMsg\',this)" value="'+ (field.attributeCompareErrorMsg) +'">';	
		html += '</div>';
		li.innerHTML = html;
		allProps.appendChild(li);
		var field_id = "Field" + currently_editing.substring(4);
	}
	// Field Setting - Predefined Country field
	if(field.type == 'address')
	{
		li = document.createElement('li');
		li.id = "listAddressDefault";
		li.className = "clear";
		li.style.display = "block";
		html = "";
		html += '<label for="fieldAddressDefault" class="desc">Predefined Country</label>';
		html += '<div>';
		html += '<select onchange="updateProperties(this.value, \'DefaultVal\')" id="fieldCountries" class="select full">';
		html += '  <option value=""></option>';
		html += option_countries_region();
		html += '</select>';
		html += '</div>';
		li.innerHTML = html;
		var country_option = li.getElementsByTagName('option');
		for(i=0; i<country_option.length; i++)
		{
			if(country_option[i].value == field.defaultValue)
			{
				country_option[i].selected = true;
			}
		}
		allProps.appendChild(li);
	}

	if(field.type == "phone"&&field.addAreaCode=='1'){
		li = document.createElement('li');
		li.id = "listChoices";
		li.className = "clear";
		li.style.display = "block";
		var areaCodeChoiceshtml = "";
		areaCodeChoiceshtml += '<fieldset class="choices">';
		areaCodeChoiceshtml += '<ul id="fieldChoices" style="width:300px;">';
		areaCodeChoiceshtml += '<legend>Area Code</legend>';
		for(i=0; i<field.choices.length; i++)
		{
			areaCodeChoiceshtml += '<li>';
			areaCodeChoiceshtml += '<input type="radio" name="allChoices" title="Make this choice pre-selected."';
			if(field.choices[i].selected)
			{
				html += ' checked="checked"';
			}
			areaCodeChoiceshtml += ' class="" onclick="changeSelectDefault(\''+field.choices[i].id+'\')"> ';
			areaCodeChoiceshtml += '<input type="text" id="xselect' + i+ '" onkeyup="updateChoice(\''+field.choices[i].id+'\', this.value);" value="'+field.choices[i].label+'" autocomplete="off" maxlength="150" class="text"> ';
			if(i > 0)
			{
				areaCodeChoiceshtml += '<img title="Add another choice." alt="Add" onclick="addAreaCodeSelect(\''+field.choices[i].id+'\',this)" src="' + imgpath + '/img/template-editor/add.png"> ';
				if(i > 1)
				{
					areaCodeChoiceshtml += '<img title="Delete this choice." alt="Delete" onclick="deleteAreaCodeSelect(\''+field.choices[i].id+'\',this)" src="' + imgpath + '/img/template-editor/delete.png">';
				}
			}
			areaCodeChoiceshtml += '</li>';
		}
		areaCodeChoiceshtml += '</ul>';
		areaCodeChoiceshtml += '</fieldset>';
		li.innerHTML = areaCodeChoiceshtml;
		allProps.appendChild(li);
	}
	// End Field Setting - Predefined Country field
	//    alert(option_countries());
        console.log(allProps)
}

//duplicates a field with all the settings
function duplicateField(c, type)
{
	//console.log(type);
	var count = total_count;
	total_count++;
	var dupElem = document.getElementById('foli'+c);
	var cur = fieldInFields('foli'+c);
	var field_dup = fields[cur];

	
	var id = "foli"+count;
	var field_html;
	var divId = "itemOnPage"+count;
	var field_html = "";
	var loadClass = false;
	var isLikert = false;
	var isAddress = false;
	var fieldDiv = document.createElement("div");
	fieldDiv.id = divId;
	fieldDiv.className = 'form-group-edit col-sm-12 col-xs-12 nopad';
	fieldDiv.style.display = 'block';
	var fieldUl = document.createElement("ul");
	var field = document.createElement("li");
	var field_label_html = '';
	var labelInnerHtml = '';
	var field_name=field_dup.label;
	var floatDiv = document.createElement('div');
	floatDiv.class = 'floatDiv';
	floatDiv.onclick = '"settingFieldOnLeftPanel(\''+count+'\');"';
	field_html += '<div class="floatDiv" onclick="settingFieldOnLeftPanel(\''+count+'\');">';
	field_html += '</div>';
	labelInnerHtml += field_name;
	fieldLabel = document.createElement("label");
	var labelClass = 'frmquestion cstm_frmquestion desc-edit desc';
	if(type == 'likert')
	{
		labelClass = 'cus_title';
	}
	fieldLabel.className = labelClass;
	fieldLabel.id = 'title'+count;
	fieldLabel.for = 'Field'+count;
	fieldLabel.innerHTML = field_name;
	requiredSpan = document.createElement("span");
	requiredSpan.id = "req_"+count;
	requiredSpan.class = "req";
	fieldLabel.appendChild(requiredSpan);
	field.id = id;
	field.className = ' form-group-edit col-sm-12 col-xs-12 nopad ';
	field.style.cursor = 'pointer';
	field.style.top = '0px';
	field.style.left = '0px';
	field.title = 'Click to edit. Drag to reorder.';
	field.zIndex = '100';

	
	
	if(type == "text")
	{
		field_html += '' +
		'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
		'</label>' +
		'<div class="frmanswer cstm_frmanswer">' +
		'<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="'+ field_dup.defaultValue + '" ' +
		'class="field text '+ field_dup.size +'" name="Field'+count+'" id="Field'+count+'">' +
		'</div>' +
		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
		'<div class="fieldActions" id="fa'+count+'">' +
		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
		'</div>';
		
	}
	else if(type == "hiddentext")
	{
		field_html += '' +
		'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
		'</label>' +
		'<div class="frmanswer cstm_frmanswer">' +
		'<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="'+ field_dup.defaultValue + '" ' +
		'class="field text '+ field_dup.size +'" name="Field'+count+'" id="Field'+count+'">' +
		'</div>' +
		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
		'<div class="fieldActions" id="fa'+count+'">' +
		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
		'</div>';
		
	}
	else if(type == "membergate")
	{
		field_html += '' +
					'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
					'</label>' +
					'<div class="frmanswer cstm_frmanswer">' +
					'<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="'+ field_dup.defaultValue + '" ' +
					'class="field text '+ field_dup.size +'" name="Field'+count+'" id="Field'+count+'">' +
					'</div>' +
					'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
					'<div class="fieldActions" id="fa'+count+'">' +
					'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
					'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
					'</div>';		
	}
	else if(type == "redeemcode")
	{
		field_html += '' +
					'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
					'</label>' +
					'<div class="frmanswer cstm_frmanswer">' +
					'<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="'+ field_dup.defaultValue + '" ' +
					'class="field text '+ field_dup.size +'" name="Field'+count+'" id="Field'+count+'">' +
					'</div>' +
					'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
					'<div class="fieldActions" id="fa'+count+'">' +
					'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
					'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
					'</div>';
	}
	else if(type == "number")
	{
		field_html +=  '' +
		'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
		'</label>' +
		'<div class="frmanswer cstm_frmanswer">' +
		'<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="'+ field_dup.defaultValue + '" ' +
		'class="field text '+ field_dup.size +'" name="Field'+count+'" id="Field'+count+'">' +
		'</div>' +
		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
		'<div class="fieldActions" id="fa'+count+'">' +
		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
		'</div>';
	}
	else if(type == "textarea")
	{
		field_html +=  '' +
		'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
		'</label>' +
		'<div class="frmanswer cstm_frmanswer">' +
		'<div class="handle"></div>' +
		'<textarea disabled="disabled" readonly="readonly" cols="50" rows="10" ' +
		'class="field textarea small" name="Field'+count+'" id="Field'+count+'">'+field_dup.defaultValue+'</textarea>' +
		'</div>' +
		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
		'<div class="fieldActions" id="fa'+count+'">' +
		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
		'</div>';
	}
	else if(type == "sketch")
	{
		field_html +=  '' +
					'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
					'</label>' +
					'<div class="frmanswer cstm_frmanswer">' +
					'<div class="handle"></div>' +
					'<textarea disabled="disabled" readonly="readonly" cols="50" rows="10" ' +
					'class="field textarea small" name="Field'+count+'" id="Field'+count+'">'+field_dup.defaultValue+'</textarea>' +
					'<canvas id="simple_sketch" style="width:380px;background-color:white;"></canvas>' +
					'</div>' +
					'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
					'<div class="fieldActions" id="fa'+count+'">' +
					'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
					'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
					'</div>';
	
	}
	else if (type == "checkbox")
	{
		field_html +=  '' +
		'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
		'</label>' +
		'<div class="frmanswer cstm_frmanswer">' +
		'<span>' +
		'<input   id="Field'+count+'-1" name="Field'+count+'-1" class="field checkbox " value="First Choice" readonly="readonly" disabled="disabled" type="checkbox">' +
		'<label   class="frmchoice " for="Field'+count+'-1">First Choice</label>' +
		'</span>' +
		'<span>' +
		'<input  id="Field'+count+'-2" name="Field'+count+'-2" class="field checkbox " value="Second Choice" readonly="readonly" disabled="disabled" type="checkbox">' +
		'<label  class="frmchoice " for="Field'+count+'-2">Second Choice</label>' +
		'</span>' +
		'<span>' +
		'<input  id="Field'+count+'-3" name="Field'+count+'-3" class="field checkbox " value="Third Choice" readonly="readonly" disabled="disabled" type="checkbox">' +
		'<label  class="frmchoice " for="Field'+count+'-3">Third Choice</label>' +
		'</span>' +
		'</div>' +
		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
		'<div class="fieldActions" id="fa'+count+'">' +
		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
		'</div>';
	}
	else if(type == "radio")
	{
		field_html +=  '' +
		'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
		'</label>' +
		'<div class="frmanswer cstm_frmanswer">' +
		'<span>' +
		'<input id="Field'+count+'-1" name="Field'+count+'-1" class="field radio" value="First Choice" readonly="readonly" disabled="disabled" type="radio">' +
		'<label class="frmchoice" for="Field'+count+'-1">First Choicess</label>' +
		'</span>' +
		'<span>' +
		'<input id="Field'+count+'-2" name="Field'+count+'-1" class="field radio" value="Second Choice" readonly="readonly" disabled="disabled" type="radio">' +
		'<label class="frmchoice" for="Field'+count+'-2">Second Choice</label>' +
		'</span>' +
		'<span>' +
		'<input id="Field'+count+'-3" name="Field'+count+'-1" class="field radio" value="Third Choice" readonly="readonly" disabled="disabled" type="radio">' +
		'<label class="frmchoice" for="Field'+count+'-3">Third Choice</label>' +
		'</span>' +
		'</div>' +
		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
		'<div class="fieldActions" id="fa'+count+'">' +
		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
		'</div>';
	}
	else if(type == "select"|| type == "limit")
	{
		field_html +=  '' +
		'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
		'</label>' +
		'<div class="frmanswer cstm_frmanswer">' +
		'<select id="Field'+count+'" name="Field'+count+'" class="field select medium" readonly="readonly" disabled="disabled">' +
		'<option id="Field'+count+'-1" value="-- Please Select --" selected="selected">-- Please Select --</option>' +
		'<option id="Field'+count+'-2" value="First Choice" >First Choice</option>' +
		'<option id="Field'+count+'-3" value="Second Choice">Second Choice</option>' +
		'<option id="Field'+count+'-4" value="Third Choice">Third Choice</option>' +
		'</select>' +
		'</div>' +
		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
		'<div class="fieldActions" id="fa'+count+'">' +
		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
		'</div>';
	}
	else if(type == "section")
	{
//        alert('hmm');
		field.className = 'section';
		field_html += '<h3 id="title'+count+'">' +
				//	 field_dup.label +
					 '</h3>' +
					 '<div class="" id="instruct'+count+'">' +
					 field_dup.defaultValue +
					 '</div>' +
					 '<div class="fieldActions" id="fa'+count+'">' +
					 '<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
					 '<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
					 '</div>';
		
	}
	else if(type == "shortname")
	{
		field_html +=  '' +
		'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
		'</label>' +
		'<div class="frmanswer cstm_frmanswer">' + 
		'<span class="shortname" style="width:46%;">' +
		'<input style="width:80%;" type="text" disabled="disabled" readonly="readonly" size="14" value="" ' +
			'class="field text fn" name="Field'+count+'-1" id="Field'+count+'-1" placeholder="First">' +
		'</span>' +
		'<span  class="symbol"  style="width:1%;height:1px;"></span>' +
		'<span  class="shortname" style="width:47%;">' +
		'<input type="text" disabled="disabled" readonly="readonly" size="14" value="" ' +
			'class="field text ln" name="Field'+count+'-2" id="Field'+count+'-2" placeholder="Last">' +
		'</span>' +
		'</div>' + 
		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
		'<div class="fieldActions" id="fa'+count+'">' +
		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
		'</div>';
	}
	else if(type == "firstname" || type == "lastname")
	{		
            console.log(1)
		field_html +=  '' +
		'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
		'</label>' +
		 '<div class="frmanswer cstm_frmanswer">' + 
		'<input type="text" disabled="disabled" readonly="readonly" size="14" value="" ' +
			'class="field text medium" name="Field'+count+'-1" id="Field'+count+'-1">' +
		'</div>' +	
		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
		'<div class="fieldActions" id="fa'+count+'">' +
		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
		'</div>';
	}
	else if(type == "date")
	{
		field_html +=  '' +
		'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
		'</label>' +
		'<span class="date">' +
		'<input id="Field'+count+'" class="field text" value="" size="14" ' +
			'maxlength="10" readonly="readonly" disabled="disabled" type="text">' +
		'<label for="Field'+count+'">MM/DD/YYYY</label>' +
		'</span>' +
		'<span id="cal'+count+'">' +
		'<img id="pick'+count+'" class="datepicker" src="' + imgpath + '/img/template-editor/calendar.png" alt="Pick a date.">' +
		'</span>' +
		'<br><br>' +
		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
		'<div class="fieldActions" id="fa'+count+'">' +
		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
		'</div>';
	}
	else if(type == "email")
	{
		field_html +=  '' +
		'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
		'</label>' +
		'<div class="frmanswer cstm_frmanswer">' +
		'<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="'+field_dup.defaultValue+'" ' +
		'class="field text medium" name="Field'+count+'" id="Field'+count+'">' +
		'</div>' +
		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
		'<div class="fieldActions" id="fa'+count+'">' +
		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
		'</div>';
	}
	else if(type == "url")
	{
		field_html +=  '' +
		'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
		'</label>' +
		'<div class="frmanswer cstm_frmanswer">' +
		'<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="'+field_dup.defaultValue+'" ' +
		'class="field text medium" name="Field'+count+'" id="Field'+count+'">' +
		'</div>' +
		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
		'<div class="fieldActions" id="fa'+count+'">' +
		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
		'</div>';
	}
	else if(type == "time")
	{
		field_html += "";
		field_html += '<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">';
		field_html += '</label>';
		field_html += '<span class="time">';
		field_html += '<select id="Field'+count+'-1" name="Field'+count+'-1" class="field select" readonly="readonly" disabled="disabled">';
		field_html += '<option value=""></option>';
		for(var i=1; i < 13; i++)
		{
			field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
		}
		field_html += '</select>';
		field_html += '<label for="Field'+count+'-1">HH</label>';
		field_html += '</span>';
		field_html += '<span class="symbol">:</span>';
		field_html += '<span class="time">';
		field_html += '<select id="Field'+count+'-2" name="Field'+count+'-2" class="field select" readonly="readonly" disabled="disabled">';
		field_html += '<option value=""></option>';
		for(var i=0; i < 60; i++)
		{
			field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
		}
		field_html += '</select>';
		field_html += '<label for="Field'+count+'-2">MM</label>';
		field_html += '</span>';
		field_html += '<span class="symbol">:</span>';
		field_html += '<span class="time">';
		field_html += '<select id="Field'+count+'-3" name="Field'+count+'-3" class="field select" readonly="readonly" disabled="disabled">';
		field_html += '<option value=""></option>';
		for(var i=0; i < 60; i++)
		{
			field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
		}
		field_html += '</select>';
		field_html += '<label for="Field'+count+'-3">SS</label>';
		field_html += '</span>';
		field_html += '<span class="ampm">';
		field_html += '<select id="Field'+count+'-4" name="Field'+count+'-4" class="field select" style="width: 4em;" readonly="readonly" disabled="disabled">';
		field_html += '<option value="AM" selected="selected">AM</option>';
		field_html += '<option value="PM">PM</option>';
		field_html += '</select>';
		field_html += '<label for="Field'+count+'-4">AM/PM</label>';
		field_html += '</span>';
		field_html += '<br><br>';
		field_html += '<p id="instruct'+count+'" class="instruct hide"><small></small></p>';
		field_html += '<div class="fieldActions" id="fa'+count+'">';
		field_html += '<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
		field_html += '<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
		field_html += '</div>';
	}
	else if(type == "phone")
	{
		field_html +=  '' +
		'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
		'</label>' +
		'<div class="frmanswer cstm_frmanswer">' +
		'<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="'+field_dup.defaultValue+'" ' +
		'class="field text medium" name="Field'+count+'" id="Field'+count+'">' +
		'</div>' +
		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
		'<div class="fieldActions" id="fa'+count+'">' +
		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
		'</div>';
	}
	else if(type == "money")
	{
		field_html +=  '' +
		'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
		'</label>' +
		'<span class="symbol">$</span>' +
		'<span class="shortname">' +
		'<input id="Field'+count+'-1" name="Field'+count+'-1" class="field text currency" value="" size="10" readonly="readonly" disabled="disabled" type="text">' +
		'<label for="Field'+count+'-1">Dollars</label>' +
		'</span>' +
		'<span class="symbol">.</span>' +
		'<span class="shortname">' +
		'<input id="Field'+count+'-2" name="Field'+count+'-2" class="field text" value="" size="2" maxlength="2" readonly="readonly" disabled="disabled" type="text">' +
		'<label for="Field'+count+'-2">Cents</label>' +
		'</span>' +
		'<br><br>' +
		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
		'<div class="fieldActions" id="fa'+count+'">' +
		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
		'</div>';
		
	}
	else if(type == "address")
	{		
		field.className += " complex";
		field.style.display = "block";
		field.style.height = "240px";
		field_html +=  '' +
					'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
					'</label>' +
					'<div class="frmanswer cstm_frmanswer">' +
					'<span class="full left">' +
					'<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">' +
					'<label for="Field">Street Address</label>' +
					'</span>' +
					'<span class="full left">' +
					'<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">' +
					'<label for="Field">Address Line 2</label>' +
					'</span>' +
					'<span class="full left">' + 
					'<span class="left">' +
					'<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">' +
					'<label for="Field">City</label>' +
					'</span>' +
					'<span class="right">' +
					'<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">' +
					'<label for="Field">State / Province / Region</label>' +
					'</span>' +
					'</span>' +
					'<span class="full left">' + 
					'<span class="left">' +
					'<input id="Field" name="Field" class="field text addr" value="" maxlength="15" readonly="readonly" disabled="disabled" type="text">' +
					'<label for="Field">Postal / Zip Code</label>' +
					'</span>' +
					'<span class="right">' +
					'<select id="Field" name="Field" class="field select addr" readonly="readonly" disabled="disabled">' +
					'<option value="" selected="selected"></option>' +
					option_countries() +
					'</select>' +
					'<label for="Field">Country</label>' +
					'</span>' +
					'</span>' +
					'</div>' +
					'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
					'<div class="fieldActions" id="fa'+count+'">' +
					'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
					'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
					'</div>';
	}
	else if(type == "pagebreak")
	{
		field_html += '<label for="Field'+count+'" id="title'+count+'" class="frmquestion cstm_frmquestion">' +
					'---------Page Break---------'+
					'<br>' +
					'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
					'<div class="fieldActions" id="fa'+count+'">' +
					'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
					'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
					'</div>';
	}
	else if(type == "likert")
	{
		field.className += " likert col4";
		field_html +=  '' +
					'<label for="Field'+count+'" id="title'+count+'" class="cus_title">' +
					'</label></br></br>' +
					'<table cellspacing="0">' +
					// '<caption id="title'+count+'">' +
					// 'Evaluate the following statements.' +
					// '<span class="req" id="req_'+count+'"></span>' +
					// '</caption>' +
					'<thead>' +
					'<tr>' +
					'<th>&nbsp;</th>' +
					'<td>Strongly Disagree</td>' +
					'<td>Disagree</td>' +
					'<td>Agree</td>' +
					'<td>Strongly Agree</td>' +
					'</tr>' +
					'</thead>' +
					'<tbody>' +
					'<tr class="">' +
					'<th><label for="Field1">Statement One</label></th>' +
					'<td title="Strongly Disagree">' +
					'<input type="radio" readonly="readonly" disabled="disabled" name="Field1" id="Field'+count+'-1">' +
					'<label for="Field'+count+'-1">1</label>' +
					'</td>' +
					'<td title="Disagree">' +
					'<input type="radio" readonly="readonly" disabled="disabled" name="Field1" id="Field'+count+'-2">' +
					'<label for="Field'+count+'-2">2</label>' +
					'</td>' +
					'<td title="Agree">' +
					'<input type="radio" readonly="readonly" disabled="disabled" name="Field1" id="Field'+count+'-3">' +
					'<label for="Field'+count+'-3">3</label>' +
					'</td>' +
					'<td title="Strongly Agree">' +
					'<input type="radio" readonly="readonly" disabled="disabled" name="Field1" id="Field'+count+'-4">' +
					'<label for="Field'+count+'-4">4</label>' +
					'</td>' +
					'</tr>' +
					'<tr class="alt">' +
					'<th><label for="Field2">Statement Two</label></th>' +
					'<td title="Strongly Disagree">' +
					'<input type="radio" readonly="readonly" disabled="disabled" name="Field2" id="Field'+count+'-1">' +
					'<label for="Field'+count+'-1">1</label>' +
					'</td>' +
					'<td title="Disagree">' +
					'<input type="radio" readonly="readonly" disabled="disabled" name="Field2" id="Field'+count+'-2">' +
					'<label for="Field'+count+'-2">2</label>' +
					'</td>' +
					'<td title="Agree">' +
					'<input type="radio" readonly="readonly" disabled="disabled" name="Field2" id="Field'+count+'-3">' +
					'<label for="Field'+count+'-3">3</label>' +
					'</td>' +
					'<td title="Strongly Agree">' +
					'<input type="radio" readonly="readonly" disabled="disabled" name="Field2" id="Field'+count+'-4">' +
					'<label for="Field'+count+'-4">4</label>' +
					'</td>' +
					'</tr>' +
					'</tbody>' +
					'</table>' +
					'<div class="fieldActions" id="fa'+count+'">' +
					'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
					'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
					'</div>';

	}
	else if(type == "birthdate")
	{
		field_html +=  '' +
					'<label for="Field'+count+'" id="title'+count+'" class="desc-edit desc">';
		field_html += '</label>';
		field_html += '<div class="frmanswer cstm_frmanswer">';
		field_html += '<span class="time">';
		field_html += '<select id="Field'+count+'-1" name="Field'+count+'-1" class="field select" readonly="readonly" disabled="disabled">';
		field_html += '<option value=""></option>';
		for(var i=1; i < 32; i++)
		{
			field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
		}
		field_html += '</select>';
		field_html += '</span>';
		
		field_html += '</div>';
		field_html += '<p id="instruct'+count+'" class="instruct hide"><small></small></p>';
		field_html += '<div class="fieldActions" id="fa'+count+'">';
		field_html += '<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
		field_html += '<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
		field_html += '</div>';
	}
	else if(type == "birthmonth")
	{
		field_html +=  '' +
					'<label for="Field'+count+'" id="title'+count+'" class="desc-edit desc">';
		field_html += '</label>';
		field_html += '<div class="frmanswer cstm_frmanswer">';
		field_html += '<span class="time">';
		field_html += '<select id="Field'+count+'-2" name="Field'+count+'-2" class="field select" readonly="readonly" disabled="disabled">';
		field_html += '<option value=""></option>';
		for(var i=1; i < 13; i++)
		{
			field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
		}
		field_html += '</select>';
		field_html += '</span>';
		field_html += '</div>';
		field_html += '<p id="instruct'+count+'" class="instruct hide"><small></small></p>';
		field_html += '<div class="fieldActions" id="fa'+count+'">';
		field_html += '<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
		field_html += '<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
		field_html += '</div>';
	}
	else if(type == "birth")
	{
		field_html +=  '' +
					'<label for="Field'+count+'" id="title'+count+'" class="desc-edit desc">';
		field_html += '</label>';
		field_html += '<div class="frmanswer cstm_frmanswer">';
		field_html += '<span class="time birthTwoColumn">';
		field_html += '<select id="Field'+count+'-1" name="Field'+count+'-1" class="field select birthSelect" readonly="readonly" disabled="disabled">';
		field_html += '<option value="">DD</option>';
		for(var i=1; i < 32; i++)
		{
			field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
		}
		field_html += '</select>';
//		field_html += '<label for="Field'+count+'-1">MM-</label>';
		field_html += '</span>';
		field_html += '<span class="time birthTwoColumn">';
		field_html += '<select id="Field'+count+'-2" name="Field'+count+'-2" class="field select mmSelect" readonly="readonly" disabled="disabled">';
		field_html += '<option value="">MM</option>';
		for(var i=1; i < 13; i++)
		{
			field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
		}
		field_html += '</select>';
		field_html += '</span>';				
		field_html += '</div>';
		field_html += '<p id="instruct'+count+'" class="instruct hide"><small></small></p>';
		field_html += '<div class="fieldActions" id="fa'+count+'">';
		field_html += '<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
		field_html += '<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
		field_html += '</div>';
	}
	// Modify By sam
	// Create Duplicate plain text interface 
	else if(type == "plaintext")
	{
		field_html += '<div id="Field'+count+'" class="medium" style="font-size:15px;">';
		field_html += 'Please enter the text';
		field_html += '</div>';
		field_html += '<p id="instruct'+count+'" class="instruct hide"><small></small></p>';
		field_html += '<div class="fieldActions" id="fa'+count+'">';
		field_html += '<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
		field_html += '<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
		field_html += '</div>';
	}
	else if(type == "agerange")
	{
//        alert('hmm');
		field_html +=  '' +
				'<label for="Field'+count+'" id="title'+count+'" class="'+labelClass+'">' +
				'</label>' +
				'<div class="frmanswer cstm_frmanswer">' +
				'<span>' +
				'<input id="Field'+count+'-1" name="Field'+count+'-1" class="field radio" value="First Choice" readonly="readonly" disabled="disabled" type="radio">' +
				'<label class="frmchoice" for="Field'+count+'-1">Under 18</label>' +
				'</span>' +
				'<span>' +
				'<input id="Field'+count+'-2" name="Field'+count+'-1" class="field radio" value="Second Choice" readonly="readonly" disabled="disabled" type="radio">' +
				'<label class="frmchoice" for="Field'+count+'-2">18-40</label>' +
				'</span>' +
				'<span>' +
				'<input id="Field'+count+'-3" name="Field'+count+'-1" class="field radio" value="Third Choice" readonly="readonly" disabled="disabled" type="radio">' +
				'<label class="frmchoice" for="Field'+count+'-3">Over 40</label>' +
				'</span>' +
				'</div>' +
				'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
				'<div class="fieldActions" id="fa'+count+'">' +
				'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
				'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
				'</div>';
	}
	// End Modify By Wood	
	else		
		return;


	field.innerHTML = field_html;
	
	fieldUl.appendChild(field);
	if($.inArray(type,['pagebreak','plaintext'])<0){
		fieldDiv.appendChild(fieldLabel);	
	}
	fieldDiv.appendChild(fieldUl);
	var formFields = document.getElementById('formFields');
	var targetEl=document.getElementById('itemOnPage'+c);
	insertAfter(fieldDiv, targetEl);
	//formFields.appendChild(fieldDiv);
	//console.log(fieldDiv);
	$('#'+id).click(makeEditable);
	bindDragEvents(divId);
	showFields(true);

//	var formFields = document.getElementById('formFields');
//	formFields.insertBefore(field, dupElem.nextSibling);

	$('#'+id).click(makeEditable);
	bindDragEvents(id);

	var i;
	var choices = [];
	if(field_dup.choices.length)
	{        
		for(i=0; i<field_dup.choices.length; i++)
			choices[i] = {id: field_dup.choices[i].id, label: field_dup.choices[i].label, maxquota: field_dup.choices[i].maxquota, otherRequired: field_dup.choices[i].otherRequired ,quota: field_dup.choices[i].quota,selected: field_dup.choices[i].selected, valueid: ''};
	}
	
	var likert = {};
	if(field_dup.likert.statements)
	{
		var statements = new Array();
		var tmp_val_arr = new Array();
		for(i=0; i<field_dup.likert.columns.length; i++){
			tmp_val_arr[i] = '';
		}
		for(i=0; i<field_dup.likert.statements.length; i++){
			statements[i] = {surveyid: '', label: field_dup.likert.statements[i]['label'], valueid: tmp_val_arr};
		}

		var columns = new Array();
		for(i=0; i<field_dup.likert.columns.length; i++)
			columns[i] = field_dup.likert.columns[i];

		likert = {statements: statements, columns: columns, selected: field_dup.likert.selected};
	}

	if(type == "likert"){
		fields.splice(fieldInFields(dupElem.id) + 1, 0, {id: id, count: count, type: field_dup.type, label: field_dup.label, required: field_dup.required, size: field_dup.size,
					duplicate: field_dup.duplicate, defaultValue: field_dup.defaultValue, choices: choices,
					date_format: field_dup.date_format, columns: field_dup.columns, likert: likert, formid: '',uniqueCheck:field_dup.uniqueCheck, validErrorMsg: field_dup.validErrorMsg });
	} else if(type == "text" || type == "textarea" || type == "number" || type == "phone") {
		fields.splice(fieldInFields(dupElem.id) + 1, 0, {id: id, count: count, type: field_dup.type, label: field_dup.label, required: field_dup.required, size: field_dup.size,
					duplicate: field_dup.duplicate, errorMessage: field_dup.errorMessage, defaultValue: field_dup.defaultValue,addAreaCode: field_dup.addAreaCode,attributeCompareErrorMsg: field_dup.attributeCompareErrorMsg,compareType: field_dup.compareType, choices: choices,uniqueErrorMsg: field_dup.uniqueErrorMsg,
					date_format: field_dup.date_format, columns: field_dup.columns, likert: likert, length:'', formid:'', surveyid: '',uniqueCheck:field_dup.uniqueCheck, validErrorMsg: field_dup.validErrorMsg });
	}else if(type == "membergate" ){
		fields.splice(fieldInFields(dupElem.id) + 1, 0, {id: id, count: count, type: field_dup.type, label: field_dup.label, required: field_dup.required, size: field_dup.size,
			duplicate: field_dup.duplicate, errorMessage: field_dup.errorMessage,defaultValue: field_dup.defaultValue,attributeCompareErrorMsg: field_dup.attributeCompareErrorMsg, choices: choices,uniqueErrorMsg: field_dup.uniqueErrorMsg,
			date_format: field_dup.date_format, columns: field_dup.columns, likert: likert, formid:'', surveyid: '',uniqueCheck:field_dup.uniqueCheck, validErrorMsg: field_dup.validErrorMsg , contactAttribute:field_dup.contactAttribute, compareType:field_dup.compareType  });
		
	}else {
		fields.splice(fieldInFields(dupElem.id) + 1, 0, {id: id, count: count, type: field_dup.type, label: field_dup.label, required: field_dup.required, size: field_dup.size,
					duplicate: field_dup.duplicate ,errorMessage: field_dup.errorMessage, defaultValue: field_dup.defaultValue, choices: choices,
					date_format: field_dup.date_format, columns: field_dup.columns, likert: likert, formid:'', surveyid: '' ,uniqueCheck:field_dup.uniqueCheck, validErrorMsg: field_dup.validErrorMsg });
	}

	if(type == "likert")
		generateLikert(id);
	else if(type == "checkbox")
		generateCheckbox(id);
	else if(type == "radio")
		generateRadio(id);
	else if(type == "select"||type == "limit" )
		generateSelect(id);
}

function insertAfter(newEl, targetEl)
{
    var parentEl = targetEl.parentNode;
            
     if(parentEl.lastChild == targetEl)
     {
           parentEl.appendChild(newEl);
      }else
      {
           parentEl.insertBefore(newEl,targetEl.nextSibling);
      }            
}
//option values for countries
function option_countries()
{
	var html = "";
	html += '<option value="Afghanistan">Afghanistan</option>';
	html += '<option value="Albania">Albania</option>';
	html += '<option value="Algeria">Algeria</option>';
	html += '<option value="Andorra">Andorra</option>';
	html += '<option value="Angola">Angola</option>';
	html += '<option value="Antigua and Barbuda">Antigua and Barbuda</option>';
	html += '<option value="Argentina">Argentina</option>';
	html += '<option value="Armenia">Armenia</option>';
	html += '<option value="Aruba">Aruba</option>';
	html += '<option value="Australia">Australia</option>';
	html += '<option value="Austria">Austria</option>';
	html += '<option value="Azerbaijan">Azerbaijan</option>';
	html += '<option value="Bahamas">Bahamas</option>';
	html += '<option value="Bahrain">Bahrain</option>';
	html += '<option value="Bangladesh">Bangladesh</option>';
	html += '<option value="Barbados">Barbados</option>';
	html += '<option value="Belarus">Belarus</option>';
	html += '<option value="Belgium">Belgium</option>';
	html += '<option value="Belize">Belize</option>';
	html += '<option value="Benin">Benin</option>';
	html += '<option value="Bermuda">Bermuda</option>';
	html += '<option value="Bhutan">Bhutan</option>';
	html += '<option value="Bolivia">Bolivia</option>';
	html += '<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>';
	html += '<option value="Botswana">Botswana</option>';
	html += '<option value="Brazil">Brazil</option>';
	html += '<option value="Brunei">Brunei</option>';
	html += '<option value="Bulgaria">Bulgaria</option>';
	html += '<option value="Burkina Faso">Burkina Faso</option>';
	html += '<option value="Burundi">Burundi</option>';
	html += '<option value="Cambodia">Cambodia</option>';
	html += '<option value="Cameroon">Cameroon</option>';
	html += '<option value="Canada">Canada</option>';
	html += '<option value="Cape Verde">Cape Verde</option>';
	html += '<option value="Central African Republic">Central African Republic</option>';
	html += '<option value="Chad">Chad</option>';
	html += '<option value="Chile">Chile</option>';
	html += '<option value="China">China</option>';
	html += '<option value="Colombia">Colombia</option>';
	html += '<option value="Comoros">Comoros</option>';
	html += '<option value="Democratic Republic of the Congo">Democratic Republic of the Congo</option>';
	html += '<option value="Republic of the Congo">Republic of the Congo</option>';
	html += '<option value="Cook Islands">Cook Islands</option>';
	html += '<option value="Costa Rica">Costa Rica</option>';
	html += '<option value="Cote d\'Ivoire">Cote d\'Ivoire</option>';
	html += '<option value="Croatia">Croatia</option>';
	html += '<option value="Cuba">Cuba</option>';
	html += '<option value="Cyprus">Cyprus</option>';
	html += '<option value="Czech Republic">Czech Republic</option>';
	html += '<option value="Denmark">Denmark</option>';
	html += '<option value="Djibouti">Djibouti</option>';
	html += '<option value="Dominica">Dominica</option>';
	html += '<option value="Dominican Republic">Dominican Republic</option>';
	html += '<option value="East Timor">East Timor</option>';
	html += '<option value="Ecuador">Ecuador</option>';
	html += '<option value="Egypt">Egypt</option>';
	html += '<option value="El Salvador">El Salvador</option>';
	html += '<option value="Equatorial Guinea">Equatorial Guinea</option>';
	html += '<option value="Eritrea">Eritrea</option>';
	html += '<option value="Estonia">Estonia</option>';
	html += '<option value="Ethiopia">Ethiopia</option>';
	html += '<option value="Faroe Islands">Faroe Islands</option>';
	html += '<option value="Fiji">Fiji</option>';
	html += '<option value="Finland">Finland</option>';
	html += '<option value="France">France</option>';
	html += '<option value="Gabon">Gabon</option>';
	html += '<option value="Gambia">Gambia</option>';
	html += '<option value="Georgia">Georgia</option>';
	html += '<option value="Germany">Germany</option>';
	html += '<option value="Ghana">Ghana</option>';
	html += '<option value="Gibraltar">Gibraltar</option>';
	html += '<option value="Greece">Greece</option>';
	html += '<option value="Grenada">Grenada</option>';
	html += '<option value="Guatemala">Guatemala</option>';
	html += '<option value="Guinea">Guinea</option>';
	html += '<option value="Guinea-Bissau">Guinea-Bissau</option>';
	html += '<option value="Guyana">Guyana</option>';
	html += '<option value="Haiti">Haiti</option>';
	html += '<option value="Honduras">Honduras</option>';
	html += '<option value="Hong Kong">Hong Kong</option>';
	html += '<option value="Hungary">Hungary</option>';
	html += '<option value="Iceland">Iceland</option>';
	html += '<option value="India">India</option>';
	html += '<option value="Indonesia">Indonesia</option>';
	html += '<option value="Iran">Iran</option>';
	html += '<option value="Iraq">Iraq</option>';
	html += '<option value="Ireland">Ireland</option>';
	html += '<option value="Israel">Israel</option>';
	html += '<option value="Italy">Italy</option>';
	html += '<option value="Jamaica">Jamaica</option>';
	html += '<option value="Japan">Japan</option>';
	html += '<option value="Jordan">Jordan</option>';
	html += '<option value="Kazakhstan">Kazakhstan</option>';
	html += '<option value="Kenya">Kenya</option>';
	html += '<option value="Kiribati">Kiribati</option>';
	html += '<option value="North Korea">North Korea</option>';
	html += '<option value="South Korea">South Korea</option>';
	html += '<option value="Kuwait">Kuwait</option>';
	html += '<option value="Kyrgyzstan">Kyrgyzstan</option>';
	html += '<option value="Laos">Laos</option>';
	html += '<option value="Latvia">Latvia</option>';
	html += '<option value="Lebanon">Lebanon</option>';
	html += '<option value="Lesotho">Lesotho</option>';
	html += '<option value="Liberia">Liberia</option>';
	html += '<option value="Libya">Libya</option>';
	html += '<option value="Liechtenstein">Liechtenstein</option>';
	html += '<option value="Lithuania">Lithuania</option>';
	html += '<option value="Luxembourg">Luxembourg</option>';
	html += '<option value="Macedonia">Macedonia</option>';
	html += '<option value="Madagascar">Madagascar</option>';
	html += '<option value="Malawi">Malawi</option>';
	html += '<option value="Malaysia">Malaysia</option>';
	html += '<option value="Maldives">Maldives</option>';
	html += '<option value="Mali">Mali</option>';
	html += '<option value="Malta">Malta</option>';
	html += '<option value="Marshall Islands">Marshall Islands</option>';
	html += '<option value="Mauritania">Mauritania</option>';
	html += '<option value="Mauritius">Mauritius</option>';
	html += '<option value="Mexico">Mexico</option>';
	html += '<option value="Micronesia">Micronesia</option>';
	html += '<option value="Moldova">Moldova</option>';
	html += '<option value="Monaco">Monaco</option>';
	html += '<option value="Mongolia">Mongolia</option>';
	html += '<option value="Montenegro">Montenegro</option>';
	html += '<option value="Morocco">Morocco</option>';
	html += '<option value="Mozambique">Mozambique</option>';
	html += '<option value="Myanmar">Myanmar</option>';
	html += '<option value="Namibia">Namibia</option>';
	html += '<option value="Nauru">Nauru</option>';
	html += '<option value="Nepal">Nepal</option>';
	html += '<option value="Netherlands">Netherlands</option>';
	html += '<option value="Netherlands Antilles">Netherlands Antilles</option>';
	html += '<option value="New Zealand">New Zealand</option>';
	html += '<option value="Nicaragua">Nicaragua</option>';
	html += '<option value="Niger">Niger</option>';
	html += '<option value="Nigeria">Nigeria</option>';
	html += '<option value="Norway">Norway</option>';
	html += '<option value="Oman">Oman</option>';
	html += '<option value="Pakistan">Pakistan</option>';
	html += '<option value="Palau">Palau</option>';
	html += '<option value="Palestine">Palestine</option>';
	html += '<option value="Panama">Panama</option>';
	html += '<option value="Papua New Guinea">Papua New Guinea</option>';
	html += '<option value="Paraguay">Paraguay</option>';
	html += '<option value="Peru">Peru</option>';
	html += '<option value="Philippines">Philippines</option>';
	html += '<option value="Poland">Poland</option>';
	html += '<option value="Portugal">Portugal</option>';
	html += '<option value="Puerto Rico">Puerto Rico</option>';
	html += '<option value="Qatar">Qatar</option>';
	html += '<option value="Romania">Romania</option>';
	html += '<option value="Russia">Russia</option>';
	html += '<option value="Rwanda">Rwanda</option>';
	html += '<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>';
	html += '<option value="Saint Lucia">Saint Lucia</option>';
	html += '<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>';
	html += '<option value="Samoa">Samoa</option>';
	html += '<option value="San Marino">San Marino</option>';
	html += '<option value="Sao Tome and Principe">Sao Tome and Principe</option>';
	html += '<option value="Saudi Arabia">Saudi Arabia</option>';
	html += '<option value="Senegal">Senegal</option>';
	html += '<option value="Serbia and Montenegro">Serbia and Montenegro</option>';
	html += '<option value="Seychelles">Seychelles</option>';
	html += '<option value="Sierra Leone">Sierra Leone</option>';
	html += '<option value="Singapore">Singapore</option>';
	html += '<option value="Slovakia">Slovakia</option>';
	html += '<option value="Slovenia">Slovenia</option>';
	html += '<option value="Solomon Islands">Solomon Islands</option>';
	html += '<option value="Somalia">Somalia</option>';
	html += '<option value="South Africa">South Africa</option>';
	html += '<option value="Spain">Spain</option>';
	html += '<option value="Sri Lanka">Sri Lanka</option>';
	html += '<option value="Sudan">Sudan</option>';
	html += '<option value="Suriname">Suriname</option>';
	html += '<option value="Swaziland">Swaziland</option>';
	html += '<option value="Sweden">Sweden</option>';
	html += '<option value="Switzerland">Switzerland</option>';
	html += '<option value="Syria">Syria</option>';
	html += '<option value="Taiwan">Taiwan</option>';
	html += '<option value="Tajikistan">Tajikistan</option>';
	html += '<option value="Tanzania">Tanzania</option>';
	html += '<option value="Thailand">Thailand</option>';
	html += '<option value="Togo">Togo</option>';
	html += '<option value="Tonga">Tonga</option>';
	html += '<option value="Trinidad and Tobago">Trinidad and Tobago</option>';
	html += '<option value="Tunisia">Tunisia</option>';
	html += '<option value="Turkey">Turkey</option>';
	html += '<option value="Turkmenistan">Turkmenistan</option>';
	html += '<option value="Tuvalu">Tuvalu</option>';
	html += '<option value="Uganda">Uganda</option>';
	html += '<option value="Ukraine">Ukraine</option>';
	html += '<option value="United Arab Emirates">United Arab Emirates</option>';
	html += '<option value="United Kingdom">United Kingdom</option>';
	html += '<option value="United States">United States</option>';
	html += '<option value="Uruguay">Uruguay</option>';
	html += '<option value="Uzbekistan">Uzbekistan</option>';
	html += '<option value="Vanuatu">Vanuatu</option>';
	html += '<option value="Vatican City">Vatican City</option>';
	html += '<option value="Venezuela">Venezuela</option>';
	html += '<option value="Vietnam">Vietnam</option>';
	html += '<option value="Yemen">Yemen</option>';
	html += '<option value="Zambia">Zambia</option>';
	html += '<option value="Zimbabwe">Zimbabwe</option>';
	
	return html;
}

//option values for countries divided in regions
function option_countries_region()
{
	var html = "";
	html += '<optgroup label="North America">';
	html += '<option value="Antigua and Barbuda">Antigua and Barbuda</option>';
	html += '<option value="Aruba">Aruba</option>';
	html += '<option value="Bahamas">Bahamas</option>';
	html += '<option value="Barbados">Barbados</option>';
	html += '<option value="Belize">Belize</option>';
	html += '<option value="Canada">Canada</option>';
	html += '<option value="Cook Islands">Cook Islands</option>';
	html += '<option value="Costa Rica">Costa Rica</option>';
	html += '<option value="Cuba">Cuba</option>';
	html += '<option value="Dominica">Dominica</option>';
	html += '<option value="Dominican Republic">Dominican Republic</option>';
	html += '<option value="El Salvador">El Salvador</option>';
	html += '<option value="Grenada">Grenada</option>';
	html += '<option value="Guatemala">Guatemala</option>';
	html += '<option value="Haiti">Haiti</option>';
	html += '<option value="Honduras">Honduras</option>';
	html += '<option value="Jamaica">Jamaica</option>';
	html += '<option value="Mexico">Mexico</option>';
	html += '<option value="Netherlands Antilles">Netherlands Antilles</option>';
	html += '<option value="Nicaragua">Nicaragua</option>';
	html += '<option value="Panama">Panama</option>';
	html += '<option value="Puerto Rico">Puerto Rico</option>';
	html += '<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>';
	html += '<option value="Saint Lucia">Saint Lucia</option>';
	html += '<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>';
	html += '<option value="Trinidad and Tobago">Trinidad and Tobago</option>';
	html += '<option value="United States">United States</option>';
	html += '</optgroup>';

	html += '<optgroup label="South America">';
	html += '<option value="Argentina">Argentina</option>';
	html += '<option value="Bolivia">Bolivia</option>';
	html += '<option value="Brazil">Brazil</option>';
	html += '<option value="Chile">Chile</option>';
	html += '<option value="Colombia">Colombia</option>';
	html += '<option value="Ecuador">Ecuador</option>';
	html += '<option value="Guyana">Guyana</option>';
	html += '<option value="Paraguay">Paraguay</option>';
	html += '<option value="Peru">Peru</option>';
	html += '<option value="Suriname">Suriname</option>';
	html += '<option value="Uruguay">Uruguay</option>';
	html += '<option value="Venezuela">Venezuela</option>';
	html += '</optgroup>';

	html += '<optgroup label="Europe">';
	html += '<option value="Albania">Albania</option>';
	html += '<option value="Andorra">Andorra</option>';
	html += '<option value="Armenia">Armenia</option>';
	html += '<option value="Austria">Austria</option>';
	html += '<option value="Azerbaijan">Azerbaijan</option>';
	html += '<option value="Belarus">Belarus</option>';
	html += '<option value="Belgium">Belgium</option>';
	html += '<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>';
	html += '<option value="Bulgaria">Bulgaria</option>';
	html += '<option value="Croatia">Croatia</option>';
	html += '<option value="Cyprus">Cyprus</option>';
	html += '<option value="Czech Republic">Czech Republic</option>';
	html += '<option value="Denmark">Denmark</option>';
	html += '<option value="Estonia">Estonia</option>';
	html += '<option value="Faroe Islands">Faroe Islands</option>';
	html += '<option value="Finland">Finland</option>';
	html += '<option value="France">France</option>';
	html += '<option value="Georgia">Georgia</option>';
	html += '<option value="Germany">Germany</option>';
	html += '<option value="Greece">Greece</option>';
	html += '<option value="Hungary">Hungary</option>';
	html += '<option value="Iceland">Iceland</option>';
	html += '<option value="Ireland">Ireland</option>';
	html += '<option value="Italy">Italy</option>';
	html += '<option value="Latvia">Latvia</option>';
	html += '<option value="Liechtenstein">Liechtenstein</option>';
	html += '<option value="Lithuania">Lithuania</option>';
	html += '<option value="Luxembourg">Luxembourg</option>';
	html += '<option value="Macedonia">Macedonia</option>';
	html += '<option value="Malta">Malta</option>';
	html += '<option value="Moldova">Moldova</option>';
	html += '<option value="Monaco">Monaco</option>';
	html += '<option value="Montenegro">Montenegro</option>';
	html += '<option value="Netherlands">Netherlands</option>';
	html += '<option value="Norway">Norway</option>';
	html += '<option value="Poland">Poland</option>';
	html += '<option value="Portugal">Portugal</option>';
	html += '<option value="Romania">Romania</option>';
	html += '<option value="San Marino">San Marino</option>';
	html += '<option value="Serbia">Serbia</option>';
	html += '<option value="Slovakia">Slovakia</option>';
	html += '<option value="Slovenia">Slovenia</option>';
	html += '<option value="Spain">Spain</option>';
	html += '<option value="Sweden">Sweden</option>';
	html += '<option value="Switzerland">Switzerland</option>';
	html += '<option value="Ukraine">Ukraine</option>';
	html += '<option value="United Kingdom">United Kingdom</option>';
	html += '<option value="Vatican City">Vatican City</option>';
	html += '</optgroup>';

	html += '<optgroup label="Asia">';
	html += '<option value="Afghanistan">Afghanistan</option>';
	html += '<option value="Bahrain">Bahrain</option>';
	html += '<option value="Bangladesh">Bangladesh</option>';
	html += '<option value="Bhutan">Bhutan</option>';
	html += '<option value="Brunei Darussalam">Brunei Darussalam</option>';
	html += '<option value="Myanmar">Myanmar</option>';
	html += '<option value="Cambodia">Cambodia</option>';
	html += '<option value="China">China</option>';
	html += '<option value="East Timor">East Timor</option>';
	html += '<option value="Hong Kong">Hong Kong</option>';
	html += '<option value="India">India</option>';
	html += '<option value="Indonesia">Indonesia</option>';
	html += '<option value="Iran">Iran</option>';
	html += '<option value="Iraq">Iraq</option>';
	html += '<option value="Israel">Israel</option>';
	html += '<option value="Japan">Japan</option>';
	html += '<option value="Jordan">Jordan</option>';
	html += '<option value="Kazakhstan">Kazakhstan</option>';
	html += '<option value="North Korea">North Korea</option>';
	html += '<option value="South Korea">South Korea</option>';
	html += '<option value="Kuwait">Kuwait</option>';
	html += '<option value="Kyrgyzstan">Kyrgyzstan</option>';
	html += '<option value="Laos">Laos</option>';
	html += '<option value="Lebanon">Lebanon</option>';
	html += '<option value="Malaysia">Malaysia</option>';
	html += '<option value="Maldives">Maldives</option>';
	html += '<option value="Mongolia">Mongolia</option>';
	html += '<option value="Nepal">Nepal</option>';
	html += '<option value="Oman">Oman</option>';
	html += '<option value="Pakistan">Pakistan</option>';
	html += '<option value="Palestine">Palestine</option>';
	html += '<option value="Philippines">Philippines</option>';
	html += '<option value="Qatar">Qatar</option>';
	html += '<option value="Russia">Russia</option>';
	html += '<option value="Saudi Arabia">Saudi Arabia</option>';
	html += '<option value="Singapore">Singapore</option>';
	html += '<option value="Sri Lanka">Sri Lanka</option>';
	html += '<option value="Syria">Syria</option>';
	html += '<option value="Taiwan">Taiwan</option>';
	html += '<option value="Tajikistan">Tajikistan</option>';
	html += '<option value="Thailand">Thailand</option>';
	html += '<option value="Turkey">Turkey</option>';
	html += '<option value="Turkmenistan">Turkmenistan</option>';
	html += '<option value="United Arab Emirates">United Arab Emirates</option>';
	html += '<option value="Uzbekistan">Uzbekistan</option>';
	html += '<option value="Vietnam">Vietnam</option>';
	html += '<option value="Yemen">Yemen</option>';
	html += '</optgroup>';

	html += '<optgroup label="Oceania">';
	html += '<option value="Australia">Australia</option>';
	html += '<option value="Fiji">Fiji</option>';
	html += '<option value="Kiribati">Kiribati</option>';
	html += '<option value="Marshall Islands">Marshall Islands</option>';
	html += '<option value="Micronesia">Micronesia</option>';
	html += '<option value="Nauru">Nauru</option>';
	html += '<option value="New Zealand">New Zealand</option>';
	html += '<option value="Palau">Palau</option>';
	html += '<option value="Papua New Guinea">Papua New Guinea</option>';
	html += '<option value="Samoa">Samoa</option>';
	html += '<option value="Solomon Islands">Solomon Islands</option>';
	html += '<option value="Tonga">Tonga</option>';
	html += '<option value="Tuvalu">Tuvalu</option>';
	html += '<option value="Vanuatu">Vanuatu</option>';
	html += '</optgroup>';

	html += '<optgroup label="Africa">';
	html += '<option value="Algeria">Algeria</option>';
	html += '<option value="Angola">Angola</option>';
	html += '<option value="Benin">Benin</option>';
	html += '<option value="Botswana">Botswana</option>';
	html += '<option value="Burkina Faso">Burkina Faso</option>';
	html += '<option value="Burundi">Burundi</option>';
	html += '<option value="Cameroon">Cameroon</option>';
	html += '<option value="Cape Verde">Cape Verde</option>';
	html += '<option value="Central African Republic">Central African Republic</option>';
	html += '<option value="Chad">Chad</option>';
	html += '<option value="Comoros">Comoros</option>';
	html += '<option value="Democratic Republic of the Congo">Democratic Republic of the Congo</option>';
	html += '<option value="Republic of the Congo">Republic of the Congo</option>';
	html += '<option value="Djibouti">Djibouti</option>';
	html += '<option value="Egypt">Egypt</option>';
	html += '<option value="Equatorial Guinea">Equatorial Guinea</option>';
	html += '<option value="Eritrea">Eritrea</option>';
	html += '<option value="Ethiopia">Ethiopia</option>';
	html += '<option value="Gabon">Gabon</option>';
	html += '<option value="Gambia">Gambia</option>';
	html += '<option value="Ghana">Ghana</option>';
	html += '<option value="Gibraltar">Gibraltar</option>';
	html += '<option value="Guinea">Guinea</option>';
	html += '<option value="Guinea-Bissau">Guinea-Bissau</option>';
	html += '<option value="Cote d\'Ivoire">Cote d\'Ivoire</option>';
	html += '<option value="Kenya">Kenya</option>';
	html += '<option value="Lesotho">Lesotho</option>';
	html += '<option value="Liberia">Liberia</option>';
	html += '<option value="Libya">Libya</option>';
	html += '<option value="Madagascar">Madagascar</option>';
	html += '<option value="Malawi">Malawi</option>';
	html += '<option value="Mali">Mali</option>';
	html += '<option value="Mauritania">Mauritania</option>';
	html += '<option value="Mauritius">Mauritius</option>';
	html += '<option value="Morocco">Morocco</option>';
	html += '<option value="Mozambique">Mozambique</option>';
	html += '<option value="Namibia">Namibia</option>';
	html += '<option value="Niger">Niger</option>';
	html += '<option value="Nigeria">Nigeria</option>';
	html += '<option value="Rwanda">Rwanda</option>';
	html += '<option value="Sao Tome and Principe">Sao Tome and Principe</option>';
	html += '<option value="Senegal">Senegal</option>';
	html += '<option value="Seychelles">Seychelles</option>';
	html += '<option value="Sierra Leone">Sierra Leone</option>';
	html += '<option value="Somalia">Somalia</option>';
	html += '<option value="South Africa">South Africa</option>';
	html += '<option value="Sudan">Sudan</option>';
	html += '<option value="Swaziland">Swaziland</option>';
	html += '<option value="United Republic of Tanzania">Tanzania</option>';
	html += '<option value="Togo">Togo</option>';
	html += '<option value="Tunisia">Tunisia</option>';
	html += '<option value="Uganda">Uganda</option>';
	html += '<option value="Zambia">Zambia</option>';
	html += '<option value="Zimbabwe">Zimbabwe</option>';
	html += '</optgroup>';

	return html;
}

function settingFieldOnLeftPanel(index){
	currently_editing = 'foli'+index;
	configureFieldSettingsOnLeftPanel();
}
