var imgpath;

$('#ml').draggable({
    stop: function (event, ui) {
        // event.toElement is the element that was responsible
        // for triggering this event. The handle, in case of a draggable.
        $(event.toElement).one('click', function (e) {
            e.stopImmediatePropagation();
        });
    }
});


function init(path)
{
    window.onresize = function ()
    {
        //      alert('The browser window was resized� or was it?');
    };
    $('#haupt').click(resetEditable);
//	$('#surveyForm').click(selectForm);
    $('#drag1_text').draggable({cursor: 'move', snap: false, stack: '#drag1_text', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag1_textarea').draggable({cursor: 'move', snap: false, stack: '#drag1_textarea', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag1_radio').draggable({cursor: 'move', snap: false, stack: '#drag1_radio', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag1_section').draggable({cursor: 'move', snap: false, stack: '#drag1_section', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag2_number').draggable({cursor: 'move', snap: false, stack: '#drag2_number', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag2_checkbox').draggable({cursor: 'move', snap: false, stack: '#drag2_checkbox', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag2_select').draggable({cursor: 'move', snap: false, stack: '#drag2_select', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag2_money').draggable({cursor: 'move', snap: false, stack: '#drag2_money', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag3_shortname').draggable({cursor: 'move', snap: false, stack: '#drag3_shortname', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag3_address').draggable({cursor: 'move', snap: false, stack: '#drag3_address', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag3_email').draggable({cursor: 'move', snap: false, stack: '#drag3_email', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag3_phone').draggable({cursor: 'move', snap: false, stack: '#drag3_phone', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag3_pagebreak').draggable({cursor: 'move', snap: false, stack: '#drag3_pagebreak', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag4_date').draggable({cursor: 'move', snap: false, stack: '#drag4_date', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag4_time').draggable({cursor: 'move', snap: false, stack: '#drag4_time', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag4_url').draggable({cursor: 'move', snap: false, stack: '#drag4_url', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag4_likert').draggable({cursor: 'move', snap: false, stack: '#drag4_likert', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag4_birth').draggable({cursor: 'move', snap: false, stack: '#drag4_birth', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag4_membergate').draggable({cursor: 'move', snap: false, stack: '#drag4_membergate', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag4_birthdate').draggable({cursor: 'move', snap: false, stack: '#drag4_birthdate', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag4_birthmonth').draggable({cursor: 'move', snap: false, stack: '#drag4_birthmonth', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag4_agerange').draggable({cursor: 'move', snap: false, stack: '#drag4_agerange', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag4_redeemcode').draggable({cursor: 'move', snap: false, stack: '#drag4_redeemcode', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag1_likert').draggable({cursor: 'move', snap: false, stack: '#drag1_likert', revert: true, revertDuration: 0, helper: 'original'});
    // Modify By Wood
    // Make the plain text button work with related function
    $('#drag1_plaintext').draggable({cursor: 'move', snap: false, stack: '#drag1_plaintext', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag4_limit').draggable({cursor: 'move', snap: false, stack: '#drag4_limit', revert: true, revertDuration: 0, helper: 'original'});
    // End Modify By Wood

    // Modified By Fireals
    $('#drag4_firstname').draggable({cursor: 'move', snap: false, stack: '#drag4_firstname', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag4_lastname').draggable({cursor: 'move', snap: false, stack: '#drag4_lastname', revert: true, revertDuration: 0, helper: 'original'});
    $('#drag4_sketch').draggable({cursor: 'move', snap: false, stack: '#drag4_sketch', revert: true, revertDuration: 0, helper: 'original'});
    // End Modified By Fireals
    var isDragging = false;
    var wasDragging = false;
    $("#modalFormBuilder .ui-draggable")
            .mousedown(function (event) {
//		alert(event.target.id);
                $(window).mousemove(function () {
                    isDragging = true;
                    $(window).unbind("mousemove");
                });
            })
            .mouseup(function (event) {
                var wasDragging = isDragging;
                isDragging = false;
                $(window).unbind("mousemove");
                //dragging
                if (wasDragging) {
                }
                else {
                    var tmpId = event.target.id;
                    if (tmpId.indexOf('_') == -1) {
                        var tmpEle = $('#' + event.target.id);
                        var tmpId = tmpEle.parent().attr('id');
                    }
                    else {
                        var tmpId = event.target.id;
                    }
                    var tmpArr = (tmpId).split('_');
                    var type = tmpArr[1];
                    addField(type);
                }
            });
    $('#haupt').droppable(
            {
                drop: function (event, ui) {
                    var tmpArr = (ui.draggable.prop('id')).split('_');
                    var type = tmpArr[1];
                    addField(type);
                    console.log('dragging-haupt')
                },
            });


    imgpath = path;
}

function replaceNewline(str, type)
{
    if (type.toLowerCase() == "display")
    {
        str = str.replace(/<br \/>/gi, "\r\n");
        str = str.replace(/<br>/gi, "\r\n");
    }
    else if (type.toLowerCase() == "html")
    {
        str = str.replace(/\r\n/g, "<br />");
        str = str.replace(/\n/g, "<br />");
    }
    return str;
}

function replaceHTML(str, type)
{
    if (type.toLowerCase() == "display")
    {
        str = str.replace(/&amp;/g, "&");
        str = str.replace(/&lt;/g, "<");
        str = str.replace(/&gt;/g, ">");
        str = str.replace(/&nbsp;/g, " ");
    }
    else if (type.toLowerCase() == "html")
    {
        str = str.replace(/&/g, "&amp;");
        str = str.replace(/</g, "&lt;");
        str = str.replace(/>/g, "&gt;");
        str = str.replace(/ /g, "&nbsp;");
    }
    return str;
}

function loadEachFieldToRightPanel(type, basic_arr, option_arr, likert_arr)
{
    var count = total_count;
    total_count++;
    var id = "foli" + count;
    var divId = "itemOnPage" + count;
    var field_html = "";
    var loadClass = false;
    var isLikert = false;
    var isAddress = false;
    var fieldDiv = document.createElement("div");
    fieldDiv.id = divId;
    fieldDiv.className = 'form-group-edit col-sm-12 col-xs-12 nopad';
    fieldDiv.style.display = 'block';
    var fieldUl = document.createElement("ul");
    var field = document.createElement("li");
    var labelInnerHtml = '';
    fieldLabel = document.createElement("label");
    fieldLabel.id = 'title' + count;
    fieldLabel.for = 'Field' + count;
    if (type == 'likert')
    {
        fieldLabel.className = 'cus_title';
    } else {
        fieldLabel.className = 'frmquestion cstm_frmquestion desc-edit desc';
    }
    labelInnerHtml += basic_arr[0];
    if (basic_arr[1] == 1) {
        labelInnerHtml += '<span class="req" id="req_' + count + '">*</span>';
    }
    else {
        labelInnerHtml += '<span class="req" id="req_' + count + '"></span>';
    }
//	if(type!='pagebreak'){
    if ($.inArray(type, ['pagebreak', 'plaintext']) < 0) {
        fieldLabel.innerHTML = labelInnerHtml;
    }
    field.id = id;
    field.className = ' form-group-edit col-sm-12 col-xs-12 nopad ';
    field.style.cursor = 'pointer';
    field.style.top = '0px';
    field.style.left = '0px';
    field.style.left = '0px';
    field.title = 'Click to edit. Drag to reorder.';
    field.zIndex = '100';
    var style = "";
    field_html += '<div class="floatDiv" onclick="settingFieldOnLeftPanel(\'' + count + '\');">';
    field_html += '</div>';
    if (type == "text")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'medium';
        }
        // basic_arr[3] = basic_arr[3].replace(/\"/g, "\\\"");
        // alert(basic_arr[3].replace(/\"/g, "\\\""));
        // var show_val = basic_arr[3]
        //var show_val = basic_arr[3].replace(/\"/g, "\\\"");
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
//		field_html += '<div >' +
//		field_html += '</div>' +
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="" class="field text ' + style + '" name="Field' + count + '" id="Field' + count + '">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1],
            size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}
            , length: basic_arr[5], formid: basic_arr[6], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "membergate")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'medium';
        }
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="" class="field text ' + style + '" name="Field' + count + '" id="Field' + count + '">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1],
            size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}
            , length: basic_arr[5], formid: basic_arr[6], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "redeemcode")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'medium';
        }
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="" class="field text ' + style + '" name="Field' + count + '" id="Field' + count + '">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1],
            size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}
            , length: basic_arr[5], formid: basic_arr[6], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "number")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'medium';
        }

        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="' + basic_arr[3] + '" class="field text ' + style + '" name="Field' + count + '" id="Field' + count + '">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}
            , length: basic_arr[5], formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "textarea")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'small';
        }
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<div class="handle"></div>';
        field_html += '<textarea disabled="disabled" readonly="readonly" cols="50" rows="10" class="field textarea ' + style + '" name="Field' + count + '" id="Field' + count + '">' + basic_arr[3] + '</textarea>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}
            , length: basic_arr[5], formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "sketch")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'small';
        }
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<div class="handle"></div>';
        field_html += '<canvas id="simple_sketch" style="width:380px;background-color:white;"></canvas>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}
            , length: basic_arr[5], formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "checkbox")
    {
//        alert('hmm');
        if (basic_arr[2] == "" || basic_arr[2] == "oneColumn")
        {
            style = "";
        }
        else
        {
            loadClass = true;
            style = basic_arr[2];
        }
        if (basic_arr[3] != "")
        {
            var str = "," + basic_arr[3] + ",";
        }
        var choice_list = new Array();
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        var otherIndex = 100;
        for (var i = 0; i < option_arr['choices'].length; i++)
        {
            var checked = 0;
            if (option_arr['selected'][i] == 1) {
                checked = 1;
            }
            field_html += '<span>';
            if (option_arr['choices'][i].indexOf("##_##") != -1) {
                field_html += '<input style="display:inline-block;" type="checkbox" id="Field' + count + '-' + otherIndex + '" name="Field' + count + '-' + (i + 1) + '"';
                otherIndex++;
            }
            else {
                field_html += '<input style="display:inline-block;" type="checkbox" id="Field' + count + '-' + (i + 1) + '" name="Field' + count + '-' + (i + 1) + '"';
            }
//			field_html += '<input style="display:inline-block;" type="checkbox" id="Field'+count+'-' + (i+1) + '" name="Field'+count+'-' + (i+1) + '"';
            if (checked == 1)
            {
                field_html += ' checked="checked"';
            }
            field_html += ' class="field checkbox" value="' + (i + 1) + '" readonly="readonly" disabled="disabled">';
            if (option_arr['choices'][i].indexOf("##_##") != -1) {
                var labelArr = new Array();
                labelArr = option_arr['choices'][i].split("##_##");
                field_html += '<label style="display:inline;margin-left: 10px;" class="frmchoice" for="Field' + count + '-100">' + labelArr[0] + '</label>';
                field_html += '<input style="display:inline-block;;width:40%;height:16px;" type="text" value="" id="Field' + count + '_otherContent' + (i + 1) + '" name="Field' + field.count + '_otherContent"></input>';
                field_html += '<span id="otherReq' + count + '" class="req" style="float:none;width:5px;margin:0px;">';
                if (option_arr['otherRequired'][i] == 1) {
                    field_html += '&nbsp;*';
                }
                field_html += '</span>';
                field_html += '</span>';
            }
            else {
                field_html += '<label class="frmchoice" for="Field' + count + '-' + (i + 1) + '">' + option_arr['choices'][i] + '</label>';
            }
//			field_html += '<label style="display:inline-block;" class="choice" for="Field'+count+'-' + (i+1) + '">' + option_arr['choices'][i] + '</label>';
            field_html += '</span>';
            choice_list[i] = {id: (i + 1), label: option_arr['choices'][i], selected: checked, valueid: option_arr['id'][i], choiceid: option_arr['choiceid'][i], otherRequired: option_arr['otherRequired'][i]};
        }
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'small', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: style, likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "radio")
    {
        if (basic_arr[2] == "" || basic_arr[2] == "oneColumn")
        {
            style = "";
        }
        else
        {
            loadClass = true;
            style = basic_arr[2];
        }
        var choice_list = new Array();
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        for (var i = 0; i < option_arr['choices'].length; i++)
        {
            var checked = 0;
            if (option_arr['selected'][i] == 1) {
                checked = 1;
            }
            field_html += '<span>';
            if (option_arr['choices'][i].indexOf("##_##") != -1) {
                field_html += '<input style="display:inline-block;" type="radio" id="Field' + count + '-100" name="Field' + count + '-' + (i + 1) + '"';
            }
            else {
                field_html += '<input style="display:inline-block;" type="radio" id="Field' + count + '-' + (i + 1) + '" name="Field' + count + '-' + (i + 1) + '"';
            }
//			field_html += '<input type="radio" id="Field'+count+'-' + (i+1) + '" name="Field'+count+'-' + (i+1) + '"';
            if (checked == 1)
            {
                field_html += ' checked="checked"';
            }
            field_html += ' class="field radio" value="' + (i + 1) + '" readonly="readonly" disabled="disabled">';
            if (option_arr['choices'][i].indexOf("##_##") != -1) {
                var labelArr = new Array();
                labelArr = option_arr['choices'][i].split("##_##");
                field_html += '<label style="display:inline;margin-left: 10px;" class="frmchoice" for="Field' + count + '-100">' + labelArr[0] + '</label>';
                field_html += '<input style="display:inline-block;;width:40%;height:16px;" type="text" value="" id="Field' + count + '_otherContent" name="Field' + field.count + '_otherContent"></input>';
                field_html += '<span id="otherReq' + count + '" class="req" style="float:none;width:5px;margin:0px;">';
                if (option_arr['otherRequired'][i] == 1) {
                    field_html += '&nbsp;*';
                }
                field_html += '</span>';
//				field_html += '</span>';
            }
            else {
                field_html += '<label class="frmchoice" for="Field' + count + '-' + (i + 1) + '">' + option_arr['choices'][i] + '</label>';
            }
//			field_html += '<label class="frmchoice" for="Field'+count+'-' + (i+1) + '">' + option_arr['choices'][i] + '</label>';
            field_html += '</span>';
            choice_list[i] = {id: (i + 1), label: option_arr['choices'][i], selected: checked, valueid: option_arr['id'][i], choiceid: option_arr['choiceid'][i], otherRequired: option_arr['otherRequired'][i]};
        }
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'small', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: style, likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "agerange")
    {
        if (basic_arr[2] == "" || basic_arr[2] == "oneColumn")
        {
            style = "";
        }
        else
        {
            loadClass = true;
            style = basic_arr[2];
        }
        var choice_list = new Array();
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        for (var i = 0; i < option_arr['choices'].length; i++)
        {
            var checked = 0;
            if (option_arr['selected'][i] == 1) {
                checked = 1;
            }
            field_html += '<span>';
            field_html += '<input style="display:inline-block;" type="radio" id="Field' + count + '-' + (i + 1) + '" name="Field' + count + '-' + (i + 1) + '"';
//			field_html += '<input type="radio" id="Field'+count+'-' + (i+1) + '" name="Field'+count+'-' + (i+1) + '"';
            if (checked == 1)
            {
                field_html += ' checked="checked"';
            }
            field_html += ' class="field radio" value="' + (i + 1) + '" readonly="readonly" disabled="disabled">';
            field_html += '<label class="frmchoice" for="Field' + count + '-' + (i + 1) + '">' + option_arr['choices'][i] + '</label>';
            field_html += '</span>';
            choice_list[i] = {id: (i + 1), label: option_arr['choices'][i], selected: checked, valueid: option_arr['id'][i], choiceid: option_arr['choiceid'][i], otherRequired: option_arr['otherRequired'][i]};
        }
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'small', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: style, likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "select")
    {
//        alert('hmm');

        var choice_list = new Array();
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<select id="Field' + count + '" name="Field' + count + '" class="field select medium" readonly="readonly" disabled="disabled">';
        for (var i = 0; i < option_arr['choices'].length; i++)
        {
            var checked = 0;
            if (option_arr['selected'][i] == 1) {
                checked = 1;
            }
            field_html += '<option id="Field' + count + '-' + (i + 1) + '" value="' + (i + 1) + '"';
            if (checked == 1)
            {
                field_html += ' selected="selected"';
            }
            field_html += '>' + option_arr['choices'][i] + '</option>';
            choice_list[i] = {id: (i + 1), label: option_arr['choices'][i], selected: checked, valueid: option_arr['id'][i], choiceid: option_arr['choiceid'][i], otherRequired: option_arr['otherRequired'][i]};
        }
        field_html += '</select>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "limit")
    {
//        alert('hmm');

        var choice_list = new Array();
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<select id="Field' + count + '" name="Field' + count + '" class="field select medium" readonly="readonly" disabled="disabled">';
        for (var i = 0; i < option_arr['choices'].length; i++)
        {
            var checked = 0;
            if (option_arr['selected'][i] == 1) {
                checked = 1;
            }
            field_html += '<option id="Field' + count + '-' + (i + 1) + '" value="' + (i + 1) + '"';
            if (checked == 1)
            {
                field_html += ' selected="selected"';
            }
            field_html += '>' + option_arr['choices'][i] + '</option>';
            choice_list[i] = {id: (i + 1), label: option_arr['choices'][i], selected: checked, quota: option_arr['quota'][i], maxquota: option_arr['maxquota'][i], valueid: option_arr['id'][i], choiceid: option_arr['choiceid'][i], otherRequired: option_arr['otherRequired'][i]};
        }
        field_html += '</select>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    /*
     else if(type == "section")
     {
     //        alert('hmm');
     field.className = 'section';
     field_html = '<h3 id="title'+count+'">' +
     'Section Break' +
     '</h3>' +
     '<div class="" id="instruct'+count+'">' +
     'A description of the section goes here.' +
     '</div>' +
     '<div class="fieldActions" id="fa'+count+'">' +
     '<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="/img/template-editor/add.png" class="faDup">' +
     '<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="/img/template-editor/delete.png" class="faDel">' +
     '</div>';
     fields.push({id: id, count: count, type: type, label: 'Section Break', required: '', size: 'medium', duplicate: 0, defaultValue: 'A description of the section goes here.', choices: [], date_format: basic_arr[4], columns: '', likert: {} });
     }
     */

    else if (type == "birth")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        if (basic_arr[4] == 'YYYY-MM-DD') {
            field_html += '<span class="time birthThreeColumn">';
            field_html += '<select id="Field' + count + '-3" name="Field' + count + '-3" class="field select birthSelect" readonly="readonly" disabled="disabled">';
            field_html += '<option value="">YYYY</option>';
            for (var i = 2013; i > 1900; i--)
            {
                field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + 'YYYY</option>';
            }
            field_html += '</select>';
//			field_html += '<label for="Field'+count+'-3">YYYY-</label>';
            field_html += '</span>';
            field_html += '<span class="time birthThreeColumn">';
            field_html += '<select id="Field' + count + '-1" name="Field' + count + '-1" class="field select birthSelect" readonly="readonly" disabled="disabled">';
            field_html += '<option value="">DD</option>';
            for (var i = 1; i < 32; i++)
            {
                field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
            }
            field_html += '</select>';
//			field_html += '<label for="Field'+count+'-1">MM-</label>';
            field_html += '</span>';
            field_html += '<span class="time birthThreeColumn">';
            field_html += '<select id="Field' + count + '-2" name="Field' + count + '-2" class="field select birthSelect" readonly="readonly" disabled="disabled">';
            field_html += '<option value="">MM</option>';
            for (var i = 1; i < 13; i++)
            {
                field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
            }
            field_html += '</select>';
//			field_html += '<label for="Field'+count+'-2">DD</label>';
            field_html += '</span>';
        }
        else {
            field_html += '<span class="time birthTwoColumn">';
            field_html += '<select id="Field' + count + '-1" name="Field' + count + '-1" class="field select birthSelect" readonly="readonly" disabled="disabled">';
            field_html += '<option value="">DD</option>';
            for (var i = 1; i < 32; i++)
            {
                field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
            }
            field_html += '</select>';
            //		field_html += '<label for="Field'+count+'-1">MM-</label>';
            field_html += '</span>';
            field_html += '<span class="time birthTwoColumn">';
            field_html += '<select id="Field' + count + '-2" name="Field' + count + '-2" class="field select mmSelect" readonly="readonly" disabled="disabled">';
            field_html += '<option value="">MM</option>';
            for (var i = 1; i < 13; i++)
            {
                field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
            }
            field_html += '</select>';
            //		field_html += '<label for="Field'+count+'-2">DD</label>';
            field_html += '</span>';
        }
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: '', likert: {}, fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "birthdate")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-1" name="Field' + count + '-1" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 1; i < 32; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '</span>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: '', likert: {}, fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "birthmonth")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-2" name="Field' + count + '-2" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 1; i < 13; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '<label for="Field' + count + '-2"></label>';
        field_html += '</span>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: '', likert: {}, fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "shortname")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
        field_html += '</label>';
        field_html += '<div class="frmquestion cstm_frmquestion">';
        field_html += '<span class="shortname" style="width: 46%; min-width: 85px;">';
        field_html += '<input style="width:80%;" type="text" disabled="disabled" placeholder="First" readonly="readonly" size="14" value="" class="field text fn" name="Field' + count + '-1" id="Field' + count + '-1">';
        field_html += '<label for="Field' + count + '-1" style="display:none;">First</label>';
        field_html += '</span>';
        field_html += '<span  class="symbol"  style="width:1%;height:1px;"></span>';
        field_html += '<span  class="shortname"  style="width: 47%; min-width: 85px;">';
        field_html += '<input type="text" disabled="disabled" placeholder="Last" readonly="readonly" size="14" value="" class="field text ln" name="Field' + count + '-2" id="Field' + count + '-2">';
        field_html += '<label for="Field' + count + '-2" style="display:none;">Last</label>';
        field_html += '</span>';
        field_html += '</div>';
//		field_html += '<br><br>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "firstname")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" size="14" value="" class="field text fn" name="Field' + count + '-1" id="Field' + count + '-1">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "lastname")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" size="14" value="" class="field text fn" name="Field' + count + '-1" id="Field' + count + '-1">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "date")
    {
//		var tmp_arr = basic_arr[4].split("/");
//		var date_format = 0;
//		if(basic_arr[4] == "DD/MM/YYYY")
//		{
//			date_format = 1;
//		}
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		field_html +='<span class="req" id="req_'+count+'">' + basic_arr[1] + '</span>';
        field_html += '</label>';
        field_html += '<span class="date">';
        field_html += '<input id="Field' + count + '" name="Field' + count + '" class="field text" value="" size="14" maxlength="10" readonly="readonly" disabled="disabled" type="text">';
        field_html += '<label for="Field' + count + '">' + basic_arr[4] + '</label>';
        field_html += '</span>';
        field_html += '<span id="cal' + count + '">';
        field_html += '<img id="pick' + count + '" class="datepicker" src="' + imgpath + '/img/template-editor/calendar.png" alt="Pick a date.">';
        field_html += '</span>';
        field_html += '<br><br>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "email")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'medium';
        }
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0]
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="' + basic_arr[3] + '" class="field text ' + style + '" name="Field' + count + '" id="Field' + count + '">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "url")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'medium';
        }
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="' + basic_arr[3] + '" class="field text ' + style + '" name="Field' + count + '" id="Field' + count + '">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: style, duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "time")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-1" name="Field' + count + '-1" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 1; i < 13; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '<label for="Field' + count + '-1">HH</label>';
        field_html += '</span>';
        field_html += '<span class="symbol">:</span>';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-2" name="Field' + count + '-2" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 0; i < 60; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '<label for="Field' + count + '-2">MM</label>';
        field_html += '</span>';
        field_html += '<span class="symbol">:</span>';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-3" name="Field' + count + '-3" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 0; i < 60; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '<label for="Field' + count + '-3">SS</label>';
        field_html += '</span>';
        field_html += '<span class="ampm">';
        field_html += '<select id="Field' + count + '-4" name="Field' + count + '-4" class="field select" style="width: 4em;" readonly="readonly" disabled="disabled">';
        field_html += '<option value="AM" selected="selected">AM</option>';
        field_html += '<option value="PM">PM</option>';
        field_html += '</select>';
        field_html += '<label for="Field' + count + '-4">AM/PM</label>';
        field_html += '</span>';
        field_html += '<br><br>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "phone")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        var mobileWidth = 'style="width:90%;"';
        if (basic_arr[11] == "1") {
            field_html += '<select disabled="disabled" readonly="readonly" id="areacode' + count + '" name="areacode' + count + '" style="margin-right:5px;"><option>Area Code</option></select>';
            mobileWidth = 'style="width:70%;"';
        }
        field_html += '<input ' + mobileWidth + ' type="text" disabled="disabled" readonly="readonly" maxlength="255" value="' + basic_arr[3] + '" ';
        field_html += 'class="field text medium" name="Field' + count + '" id="Field' + count + '">';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        //areacode
        var choice_list = new Array();
        for (var i = 0; i < option_arr['choices'].length; i++)
        {
            var checked = 0;
            if (option_arr['selected'][i] == 1) {
                checked = 1;
            }
            choice_list[i] = {id: (i + 1), label: option_arr['choices'][i], selected: checked, valueid: option_arr['id'][i], choiceid: option_arr['choiceid'][i], otherRequired: option_arr['otherRequired'][i]};
        }
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: basic_arr[3], choices: choice_list, date_format: basic_arr[4], columns: '', likert: {}
            , length: basic_arr[5], formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "money")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<span class="symbol">$</span>';
        field_html += '<span class="shortname">';
        field_html += '<input type="text" id="Field' + count + '-1" name="Field' + count + '-1" class="field text currency" value="' + basic_arr[3] + '" size="10" readonly="readonly" disabled="disabled">';
        field_html += '<label for="Field' + count + '-1">Dollars</label>';
        field_html += '</span>';
        field_html += '<span class="symbol">.</span>';
        field_html += '<span class="shortname">';
        field_html += '<input type="text" id="Field' + count + '-2" name="Field' + count + '-2" class="field text" value="" size="2" maxlength="2" readonly="readonly" disabled="disabled">';
        field_html += '<label for="Field' + count + '-2">Cents</label>';
        field_html += '</span>';
        field_html += '<br><br>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "address")
    {
        if (basic_arr[3] != "")
        {
            isAddress = true;
        }
        field.className += " complex";
        field.style.display = "block";
        field.style.height = "240px";
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion desc-edit desc">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<span class="full left">';
        field_html += '<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">';
        field_html += '<label for="Field">Street Address</label>';
        field_html += '</span>';
        field_html += '<span class="full left">';
        field_html += '<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">';
        field_html += '<label for="Field">Address Line 2</label>';
        field_html += '</span>';
        field_html += '<span class="full left">';
        field_html += '<span class="left">';
        field_html += '<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">';
        field_html += '<label for="Field">City</label>';
        field_html += '</span>';
        field_html += '<span class="right">';
        field_html += '<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">';
        field_html += '<label for="Field">State / Province / Region</label>';
        field_html += '</span>';
        field_html += '</span>';
        field_html += '<span class="full left">';
        field_html += '<span class="left">';
        field_html += '<input id="Field" name="Field" class="field text addr" value="" maxlength="15" readonly="readonly" disabled="disabled" type="text">';
        field_html += '<label for="Field">Postal / Zip Code</label>';
        field_html += '</span>';
        field_html += '<span class="right">';
        field_html += '<select id="Field" name="Field" class="field select addr" readonly="readonly" disabled="disabled">';
        field_html += '<option value="" selected="selected"></option>';
        field_html += option_countries();
        field_html += '</select>';
        field_html += '<label for="Field">Country</label>';
        field_html += '</span>';
        field_html += '</span>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "pagebreak") {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion">';
        //field_html += basic_arr[0];
        //field_html += '<span class="req" id="req_'+count+'">' + basic_arr[1] + '</span>';
        field_html += '---------Page x of y---------';
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<input type="button" id="backpage" name="backpage" value="Back">&nbsp;<input type="button" id="nextpage" name="nextpage" value="Next">';
        field_html += '</div>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: basic_arr[3], choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    else if (type == "likert")
    {
        var statement_list = new Array();
        //selected
        if (basic_arr[3] == "")
        {
            basic_arr[3] = -1;
        }

        if (basic_arr[3] > -1)
        {
            isLikert = true;
        }

        field.className += " likert col" + option_arr['choices'].length;

        field_html += '<label for="Field' + count + '" id="title' + count + '" class="cus_title">';
//		field_html += basic_arr[0];
//		if(basic_arr[1]==1){
//			field_html += '<span class="req" id="req_'+count+'">*</span>';	
//		}
//		else{
//			field_html += '<span class="req" id="req_'+count+'"></span>';
//		}
        field_html += '</label></br></br>';

        field_html += '<table cellspacing="0">';
        // field_html += '<caption id="title'+count+'">';
        // field_html += basic_arr[0];
        // field_html += '<span class="req" id="req_'+count+'">' + basic_arr[1] + '</span>';
        // field_html += '</caption>';
        field_html += '<thead>';
        field_html += '<tr>';
        field_html += '<th>&nbsp;</th>';
        var originalColumns = new Array();
        for (var i = 0; i < option_arr['choices'].length; i = i + likert_arr.length)
        {
            originalColumns.push(option_arr['choices'][i]);
        }
        for (var i = 0; i < originalColumns.length; i++)
        {
            field_html += '<td>' + originalColumns[i] + '</td>';
        }
        field_html += '</tr>';
        field_html += '</thead>';
        field_html += '<tbody>';
        var x_index = 0;
        var likert_num = likert_arr.length;
        for (var i = 0; i < likert_arr.length; i++)
        {
            if (i % 2 == 0)
            {
                field_html += '<tr class="">';
            }
            else
            {
                field_html += '<tr class="alt">';
            }
            field_html += '<th><label for="Field' + (i + 1) + '">' + likert_arr[i]['label'] + '</label></th>';
            for (var j = 0; j < originalColumns.length; j++)
            {
                field_html += '<td title="' + originalColumns[j] + '">';
                if (option_arr['selected'][x_index] == "1")
                {
                    field_html += '<input type="radio" name="Field' + (j + 1) + '" id="Field' + count + '-' + (j + 1) + ' checked="checked" readonly="readonly" disabled="disabled">';
                } else {
                    field_html += '<input type="radio" name="Field' + (j + 1) + '" id="Field' + count + '-' + (j + 1) + ' readonly="readonly" disabled="disabled">';
                }
                field_html += '<label for="Field' + count + '-' + (j + 1) + '">' + (j + 1) + '</label>';
                field_html += '</td>';
                x_index++;
            }
            field_html += '</tr>';
            statement_list[i] = {likertid: likert_arr[i]['id'], label: likert_arr[i]['label'], valueid: likert_arr[i]['vid']};
        }
        choice_list = new Array();
        for (var i = 0; i < option_arr['choices'].length; i++)
        {
            var checked = 0;
            if (option_arr['selected'][i] == 1) {
                checked = 1;
            }
            choice_list[i] = {id: (i + 1), label: option_arr['choices'][i], selected: checked, valueid: option_arr['id'][i], choiceid: option_arr['choiceid'][i], otherRequired: option_arr['otherRequired'][i], likertid: option_arr['likertid'][i]};
        }
        field_html += '</tbody>';
        field_html += '</table>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
//		option_obj = toObject(option_arr);
//		console.log(option_obj);
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: basic_arr[1], size: 'medium', duplicate: 0, defaultValue: '', choices: choice_list, date_format: basic_arr[4], columns: '', likert: {statements: statement_list, columns: originalColumns, selected: basic_arr[3], originalStatementCount: statement_list.length}, formid: basic_arr[6], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16], });
    }
    // Modify By Wood
    // Draw the plain text interface for load up web form
    else if (type == "plaintext")
    {
        if (basic_arr[2] != "")
        {
            style = basic_arr[2];
        }
        else
        {
            style = 'medium';
        }
        field_html += '<div id="Field' + count + '" class="' + style + '" style="font-size:15px;">';
        field_html += basic_arr[0];
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: basic_arr[0], required: '', size: style, duplicate: 0, defaultValue: '', choices: [], date_format: basic_arr[4], columns: '', likert: {}, formid: basic_arr[6], surveyid: basic_arr[7], fieldid: basic_arr[8], uniqueCheck: basic_arr[9], errorMessage: basic_arr[10], addAreaCode: basic_arr[11], uniqueErrorMsg: basic_arr[12], validErrorMsg: basic_arr[13], contactAttribute: basic_arr[14], compareType: basic_arr[15], attributeCompareErrorMsg: basic_arr[16]});
    }
    // End Modify By Wood
    else
    {
        return;
    }
    field.innerHTML = field_html;
    fieldUl.appendChild(field);
    fieldDiv.appendChild(fieldLabel);
    fieldDiv.appendChild(fieldUl);
    var formFields = document.getElementById('formFields');
    formFields.appendChild(fieldDiv);
//	formFields.appendChild(field);
    if ((type == "text" || type == "number" || type == "textarea" || type == "email" || type == "url") && basic_arr[3] != "")
    {
        document.getElementById('Field' + count).value = basic_arr[3];
    }
    $('li#' + id).click(makeEditable);
//	$('#Field'+count).mousedown(function(){
//		alert('123');
//	});
    if (loadClass)
    {
        $('#' + id).addClass(style);
    }
    bindDragEvents(divId);

    if (isLikert)
    {
        generateLikert(id);
    }
    else if (isAddress)
    {
        all_options = document.getElementById(id).getElementsByTagName('option');
        for (var i = 0; i < all_options.length; i++)
        {
            if (all_options[i].value == basic_arr[3])
            {
                all_options[i].selected = true;
                break;
            }
        }
    }
}


function addField(type)
{
    var count = total_count;
    total_count++;
    var id = "foli" + count;
    var field_html;
    var divId = "itemOnPage" + count;
    var field_html = "";
    var loadClass = false;
    var isLikert = false;
    var isAddress = false;
    var fieldDiv = document.createElement("div");
    fieldDiv.id = divId;
    fieldDiv.className = 'form-group-edit col-sm-12 col-xs-12 nopad';
    fieldDiv.style.display = 'block';
    var fieldUl = document.createElement("ul");
    var field = document.createElement("li");
    var field_label_html = '';
    var labelInnerHtml = '';
    var field_name = '';
    switch (type) {
        case 'text':
            field_name = 'Untitled';
            break;
        case 'number':
            field_name = 'Number';
            break;
        case 'textarea':
            field_name = 'Untitled';
            break;
        case 'checkbox':
            field_name = 'Untitled';
            break;
        case 'radio':
            field_name = 'Untitled';
            break;
        case 'select':
            field_name = 'Select a Choice';
            break;
        case 'limit':
            field_name = 'Select a Choice';
            break;
        case 'shortname':
            field_name = 'Name';
            break;
        case 'firstname':
            field_name = 'First Name';
            break;
        case 'lastname':
            field_name = 'Last Name';
            break;
        case 'date':
            field_name = 'Date';
            break;
        case 'email':
            field_name = 'Email';
            break;
        case 'url':
            field_name = 'Web Site';
            break;
        case 'time':
            field_name = 'Time';
            break;
        case 'phone':
            field_name = 'Mobile Number';
            break;
        case 'money':
            field_name = 'Price';
            break;
        case 'address':
            field_name = 'Address';
            break;
        case 'likert':
            field_name = 'Evaluate the following statements';
            break;
        case 'birthdate':
            field_name = 'Birth Date';
            break;
        case 'birthmonth':
            field_name = 'Birth Month';
            break;
        case 'birth':
            field_name = 'Birthday';
            break;
        case 'agerange':
            field_name = 'Age Range';
            break;
        case 'membergate':
            field_name = 'Member Gate';
            break;
        case 'redeemcode':
            field_name = 'Redeem Code';
            break;
        case 'pagebreak':
            field_name = '';
            break;
        case 'sketch':
            field_name = 'Signature';
            break;
        default:
            field_name = 'Untitled';
            break;
    }
    var floatDiv = document.createElement('div');
    floatDiv.class = 'floatDiv';
    floatDiv.onclick = '"settingFieldOnLeftPanel(\'' + count + '\');"';
    field_html += '<div class="floatDiv" onclick="settingFieldOnLeftPanel(\'' + count + '\');">';
    field_html += '</div>';
    labelInnerHtml += field_name;
    fieldLabel = document.createElement("label");
    var labelClass = 'frmquestion cstm_frmquestion desc-edit desc';
    if (type == 'likert')
    {
        labelClass = 'cus_title';
    }
    fieldLabel.className = labelClass;
    fieldLabel.id = 'title' + count;
    fieldLabel.for = 'Field' + count;
    fieldLabel.innerHTML = field_name;
    requiredSpan = document.createElement("span");
    requiredSpan.id = "req_" + count;
    requiredSpan.class = "req";
    fieldLabel.appendChild(requiredSpan);
    field.id = id;
    field.className = ' form-group-edit col-sm-12 col-xs-12 nopad ';
    field.style.cursor = 'pointer';
    field.style.top = '0px';
    field.style.left = '0px';
    field.title = 'Click to edit. Drag to reorder.';
    field.zIndex = '100';

    if (type == "text")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="" ' +
                'class="field text medium" name="Field' + count + '" id="Field' + count + '">' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Untitled', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Untitled';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "membergate")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="" ' +
                'class="field text medium" name="Field' + count + '" id="Field' + count + '">' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Member Gate', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', contactAttribute: 'mobilephone', compareType: 'contains', attributeCompareErrorMsg: '', uniqueCheck: 0});
        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Untitled';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "redeemcode")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="" ' +
                'class="field text medium" name="Field' + count + '" id="Field' + count + '">' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Redeem Code', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', contactAttribute: 'mobilephone', compareType: 'contains', attributeCompareErrorMsg: '', uniqueCheck: 0});
        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Untitled';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "number")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="" ' +
                'class="field text medium" name="Field' + count + '" id="Field' + count + '">' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Number', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', validErrorMsg: 'Please fill valid information', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Number';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "textarea")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<div class="handle"></div>' +
                '<textarea disabled="disabled" readonly="readonly" cols="50" rows="10" ' +
                'class="field textarea small" name="Field' + count + '" id="Field' + count + '"></textarea>' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Untitled', required: '', size: 'small', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Untitled';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "sketch")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<div class="handle"></div>' +
//					'<textarea disabled="disabled" readonly="readonly" cols="50" rows="10" ' +
//					'class="field textarea small" name="Field'+count+'" id="Field'+count+'"></textarea>' +
                '<canvas id="simple_sketch" style="width:380px;background-color:white;"></canvas>' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Signature', required: '', size: 'small', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Untitled';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "checkbox")
    {
//        alert('hmm');
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<span>' +
                '<input   id="Field' + count + '-1" name="Field' + count + '-1" class="field checkbox " value="First Choice" readonly="readonly" disabled="disabled" type="checkbox" style="visibility: visible;">' +
                '<label   class="frmchoice " for="Field' + count + '-1"  style="margin-top:0px">First Choice</label>' +
                '</span>' +
                '<span>' +
                '<input  id="Field' + count + '-2" name="Field' + count + '-2" class="field checkbox " value="Second Choice" readonly="readonly" disabled="disabled" type="checkbox" style="visibility: visible;">' +
                '<label  class="frmchoice " for="Field' + count + '-2"  style="margin-top:0px">Second Choice</label>' +
                '</span>' +
                '<span>' +
                '<input  id="Field' + count + '-3" name="Field' + count + '-3" class="field checkbox " value="Third Choice" readonly="readonly" disabled="disabled" type="checkbox" style="visibility: visible;">' +
                '<label  class="frmchoice " for="Field' + count + '-3"  style="margin-top:0px">Third Choice</label>' +
                '</span>' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Untitled', required: '', size: 'small', duplicate: 0, defaultValue: '', choices: [{id: 1, label: 'First Choice', selected: 0, valueid: ''}, {id: 2, label: 'Second Choice', selected: 0, valueid: ''}, {id: 3, label: 'Third Choice', selected: 0, valueid: ''}], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', uniqueCheck: 0});
    }
    else if (type == "radio")
    {
//        alert('hmm');
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<span>' +
                '<input id="Field' + count + '-1" name="Field' + count + '-1" class="field radio" value="First Choice" readonly="readonly" disabled="disabled" type="radio">' +
                '<label class="frmchoice" for="Field' + count + '-1">First Choice</label>' +
                '</span>' +
                '<span>' +
                '<input id="Field' + count + '-2" name="Field' + count + '-1" class="field radio" value="Second Choice" readonly="readonly" disabled="disabled" type="radio">' +
                '<label class="frmchoice" for="Field' + count + '-2">Second Choice</label>' +
                '</span>' +
                '<span>' +
                '<input id="Field' + count + '-3" name="Field' + count + '-1" class="field radio" value="Third Choice" readonly="readonly" disabled="disabled" type="radio">' +
                '<label class="frmchoice" for="Field' + count + '-3">Third Choice</label>' +
                '</span>' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Untitled', required: '', size: 'small', duplicate: 0, defaultValue: '', choices: [{id: 1, label: 'First Choice', selected: 0, valueid: ''}, {id: 2, label: 'Second Choice', selected: 0, valueid: ''}, {id: 3, label: 'Third Choice', selected: 0, valueid: ''}], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', uniqueCheck: 0});
    }
    else if (type == "select")
    {
//        alert('hmm');
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<select id="Field' + count + '" name="Field' + count + '" class="field select medium" readonly="readonly" disabled="disabled">' +
                '<option id="Field' + count + '-1" value="-- Please Select --" selected="selected">-- Please Select --</option>' +
                '<option id="Field' + count + '-2" value="First Choice" >First Choice</option>' +
                '<option id="Field' + count + '-3" value="Second Choice">Second Choice</option>' +
                '<option id="Field' + count + '-4" value="Third Choice">Third Choice</option>' +
                '</select>' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Select a Choice', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [{id: 1, label: '-- Please Select --', selected: 1, valueid: ''}, {id: 2, label: 'First Choice', selected: 0, valueid: ''}, {id: 3, label: 'Second Choice', selected: 0, valueid: ''}, {id: 4, label: 'Third Choice', selected: 0, valueid: ''}], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Select a Choice';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "limit")
    {
//        alert('hmm');
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<select id="Field' + count + '" name="Field' + count + '" class="field select medium" readonly="readonly" disabled="disabled">' +
                '<option id="Field' + count + '-1" value="-- Please Select --" selected="selected">-- Please Select --</option>' +
                '<option id="Field' + count + '-2" value="First Choice" >First Choice</option>' +
                '<option id="Field' + count + '-3" value="Second Choice">Second Choice</option>' +
                '<option id="Field' + count + '-4" value="Third Choice">Third Choice</option>' +
                '</select>' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Select a Choice', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [{id: 1, label: '-- Please Select --', selected: 1, valueid: '', quota: '999999', maxquota: '999999'}, {id: 2, label: 'First Choice', selected: 0, valueid: '', quota: '0', maxquota: '0'}, {id: 3, label: 'Second Choice', selected: 0, valueid: '', quota: '0', maxquota: '0'}, {id: 4, label: 'Third Choice', selected: 0, valueid: '', quota: '0', maxquota: '0'}], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Select a Choice';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "section")
    {
//        alert('hmm');
        field.className = 'section';
        field_html += '<h3 id="title' + count + '">' +
                'Section Break' +
                '</h3>' +
                '<div class="" id="instruct' + count + '">' +
                'A description of the section goes here.' +
                '</div>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Section Break', required: '', size: 'medium', duplicate: 0, defaultValue: 'A description of the section goes here.', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', uniqueCheck: 0});
    }
    else if (type == "shortname")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<span class="shortname" style="width:46%;">' +
                '<input style="width:80%;" type="text" disabled="disabled" readonly="readonly" size="14" value="" ' +
                'class="field text fn" name="Field' + count + '-1" id="Field' + count + '-1" placeholder="First">' +
                '</span>' +
                '<span  class="symbol"  style="width:1%;height:1px;"></span>' +
                '<span  class="shortname" style="width:47%;">' +
                '<input type="text" disabled="disabled" readonly="readonly" size="14" value="" ' +
                'class="field text ln" name="Field' + count + '-2" id="Field' + count + '-2" placeholder="Last">' +
                '</span>' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Name', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Name';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "firstname")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
//					'<span class="shortname">' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<input type="text" disabled="disabled" readonly="readonly" size="14" value="" ' +
                'class="field text medium" name="Field' + count + '-1" id="Field' + count + '-1">' +
//					'</span>' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'First Name', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-First Name';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "lastname")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
//					'<span class="shortname">' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<input type="text" disabled="disabled" readonly="readonly" size="14" value="" ' +
                'class="field text medium" name="Field' + count + '-1" id="Field' + count + '-1">' +
                '</div>' +
                //					'</span>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Last Name', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Last Name';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "date")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<span class="date">' +
                '<input id="Field' + count + '" class="field text" value="" size="14" ' +
                'maxlength="10" readonly="readonly" disabled="disabled" type="text">' +
                '<label for="Field' + count + '">MM/DD/YYYY</label>' +
                '</span>' +
                '<span id="cal' + count + '">' +
                '<img id="pick' + count + '" class="datepicker" src="' + imgpath + '/img/template-editor/calendar.png" alt="Pick a date.">' +
                '</span>' +
                '<br><br>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Date', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', validErrorMsg: 'Please fill valid information', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Date';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "email")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="" ' +
                'class="field text medium" name="Field' + count + '" id="Field' + count + '">' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Email', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', validErrorMsg: 'Please fill valid information', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Email';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "url")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="http://" ' +
                'class="field text medium" name="Field' + count + '" id="Field' + count + '">' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Web Site', required: '', size: 'medium', duplicate: 0, defaultValue: 'http://', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', validErrorMsg: 'Please fill valid information', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Web Site';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "time")
    {
        field_html += "";
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">';
        field_html += '</label>';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-1" name="Field' + count + '-1" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 1; i < 13; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '<label for="Field' + count + '-1">HH</label>';
        field_html += '</span>';
        field_html += '<span class="symbol">:</span>';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-2" name="Field' + count + '-2" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 0; i < 60; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '<label for="Field' + count + '-2">MM</label>';
        field_html += '</span>';
        field_html += '<span class="symbol">:</span>';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-3" name="Field' + count + '-3" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 0; i < 60; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '<label for="Field' + count + '-3">SS</label>';
        field_html += '</span>';
        field_html += '<span class="ampm">';
        field_html += '<select id="Field' + count + '-4" name="Field' + count + '-4" class="field select" style="width: 4em;" readonly="readonly" disabled="disabled">';
        field_html += '<option value="AM" selected="selected">AM</option>';
        field_html += '<option value="PM">PM</option>';
        field_html += '</select>';
        field_html += '<label for="Field' + count + '-4">AM/PM</label>';
        field_html += '</span>';
        field_html += '<br><br>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: 'Time', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', validErrorMsg: 'Please fill valid information', uniqueCheck: 0});
    }
    else if (type == "phone")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<input type="text" disabled="disabled" readonly="readonly" maxlength="255" value="" ' +
                'class="field text medium" name="Field' + count + '" id="Field' + count + '">' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Mobile Number', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', validErrorMsg: 'Please fill valid information', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Mobile Number';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "money")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<span class="symbol">$</span>' +
                '<span class="shortname">' +
                '<input id="Field' + count + '-1" name="Field' + count + '-1" class="field text currency" value="" size="10" readonly="readonly" disabled="disabled" type="text">' +
                '<label for="Field' + count + '-1">Dollars</label>' +
                '</span>' +
                '<span class="symbol">.</span>' +
                '<span class="shortname">' +
                '<input id="Field' + count + '-2" name="Field' + count + '-2" class="field text" value="" size="2" maxlength="2" readonly="readonly" disabled="disabled" type="text">' +
                '<label for="Field' + count + '-2">Cents</label>' +
                '</span>' +
                '<br><br>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Price', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', validErrorMsg: 'Please fill valid information', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Price';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "address")
    {
        field.className += " complex";
        field.style.display = "block";
        field.style.height = "240px";
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<span class="full left">' +
                '<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">' +
                '<label for="Field">Street Address</label>' +
                '</span>' +
                '<span class="full left">' +
                '<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">' +
                '<label for="Field">Address Line 2</label>' +
                '</span>' +
                '<span class="full left">' +
                '<span class="left">' +
                '<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">' +
                '<label for="Field">City</label>' +
                '</span>' +
                '<span class="right">' +
                '<input id="Field" name="Field" class="field text addr" value="" readonly="readonly" disabled="disabled" type="text">' +
                '<label for="Field">State / Province / Region</label>' +
                '</span>' +
                '</span>' +
                '<span class="full left">' +
                '<span class="left">' +
                '<input id="Field" name="Field" class="field text addr" value="" maxlength="15" readonly="readonly" disabled="disabled" type="text">' +
                '<label for="Field">Postal / Zip Code</label>' +
                '</span>' +
                '<span class="right">' +
                '<select id="Field" name="Field" class="field select addr" readonly="readonly" disabled="disabled">' +
                '<option value="" selected="selected"></option>' +
                option_countries() +
                '</select>' +
                '<label for="Field">Country</label>' +
                '</span>' +
                '</span>' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Address', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', validErrorMsg: 'Please fill valid information', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-Address';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "pagebreak")
    {
        field_html += '<label for="Field' + count + '" id="title' + count + '" class="frmquestion cstm_frmquestion">' +
                '---------Page Break---------' +
                '<br>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'pagebreak', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', uniqueCheck: 0});

        var new_choice = document.createElement('option');
        new_choice.value = '';
        new_choice.innerHTML = 'Q' + count + '-pagebreak';
        document.getElementById('cstmField').appendChild(new_choice);
    }
    else if (type == "likert")
    {
        field.className += " likert col4";
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="cus_title">' +
                '</label></br></br>' +
                '<table cellspacing="0">' +
                '<thead>' +
                '<tr>' +
                '<th>&nbsp;</th>' +
                '<td>Strongly Disagree</td>' +
                '<td>Disagree</td>' +
                '<td>Agree</td>' +
                '<td>Strongly Agree</td>' +
                '</tr>' +
                '</thead>' +
                '<tbody>' +
                '<tr class="">' +
                '<th><label for="Field1">Statement One</label></th>' +
                '<td title="Strongly Disagree">' +
                '<input type="radio" readonly="readonly" disabled="disabled" name="Field1" id="Field' + count + '-1">' +
                '<label for="Field' + count + '-1">1</label>' +
                '</td>' +
                '<td title="Disagree">' +
                '<input type="radio" readonly="readonly" disabled="disabled" name="Field1" id="Field' + count + '-2">' +
                '<label for="Field' + count + '-2">2</label>' +
                '</td>' +
                '<td title="Agree">' +
                '<input type="radio" readonly="readonly" disabled="disabled" name="Field1" id="Field' + count + '-3">' +
                '<label for="Field' + count + '-3">3</label>' +
                '</td>' +
                '<td title="Strongly Agree">' +
                '<input type="radio" readonly="readonly" disabled="disabled" name="Field1" id="Field' + count + '-4">' +
                '<label for="Field' + count + '-4">4</label>' +
                '</td>' +
                '</tr>' +
                '<tr class="alt">' +
                '<th><label for="Field2">Statement Two</label></th>' +
                '<td title="Strongly Disagree">' +
                '<input type="radio" readonly="readonly" disabled="disabled" name="Field2" id="Field' + count + '-1">' +
                '<label for="Field' + count + '-1">1</label>' +
                '</td>' +
                '<td title="Disagree">' +
                '<input type="radio" readonly="readonly" disabled="disabled" name="Field2" id="Field' + count + '-2">' +
                '<label for="Field' + count + '-2">2</label>' +
                '</td>' +
                '<td title="Agree">' +
                '<input type="radio" readonly="readonly" disabled="disabled" name="Field2" id="Field' + count + '-3">' +
                '<label for="Field' + count + '-3">3</label>' +
                '</td>' +
                '<td title="Strongly Agree">' +
                '<input type="radio" readonly="readonly" disabled="disabled" name="Field2" id="Field' + count + '-4">' +
                '<label for="Field' + count + '-4">4</label>' +
                '</td>' +
                '</tr>' +
                '</tbody>' +
                '</table>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        var statementArr = [{surveyid: '', label: 'Statement One', valueid: ['', '', '', '']}, {surveyid: '', label: 'Statement Two', valueid: ['', '', '', '']}];
        fields.push({id: id, count: count, type: type, label: 'Evaluate the following statements.', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {statements: statementArr, columns: ['Strongly Disagree', 'Disagree', 'Agree', 'Strongly Agree'], selected: -1, originalStatementCount: statementArr.length}, formid: '', validErrorMsg: 'Please fill valid information', uniqueCheck: 0});
    }

    else if (type == "birthdate")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="desc-edit desc">';
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-1" name="Field' + count + '-1" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 1; i < 32; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '</span>';

        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: 'Birth Date', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', uniqueCheck: 0, likert: {}});
    }
    else if (type == "birthmonth")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="desc-edit desc">';
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<span class="time">';
        field_html += '<select id="Field' + count + '-2" name="Field' + count + '-2" class="field select" readonly="readonly" disabled="disabled">';
        field_html += '<option value=""></option>';
        for (var i = 1; i < 13; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
        field_html += '</span>';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: 'Birth Month', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', uniqueCheck: 0, likert: {}});
    }
    else if (type == "birth")
    {
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="desc-edit desc">';
        field_html += '</label>';
        field_html += '<div class="frmanswer cstm_frmanswer">';
        field_html += '<span class="time birthTwoColumn">';
        field_html += '<select id="Field' + count + '-1" name="Field' + count + '-1" class="field select birthSelect" readonly="readonly" disabled="disabled">';
        field_html += '<option value="">DD</option>';
        for (var i = 1; i < 32; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
//		field_html += '<label for="Field'+count+'-1">MM-</label>';
        field_html += '</span>';
        field_html += '<span class="time birthTwoColumn">';
        field_html += '<select id="Field' + count + '-2" name="Field' + count + '-2" class="field select mmSelect" readonly="readonly" disabled="disabled">';
        field_html += '<option value="">MM</option>';
        for (var i = 1; i < 13; i++)
        {
            field_html += '<option value="' + str_pad(i, 2, "0", "left") + '">' + str_pad(i, 2, "0", "left") + '</option>';
        }
        field_html += '</select>';
//		field_html += '<label for="Field'+count+'-2">DD</label>';
        field_html += '</span>';

//		field_html += '<span class="symbol">-</span>';
//		field_html += '<span class="time">';
//		field_html += '<select id="Field'+count+'-3" name="Field'+count+'-3" class="field select" readonly="readonly" disabled="disabled">';
//		field_html += '<option value=""></option>';
//		for(var i=2013; i > 1900; i--)
//		{
//			field_html += '<option value="' + i + '">' + i + '</option>';
//		}
//		field_html += '</select>';
//		field_html += '<label for="Field'+count+'-3">YYYY</label>';
//		field_html += '</span>';

        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: 'Birthday', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, validErrorMsg: 'Please fill valid information', uniqueCheck: 0});
    }
    // Modify By Wood
    // Draw the new plain text interface
//	else if(type == "plaintext")
//	{
//		field_html += '<div id="Field'+count+'" class="medium">' +
//		'Please enter the text' +
//		'</div>' +
//		'<p id="instruct'+count+'" class="instruct hide"><small></small></p>' +
//		'<div class="fieldActions" id="fa'+count+'">' +
//		'<img onclick="duplicateField('+count+', \''+type+'\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
//		'<img onclick="removeField('+count+', \''+type+'\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
//		'</div>';
//		
//	}
    else if (type == "plaintext")
    {
        field_html += '<div id="Field' + count + '" class="medium" style="font-size:15px;">';
        field_html += 'Please enter the text';
        field_html += '</div>';
        field_html += '<p id="instruct' + count + '" class="instruct hide"><small></small></p>';
        field_html += '<div class="fieldActions" id="fa' + count + '">';
        field_html += '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">';
        field_html += '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">';
        field_html += '</div>';
        fields.push({id: id, count: count, type: type, label: 'Please enter the text', required: '', size: 'medium', duplicate: 0, defaultValue: '', choices: [], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', uniqueCheck: 0});
    }
    else if (type == "agerange")
    {
//        alert('hmm');
        field_html += '' +
                '<label for="Field' + count + '" id="title' + count + '" class="' + labelClass + '">' +
                '</label>' +
                '<div class="frmanswer cstm_frmanswer">' +
                '<span>' +
                '<input id="Field' + count + '-1" name="Field' + count + '-1" class="field radio" value="First Choice" readonly="readonly" disabled="disabled" type="radio">' +
                '<label class="frmchoice" for="Field' + count + '-1">Under 18</label>' +
                '</span>' +
                '<span>' +
                '<input id="Field' + count + '-2" name="Field' + count + '-1" class="field radio" value="Second Choice" readonly="readonly" disabled="disabled" type="radio">' +
                '<label class="frmchoice" for="Field' + count + '-2">18-40</label>' +
                '</span>' +
                '<span>' +
                '<input id="Field' + count + '-3" name="Field' + count + '-1" class="field radio" value="Third Choice" readonly="readonly" disabled="disabled" type="radio">' +
                '<label class="frmchoice" for="Field' + count + '-3">Over 40</label>' +
                '</span>' +
                '</div>' +
                '<p id="instruct' + count + '" class="instruct hide"><small></small></p>' +
                '<div class="fieldActions" id="fa' + count + '">' +
                '<img onclick="duplicateField(' + count + ', \'' + type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
                '<img onclick="removeField(' + count + ', \'' + type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
                '</div>';
        fields.push({id: id, count: count, type: type, label: 'Age Range', required: '', size: 'small', duplicate: 0, defaultValue: '', choices: [{id: 1, label: 'Under 18', selected: 0, valueid: ''}, {id: 2, label: '18-40', selected: 0, valueid: ''}, {id: 3, label: 'Over 40', selected: 0, valueid: ''}], date_format: 0, columns: '', likert: {}, formid: '', surveyid: '', uniqueCheck: 0});
    }
    // End Modify By Wood

    else
    {
        return;
    }
    field.innerHTML = field_html;
    fieldUl.appendChild(field);
    if ($.inArray(type, ['pagebreak', 'plaintext']) < 0) {
        fieldDiv.appendChild(fieldLabel);
    }
    fieldDiv.appendChild(fieldUl);
    var formFields = document.getElementById('formFields');
    formFields.appendChild(fieldDiv);
    $('#' + id).click(makeEditable);
    bindDragEvents(divId);
    showFields(true);
}

//function changeDateFieldType(val)
//{
//	var cur = fieldInFields(currently_editing);
//	var field = fields[cur];
//	var span = document.getElementById(currently_editing).getElementsByTagName('span');
//	if(val == 'eurodate')
//	{
//		span[1].getElementsByTagName('label')[0].innerHTML = "DD/MM/YYYY";
//		field.date_format = 1;
//	}
//	else
//	{
//		span[1].getElementsByTagName('label')[0].innerHTML = "MM/DD/YYYY";
//		field.date_format = 0;
//	}
//}

function changeSelectDefault(id)
{
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    for (var i = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            document.getElementById('Field' + field.count + '-' + field.choices[i].id).selected = true;
            field.choices[i].selected = 1;
        }
        else
        {
            document.getElementById('Field' + field.count + '-' + field.choices[i].id).selected = false;
            field.choices[i].selected = 0;
        }
    }
}

function changeRadioDefault(id)
{
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    for (var i = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            if (document.getElementById('Field' + field.count + '-' + field.choices[i].id).checked)
            {
                document.getElementById("fieldProperties").allChoices[i].checked = false;
                document.getElementById('Field' + field.count + '-' + field.choices[i].id).checked = false;
                field.choices[i].selected = 0;
            }
            else
            {
                document.getElementById('Field' + field.count + '-' + field.choices[i].id).checked = true;
                field.choices[i].selected = 1;
            }
        }
        //scott add do nothing for 'other' option
        else if (field.choices[i].id == "100") {

        }
        else
        {
            document.getElementById('Field' + field.count + '-' + field.choices[i].id).checked = false;
            field.choices[i].selected = 0;
        }
    }
}

function changeCheckboxDefault(id)
{
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
//    alert('Field'+field.count+'-'+id);
    for (var i = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            if (field.choices[i].selected == 0)
            {
                document.getElementById('Field' + field.count + '-' + id).checked = true;
                field.choices[i].selected = 1;
            }
            else
            {
                document.getElementById('Field' + field.count + '-' + id).checked = false;
                field.choices[i].selected = 0;
            }
        }
    }
}

function updateChoice(id, val)
{
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    for (var i = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            field.choices[i].label = val;
            if (document.getElementById('Field' + field.count + '-' + id).parentNode.getElementsByTagName('label')[0])
            {
                document.getElementById('Field' + field.count + '-' + id).parentNode.getElementsByTagName('label')[0].innerHTML = val;
            }
            else
            {
                document.getElementById('Field' + field.count + '-' + id).innerHTML = val;
            }
        }
    }
}

function updateQuota(id, val)
{
    if (!isNaN(val)) {
        var cur = fieldInFields(currently_editing);
        var field = fields[cur];

        for (var i = 0; i < field.choices.length; i++)
        {

            if (field.choices[i].id == id)
            {
                if ($('#xselect_maxquota' + (Number(id) - 1)).val() < Number(val))
                {
                    //alert('the Quota more than MaxQuota');
                    $('#errorquota').html('the Quota more than MaxQuota in ' + field.choices[i].label);
                    $('#errorquota').show();
                    $('#xselect_quota' + i).focus();
                    return false;
                }
                else
                {
                    field.choices[i].maxquota = $('#xselect_maxquota' + (Number(id) - 1)).val();
                    field.choices[i].quota = val;
                    $('#errorquota').hide();
                }
            }
        }
    } else
    {
        //alert('the input is not number');
        $('#errorquota').html('the input is not number');
        $('#errorquota').show();
        $('#xselect_quota' + (Number(id) - 1)).focus();
        return false;
    }

}
function updateMaxQuota(id, val)
{

    if (!isNaN(val)) {
        var cur = fieldInFields(currently_editing);
        var field = fields[cur];
        for (var i = 0; i < field.choices.length; i++)
        {
            if (field.choices[i].id == id)
            {
                if ($('#xselect_quota' + (Number(id) - 1)).val() > Number(val))
                {
                    //alert('the MaxQuota less than Quota');
                    $('#errorquota').html('the MaxQuota less than Quota in ' + field.choices[i].label);
                    $('#errorquota').show();
                    $('#xselect_maxquota' + i).focus();
                    return false;
                }
                else
                {
                    field.choices[i].quota = $('#xselect_quota' + (Number(id) - 1)).val();
                    console.log('xselect' + (Number(id) - 1));
                    field.choices[i].maxquota = val;
                    $('#errorquota').hide();
                }
            }
        }
    } else {
        //alert('the input is not number');
        $('#errorquota').html('the input is not number');
        $('#errorquota').show();
        $('#xselect_maxquota' + (Number(id) - 1)).focus();
        return false;
    }


}
function updateLikertStatement(obj)
{
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var editingElem = document.getElementById(currently_editing);
    var i, pos, max;
    var parent = obj.parentNode.parentNode;
    var li = parent.getElementsByTagName('li');
    for (i = 0; i < li.length; i++)
    {
        if (li[i] == obj.parentNode)
        {
            pos = i;
            break;
        }
    }
    //pos+1 bcz the first tr is heading
    var label = editingElem.getElementsByTagName('tr')[pos + 1].getElementsByTagName('th')[0].getElementsByTagName('label')[0];
    label.innerHTML = obj.value;
    field.likert.statements[pos]['label'] = obj.value;
//    alert(obj.value);
}

function updateLikertColumn(obj)
{
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var editingElem = document.getElementById(currently_editing);
    var i, pos;
    var parent = obj.parentNode.parentNode;
    var li = parent.getElementsByTagName('li');
    for (i = 0; i < li.length; i++)
    {
        if (li[i] == obj.parentNode)
        {
            pos = i;
            break;
        }
    }
    //pos+1 bcz the first tr is heading
    var col_td = editingElem.getElementsByTagName('tr')[0].getElementsByTagName('td')[pos];
    col_td.innerHTML = obj.value;
    field.likert.columns[pos] = obj.value;
    var choiceIndex = field.likert.statements.length * pos;
//	console.log(field.choices);
    for (i = 0; i < field.likert.statements.length; i++) {
//		console.log(choiceIndex);
        field.choices[choiceIndex].label = obj.value;
        choiceIndex++;
    }
//    alert(obj.value);
}

function changeLikertDefault(obj)
{
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, j, pos;
    var parent = obj.parentNode.parentNode;
    var li = parent.getElementsByTagName('li');

    for (i = 0; i < li.length; i++)
    {
        if (li[i] == obj.parentNode)
        {
            pos = i;
            break;
        }
    }
    if (pos == field.likert.selected)
    {
        field.likert.selected = -1;
        obj.checked = false;
    }
    else
    {
        field.likert.selected = pos;
    }
    generateLikert(currently_editing);
}


function addLikertStatement(obj)
{
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, j, pos;
    var parent = obj.parentNode.parentNode;
    var li = parent.getElementsByTagName('li');
    for (i = 0; i < li.length; i++)
    {
        if (li[i] == obj.parentNode)
        {
            pos = i;
            break;
        }
    }
    var tmp_val_arr = new Array();
    for (i = 0; i < field.likert.columns.length; i++) {
        tmp_val_arr[i] = '';
    }
    var newChoiceArr = new Array();
//	for(i=0;i<field.likert.columns.length;i++){
//		newChoiceArr[i] = new Array();
//		newChoiceArr[i] = field.choices[(i*(field.likert.statements.length))];
//		newChoiceArr[i] = {'':,'':,'':,'':,'choiceid':,'id':,'label':,'likertid':,};
//		newChoiceArr[i].likertid = '';
////		field.choices.splice((pos+1)+(i*field.likert.statements.length),0,newChoiceArr);
//	}
//	for(i=field.likert.columns.length-1;i>=0;i--){
//		console.log((pos+1)+(i*field.likert.statements.length));
//		field.choices.splice((pos+1)+(i*field.likert.statements.length),0,newChoiceArr[i]);
//	}
    field.likert.statements.splice(pos + 1, 0, {surveyid: "", label: "", valueid: tmp_val_arr});//first add the new statement to array
    generateLikert(currently_editing);
    var html;
    html = "";
    html += '<input type="text" onkeyup="updateLikertStatement(this);" value="" class="text statement"> ';
    html += '<img title="Add another choice." onclick="addLikertStatement(this);" alt="Add" src="' + imgpath + '/img/template-editor/add.png"> ';
    html += '<img title="Delete this choice." onclick="deleteLikertStatement(this)" alt="Delete" src="' + imgpath + '/img/template-editor/delete.png">';
    var new_li = document.createElement('li');//this is for adding editing option for newly added choice in the left side
    new_li.innerHTML = html;
    parent.insertBefore(new_li, obj.parentNode.nextSibling);
}

function addLikertChoice(obj)
{
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, j, pos;
    var parent = obj.parentNode.parentNode;
    var cols = field.likert.columns.length;
    if (cols == 10)
    {
        alert("Sorry! Maximum of 10 columns can be added.")
        return;
    }
    var li = parent.getElementsByTagName('li');
    for (i = 0; i < li.length; i++)
    {
        if (li[i] == obj.parentNode)
        {
            pos = i;
            break;
        }
    }
    field.likert.columns.splice(pos + 1, 0, "");//first add the new column to array
    var choiceIndex = field.likert.statements.length * (pos + 1);
    if (field.hasOwnProperty('fieldid')) {
        for (i = 0; i < field.likert.statements.length; i++) {
            var newChoiceArr = {'choiceid': '', 'id': choiceIndex, 'label': '', };
            newChoiceArr.likertid = field.likert.statements[i].likertid;
            field.choices.splice(choiceIndex, 0, newChoiceArr);
            choiceIndex++;
        }
    }
//	console.log('i:'+i);
//	for(i=0; i<field.likert.statements.length; i++){
//		field.likert.statements[i]['valueid'].splice(pos+1,0,"");
//	}
    generateLikert(currently_editing);
    var html;
    html = "";
    html += '<input type="radio" title="Make this column pre-selected." onclick="changeLikertDefault(this);" name="allChoices"> ';
    html += '<input type="text" onkeyup="updateLikertColumn(this);" value="" class="text"> ';
    html += '<img title="Add another choice." onclick="addLikertChoice(this);" alt="Add" src="' + imgpath + '/img/template-editor/add.png"> ';
    html += '<img title="Delete this column." onclick="deleteLikertChoice(this);" alt="Delete" src="' + imgpath + '/img/template-editor/delete.png">';
    var new_li = document.createElement('li');//this is for adding editing option for newly added choice in the left side
    new_li.innerHTML = html;
    parent.insertBefore(new_li, obj.parentNode.nextSibling);
    var editingElem = document.getElementById(currently_editing);
    editingElem.className = editingElem.className.replace("col" + cols, "col" + (cols + 1));
}

function deleteLikertStatement(obj)
{
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, j, pos;
    var parent = obj.parentNode.parentNode;
    var li = parent.getElementsByTagName('li');
    for (i = 0; i < li.length; i++)
    {
        if (li[i] == obj.parentNode)
        {
            pos = i;
            break;
        }
    }
    field.likert.statements.splice(pos, 1);
    generateLikert(currently_editing);
    obj.parentNode.parentNode.removeChild(obj.parentNode);
}

function deleteLikertChoice(obj)
{
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, j, pos;
    var parent = obj.parentNode.parentNode;
    var cols = field.likert.columns.length;
    var li = parent.getElementsByTagName('li');
    for (i = 0; i < li.length; i++)
    {
        if (li[i] == obj.parentNode)
        {
            pos = i;
            break;
        }
    }
    if (field.likert.selected == pos)
    {
        field.likert.selected = -1;
    }
    var choiceIndex = field.likert.statements.length * pos;
    field.choices.splice(choiceIndex, field.likert.statements.length);
    field.likert.columns.splice(pos, 1);
//	for(i=0; i<field.likert.statements.length; i++){
//		field.likert.statements[i]['valueid'].splice(pos,1);
//	}
    generateLikert(currently_editing);
    obj.parentNode.parentNode.removeChild(obj.parentNode);
    var editingElem = document.getElementById(currently_editing);
    editingElem.className = editingElem.className.replace("col" + cols, "col" + (cols - 1));
}

function generateCheckbox(field_id)
{
    var editingElem = document.getElementById(field_id);
    var cur = fieldInFields(field_id);
    var field = fields[cur];
    var i;
    var html = "";
    html = '<label for="Field' + field.count + '" id="title' + field.count + '" class="frmquestion cstm_frmquestion">' +
            field.label +
            '<span class="req" id="req_' + field.count + '">' + field.required + '</span>' +
            '</label>' +
            '<div>';
    for (i = 0; i < field.choices.length; i++)
    {
        html += '<span>';
        html += '<input type="checkbox" id="Field' + field.count + '-' + (i + 1) + '" name="Field' + field.count + '-' + (i + 1) + '" value="First Choice"';
        if (field.choices[i].selected)
        {
            html += ' checked="checked"';
        }
        html += ' readonly="readonly" disabled="disabled" class="field checkbox">';
        html += '<label class="choice" for="Field' + field.count + '-' + (i + 1) + '-">' + field.choices[i].label + '</label>';
        html += '</span>';
    }
    html += '</div>' +
            '<p id="instruct' + field.count + '" class="instruct hide"><small></small></p>' +
            '<div class="fieldActions" id="fa' + field.count + '">' +
            '<img onclick="duplicateField(' + field.count + ', \'' + field.type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
            '<img onclick="removeField(' + field.count + ', \'' + field.type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
            '</div>';
    editingElem.innerHTML = html;

    $('#' + field_id).addClass(field.columns);
}

function generateRadio(field_id)
{
    var editingElem = document.getElementById(field_id);
    var cur = fieldInFields(field_id);
    var field = fields[cur];
    var i;
    var html = "";
    html = '<label for="Field' + field.count + '" id="title' + field.count + '" class="frmquestion cstm_frmquestion">' +
            field.label +
            '<span class="req" id="req_' + field.count + '">' + field.required + '</span>' +
            '</label>' +
            '<div>';
    for (i = 0; i < field.choices.length; i++)
    {
        html += '<span>';
        html += '<input type="radio" id="Field' + field.count + '-' + (i + 1) + '" name="Field' + field.count + '-1" value="First Choice"';
        if (field.choices[i].selected)
        {
            html += ' checked="checked"';
        }
        html += ' readonly="readonly" disabled="disabled" class="field radio">';
        html += '<label class="frmchoice" for="Field' + field.count + '-' + (i + 1) + '-">' + field.choices[i].label + '</label>';
        html += '</span>';
    }
    html += '</div>' +
            '<p id="instruct' + field.count + '" class="instruct hide"><small></small></p>' +
            '<div class="fieldActions" id="fa' + field.count + '">' +
            '<img onclick="duplicateField(' + field.count + ', \'' + field.type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
            '<img onclick="removeField(' + field.count + ', \'' + field.type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
            '</div>';
    editingElem.innerHTML = html;

    $('#' + field_id).addClass(field.columns);
}

function generateSelect(field_id)
{
    var editingElem = document.getElementById(field_id);
    var cur = fieldInFields(field_id);
    var field = fields[cur];
    var i;
    var html = "";
    html = '<label for="Field' + field.count + '" id="title' + field.count + '" class="frmquestion cstm_frmquestion">' +
            field.label +
            '<span class="req" id="req_' + field.count + '">' + field.required + '</span>' +
            '</label>' +
            '<div>' +
            '<select id="Field' + field.count + '" name="Field' + field.count + '" class="field select medium" readonly="readonly" disabled="disabled">';
    for (i = 0; i < field.choices.length; i++)
    {
        html += '<option id="Field' + field.count + '-' + (i + 1) + '" value="' + field.choices[i].label + '"';
        if (field.choices[i].selected)
        {
            html += ' selected="selected"';
        }
        html += '>' + field.choices[i].label + '</option>';
    }
    html += '</select>' +
            '</div>' +
            '<p id="instruct' + field.count + '" class="instruct hide"><small></small></p>' +
            '<div class="fieldActions" id="fa' + field.count + '">' +
            '<img onclick="duplicateField(' + field.count + ', \'' + field.type + '\'); return false;" title="Duplicate." alt="Duplicate." src="' + imgpath + '/img/template-editor/add.png" class="faDup">' +
            '<img onclick="removeField(' + field.count + ', \'' + field.type + '\'); return false;" title="Delete." alt="Delete." src="' + imgpath + '/img/template-editor/delete.png" class="faDel">' +
            '</div>';
    editingElem.innerHTML = html;
}

function updateChoiceSelectState(field_id) {
    var editingElem = document.getElementById(field_id);
    var cur = fieldInFields(field_id);
    var field = fields[cur];
    var choiceArr = editingElem.getElementsByTagName('input');
    for (i = 0; i < choiceArr.length; i++)
    {
        obj = choiceArr[i];
        if (obj.getAttribute("column") == field.likert.selected)
        {
            obj.checked = "checked";
        }
    }

}

function generateLikert(field_id)//generate likert thead and tr and add them to the selected element likert table
{
    var editingElem = document.getElementById(field_id);
    var cur = fieldInFields(field_id);
    var field = fields[cur];
    var i;
    var thead = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.innerHTML = '&nbsp;';
    tr.appendChild(th);
    var td;
    for (i = 0; i < field.likert.columns.length; i++)
    {
        td = document.createElement('td');
        td.innerHTML = field.likert.columns[i];
        tr.appendChild(td);
    }
    thead.appendChild(tr);
    var parent = editingElem.getElementsByTagName('thead')[0].parentNode;
    parent.replaceChild(thead, editingElem.getElementsByTagName('thead')[0]);
    var tbody = document.createElement('tbody');
    var label;
    for (j = 0; j < field.likert.statements.length; j++)
    {
        tr = document.createElement('tr');
        if (j % 2 == 0)
        {
            tr.className = "";
        }
        else
        {
            tr.className = "alt";
        }
        th = document.createElement('th');
        label = document.createElement('label');
        label.innerHTML = field.likert.statements[j]['label'];
        th.appendChild(label);
        tr.appendChild(th);
        for (i = 0; i < field.likert.columns.length; i++)
        {
            td = document.createElement('td');
            td.title = field.likert.columns[i];
            if (i == field.likert.selected)
            {
                td.innerHTML = '<input type="radio" checked="checked" readonly="readonly" column="' + i + '" disabled="disabled" name="' + field_id + '-' + j + '">';
            }
            else
            {
                td.innerHTML = '<input type="radio" readonly="readonly" disabled="disabled" column="' + i + '" name="' + field_id + '-' + j + '">';
            }
            td.innerHTML += '<label>' + (i + 1) + '</label>';
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }
    parent = editingElem.getElementsByTagName('tbody')[0].parentNode;
    parent.replaceChild(tbody, editingElem.getElementsByTagName('tbody')[0]);
    updateChoiceSelectState(field_id);
}

function addCheckbox(id, obj)
{
    var editing = document.getElementById(currently_editing);
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, pos, max;
    for (i = 0, pos = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            pos = i;
            break;
        }
    }
    for (i = 0, max = -1; i < field.choices.length; i++)
    {
        if (field.choices[i].id < 100) {
            if (field.choices[i].id > max)
            {
                max = field.choices[i].id;
            }
        }
    }
    max++;
    var new_choice = document.createElement('span');
    new_choice.innerHTML = '<input style="display:inline-block;" id="Field' + field.count + '-' + max + '" name="Field' + field.count + '-' + max + '" class="field checkbox" value="First Choice" readonly="readonly" disabled="disabled" type="checkbox">' +
            '<label style="" class="frmchoice" for="Field' + field.count + '-' + max + '">New Choice</label>';
    var li_div = editing.getElementsByTagName('div')[1];
    if (li_div.getElementsByTagName('span')[pos + 1])
    {
        li_div.insertBefore(new_choice, li_div.getElementsByTagName('span')[pos + 1]);
    }
    else
    {
        li_div.appendChild(new_choice);
    }
    var new_choice_obj = {id: max, label: 'New Choice', selected: 0};
    field.choices.splice(pos + 1, 0, new_choice_obj);
    var new_li = document.createElement('li');//this is for adding editing option for newly added choice in the left side
    new_li.innerHTML = '<input type="checkbox" title="Make this choice pre-selected." onclick="changeCheckboxDefault(\'' + max + '\')" class="" name="allChoices"> ' +
            '<input type="text" onkeyup="updateChoice(\'' + max + '\', this.value);" value="New Choice" autocomplete="off" maxlength="150" class="text"> ' +
            '<img title="Add another choice." alt="Add" onclick="addCheckbox(\'' + max + '\',this)" src="' + imgpath + '/img/template-editor/add.png"> ' +
            '<img title="Delete this choice." alt="Delete" onclick="deleteCheckbox(\'' + max + '\',this)" src="' + imgpath + '/img/template-editor/delete.png">';
    obj.parentNode.parentNode.insertBefore(new_li, obj.parentNode.nextSibling);
}

function addRadio(id, obj)
{
    var editing = document.getElementById(currently_editing);
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, pos, max;
    for (i = 0, pos = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            pos = i;
            break;
        }
    }
    for (i = 0, max = -1; i < field.choices.length; i++)
    {
        if (field.choices[i].id < 100) {
            if (field.choices[i].id > max)
            {
                max = field.choices[i].id;
            }
        }
    }
    max++;
    var new_choice = document.createElement('span');
    new_choice.innerHTML = '<input id="Field' + field.count + '-' + max + '" name="Field' + field.count + '-1" class="field checkbox" value="First Choice" readonly="readonly" disabled="disabled" type="radio">' +
            '<label class="frmchoice" for="Field' + field.count + '-' + max + '">New Choice</label>';
    var li_div = editing.getElementsByTagName('div')[1];
    if (li_div.getElementsByTagName('span')[pos + 1])
    {
        li_div.insertBefore(new_choice, li_div.getElementsByTagName('span')[pos + 1]);
    }
    else
    {
        li_div.appendChild(new_choice);
    }
    var new_choice_obj = {id: max, label: 'New Choice', selected: 0, valueid: ''};
    field.choices.splice(pos + 1, 0, new_choice_obj);
    var new_li = document.createElement('li');//this is for adding editing option for newly added choice in the left side
    new_li.innerHTML = '<input type="radio" title="Make this choice pre-selected." onclick="changeRadioDefault(\'' + max + '\')" class="" name="allChoices"> ' +
            '<input type="text" onkeyup="updateChoice(\'' + max + '\', this.value);" value="New Choice" autocomplete="off" maxlength="150" class="text"> ' +
            '<img title="Add another choice." alt="Add" onclick="addRadio(\'' + max + '\',this)" src="' + imgpath + '/img/template-editor/add.png"> ' +
            '<img title="Delete this choice." alt="Delete" onclick="deleteRadio(\'' + max + '\',this)" src="' + imgpath + '/img/template-editor/delete.png">';
    obj.parentNode.parentNode.insertBefore(new_li, obj.parentNode.nextSibling);
}

function addSelect(id, obj)
{
    var editing = document.getElementById(currently_editing);
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, pos, max;
    for (i = 0, pos = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            pos = i;
            break;
        }
    }
    for (i = 0, max = -1; i < field.choices.length; i++)
    {
        if (field.choices[i].id > max)
        {
            max = field.choices[i].id;
        }
    }
    max++;
    var new_choice = document.createElement('option');
    new_choice.id = 'Field' + field.count + '-' + max;
    new_choice.value = '';
    new_choice.innerHTML = '';
    var li_div_select = editing.getElementsByTagName('div')[1].getElementsByTagName('select')[0];
    if (li_div_select.getElementsByTagName('option')[pos + 1])
    {
        li_div_select.insertBefore(new_choice, li_div_select.getElementsByTagName('option')[pos + 1]);
    }
    else
    {
        li_div_select.appendChild(new_choice);
    }
    var new_choice_obj = {id: max, label: 'New Choice', selected: 0};
    field.choices.splice(pos + 1, 0, new_choice_obj);
    var new_li = document.createElement('li');//this is for adding editing option for newly added choice in the left side
    new_li.innerHTML = '<input type="radio" title="Make this choice pre-selected." onclick="changeSelectDefault(\'' + max + '\')" class="" name="allChoices"> ' +
            '<input type="text" onkeyup="updateChoice(\'' + max + '\', this.value);" value="New Choice" autocomplete="off" maxlength="150" class="text"> ' +
            '<img title="Add another choice." alt="Add" onclick="addSelect(\'' + max + '\',this)" src="' + imgpath + '/img/template-editor/add.png"> ' +
            '<img title="Delete this choice." alt="Delete" onclick="deleteSelect(\'' + max + '\',this)" src="' + imgpath + '/img/template-editor/delete.png">';
    obj.parentNode.parentNode.insertBefore(new_li, obj.parentNode.nextSibling);
}

function addAreaCodeSelect(id, obj)
{
    var editing = document.getElementById(currently_editing);
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, pos, max;
    for (i = 0, pos = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            pos = i;
            break;
        }
    }
    for (i = 0, max = -1; i < field.choices.length; i++)
    {
        if (field.choices[i].id > max)
        {
            max = field.choices[i].id;
        }
    }
    max++;
    var new_choice = document.createElement('option');
    new_choice.id = 'Field' + field.count + '-' + max;
    new_choice.value = '';
    new_choice.innerHTML = '';
    var li_div_select = editing.getElementsByTagName('div')[1].getElementsByTagName('select')[0];
    if (li_div_select.getElementsByTagName('option')[pos + 1])
    {
        li_div_select.insertBefore(new_choice, li_div_select.getElementsByTagName('option')[pos + 1]);
    }
    else
    {
        li_div_select.appendChild(new_choice);
    }
    var new_choice_obj = {id: max, label: 'New Area Code', selected: 0};
    field.choices.splice(pos + 1, 0, new_choice_obj);
    var new_li = document.createElement('li');//this is for adding editing option for newly added choice in the left side
    new_li.innerHTML = '<input type="radio" title="Make this choice pre-selected." onclick="changeSelectDefault(\'' + max + '\')" class="" name="allChoices"> ' +
            '<input type="text" onkeyup="updateChoice(\'' + max + '\', this.value);" value="New Area Code" autocomplete="off" maxlength="150" class="text"> ' +
            '<img title="Add another choice." alt="Add" onclick="addAreaCodeSelect(\'' + max + '\',this)" src="' + imgpath + '/img/template-editor/add.png"> ' +
            '<img title="Delete this choice." alt="Delete" onclick="deleteAreaCodeSelect(\'' + max + '\',this)" src="' + imgpath + '/img/template-editor/delete.png">';
    obj.parentNode.parentNode.insertBefore(new_li, obj.parentNode.nextSibling);
}

function addLimitSelect(id, obj)
{

    var editing = document.getElementById(currently_editing);
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, pos, max;
    for (i = 0, pos = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            pos = i;
            break;
        }
    }
    for (i = 0, max = -1; i < field.choices.length; i++)
    {
        if (field.choices[i].id > max)
        {
            max = field.choices[i].id;
        }
    }
    max++;
    var new_choice = document.createElement('option');
    new_choice.id = 'Field' + field.count + '-' + max;
    new_choice.value = '';
    new_choice.innerHTML = '';
    var li_div_select = editing.getElementsByTagName('div')[1].getElementsByTagName('select')[0];
    if (li_div_select.getElementsByTagName('option')[pos + 1])
    {
        li_div_select.insertBefore(new_choice, li_div_select.getElementsByTagName('option')[pos + 1]);
    }
    else
    {
        li_div_select.appendChild(new_choice);
    }
    var new_choice_obj = {id: max, label: 'New Choice', selected: 0, quota: '0', maxquota: '0'};
    field.choices.splice(pos + 1, 0, new_choice_obj);
    var new_li = document.createElement('li');//this is for adding editing option for newly added choice in the left side
    new_li.innerHTML = '<input type="radio" title="Make this choice pre-selected." onclick="changeSelectDefault(\'' + max + '\')" class="" name="allChoices"> ' +
            '<input type="text" onkeyup="updateChoice(\'' + max + '\', this.value);" value="New Choice" autocomplete="off" maxlength="150" class="text" style="width:135px;"> ' +
            '<input id="xselect_quota' + (max - 1) + '" type="text" onkeyup="updateQuota(\'' + max + '\', this.value);" value="0" autocomplete="off" maxlength="5" class="text" style="width:30px;"> ' +
            '<input id="xselect_maxquota' + (max - 1) + '" type="text" onkeyup="updateMaxQuota(\'' + max + '\', this.value);" value="0" autocomplete="off" maxlength="5" class="text" style="width:30px;"> ' +
            '<img title="Add another choice." alt="Add" onclick="addLimitSelect(\'' + max + '\',this)" src="' + imgpath + '/img/template-editor/add.png"> ' +
            '<img title="Delete this choice." alt="Delete" onclick="deleteSelect(\'' + max + '\',this)" src="' + imgpath + '/img/template-editor/delete.png">';
    obj.parentNode.parentNode.insertBefore(new_li, obj.parentNode.nextSibling);
    console.log(fields);
}

function deleteCheckbox(id, obj)
{
    var editing = document.getElementById(currently_editing);
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, pos, max;
    for (i = 0, pos = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            pos = i;
            break;
        }
    }
    for (i = 0, max = -1; i < field.choices.length; i++)
    {
        if (field.choices[i].id > max)
        {
            max = field.choices[i].id;
        }
    }
    max++;
    var li_div = editing.getElementsByTagName('div')[1];
    li_div.removeChild(li_div.getElementsByTagName('span')[pos]);
    field.choices.splice(pos, 1);
    obj.parentNode.parentNode.removeChild(obj.parentNode);
}

function deleteRadio(id, obj)
{
    var editing = document.getElementById(currently_editing);
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, pos, max;
    for (i = 0, pos = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            pos = i;
            break;
        }
    }
    for (i = 0, max = -1; i < field.choices.length; i++)
    {
        if (field.choices[i].id > max)
        {
            max = field.choices[i].id;
        }
    }
    max++;
    var li_div = editing.getElementsByTagName('div')[1];
    li_div.removeChild(li_div.getElementsByTagName('span')[pos]);
    field.choices.splice(pos, 1);
    obj.parentNode.parentNode.removeChild(obj.parentNode);
}

function deleteSelect(id, obj)
{
    var editing = document.getElementById(currently_editing);

    var cur = fieldInFields(currently_editing);

    var field = fields[cur];
    var i, pos, max;
    for (i = 0, pos = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            pos = i;
            break;
        }
    }
    for (i = 0, max = -1; i < field.choices.length; i++)
    {
        if (field.choices[i].id > max)
        {
            max = field.choices[i].id;
        }
    }
    max++;
    var li_div_select = editing.getElementsByTagName('div')[1].getElementsByTagName('select')[0];
    li_div_select.removeChild(li_div_select.getElementsByTagName('option')[pos]);
    field.choices.splice(pos, 1);
    obj.parentNode.parentNode.removeChild(obj.parentNode);
}
function deleteAreaCodeSelect(id, obj)
{
//	console.log('id is---'+id);
//	console.log('obj is---');
//	console.log(obj);
    var editing = document.getElementById(currently_editing);
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, pos, max;
    for (i = 0, pos = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            pos = i;
            break;
        }
    }
    for (i = 0, max = -1; i < field.choices.length; i++)
    {
        if (field.choices[i].id > max)
        {
            max = field.choices[i].id;
        }
    }
    max++;
    var li_div_select = editing.getElementsByTagName('div')[1].getElementsByTagName('select')[0];
//	li_div_select.removeChild(li_div_select.getElementsByTagName('option')[pos]);
    field.choices.splice(pos, 1);
    obj.parentNode.parentNode.removeChild(obj.parentNode);
}

function removeField(c, type)
{
    var cur = fieldInFields('foli' + c);
    fields.splice(cur, 1);
    var field = document.getElementById('foli' + c);
    var label = document.getElementById('title' + c);
    var div = $('#itemOnPage' + c);
    div.remove();
//	label.parentNode.remove(label);
//	field.parentNode.remove();
    showFields(true);//show field settings 
    configureFieldSettingsOnLeftPanel();
    if (type != 'checkbox' && type != 'radio' && type != 'likert' && type != 'section' && type != 'plaintext') {
        var cstmFieldOpt = document.getElementById('cstmField').options;
        var prefixLen = parseInt(c.toString().length) + 2;
        for (i = 0; i < cstmFieldOpt.length; i++) {
            if (cstmFieldOpt[i].text.substr(0, prefixLen) == 'Q' + c + '-') {
                document.getElementById('cstmField').removeChild(cstmFieldOpt[i]);
                break;
            }
        }
        updateSMSContentLength();
    }
}

function makeEditable(event)
{
    console.log(event)
    var childs = document.getElementById('haupt').getElementsByTagName('li');
    for (i = 0; i < childs.length; i++)
    {
        if (childs[i].id)
        {
            var Elem = document.getElementById(childs[i].id);
            $('#' + Elem.id).removeClass('editing');
        }
    }
    //alert('clicked...'+this.id);
    $('#' + this.id).addClass('editing');
    event.stopPropagation();
    currently_editing = this.id;
    showFields(false);//show field settings
    //configureFieldSettingsOnLeftPanel();

}

function resetEditable(event)
{
//	event.stopPropagation();
//        console.log(222222)
    var childs = document.getElementById('haupt').getElementsByTagName('li');
    for (i = 0; i < childs.length; i++)
    {
        if (childs[i].id)
        {
            var Elem = document.getElementById(childs[i].id);
            $('#' + Elem.id).removeClass('editing');
        }
    }
    currently_editing = "";
//    document.getElementById('fieldTitle').innerHTML = "";
    showFields(true);
}

function showFields(show)//shows add a field if true
{
    $('#surveyForm').removeClass("editFrmTitle");
    if (show)
    {
        document.getElementById('formProperties').style.display = "none";
        document.getElementById('addFields').style.display = "block";
        document.getElementById('fieldProperties').style.display = "none";
        //document.getElementById('formProperties').style.display = "none";
        $('#stage').removeClass('cfo');
        $('#stage').removeClass('cfi');
        $('#stage').addClass('afi');
    }
    else
    {
        document.getElementById('formProperties').style.display = "none";
        document.getElementById('addFields').style.display = "none";
        document.getElementById('fieldProperties').style.display = "block";
        //document.getElementById('formProperties').style.display = "none";
        $('#stage').removeClass('cfo');
        $('#stage').removeClass('afi');
        $('#stage').addClass('cfi');
        configureFieldSettingsOnLeftPanel();
    }
}

function selectForm(event)
{
    /*
     document.getElementById('addFields').style.display = "none";
     document.getElementById('fieldProperties').style.display = "none";
     document.getElementById('formProperties').style.display = "block";
     $('#stage').removeClass('afi');
     $('#stage').removeClass('cfi');
     $('#stage').addClass('cfo');
     */
    document.getElementById('formProperties').style.display = "none";
    document.getElementById('addFields').style.display = "none";
    document.getElementById('fieldProperties').style.display = "block";
    // document.getElementById('formProperties').style.display = "none";
    $('#stage').removeClass('cfo');
    $('#stage').removeClass('afi');
    // $('#stage').removeClass('cfo');
    $('#stage').addClass('cfi');
    var childs = document.getElementById('haupt').getElementsByTagName('li');
    for (i = 0; i < childs.length; i++)
    {
        if (childs[i].id)
        {
            var Elem = document.getElementById(childs[i].id);
            $('#' + Elem.id).removeClass('editing');
        }
    }

    currently_editing = "";
    // document.getElementById('formName').innerHTML = document.getElementById("fname").innerHTML;
    // document.getElementById('formDescription').innerHTML = replaceNewline(document.getElementById("fdescription").innerHTML, "display");
    if (event)
    {
        event.stopPropagation();
    }
    formFieldsSettings();
}

function formSetting(event)
{
    document.getElementById('addFields').style.display = "none";
    document.getElementById('fieldProperties').style.display = "none";
    document.getElementById('formProperties').style.display = "block";
    $('#stage').removeClass('afi');
    $('#stage').removeClass('cfi');
    $('#stage').addClass('cfo');
    $('#surveyForm').removeClass('editFrmTitle');
    var childs = document.getElementById('haupt').getElementsByTagName('li');
    for (i = 0; i < childs.length; i++)
    {
        if (childs[i].id)
        {
            var Elem = document.getElementById(childs[i].id);
            $('#' + Elem.id).removeClass('editing');
        }
    }

    currently_editing = "";
    if (event)
    {
        event.stopPropagation();
    }
}

function updateForm(val, fieldName)
{
    var editingElem;
    if (fieldName == "Name")
    {
        editingElem = document.getElementById('fname');
        editingElem.innerHTML = val;
        document.getElementById("hidname").value = val;
    }
    else if (fieldName == "Description")
    {
        editingElem = document.getElementById('fdescription');
        editingElem.innerHTML = replaceNewline(val, "html");
        document.getElementById("hiddescription").value = val;
    }
}

function updateFromWidth(val)
{
    document.getElementById("hidwebwidth").value = val
}

function updateHideQuestion(checked)
{
    if (checked)
    {
        document.getElementById("hidwebHidQuestionNo").value = 1;
    }
    else
    {
        document.getElementById("hidwebHidQuestionNo").value = 0;
    }
}
function updateheaderAndFooter(checked)
{
    if (checked)
    {
        document.getElementById("hiduseHeaderAndFooter").value = 1;
    }
    else
    {
        document.getElementById("hiduseHeaderAndFooter").value = 0;
    }
}
function updateAlertErrorMessage(checked)
{
    if (checked)
    {
        document.getElementById("hidalertErrorMessage").value = 1;
    }
    else
    {
        document.getElementById("hidalertErrorMessage").value = 0;
    }
}

function updateLikeRequire(checked)
{
    if (checked)
    {
        document.getElementById("hidweblikerequire").value = 1;
    }
    else
    {
        document.getElementById("hidweblikerequire").value = 0;
    }
}

function updateSMSTrigger(checked)
{
    if (checked)
    {
        document.getElementById("hiduseSmsTrigger").value = 1;
        document.getElementById("smspanel").style.display = "block";
    }
    else
    {
        document.getElementById("hiduseSmsTrigger").value = 0;
        document.getElementById("smspanel").style.display = "none";
    }
}

function updateSMSContentLength()
{
    var msg = document.getElementById("smsContent").value
    document.getElementById("hidsmsContent").value = msg;
    var buffer = msg.split("{#RandomCode#}");
    var length = 0;
    if (buffer.length > 1)
    {
        for (var i = 1; i < buffer.length; i++)
        {
            length += buffer[i].length + code_length;
        }
    }
    length += buffer[0].length;

    var matched = true;
    var varLen = false;
    while (matched) {
        var re = new RegExp('{\\$Q(\\d+).+?}');
        var m = re.exec(msg);
        if (m == null) {
            matched = false;
        } else {
            cur = fieldInFields('foli' + m[1]);
            if (cur != -1) {
                length -= m[0].length;
                if (fields[cur].hasOwnProperty('length') && fields[cur].length != '' && fields[cur].length != 0) {
                    length += parseInt(fields[cur].length);
                } else {
                    varLen = true;
                }
            }
            msg = msg.substr(0, m.index) + msg.substr(m.index + m[0].length);
        }
    }
    if (varLen) {
        document.getElementById("wordcount").value = length.toString() + '+';
    } else {
        document.getElementById("wordcount").value = length;
    }
}

// Modify By Wood
// Show/Hidden the official Rule related panel
function updateOfficialRuleTrigger(checked)
{
    if (checked)
    {
        document.getElementById("hiduseOfficialRule").value = 1;
        document.getElementById("officialRuleContentpanel").style.display = "block";
        document.getElementById("officialRuleWordpanel").style.display = "block";
        document.getElementById("officialrulemandatorypanel").style.display = "block";

    }
    else
    {
        document.getElementById("hiduseOfficialRule").value = 0;
        document.getElementById("officialRuleContentpanel").style.display = "none";
        document.getElementById("officialRuleWordpanel").style.display = "none";
        document.getElementById("officialrulemandatorypanel").style.display = "none";
    }
}

// Update Required mandatory official rule hidden variable value 
function updateOfficialRuleRequire(checked)
{
    if (checked)
    {
        document.getElementById("hidmandatoryOfficialRule").value = 1;
    }
    else
    {
        document.getElementById("hidmandatoryOfficialRule").value = 0;
    }
}
// End Modify By Wood

function updateEntriesLimit(val)
{
    document.getElementById("hidentrieslimit").value = val
}

function fieldInFields(field_id)//returns the position of the field with id field_id in the fields array
{
    for (var i = 0; i < fields.length; i++)
    {
        if (fields[i].id == field_id)
        {
            return i;
        }
    }
    return -1;
}
function fieldInFieldsForRecorder(field_id)//returns the position of the field with id field_id in the fields array
{
    for (var i = 0; i < fields.length; i++)
    {
        if (fields[i].id == ('foli' + field_id))
        {
            return i;
        }
    }
    return -1;
}

function toggleChoicesLayout(val)
{

    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    $('#' + currently_editing).removeClass(field.columns);
    $('#' + currently_editing).addClass(val);
    field.columns = val;
}

function updateProperties(val, fieldProp, htmlObj)
{
//    alert(fieldProp);
    var editingElem;
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    if (currently_editing == "")
    {
        return;
    }
    if (fieldProp == "Title")
    {
//		editingElem = document.getElementById(currently_editing).getElementsByTagName('label')[0];
        var index = currently_editing.substring(4);
        editingElem = $('#title' + index);
        // if(field.type == "likert")
        // {
        // editingElem = document.getElementById(currently_editing).getElementsByTagName('caption')[0];
        // }
        if (field.type == "section")
        {
            editingElem = document.getElementById(currently_editing).getElementsByTagName('h3')[0];
        }
        if (val == "")
        {
            val = field.label;
            document.getElementById('fieldTitle').innerHTML = val;
        }
        //document.getElementById('fieldTitle').innerHTML = 'asdad';
        field.label = val;
        var requiredSign = '';
        if (field.required == 1) {
            requiredSign = '*';
        }
//		editingElem.innerHTML = replaceNewline(val, "html") + '<span class="req" id="req_'+field.count+'">'+ requiredSign +'</span>';
        editingElem.html(replaceNewline(val, "html") + '<span class="req" id="req_' + field.count + '">' + requiredSign + '</span>');

        if (field.type != 'checkbox' && field.type != 'radio' && field.type != 'likert' && field.type != 'section' && field.type != 'plaintext') {
            var cstmFieldOpt = document.getElementById('cstmField').options;
            var prefixLen = parseInt(field.count.toString().length) + 2;
            for (i = 0; i < cstmFieldOpt.length; i++) {
                if (cstmFieldOpt[i].text.substr(0, prefixLen) == 'Q' + field.count + '-') {
                    cstmFieldOpt[i].text = 'Q' + field.count + '-' + val;
                    break;
                }
            }
            var msg = document.getElementById("smsContent").value;
            var re = new RegExp('{\\$Q' + field.count + '-.+?}');
            var m = re.exec(msg);
            if (m != null) {
                var firstpart = msg.substr(0, m.index);
                document.getElementById("smsContent").value = firstpart + '{$Q' + field.count + '-' + val + '}' + msg.substr(firstpart.length + m[0].length);
                updateSMSContentLength();
            }
        }
    }
    else if (fieldProp == "Size")
    {
//        alert(val);
        editingElem = document.getElementById(currently_editing).getElementsByTagName('input')[0];
        if (!editingElem)
        {
            editingElem = document.getElementById(currently_editing).getElementsByTagName('textarea')[0];
        }
        if (!editingElem)
        {
            editingElem = document.getElementById(currently_editing).getElementsByTagName('select')[0];
        }
        $('#' + editingElem.id).removeClass(field.size);
        field.size = val;
        $('#' + editingElem.id).addClass(val);
    }
    else if (fieldProp == "IsRequired")
    {
        editingElem = $('#' + currently_editing).parent().prev();
        var index = currently_editing.substring(4);
        editingElem = $('#req_' + index);
//		editingElem = document.getElementById(currently_editing).getElementsByTagName('label')[0].getElementsByTagName('span')[0];
        // if(field.type == "likert")
        // {
        // editingElem = document.getElementById(currently_editing).getElementsByTagName('caption')[0].getElementsByTagName('span')[0];
        // }
        if (val == 1)
        {
            editingElem.html('*');
            field.required = 1;
            field.errorMessage = 'Please fill in your information';
            html = "";
            html += '<label class=" desc " for="errorMessage">Customize Empty Input Error Message</label>';
            html += '<div>';
            html += '<input type="text" id="errorMessage" name="errorMessage" maxlength="255" class="text cstmErrorMsg" onmouseup="updateProperties(this.value, \'errorMessage\',this)"';
            html += '	onkeyup="updateProperties(this.value, \'errorMessage\',this)" value="' + (field.errorMessage) + '">';
            html += '</div>';
            $('label[for="fieldRequired"]').after(html);
        }
        else
        {
            editingElem.html('');
            field.required = 0;
            field.errorMessage = '';
            $('#errorMessage').parent().remove();
            $('label[for="errorMessage"]').remove();
        }
    }
    else if (fieldProp == "UniqueCheck")
    {
        if (val == 1)
        {
            field.uniqueCheck = 1;
            uniqueErrorMsg = 'One ' + field.type + ' can only submit once';
            html = "";
            html += '<label class=" desc " for="uniqueErrorMsg">Customize Duplicate Input Error Message</label>';
            html += '<div>';
            html += '<input type="text" id="uniqueErrorMsg" name="uniqueErrorMsg" maxlength="255" class="text cstmErrorMsg" onmouseup="updateProperties(this.value, \'uniqueErrorMsg\',this)"';
            var uniqueCheckMsg = '';

            if (field.type == 'email') {
                uniqueCheckMsg = 'One ' + field.type + ' can only submit once';
            }
            else if (field.type == 'phone') {
                uniqueCheckMsg = 'One mobile number can only submit once';
            }
            else if (field.type == 'text') {
                uniqueCheckMsg = 'The answer is submitted before';
            }
            html += '	onkeyup="updateProperties(this.value, \'uniqueErrorMsg\',this)" value="' + uniqueCheckMsg + '">';
            html += '</div>';
            $('label[for="uniqueCheck"]').after(html);
        }
        else
        {
            field.uniqueCheck = 0;
            field.uniqueErrorMsg = '';
            $('#uniqueErrorMsg').parent().remove();
            $('label[for="uniqueErrorMsg"]').remove();
        }
    }
//	else if(fieldProp == "errorMessage")
//	{
//		field.errorMessage = val;
//	}
//	else if(fieldProp == "uniqueErrorMsg")
//	{
//		field.uniqueErrorMsg = val;
//	}
//	else if(fieldProp == "validErrorMsg")
//	{
//		field.validErrorMsg = val;
//	}
    else if ($.inArray(fieldProp, ["errorMessage", "uniqueErrorMsg", "validErrorMsg", "attributeCompareErrorMsg", "contactAttribute", "compareType"]) !== -1)
    {
        field[fieldProp] = val;
    }
    else if (fieldProp == "DefaultVal")
    {
        if (field.type == 'address')
        {
            all_options = document.getElementById(currently_editing).getElementsByTagName('option');
            for (var i = 0; i < all_options.length; i++)
            {
                if (all_options[i].value == val)
                {
                    all_options[i].selected = true;
                    field.defaultValue = val;
                }
            }
        }
        else
        {
            editingElem = document.getElementById(currently_editing).getElementsByTagName('input')[0];
            if (editingElem)
            {
                editingElem.value = val;
            }
            if (field.type == "section")
            {
                editingElem = document.getElementById(currently_editing).getElementsByTagName('div')[0];
                editingElem.innerHTML = val;
            }
            if (!editingElem)
            {
                editingElem = document.getElementById(currently_editing).getElementsByTagName('textarea')[0];
                editingElem.innerHTML = val;
            }
            if (!editingElem)
            {
                editingElem = document.getElementById(currently_editing).getElementsByTagName('div')[0];
                editingElem.innerHTML = val;
            }


            field.defaultValue = val;
        }
//        alert(val);
    }

    // Modify By Wood
    // Update the plain text value
    else if (fieldProp == "PlainText")
    {
        editingElem = document.getElementById(currently_editing).getElementsByTagName('div')[0];
        if (val == "")
        {
            val = field.label;
            document.getElementById('fieldTitle').innerHTML = val;
        }
        //document.getElementById('fieldTitle').innerHTML = 'asdad';
        field.label = val;
        editingElem.innerHTML = replaceNewline(val, "html");
    }
    else if (fieldProp == "PlainTextSize")
    {
        editingElem = document.getElementById(currently_editing).getElementsByTagName('div')[0];
        $('#' + editingElem.id).removeClass(field.size);
        field.size = val;
        $('#' + editingElem.id).addClass(val);
    }
    // End Modify By Wood

    // add by David NG - length check - 2011-0523 - start
    else if (fieldProp == "Length")
    {
        editingElem = document.getElementById(currently_editing).getElementsByTagName('input')[0];
        if (!editingElem)
        {
            editingElem = document.getElementById(currently_editing).getElementsByTagName('textarea')[0];
        }
        if (editingElem)
        {
            var tmp_val = val.replace(/[^\d]*/g, "");
            if (htmlObj)
            {
                htmlObj.value = tmp_val;
            }
            field.length = tmp_val;
        }
    }
    // add by David NG - length check - 2011-0523 - end
}

///////////////////////////////

function updateMobileAreaCode(val) {
    var editingElem;
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    if (currently_editing == "") {
        return;
    }
    var index = currently_editing.slice(4, 6);
    if (val == 1) {
        //update right panel ui
        var areacode_html = '<select disabled="disabled" readonly="readonly" id="areacode' + index + '" name="areacode' + index + '" style="margin-right:5px;"><option>Area Code</option></select>';
        var inputHtml = $('#Field' + index).before(areacode_html);

        field.addAreaCode = 1;
        //update the js object
        var editing = document.getElementById(currently_editing);
        var cur = fieldInFields(currently_editing);
        var field = fields[cur];
        if (field.choices.length == 0) {
            max = 0;
            pos = 0;
            var new_choice_obj1 = {id: max, label: '852', selected: 0};
            var new_choice_obj2 = {id: max + 1, label: '86', selected: 0};
            field.choices.splice(pos + 1, 0, new_choice_obj1, new_choice_obj2);
        }
        //update the ui in left panel
        li = document.createElement('li');
        li.id = "listChoices";
        li.className = "clear";
        li.style.display = "block";
        var areaCodeChoiceshtml = "";
        areaCodeChoiceshtml += '<fieldset class="choices">';
        areaCodeChoiceshtml += '<ul id="fieldChoices" style="width:300px;">';
        areaCodeChoiceshtml += '<legend>Area Code</legend>';
        for (i = 0; i < field.choices.length; i++) {
            areaCodeChoiceshtml += '<li>';
            areaCodeChoiceshtml += '<input type="radio" name="allChoices" title="Make this choice pre-selected."';
            if (field.choices[i].selected)
            {
                html += ' checked="checked"';
            }
            areaCodeChoiceshtml += ' class="" onclick="changeSelectDefault(\'' + field.choices[i].id + '\')"> ';
            areaCodeChoiceshtml += '<input type="text" id="xselect' + i + '" onkeyup="updateChoice(\'' + field.choices[i].id + '\', this.value);" value="' + field.choices[i].label + '" autocomplete="off" maxlength="150" class="text"> ';
            if (i > 0)
            {
                areaCodeChoiceshtml += '<img title="Add another choice." alt="Add" onclick="addAreaCodeSelect(\'' + field.choices[i].id + '\',this)" src="' + imgpath + '/img/template-editor/add.png"> ';
                if (i > 1)
                {
                    areaCodeChoiceshtml += '<img title="Delete this choice." alt="Delete" onclick="deleteAreaCodeSelect(\'' + field.choices[i].id + '\',this)" src="' + imgpath + '/img/template-editor/delete.png">';
                }
            }
            areaCodeChoiceshtml += '</li>';
        }
        areaCodeChoiceshtml += '</ul>';
        areaCodeChoiceshtml += '</fieldset>';
        li.innerHTML = areaCodeChoiceshtml;
        $('#Field' + index).css('width', '70%');
        allProps.appendChild(li);
    }
    else {
        $('#areacode' + index).remove();
        $('#Field' + index).css('width', '90%');
        field.addAreaCode = 0;
        $('#listChoices').remove();
        field.choices = [];
    }
}


function addYearOptionInBirth(val) {
    var editingElem;
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    if (currently_editing == "") {
        return;
    }
    var index = currently_editing.slice(4, 6);
    if (val == 1) {
        field_html = '';
        field_html += '<span class="time birthThreeColumn">';
        field_html += '<select id="Field' + index + '-3" name="Field' + index + '-3" class="field select birthSelect" readonly="readonly" disabled="disabled">';
        field_html += '<option value="">YYYY</option>';
        for (var i = 2013; i > 1900; i--)
        {
            field_html += '<option value="' + i + '">' + i + '</option>';
        }
        field_html += '</select>';
//		field_html += '<label for="Field'+index+'-3">YYYY-</label>';
        field_html += '</span>';
        $('#foli' + index + ' div span:first').before(field_html);
        $('#foli' + index + ' div span:eq(1)').removeClass('birthTwoColumn');
        $('#foli' + index + ' div span:eq(1)').addClass('birthThreeColumn');
        $('#foli' + index + ' div span:eq(2)').removeClass('birthTwoColumn');
        $('#foli' + index + ' div span:eq(2)').addClass('birthThreeColumn');
        $('#foli' + index + ' div span:eq(2) select').removeClass('mmSelect');
        $('#foli' + index + ' div span:eq(2) select').addClass('birthSelect');
        field.date_format = 'YYYY-MM-DD';
    }
    else {
        $('#foli' + index + ' div span:first').remove();
        $('#foli' + index + ' div span:first').removeClass('birthThreeColumn');
        $('#foli' + index + ' div span:first').addClass('birthTwoColumn');
        $('#foli' + index + ' div span:eq(1)').removeClass('birthThreeColumn');
        $('#foli' + index + ' div span:eq(1)').addClass('birthTwoColumn');
        $('#foli' + index + ' div span:eq(1) select').removeClass('birthSelect');
        $('#foli' + index + ' div span:eq(1) select').addClass('mmSelect');
        field.date_format = 'MM-DD';
    }
    ;
}

function updateOtherRequire(val) {
    var editingElem;
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    if (currently_editing == "") {
        return;
    }
    var index = currently_editing.slice(4, 6);
    editingElem = $("#otherReq" + index);
    if (val == 1)
    {
        editingElem.html('*');
        field.choices[field.choices.length - 1].otherRequired = 1;
    }
    else
    {
        editingElem.html('');
        field.choices[field.choices.length - 1].otherRequired = 0;
    }
}
function bindDragEvents(id)
{
    $('#' + id).draggable({
        containment: 'parent',
        cursor: 'pointer',
        snap: false,
        stack: '#' + id,
        revert: true,
        revertDuration: 0,
        helper: helper,
        cursorAt: {left: 0, top: 0}
    });

    $('#' + id).droppable({
        //tolerance: 'intersect',
        //hoverClass: 'nohvr',
        drop: dropped, over: reorderQuestion, out: out
    });
}

function helper()
{
    var blank_div = document.createElement("div");
    blank_div.style.width = "0px";
    blank_div.style.height = "0px";
    blank_div.style.display = "block";
    blank_div.style.borderStyle = "solid";
    blank_div.style.borderWidth = "0px";
    blank_div.style.backgroundColor = "white";
    blank_div.innerHTML = "";
    return blank_div;
}

function unbindDragEvents(id)
{
    //  $('#'+id).draggable("destroy");
    //  $('#'+id).droppable("destroy");
}

function dropped(ev, ui)
{
//    alert('dropped');
}
//for drag to reroder the question
function reorderQuestion(event, ui)
{
    var dragId = ui.draggable.attr('id');
    if (dragId.indexOf('itemOnPage') != -1) {
        var dragged = document.getElementById(ui.draggable.attr('id'));
        var parent = this.parentNode;
        var dragged_index;
        var over_index;
        var temp;
        if (this.nextSibling.id == dragged.id)	//move up
        {
            console.log('move-up')
            parent.removeChild(this);
            parent.insertBefore(this, dragged.nextSibling);
        }
        else // move down
        {
            console.log('move-down')
            parent.removeChild(dragged);
            parent.insertBefore(dragged, this.nextSibling);
        }
        dragged_index = fieldInFieldsForRecorder(dragged.id.substr(10));
        over_index = fieldInFieldsForRecorder(this.id.substr(10));

        if (dragged_index - over_index == 1) {
            temp = fields[dragged_index];
            fields[dragged_index] = fields[over_index];
            fields[over_index] = temp;
        } else if (dragged_index > over_index) {
            for (i = dragged_index; i > over_index + 1; i--) {
                temp = fields[i];
                fields[i] = fields[i - 1];
                fields[i - 1] = temp;
            }
        } else {
            for (i = dragged_index; i < over_index; i++) {
                temp = fields[i];
                fields[i] = fields[i + 1];
                fields[i + 1] = temp;
            }
        }
    }
}
function out(event, ui)
{
//    alert('out');
}
//////////////////////
function editOtherRadio(id, obj) {
    var checked = $("#xradio99").attr("checked");
    if (checked) {
        addOtherRadio(id, obj);
        //add other require option
        var otherRequire_html = '<input type="checkbox" id="radioOtherRequire" value=""';
        otherRequire_html += ' class="checkbox" onclick="(this.checked) ? checkVal = \'1\' : checkVal = \'0\';updateOtherRequire(checkVal)">';
        otherRequire_html += '<label for="radioOtherRequire" class="choice">&nbsp;&nbsp;Require for other Option</label>';
        $("label[for='fieldRequired']").after(otherRequire_html);
    }
    else {
        $("#xradio100").prop("disabled", 'disabled');
        deleteOtherRadio(100, obj);
        //remove other require option
        $("label[for='radioOtherRequire']").remove();
        $("#radioOtherRequire").remove();
    }
}

function addOtherRadio(id, obj) {
    var editing = document.getElementById(currently_editing);
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    max = 100;
    pos = max;
    var new_choice = document.createElement('span');
    var otherContent = $("#xradio100").val();
    $("#xradio100").prop("disabled", false);
    new_choice.innerHTML = '<input style="display:inline-block;" id="Field' + field.count + '-' + max + '" name="Field' + field.count + '-1" class="field checkbox" value="First Choice" readonly="readonly" disabled="disabled" type="radio" style="display:inline-block;" style="MARGIN: -23px 0px 0px 23px;">' +
            '<label style="display:inline;margin-left:10px;margin-right:10px;" class="frmchoice" for="Field' + field.count + '-' + max + '">' + otherContent + '</label>';
    new_choice.innerHTML += '<input style="display:inline-block;;width:40%;height:16px;" type="text" value="" id="Field' + field.count + '_otherContent" name="Field' + field.count + '_otherContent"></input>';
    new_choice.innerHTML += '<span id="otherReq' + field.count + '" class="req" style="float:none;width:5px;margin:0px;">';
    new_choice.innerHTML += '</span>';
    var li_div = editing.getElementsByTagName('div')[1];
    if (li_div.getElementsByTagName('span')[pos + 1])
    {
        li_div.insertBefore(new_choice, li_div.getElementsByTagName('span')[pos + 1]);
    }
    else
    {
        li_div.appendChild(new_choice);
    }
    var new_choice_obj = {id: max, label: 'Others:', selected: 0, valueid: ''};
    field.choices.splice(pos + 1, 0, new_choice_obj);
}

function editOtherCheckbox(id, obj) {
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var checked = $("#xchoices99").attr("checked");
    if (checked) {
        addOtherCheckbox(id, obj);
        $(obj).next().next().css("visibility", "visible");
        //add other require option
        var otherRequire_html = '<input type="checkbox" id="checkboxOtherRequire" value=""';
        otherRequire_html += ' class="checkbox" onclick="(this.checked) ? checkVal = \'1\' : checkVal = \'0\';updateOtherRequire(checkVal)">';
        otherRequire_html += '<label for="checkboxOtherRequire" class="choice">&nbsp;&nbsp;Require for other Option</label>';
        $("label[for='fieldRequired']").after(otherRequire_html);
    }
    else {
        $("#xchoices100").prop("disabled", 'disabled');
        deleteOtherCheckbox(100, obj);
        $(".multiOtherCheckbox").each(function () {
            deleteOtherCheckbox(this.id.substr(8), this);
        });
        $(obj).next().next().css("visibility", "hidden");
        //remove other require option
        $("label[for='checkboxOtherRequire']").remove();
        $("#checkboxOtherRequire").remove();
    }
}

function addOtherCheckbox(id, obj) {
    var editing = document.getElementById(currently_editing);
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, pos, max;
    for (i = 0, pos = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            pos = i;
            break;
        }
    }
    for (i = 0, max = -1; i < field.choices.length; i++)
    {
        if (field.choices[i].id > max)
        {
            max = field.choices[i].id;
        }
    }
    if (max < 99) {
        max = 99;
    }
    max++;
    if (pos == 0) {
        pos = max;
    }
    var new_choice = document.createElement('span');
    var otherContent = $("#xchoices100").val();
    $("#xchoices100").prop("disabled", false);
    new_choice.innerHTML = '<input style="display:inline-block;" id="Field' + field.count + '-' + max + '" name="Field' + field.count + '-1" class="field checkbox" value="First Choice" readonly="readonly" disabled="disabled" type="checkbox" style="display:inline-block;">';
    if (max == 100) {
        new_choice.innerHTML += '<label style="display:inline;margin-left:10px;margin-right:10px;" class="frmchoice" for="Field' + field.count + '-' + max + '">' + otherContent + '</label>';
    }
    else {
        new_choice.innerHTML += '<label style="display:inline;margin-left:10px;margin-right:10px;" class="frmchoice" for="Field' + field.count + '-' + max + '">New Other Option </label>';
    }
    new_choice.innerHTML += '<input style="display:inline-block;width:40%;height:16px;" type="text" value="" id="Field' + field.count + '_otherContent' + (pos + 1) + '" name="Field' + field.count + '_otherContent' + (pos + 1) + '"></input>';
    new_choice.innerHTML += '<span id="otherReq' + field.count + '" class="req" style="float:none;width:5px;margin:0px;">';
    new_choice.innerHTML += '</span>';
    var li_div = editing.getElementsByTagName('div')[1];
    if (li_div.getElementsByTagName('span')[pos + 1])
    {
        li_div.insertBefore(new_choice, li_div.getElementsByTagName('span')[pos + 1]);
    }
    else
    {
        li_div.appendChild(new_choice);
    }
    if (id == 99) {
        var new_choice_obj = {id: max, label: 'Others:', selected: 0, valueid: ''};
    }
    else {
        var new_choice_obj = {id: max, label: 'New Others Option', selected: 0, valueid: ''};
    }
    field.choices.splice(pos + 1, 0, new_choice_obj);
    var new_li = document.createElement('li');//this is for adding editing option for newly added choice in the left side
    new_li.innerHTML = '<input type="checkbox" style="visibility:hidden;" class="" name="allChoices"> ' +
            '<input type="text" id="xchoices' + max + '" onkeyup="updateChoice(\'' + max + '\', this.value);" value="New Others Option " autocomplete="off" maxlength="150" class="text multiOtherCheckbox"> ' +
//						'<img title="Add another choice." alt="Add" onclick="addOtherCheckbox(\''+max+'\',this)" src="' + imgpath + '/img/template-editor/add.png"> ' +
            '<img title="Delete this choice." alt="Delete" onclick="deleteOtherCheckbox(\'' + max + '\',this)" src="' + imgpath + '/img/template-editor/delete.png">';
    if (id != 99) {
        obj.parentNode.parentNode.insertBefore(new_li, obj.parentNode.nextSibling);
    }
}

function deleteOtherCheckbox(id, obj)
{
    var editing = document.getElementById(currently_editing);
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, pos, max;
    for (i = 0, pos = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            pos = i;
            break;
        }
    }
    for (i = 0, max = -1; i < field.choices.length; i++)
    {
        if (field.choices[i].id > max)
        {
            max = field.choices[i].id;
        }
    }
    if (pos == 0) {
        pos = max;
    }
    max++;
    var li_div = editing.getElementsByTagName('div')[1];
    li_div.removeChild(li_div.getElementsByTagName('span')[pos]);
    field.choices.splice(pos, 1);
    if (id != 100) {
        obj.parentNode.parentNode.removeChild(obj.parentNode);
    }
}


function deleteOtherRadio(id, obj)
{
    var editing = document.getElementById(currently_editing);
    var cur = fieldInFields(currently_editing);
    var field = fields[cur];
    var i, pos, max;
    for (i = 0, pos = 0; i < field.choices.length; i++)
    {
        if (field.choices[i].id == id)
        {
            pos = i;
            break;
        }
    }
    for (i = 0, max = -1; i < field.choices.length; i++)
    {
        if (field.choices[i].id > max)
        {
            max = field.choices[i].id;
        }
    }
    max++;
    var li_div = editing.getElementsByTagName('div')[1];
    li_div.removeChild(li_div.getElementsByTagName('span')[pos]);
    field.choices.splice(pos, 1);
}
function toObject(arr) {
    var rv = {};
    for (var i = 0; i < arr.length; ++i)
        if (arr[i] !== undefined)
            rv[arr[i].id] = arr[i].name;
    return rv;
}
