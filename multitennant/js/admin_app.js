/*
 * Generate password
 */
var keylist = "abcdefghijklmnopqrstuvwxyz123456789";
var temp = '';

// использование Math.round() даст неравномерное распределение!
function getRandomInt(min, max)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


function generatepass(plength) {
    temp = '';
    for (i = 0; i < plength; i++)
        temp += keylist.charAt(Math.floor(Math.random() * keylist.length));
    return temp;
}

function str_replace(search, replace, subject) {	// Replace all occurrences of the search string with the replacement strin

    if (!(replace instanceof Array)) {
        replace = new Array(replace);
        if (search instanceof Array) {//If search	is an array and replace	is a string, then this replacement string is used for every value of search
            while (search.length > replace.length) {
                replace[replace.length] = replace[0];
            }
        }
    }

    if (!(search instanceof Array))
        search = new Array(search);
    while (search.length > replace.length) {//If replace	has fewer values than search , then an empty string is used for the rest of replacement values
        replace[replace.length] = '';
    }

    if (subject instanceof Array) {//If subject is an array, then the search and replace is performed with every entry of subject , and the return value is an array as well.
        for (k in subject) {
            subject[k] = str_replace(search, replace, subject[k]);
        }
        return subject;
    }

    for (var k = 0; k < search.length; k++) {
        var i = subject.indexOf(search[k]);
        while (i > -1) {
            subject = subject.replace(search[k], replace[k]);
            i = subject.indexOf(search[k], i);
        }
    }

    return subject;

}

String.prototype.replaceAll = function( token, newToken, ignoreCase ) {
    var _token;
    var str = this + "";
    var i = -1;

    if ( typeof token === "string" ) {

        if ( ignoreCase ) {

            _token = token.toLowerCase();

            while( (
                i = str.toLowerCase().indexOf(
                    token, i >= 0 ? i + newToken.length : 0
                ) ) !== -1
            ) {
                str = str.substring( 0, i ) +
                    newToken +
                    str.substring( i + token.length );
            }

        } else {
            return this.split( token ).join( newToken );
        }

    }
return str;
};


function replaceVal(el, type) {
    var str = el.val();
    str = str_replace('/', '', str);
    str = str_replace('http', '', str);
    str = str_replace(':', '', str);
    str = str_replace(' ', '', str);
    str = str_replace('?', '', str);
    str = str_replace(',', '', str);
    if (type != 'url') {
        str = str_replace('.', '', str);
    }
    el.val(str);
}
// rgba to hex
function rgb2hex(rgb){
    rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
    return (rgb && rgb.length === 4) ? "#" +
    ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
    ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
    ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
}
//converter byte in kb,mb and gb
var fsize;
function conv_size(WeightFile) {
    var fsizekb = WeightFile / 1024;
    var fsizemb = fsizekb / 1024;
    var fsizegb = fsizemb / 1024;
    var fsizetb = fsizegb / 1024;
    if (fsizekb <= 1024) {
        fsize = fsizekb.toFixed(3) + ' kb';
    } else if (fsizekb >= 1024 && fsizemb <= 1024) {
        fsize = fsizemb.toFixed(3) + ' mb';
    } else if (fsizemb >= 1024 && fsizegb <= 1024) {
        fsize = fsizegb.toFixed(3) + ' gb';
    } else {
        fsize = fsizetb.toFixed(3) + ' tb';
    }
    return fsize;
}
function getWeightObj(srcBgImg) {
    var src = srcBgImg,
        imgq = new Image();
    imgq.src = src;
    imgq.onload = function () {
        var sizePrevbg = this.naturalWidth + 'x' + this.naturalHeight;
        var xhr = new XMLHttpRequest();
        xhr.open('HEAD', srcBgImg, true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var WeightFile = xhr.getResponseHeader('Content-Length');
                    conv_size(WeightFile);
                    $('#bgPrevWeight span').text(fsize);
                } else {
                    alert('ERROR');
                }
            }
        };
        xhr.send(null);
        $("#bgPrevSize span").text(sizePrevbg);
    };
}
// проверка на цифры
function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
var bodySelector = $('body');

//Get user oc
function getOCUser() {
    var OSName="Unknown OS";
    if (navigator.appVersion.indexOf("Win")!=-1) OSName="windows";
    if (navigator.appVersion.indexOf("Mac")!=-1) OSName="mac";
    if (navigator.appVersion.indexOf("Linux")!=-1) OSName="linux";
    return OSName;
}
// function insertAfter(elem, refElem) {
//     var parent = refElem.parentNode;
//     var next = refElem.nextSibling;
//     if (next) {
//         return parent.insertBefore(elem, next);
//     } else {
//         return parent.appendChild(elem);
//     }
// }
