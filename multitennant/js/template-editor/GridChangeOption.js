$(".gridOptin li, #organized").click(function () {
    $(".gridOptin li").removeClass("gridOptinActive");
    $(this).addClass("gridOptinActive");
    var countsGrid = $(this).attr('data-buttonId');
    $('.contForGridSpinner').attr('data-buttonId', countsGrid);
    switch (countsGrid) {
        case "1":
        case "4":
        case "5":
            GridOptionSetting.TwoGridColumn();
            GridOptionSetting.run(GridOptionSetting.drag1);
            break;
        case "2":
        case "6":
            GridOptionSetting.ThreeGridColumn();
            GridOptionSetting.run(GridOptionSetting.drag2);
            break;
        case "3":
        case "7":
            GridOptionSetting.fourGridColumn();
            GridOptionSetting.run(GridOptionSetting.drag3);
            break;
        case "8":
            GridOptionSetting.fiveGridColumn();
            GridOptionSetting.run(GridOptionSetting.drag4);
            break;
        case "9":
            GridOptionSetting.sixGridColumn();
            GridOptionSetting.run(GridOptionSetting.drag5);
            break;
    }
    if (grid.currentItem.hasClass('column')) {
        GridOptionSetting.row = grid.currentItem.parent();
    } else {
        GridOptionSetting.row = grid.currentItem;
    }
    GridOptionSetting.countColInRow = GridOptionSetting.row.find('.column').length;
    GridOptionSetting.column = GridOptionSetting.row.find('.column');
    GridOptionSetting.dragElement();
});


$(".gridOptinTablet:not(.gridOptinPhone) li").click(function () {
    $(".gridOptinTablet:not(.gridOptinPhone) li").removeClass("gridOptinActive");
    $(this).addClass("gridOptinActive");
    var id = $(this).attr('data-buttonid');
    GridOptionSetting.newColClassTabletMobile(id, 'sm');
});
$(".gridOptinTablet.gridOptinPhone li").click(function () {
    $(".gridOptinTablet.gridOptinPhone li").removeClass("gridOptinActive");
    $(this).addClass("gridOptinActive");
    var id = $(this).attr('data-buttonid');
    GridOptionSetting.newColClassTabletMobile(id, 'xs');
});
var GridOptionSetting = new function () {
    var self = this;
    this.row;
    this.countColInRow;
    this.zoneCont1;
    this.zoneCont2;
    this.zoneCont3;
    this.zoneCont4;
    this.zoneCont5;
    this.zoneCont6;
    this.countsGridInner;
    this.zoneNumb = [];
    this.dragSpinnerPos = ['', '15', '33', '51', '69', '87', '105', '127', '141', '159', '177', '195'];
    this.returnDynamycNumb = 0;
    this.column;
    this.run = function (callback) {
        self.zoneCont1 = $('#zoneCont1');
        self.zoneCont2 = $('#zoneCont2');
        self.zoneCont3 = $('#zoneCont3');
        self.zoneCont4 = $('#zoneCont4');
        self.zoneCont5 = $('#zoneCont5');
        self.zoneCont6 = $('#zoneCont6');
        self.countsGridInner = $('.contForGridSpinner').attr('data-buttonId');

        $(".dragElement").on("drag", function (event, ui) {
            //var flagGet = false;
            self.dragElement();
        });
        callback();
    };
    this.firstChangesGrid1 = function () {
        var drag1 = parseInt($('#drag1').css("left"));
        var DivWithHandle = parseInt($('.DivWithHandle').css("width"));
        var intervalDrag1Right = DivWithHandle - drag1;
        self.zoneCont1.css("width", drag1);
        self.zoneCont2.css({
            left: drag1 + 20,
            width: intervalDrag1Right - 6
        });
        var zoneCont1Numb = parseInt(self.zoneCont1.css("width"));
        self.zoneNumb['zone1'] = Math.ceil(zoneCont1Numb / 18);
        self.zoneCont1.find("p").text(self.zoneNumb['zone1']);

        var zoneCont2Numb = parseInt(self.zoneCont2.css("width"));
        self.zoneNumb['zone2'] = Math.ceil(zoneCont2Numb / 18);
        self.zoneCont2.find("p").text(self.zoneNumb['zone2']);
        self.zoneNumb['zone3'] = 0;
        self.zoneNumb['zone4'] = 0;
        self.zoneNumb['zone5'] = 0;
        self.zoneNumb['zone6'] = 0;
    };
    this.firstChangesGrid2 = function () {
        var drag1 = parseInt($('#drag1').css("left"));
        var drag2 = parseInt($('#drag2').css("left"));
        var drag2and1Interval = drag2 - drag1;
        var DivWithHandle = parseInt($('.DivWithHandle').css("width"));
        var intervalDrag2Right = DivWithHandle - drag2;
        self.zoneCont1.css("width", drag1);
        self.zoneCont2.css({
            left: drag1 + 20,
            width: drag2and1Interval
        });
        self.zoneCont3.css({
            left: drag2 + 24,
            width: intervalDrag2Right - 9
        });
        var zoneCont1Numb = parseInt(self.zoneCont1.css("width"));
        self.zoneNumb['zone1'] = Math.ceil(zoneCont1Numb / 18);
        self.zoneCont1.find("p").text(self.zoneNumb['zone1']);

        var zoneCont2Numb = parseInt(self.zoneCont2.css("width"));
        self.zoneNumb['zone2'] = Math.ceil(zoneCont2Numb / 18);
        self.zoneCont2.find("p").text(self.zoneNumb['zone2']);

        var zoneCont3Numb = parseInt(self.zoneCont3.css("width"));
        self.zoneNumb['zone3'] = Math.ceil(zoneCont3Numb / 18);
        self.zoneCont3.find("p").text(self.zoneNumb['zone3']);
        self.zoneNumb['zone4'] = 0;
        self.zoneNumb['zone5'] = 0;
        self.zoneNumb['zone6'] = 0;
    };
    this.firstChangesGrid3 = function () {
        var drag1 = parseInt($('#drag1').css("left"));
        var drag2 = parseInt($('#drag2').css("left"));
        var drag3 = parseInt($('#drag3').css("left"));
        var drag2and1Interval = drag2 - drag1;
        var drag2and3Interval = drag3 - drag2;
        var DivWithHandle = parseInt($('.DivWithHandle').css("width"));
        var intervalDrag2Right = DivWithHandle - drag3;
        self.zoneCont1.css("width", drag1);
        self.zoneCont2.css({
            left: drag1 + 21,
            width: drag2and1Interval
        });
        self.zoneCont3.css({
            left: drag2 + 27,
            width: drag2and3Interval
        });
        self.zoneCont4.css({
            left: drag3 + 32,
            width: intervalDrag2Right - 17
        });
        var zoneCont1Numb = parseInt(self.zoneCont1.css("width"));
        self.zoneNumb['zone1'] = Math.ceil(zoneCont1Numb / 18);
        self.zoneCont1.find("p").text(self.zoneNumb['zone1']);

        var zoneCont2Numb = parseInt(self.zoneCont2.css("width"));
        self.zoneNumb['zone2'] = Math.ceil(zoneCont2Numb / 18);
        self.zoneCont2.find("p").text(self.zoneNumb['zone2']);

        var zoneCont3Numb = parseInt(self.zoneCont3.css("width"));
        self.zoneNumb['zone3'] = Math.ceil(zoneCont3Numb / 18);
        self.zoneCont3.find("p").text(self.zoneNumb['zone3']);

        var zoneCont4Numb = parseInt(self.zoneCont4.css("width"));
        self.zoneNumb['zone4'] = Math.ceil(zoneCont4Numb / 18);
        self.zoneCont4.find("p").text(self.zoneNumb['zone4']);
        self.zoneNumb['zone5'] = 0;
        self.zoneNumb['zone6'] = 0;
    };
    this.firstChangesGrid4 = function () {
        var drag1 = parseInt($('#drag1').css("left"));
        var drag2 = parseInt($('#drag2').css("left"));
        var drag3 = parseInt($('#drag3').css("left"));
        var drag4 = parseInt($('#drag4').css("left"));
        var drag2and1Interval = drag2 - drag1;
        var drag2and3Interval = drag3 - drag2;
        var drag3and4Interval = drag4 - drag3;
        var DivWithHandle = parseInt($('.DivWithHandle').css("width"));
        var intervalDrag2Right = DivWithHandle - drag4;
        self.zoneCont1.css("width", drag1);
        self.zoneCont2.css({
            left: drag1 + 21,
            width: drag2and1Interval
        });
        self.zoneCont3.css({
            left: drag2 + 27,
            width: drag2and3Interval
        });
        self.zoneCont4.css({
            left: drag3 + 32,
            width: drag3and4Interval
        });
        self.zoneCont5.css({
            left: drag4 + 37,
            width: intervalDrag2Right - 22
        });
        var zoneCont1Numb = parseInt(self.zoneCont1.css("width"));
        self.zoneNumb['zone1'] = Math.ceil(zoneCont1Numb / 18);
        self.zoneCont1.find("p").text(self.zoneNumb['zone1']);

        var zoneCont2Numb = parseInt(self.zoneCont2.css("width"));
        self.zoneNumb['zone2'] = Math.ceil(zoneCont2Numb / 18);
        self.zoneCont2.find("p").text(self.zoneNumb['zone2']);

        var zoneCont3Numb = parseInt(self.zoneCont3.css("width"));
        self.zoneNumb['zone3'] = Math.ceil(zoneCont3Numb / 18);
        self.zoneCont3.find("p").text(self.zoneNumb['zone3']);

        var zoneCont4Numb = parseInt(self.zoneCont4.css("width"));
        self.zoneNumb['zone4'] = Math.ceil(zoneCont4Numb / 18);
        self.zoneCont4.find("p").text(self.zoneNumb['zone4']);

        var zoneCont5Numb = parseInt(self.zoneCont5.css("width"));
        self.zoneNumb['zone5'] = Math.ceil(zoneCont5Numb / 18);
        self.zoneCont5.find("p").text(self.zoneNumb['zone5']);
        self.zoneNumb['zone6'] = 0;
    };
    this.firstChangesGrid5 = function () {
        var drag1 = parseInt($('#drag1').css("left"));
        var drag2 = parseInt($('#drag2').css("left"));
        var drag3 = parseInt($('#drag3').css("left"));
        var drag4 = parseInt($('#drag4').css("left"));
        var drag5 = parseInt($('#drag5').css("left"));
        var drag2and1Interval = drag2 - drag1;
        var drag2and3Interval = drag3 - drag2;
        var drag3and4Interval = drag4 - drag3;
        var drag4and5Interval = drag5 - drag4;
        var DivWithHandle = parseInt($('.DivWithHandle').css("width"));
        var intervalDrag2Right = DivWithHandle - drag5;
        self.zoneCont1.css("width", drag1);
        self.zoneCont2.css({
            left: drag1 + 24,
            width: drag2and1Interval - 3
        });
        self.zoneCont3.css({
            left: drag2 + 26,
            width: drag2and3Interval - 1
        });
        self.zoneCont4.css({
            left: drag3 + 35,
            width: drag3and4Interval - 2
        });
        self.zoneCont5.css({
            left: drag4 + 40,
            width: drag4and5Interval
        });
        self.zoneCont6.css({
            left: drag5 + 45,
            width: intervalDrag2Right - 27
        });
        var zoneCont1Numb = parseInt(self.zoneCont1.css("width"));
        self.zoneNumb['zone1'] = Math.ceil(zoneCont1Numb / 18);
        self.zoneCont1.find("p").text(self.zoneNumb['zone1']);

        var zoneCont2Numb = parseInt(self.zoneCont2.css("width"));
        self.zoneNumb['zone2'] = Math.ceil(zoneCont2Numb / 18);
        self.zoneCont2.find("p").text(self.zoneNumb['zone2']);

        var zoneCont3Numb = parseInt(self.zoneCont3.css("width"));
        self.zoneNumb['zone3'] = Math.ceil(zoneCont3Numb / 18);
        self.zoneCont3.find("p").text(self.zoneNumb['zone3']);

        var zoneCont4Numb = parseInt(self.zoneCont4.css("width"));
        self.zoneNumb['zone4'] = Math.ceil(zoneCont4Numb / 18);
        self.zoneCont4.find("p").text(self.zoneNumb['zone4']);

        var zoneCont5Numb = parseInt(self.zoneCont5.css("width"));
        self.zoneNumb['zone5'] = Math.ceil(zoneCont5Numb / 18);
        self.zoneCont5.find("p").text(self.zoneNumb['zone5']);

        var zoneCont6Numb = parseInt(self.zoneCont6.css("width"));
        self.zoneNumb['zone6'] = Math.ceil(zoneCont6Numb / 18);
        self.zoneCont6.find("p").text(self.zoneNumb['zone6']);
    };
    this.drag1 = function () {
        $(".DivWithHandle *").on("drag", function (event, ui) {
            self.firstChangesGrid1();
        });
        //self.firstChangesGrid1();
        $('#drag1').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                //var flagGet = false;
                var drag1 = parseInt($('#drag1').css("left")) - 15;
                //var startPosition = 15;
                if (ui.position.left > 195) ui.position.left = 195;
                if (ui.position.left < 15) ui.position.left = 15;
            }
        });
        switch (self.countsGridInner) {
            case "1":
                $('.TwoGridColumn #drag1').css("left", "105px");
                self.firstChangesGrid1();
                break;
            case "4":
                $('.TwoGridColumn #drag1').css("left", "51px");
                self.firstChangesGrid1();
                break;
            case "5":
                $('.TwoGridColumn #drag1').css("left", "159px");
                self.firstChangesGrid1();
                break;
            default:
                self.firstChangesGrid1();
                break;
        }
    };
    this.drag2 = function () {
        //firstchangesGrid2();
        $(".DivWithHandle *").on("drag", function (event, ui) {
            $(this).css("borderColor", "#337ab7");
            self.firstChangesGrid2();
        });
        $('#drag1').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                //var flagGet = false;
                var drag2 = parseInt($('#drag2').css("left")) - 18;
                //var startPosition = 15;
                if (ui.position.left > drag2) ui.position.left = drag2;
                if (ui.position.left < 15) ui.position.left = 15;
            }
        });
        $('#drag2').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                //var flagGet = false;
                var DivWithHandle = parseInt($('.DivWithHandle').css("width")) - 27;
                var Zone1Width = parseInt($('#zoneCont1').css("width")) + 18;
                var startPosition = DivWithHandle;
                if (ui.position.left > startPosition) ui.position.left = startPosition;
                if (ui.position.left < Zone1Width) ui.position.left = Zone1Width;
            }
        });
        switch (self.countsGridInner) {
            case "2":
                $('#drag1').css("left", "69px");
                $('#drag2').css("left", "141px");
                self.firstChangesGrid2();
                break;
            case "6":
                $('#drag1').css("left", "51px");
                $('#drag2').css("left", "158px");
                self.firstChangesGrid2();
                break;
            default:
                self.firstChangesGrid2();
                break;
        }
    };
    this.drag3 = function () {
        //firstchangesGrid3();
        $(".DivWithHandle *").on("drag", function (event, ui) {
            //$(this).css("borderColor", "#337ab7");
            self.firstChangesGrid3();
        });
        $('#drag1').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                //var flagGet = false;
                var drag2 = parseInt($('#drag2').css("left")) - 18;
                //var startPosition = 18;
                if (ui.position.left > drag2) ui.position.left = drag2;
                if (ui.position.left < 18) ui.position.left = 18;
            }
        });
        $('#drag2').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                //var flagGet = false;
                var Zone1Width = parseInt($('#zoneCont1').css("width")) + 18;
                var drag2 = parseInt($('#zoneCont4').css("left")) - 40;
                if (ui.position.left > drag2) ui.position.left = drag2;
                if (ui.position.left < Zone1Width) ui.position.left = Zone1Width;
            }
        });
        $('#drag3').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                //var flagGet = false;
                var DivWithHandle = parseInt($('.DivWithHandle').css("width")) - 35;
                //var Zone1Width = parseInt($('#zoneCont2').css("width"))+68;

                var drag1Z = parseInt($('#drag2').css("left"));
                var Zone3Full = drag1Z + 17;

                var startPosition = DivWithHandle;
                if (ui.position.left > startPosition) ui.position.left = startPosition;
                if (ui.position.left < Zone3Full) ui.position.left = Zone3Full;
            }
        });
        switch (self.countsGridInner) {
            case "3":
                $('.fourGridColumn #drag1').css("left", "51px");
                $('.fourGridColumn #drag2').css("left", "105px");
                $('.fourGridColumn #drag3').css("left", "159px");
                self.firstChangesGrid3();
                break;
            case "7":
                $('.fourGridColumn #drag1').css("left", "36px");
                $('.fourGridColumn #drag2').css("left", "87px");
                $('.fourGridColumn #drag3').css("left", "177px");
                self.firstChangesGrid3();
                break;
            default:
                self.firstChangesGrid3();
                break;
        }
    };
    this.drag4 = function () {
        //firstchangesGrid4();
        $(".DivWithHandle *").on("drag", function (event, ui) {
            self.firstChangesGrid4();
        });
        $('#drag1').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                //var flagGet = false;
                var drag2 = parseInt($('#drag2').css("left")) - 18;
                //var startPosition = 15;
                if (ui.position.left > drag2) ui.position.left = drag2;
                if (ui.position.left < 15) ui.position.left = 15;
            }
        });
        $('#drag2').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                //var flagGet = false;
                var drag3 = parseInt($('#drag3').css("left")) - 18;
                var Zone1Width = parseInt($('#zoneCont1').css("width")) + 15;
                if (ui.position.left > drag3) ui.position.left = drag3;
                if (ui.position.left < Zone1Width) ui.position.left = Zone1Width;
            }
        });
        $('#drag3').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                //var flagGet = false;
                var drag3 = parseInt($('#drag4').css("left")) - 18;
                var drag1Z = parseInt($('#drag2').css("left"));
                var Zone1Full = drag1Z + 15;
                if (ui.position.left > drag3) ui.position.left = drag3;
                if (ui.position.left < Zone1Full) ui.position.left = Zone1Full;
            }
        });
        $('#drag4').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                //var flagGet = false;
                var DivWithHandle = parseInt($('.DivWithHandle').css("width")) - 37;
                var drag1Z = parseInt($('#drag3').css("left"));
                var Zone3Full = drag1Z + 15;
                var startPosition = DivWithHandle;
                if (ui.position.left > startPosition) ui.position.left = startPosition;
                if (ui.position.left < Zone3Full) ui.position.left = Zone3Full;
            }
        });
        self.firstChangesGrid4();
        switch (self.countsGridInner) {
            case "8":
                $('#drag1').css("left", "33px");
                $('#drag2').css("left", "66px");
                $('#drag3').css("left", "99px");
                $('#drag4').css("left", "159px");
                self.firstChangesGrid4();
                break;
            default:
                self.firstChangesGrid4();
                break;
        }
    };
    this.drag5 = function () {
        $('#drag1').css("left", "33px");
        $('#drag2').css("left", "66px");
        $('#drag3').css("left", "103px");
        $('#drag4').css("left", "133px");
        $('#drag5').css("left", "159px");

        self.firstChangesGrid5();
        $(".DivWithHandle *").on("drag", function (event, ui) {
            self.firstChangesGrid5();
        });
        $('#drag1').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                var flagGet = false;
                var drag2 = parseInt($('#drag2').css("left")) - 18;
                var startPosition = 15;
                if (ui.position.left > drag2) ui.position.left = drag2;
                if (ui.position.left < 15) ui.position.left = 15;
            }
        });
        $('#drag2').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                //var flagGet = false;
                var drag3 = parseInt($('#drag3').css("left")) - 18;
                var Zone1Width = parseInt($('#zoneCont1').css("width")) + 15;
                if (ui.position.left > drag3) ui.position.left = drag3;
                if (ui.position.left < Zone1Width) ui.position.left = Zone1Width;
            }
        });
        $('#drag3').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                //var flagGet = false;
                var drag3 = parseInt($('#drag4').css("left")) - 18;
                var drag1Z = parseInt($('#drag2').css("left"));
                var Zone1Full = drag1Z + 15;
                if (ui.position.left > drag3) ui.position.left = drag3;
                if (ui.position.left < Zone1Full) ui.position.left = Zone1Full;
            }
        });
        $('#drag4').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                //var flagGet = false;
                var drag5 = parseInt($('#drag5').css("left")) - 18;
                var drag1Z = parseInt($('#drag3').css("left"));
                var Zone3Full = drag1Z + 15;
                if (ui.position.left > drag5) ui.position.left = drag5;
                if (ui.position.left < Zone3Full) ui.position.left = Zone3Full;
            }
        });
        $('#drag5').draggable({
            axis: "x",
            containment: "parent",
            grid: [18],
            drag: function drag(event, ui) {
                //var flagGet = false;
                var DivWithHandle = parseInt($('.DivWithHandle').css("width")) - 45;
                var drag1Z = parseInt($('#drag4').css("left"));
                var Zone3Full = drag1Z + 15;
                var startPosition = DivWithHandle;
                if (ui.position.left > startPosition) ui.position.left = startPosition;
                if (ui.position.left < Zone3Full) ui.position.left = Zone3Full;
            }
        });
    };
    this.TwoGridColumn = function () {
        $(".contForGridSpinner").html('<div class="DivWithHandle TwoGridColumn"> \
                          <div class="ZoneContFull" id="zoneCont1"><p>1</p></div> \
                          <div id="drag1" class="dragElement"></div> \
                          <div class="ZoneContFull" id="zoneCont2"><p>2</p></div> \
                       </div>');
    };
    this.ThreeGridColumn = function () {
        $(".contForGridSpinner").html('<div class="DivWithHandle threeGridColumn"> \
                       <div class="ZoneContFull" id="zoneCont1"><p>1</p></div> \
                       <div id="drag1" class="dragElement"></div> \
                       <div class="ZoneContFull" id="zoneCont2"><p>2</p></div> \
                       <div id="drag2" class="dragElement"></div> \
                       <div class="ZoneContFull" id="zoneCont3"><p>3</p></div> \
                   </div>');
        //self.drag2();
    };
    this.fourGridColumn = function () {
        $(".contForGridSpinner").html('<div class="DivWithHandle fourGridColumn"> \
                            <div class="ZoneContFull" id="zoneCont1"><p>1</p></div> \
                            <div id="drag1" class="dragElement"></div> \
                            <div class="ZoneContFull" id="zoneCont2"><p>2</p></div> \
                            <div id="drag2" class="dragElement"></div> \
                            <div class="ZoneContFull" id="zoneCont3"><p>3</p></div> \
                            <div id="drag3" class="dragElement"></div> \
                            <div class="ZoneContFull" id="zoneCont4"><p>4</p></div> \
                         </div>');
        //self.drag3();
    };
    this.fiveGridColumn = function () {
        $(".contForGridSpinner").html('<div class="DivWithHandle FiveGridColumn"> \
                          <div class="ZoneContFull" id="zoneCont1"><p>1</p></div> \
                          <div id="drag1" class="dragElement"></div> \
                          <div class="ZoneContFull" id="zoneCont2"><p>2</p></div> \
                          <div id="drag2" class="dragElement"></div> \
                          <div class="ZoneContFull" id="zoneCont3"><p>3</p></div> \
                          <div id="drag3" class="dragElement"></div> \
                          <div class="ZoneContFull" id="zoneCont4"><p>4</p></div> \
                          <div id="drag4" class="dragElement"></div> \
                          <div class="ZoneContFull" id="zoneCont5"><p>4</p></div> \
                    </div>');
        //self.drag4();
    };
    this.sixGridColumn = function () {
        $(".contForGridSpinner").html('<div class="DivWithHandle SixGridColumn"> \
                         <div class="ZoneContFull" id="zoneCont1"><p>1</p></div> \
                         <div id="drag1" class="dragElement"></div> \
                         <div class="ZoneContFull" id="zoneCont2"><p>2</p></div> \
                         <div id="drag2" class="dragElement"></div> \
                         <div class="ZoneContFull" id="zoneCont3"><p>3</p></div> \
                         <div id="drag3" class="dragElement"></div> \
                         <div class="ZoneContFull" id="zoneCont4"><p>4</p></div> \
                         <div id="drag4" class="dragElement"></div> \
                         <div class="ZoneContFull" id="zoneCont5"><p>4</p></div> \
                         <div id="drag5" class="dragElement"></div> \
                         <div class="ZoneContFull" id="zoneCont6"><p>4</p></div> \
                     </div>');
        //self.drag5();
    };
    this.newColClassTabletMobile = function (id, colType) {
        var arr = [];
        switch (Number(id)) {
            case 0:
                arr = [12];
                break;
            case 1:
                arr = [6, 6];
                break;
            case 2:
                arr = [4, 4, 4];
                break;
            case 3:
                arr = [3, 3, 3, 3];
                break;
            case 4:
                arr = [3, 9];
                break;
            case 5:
                arr = [9, 3];
                break;
            case 6:
                arr = [3, 6, 3];
                break;
            case 7:
                arr = [2, 3, 5, 2];
                break;
            case 8:
                arr = [2, 2, 2, 4, 2];
                break;
            case 9:
                arr = [2, 2, 2, 2, 2, 2];
                break;
            case 10:
                break;
            case 11:
                arr = [6, 6, 6, 6];
                break;
            case 12:
                arr = [4, 4, 4, 4, 4, 4];
                break;
            default:
                arr = [];
                break;
        }
        self.changeMobileDeviceCol(arr, colType);
    };
    this.changeMobileDeviceCol = function (arr, colType) {
        if (arr.length > 0) {
            var row = grid.currentItem.parent();
            var column = row.find('.column');
            var index = 0;
            $.each(row.find('.column'), function () {
                self.removeColClass($(this), colType);
                $(this).addClass('col-' + colType + '-' + arr[index]);
                if (arr.length !== 1) {
                    index++;
                }
            });
            panels.changeContent();
            panels.changeHeightIframe();
        }
    };
    this.dragElement = function (e, flagGet) {
        self.countColInRow = self.row.find('.column').length;
        var drag1 = $('#drag1').css("left");
        var drag2 = $('#drag2').css("left");
        var drag3 = $('#drag3').css("left");
        var drag4 = $('#drag4').css("left");
        var drag5 = $('#drag5').css("left");
        self.deletActiveGrid();
        self.customGrid();
        // one dragable element
        if (self.countsGridInner === "1" || self.countsGridInner === "4" || self.countsGridInner === "5" || self.countsGridInner === "two") {
            if (drag1 === "105px") {
                self.deletActiveGrid();
                $("#GridOption1").addClass("gridOptinActive");
            } else if (drag1 === "51px") {
                self.deletActiveGrid();
                $("#GridOption4").addClass("gridOptinActive");
            } else if (drag1 === "159px") {
                self.deletActiveGrid();
                $("#GridOption5").addClass("gridOptinActive");
            } else {
                self.customGrid();
            }
            self.changeColumnGrid(2);
        }
        // two dragable element
        if (self.countsGridInner === "2" || self.countsGridInner === "6" || self.countsGridInner === "three") {
            if (drag1 === "69px" && drag2 === "141px" || drag2 === "140px") {
                self.deletActiveGrid();
                $("#GridOption2").addClass("gridOptinActive");
            } else if (drag1 === "51px" && drag2 === "158px" || drag2 === "159px") {
                self.deletActiveGrid();
                $("#GridOption6").addClass("gridOptinActive");
            } else {
                self.customGrid();
            }
            self.changeColumnGrid(3);
        }
        // three dragable element
        if (self.countsGridInner === "3" || self.countsGridInner === "7" || self.countsGridInner === 'four') {
            if (drag1 === "51px" || drag2 === "54px" && drag3 === "105px" && drag3 === "159px") {
                self.deletActiveGrid();
                $("#GridOption3").addClass("gridOptinActive");
            } else if (drag1 === "36px" && drag2 === "87px" && drag3 === "177px") {
                self.deletActiveGrid();
                $("#GridOption7").addClass("gridOptinActive");
            } else {
                self.customGrid();
            }
            self.changeColumnGrid(4);
        }
        // four dragable element
        if (self.countsGridInner === "8") {
            if (drag1 === "33px" && drag2 === "69px" && drag3 === "105px" && drag4 === "159px" || self.countsGridInner === 'five') {
                self.deletActiveGrid();
                $("#GridOption8").addClass("gridOptinActive");
            } else {
                this.customGrid();
            }
            this.changeColumnGrid(5);
        }
        // five dragable element
        if (self.countsGridInner === "9") {
            if (drag1 === "33px" && drag2 === "66px" && drag3 === "103px" && drag4 === "133px" && drag5 === "159px" || drag5 === "158px") {
                self.deletActiveGrid();
                $("#GridOption9").addClass("gridOptinActive");
            } else if (drag1 === "33px" && drag2 === "66px" && drag3 === "97px" && drag4 === "123px" && drag5 === "159px") {
                self.deletActiveGrid();
                $("#GridOption9").addClass("gridOptinActive");
            } else {
                self.customGrid();
            }
            self.changeColumnGrid(6);
        }
        panels.changeContent();
        panels.changeHeightIframe();
        if (flagGet == true) {
            grid.chekckcc(e);
        }
    };
    this.deletActiveGrid = function () {
        $(".gridOptin li").removeClass("gridOptinActive");
    };
    this.customGrid = function () {
        $(".gridOptin li").removeClass("gridOptinActive");
        $("#GridOption10").addClass("gridOptinActive");
    };
    this.removeColClass = function (element, col) {
        var classes = element.attr('class').split(/\s+/);
        var pattern = /^col-/;
        if (col && col === 'sm') {
            pattern = /^col-sm-/;
        }
        if (col && col === 'xs') {
            pattern = /^col-xs-/;
        }
        for (var i = 0; i < classes.length; i++) {
            var className = classes[i];
            if (className.match(pattern)) {
                element.removeClass(className);
            }
        }
    };
    this.newColClass = function () {
        var index = 1;
        $.each(self.row.find('.column'), function () {
            self.removeColClass($(this));
            $(this).addClass('col-lg-' + self.zoneNumb['zone' + index]);
            $(this).addClass('col-md-' + self.zoneNumb['zone' + index]);
            $(this).addClass('col-sm-' + self.zoneNumb['zone' + index]);
            $(this).addClass('col-xs-' + self.zoneNumb['zone' + index]);
            index++;
        });
    };
    this.changeColumnDeviceTablet = function () {
        var dragElementSel = $('.dragElement');
        var gridOptionTablet = $('.gridOptinTablet');
        var idbuttonId = $('.gridOptin').find('li.gridOptinActive').attr('data-buttonId');
        var buttonActTablet = gridOptionTablet.find('li[data-buttonId=' + idbuttonId + ']');
        gridOptionTablet.find('li').hide();
        gridOptionTablet.find('li[data-buttonId="0"]').css('display', 'inline-block');
        switch (idbuttonId) {
            case "1":
                gridOptionTablet.find('li[data-buttonId="4"]').css('display', 'inline-block');
                gridOptionTablet.find('li[data-buttonId="5"]').css('display', 'inline-block');
                buttonActTablet.css('display', 'inline-block');
                break;
            case "2":
                gridOptionTablet.find('li[data-buttonId="6"]').css('display', 'inline-block');
                buttonActTablet.css('display', 'inline-block');
                break;
            case "3":
                gridOptionTablet.find('li[data-buttonId="7"]').css('display', 'inline-block');
                gridOptionTablet.find('li[data-buttonId="11"]').css('display', 'inline-block');
                buttonActTablet.css('display', 'inline-block');
                break;
            case "4":
                gridOptionTablet.find('li[data-buttonId="5"]').css('display', 'inline-block');
                gridOptionTablet.find('li[data-buttonId="1"]').css('display', 'inline-block');
                buttonActTablet.css('display', 'inline-block');
                break;
            case "5":
                gridOptionTablet.find('li[data-buttonId="1"]').css('display', 'inline-block');
                gridOptionTablet.find('li[data-buttonId="4"]').css('display', 'inline-block');
                buttonActTablet.css('display', 'inline-block');
                break;
            case "6":
                gridOptionTablet.find('li[data-buttonId="2"]').css('display', 'inline-block');
                buttonActTablet.css('display', 'inline-block');
                break;
            case "7":
                gridOptionTablet.find('li[data-buttonId="3"]').css('display', 'inline-block');
                gridOptionTablet.find('li[data-buttonId="11"]').css('display', 'inline-block');
                buttonActTablet.css('display', 'inline-block');
                break;
            case "8":
                gridOptionTablet.find('li[data-buttonId="8"]').css('display', 'inline-block');
                buttonActTablet.css('display', 'inline-block');
                break;
            case "9":
                gridOptionTablet.find('li[data-buttonId="9"]').css('display', 'inline-block');
                gridOptionTablet.find('li[data-buttonId="12"]').css('display', 'inline-block');
                buttonActTablet.css('display', 'inline-block');
                break;
            case "10":
                if ($('#drag1').length != "" && $('#drag2').length == "") {
                    gridOptionTablet.find('li[data-buttonId="4"]').css('display', 'inline-block');
                    gridOptionTablet.find('li[data-buttonId="5"]').css('display', 'inline-block');
                    gridOptionTablet.find('li[data-buttonId="1"]').css('display', 'inline-block');
                    buttonActTablet.css('display', 'inline-block');
                }
                if ($('#drag2').length != "" && $('#drag3').length == "") {
                    gridOptionTablet.find('li[data-buttonId="6"]').css('display', 'inline-block');
                    gridOptionTablet.find('li[data-buttonId="2"]').css('display', 'inline-block');
                    buttonActTablet.css('display', 'inline-block');
                }
                if ($('#drag3').length != "" && $('#drag4').length == "") {
                    gridOptionTablet.find('li[data-buttonId="3"]').css('display', 'inline-block');
                    gridOptionTablet.find('li[data-buttonId="7"]').css('display', 'inline-block');
                    gridOptionTablet.find('li[data-buttonId="11"]').css('display', 'inline-block');
                    buttonActTablet.css('display', 'inline-block');
                }
                if ($('#drag4').length != "" && $('#drag5').length == "") {
                    gridOptionTablet.find('li[data-buttonId="8"]').css('display', 'inline-block');
                    buttonActTablet.css('display', 'inline-block');
                }
                if ($('#drag5').length != "") {
                    gridOptionTablet.find('li[data-buttonId="9"]').css('display', 'inline-block');
                    gridOptionTablet.find('li[data-buttonId="12"]').css('display', 'inline-block');
                    buttonActTablet.css('display', 'inline-block');
                }
                break;
        }
    };
    this.getColSize = function (pattern, classColElem) {
        var classIndex = classColElem.getAttribute('class').indexOf(pattern) + pattern.length;
        var searchedClass = classColElem.getAttribute('class').substring(classColElem.getAttribute('class').indexOf(pattern), classColElem.getAttribute('class').indexOf(pattern) + pattern.length + 2).trim();
        var splitedArray = searchedClass.split("-");
        var numbCol = splitedArray[splitedArray.length - 1];
        return numbCol;
    };
    this.getColumnSize = function (item, e, flagGet) {
        var childrenElemRow;
        if (item.hasClass('column')) {
            var parentElemRow = item.parent('.row');
            childrenElemRow = parentElemRow.children('.column');
            self.row = parentElemRow;
        } else {
            childrenElemRow = item.children('.column');
            self.row = item;
        }

        self.countColInRow = self.row.find('.column').length;
        self.column = self.row.find('.column');
        var numbCol = [];
        var drags1;
        var drags2;
        var drags3;
        var drags4;
        var drags5;
        var posFirstSpin;
        var posSecondSpin;
        var posTheerSpin;
        var posFiveSpin;
        var contForGridSpinner = $('.contForGridSpinner');
        var colColumn = childrenElemRow.length;
        var i = 0;
        if (colColumn === 2) {
            contForGridSpinner.attr('data-buttonid', 'two');
            self.TwoGridColumn();
            childrenElemRow.each(function (index) {
                var classColElem = childrenElemRow[index];
                var pattern = "col-lg-";
                numbCol.push(self.getColSize(pattern, classColElem));
            });
            drags1 = $('#drag1');
            for (i = 0; i < numbCol.length; i++) {
                if (i === 0) {
                    posFirstSpin = parseInt(numbCol[0]);
                    firstWithSec = posFirstSpin;
                    drags1.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                }
            }
            self.run(self.drag1);
            //setTimeout(function () {
            //    //$('#organized').click();
            //    //contForGridSpinner.attr('data-buttonid', 'two');
            //    //self.drag1();
            //    //self.run(self.drag1);
            //    //GridOptionSetting();
            //    $('.contForGridSpinner').attr('data-buttonid', '');
            //    numbCol = [];
            //    //dragElement(e, flagGet);
            //}, 100)

        } else if (colColumn === 3) {
            contForGridSpinner.attr('data-buttonid', 'three');
            self.ThreeGridColumn();
            childrenElemRow.each(function (index) {
                var classColElem = childrenElemRow[index];
                var pattern = "col-lg-";
                numbCol.push(self.getColSize(pattern, classColElem));
            });
            drags1 = $('#drag1');
            drags2 = $('#drag2');
            for (i = 0; i < numbCol.length; i++) {
                if (i === 0) {
                    var posFirstSpin = parseInt(numbCol[0]);
                    firstWithSec = posFirstSpin;
                    drags1.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                } else if (i === 1) {
                    //var posFirstSpin = parseInt(numbCol[0]);
                    var posSecondSpin = parseInt(numbCol[1]);
                    var firstWithSec = posFirstSpin + posSecondSpin;
                    drags2.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                }
            }
            self.run(self.drag2);
            //setTimeout(function () {
            //    // self.dragSpinnerPos
            //    //$('#organized1').click();
            //    contForGridSpinner.attr('data-buttonid', 'three');
            //    self.run(self.drag2);
            //    contForGridSpinner.attr('data-buttonid', '');
            //    numbCol = [];
            //    // dragElement(e, flagGet);
            //}, 100)
        } else if (colColumn === 4) {
            contForGridSpinner.attr('data-buttonid', 'four');
            self.fourGridColumn();
            childrenElemRow.each(function (index) {
                var classColElem = childrenElemRow[index];
                var pattern = "col-lg-";
                numbCol.push(self.getColSize(pattern, classColElem));
            });
            drags1 = $('#drag1');
            drags2 = $('#drag2');
            drags3 = $('#drag3');
            for (i = 0; i < numbCol.length; i++) {
                if (i === 0) {
                    posFirstSpin = parseInt(numbCol[0]);
                    firstWithSec = posFirstSpin;
                    drags1.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                } else if (i === 1) {
                    //posFirstSpin = parseInt(numbCol[0]);
                    posSecondSpin = parseInt(numbCol[1]);
                    firstWithSec = posFirstSpin + posSecondSpin;
                    drags2.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                } else if (i === 2) {
                    //posFirstSpin = parseInt(numbCol[0]);
                    //posSecondSpin = parseInt(numbCol[1]);
                    var posTheerSpin = parseInt(numbCol[2]);
                    firstWithSec = posFirstSpin + posSecondSpin + posTheerSpin;
                    drags3.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                }
            }
            self.run(self.drag3);
            //setTimeout(function () {
            //    // self.dragSpinnerPos
            //    //$('#organized2').click();
            //    contForGridSpinner.attr('data-buttonid', 'four');
            //    self.run(self.drag3);
            //    contForGridSpinner.attr('data-buttonid', '');
            //    numbCol = [];
            //    //dragElement(e, flagGet);
            //}, 100)
        } else if (colColumn === 5) {
            contForGridSpinner.attr('data-buttonid', 'five');
            self.fiveGridColumn();
            childrenElemRow.each(function (index) {
                var classColElem = childrenElemRow[index];
                var pattern = "col-lg-";
                numbCol.push(self.getColSize(pattern, classColElem));
            });
            drags1 = $('#drag1');
            drags2 = $('#drag2');
            drags3 = $('#drag3');
            drags4 = $('#drag4');
            for (i = 0; i < numbCol.length; i++) {
                if (i === 0) {
                    posFirstSpin = parseInt(numbCol[0]);
                    firstWithSec = posFirstSpin;
                    drags1.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                } else if (i === 1) {
                    //posFirstSpin = parseInt(numbCol[0]);
                    posSecondSpin = parseInt(numbCol[1]);
                    firstWithSec = posFirstSpin + posSecondSpin;
                    drags2.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                } else if (i === 2) {
                    //posFirstSpin = parseInt(numbCol[0]);
                    //posSecondSpin = parseInt(numbCol[1]);
                    posTheerSpin = parseInt(numbCol[2]);
                    firstWithSec = posFirstSpin + posSecondSpin + posTheerSpin;
                    drags3.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                } else if (i === 3) {
                    //posFirstSpin = parseInt(numbCol[0]);
                    //posSecondSpin = parseInt(numbCol[1]);
                    //posTheerSpin = parseInt(numbCol[2]);
                    var posFiveSpin = parseInt(numbCol[3]);
                    firstWithSec = posFirstSpin + posSecondSpin + posTheerSpin + posFiveSpin;
                    drags4.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                }
            }
            self.run(self.drag4);
            //setTimeout(function () {
            //    // self.dragSpinnerPos
            //    //$('#organized3').click();
            //    contForGridSpinner.attr('data-buttonid', 'five');
            //    self.run(self.drag4);
            //    contForGridSpinner.attr('data-buttonid', '');
            //    numbCol = [];
            //    //dragElement(e, flagGet);
            //}, 100);
        } else if (colColumn === 6) {
            contForGridSpinner.attr('data-buttonid', 'six');
            self.sixGridColumn();
            childrenElemRow.each(function (index) {
                var classColElem = childrenElemRow[index];
                var pattern = "col-lg-";
                numbCol.push(self.getColSize(pattern, classColElem));
            });
            drags1 = $('#drag1');
            drags2 = $('#drag2');
            drags3 = $('#drag3');
            drags4 = $('#drag4');
            drags5 = $('#drag5');
            for (i = 0; i < numbCol.length; i++) {
                if (i === 0) {
                    posFirstSpin = parseInt(numbCol[0]);
                    firstWithSec = posFirstSpin;
                    drags1.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                } else if (i === 1) {
                    posSecondSpin = parseInt(numbCol[1]);
                    firstWithSec = posFirstSpin + posSecondSpin;
                    drags2.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                } else if (i === 2) {
                    posTheerSpin = parseInt(numbCol[2]);
                    firstWithSec = posFirstSpin + posSecondSpin + posTheerSpin;
                    drags3.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                } else if (i === 3) {
                    var posFiveSpin = parseInt(numbCol[3]);
                    firstWithSec = posFirstSpin + posSecondSpin + posTheerSpin + posFiveSpin;
                    drags4.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                } else if (i === 4) {
                    var posSixSpin = parseInt(numbCol[4]);
                    firstWithSec = posFirstSpin + posSecondSpin + posTheerSpin + posFiveSpin + posSixSpin;
                    drags5.css('left', self.dragSpinnerPos[firstWithSec] + 'px');
                }
            }
            self.run(self.drag5);
            //setTimeout(function () {
            //    // self.dragSpinnerPos
            //    //$('#organized4').click();
            //    contForGridSpinner.attr('data-buttonid', 'six');
            //    self.run(self.drag5);
            //    contForGridSpinner.attr('data-buttonid', '');
            //    numbCol = [];
            //    // dragElement(e, flagGet);
            //}, 200);

        }
    };
    this.changeColumnGrid = function (num) {
        var resCol = self.countColInRow - num;
        if (resCol > 0) {
            for (var i = 0; i < resCol; i++) {
                var res = self.countColInRow - (i + 1);
                $(self.column[res]).remove();
            }
        }
        var resCol = num - self.countColInRow;
        if (resCol > 0) {
            for (var i = 0; i < resCol; i++) {
                var rand = getRandomInt(1111111, 9999999);
                self.row.append('<div class="column" data-e-id="' + rand + '"></div>');
            }
        }
        self.changeColumnDeviceTablet();
        self.newColClass();
        grid.initElements();
        pages.recurseDomChildrenUpdate();
    }
};












