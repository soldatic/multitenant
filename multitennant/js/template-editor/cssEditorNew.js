var globalCssEditor = new function() {
    var selfGlobal = this;
    //создаем глобальную переменную currentItem
    this.currentItem;
    // get styles Item
    this.getStyleItem = function (item) {
        selfGlobal.currentItem = item;
        var style = window.getComputedStyle(selfGlobal.currentItem[0], null);
        this.marginTop = style.marginTop;
        this.marginBottom = style.marginBottom;
        this.marginLeft = style.marginLeft;
        this.marginRight = style.marginRight;
        this.paddingTop = style.paddingTop;
        this.paddingBottom = style.paddingBottom;
        this.paddingLeft = style.paddingLeft;
        this.paddingRight = style.paddingRight;
        this.display = style.display;
        this.float = style.float;
        this.clear = style.clear;
        this.overflow = style.overflow;
        this.position = style.position;
        this.positionTop = style.top;
        this.positionBottom = style.bottom;
        this.positionLeft = style.left;
        this.positionRight = style.right;
        this.zIndex = style.zIndex;
        //for background
        this.backgroundAttachment = style.backgroundAttachment;
        this.backgroundColor = style.backgroundColor;
        this.backgroundImage = style.backgroundImage;
        this.backgroundPosition = style.backgroundPosition;
        this.backgroundRepeat = style.backgroundRepeat;
        this.backgroundSize = style.backgroundSize;
        this.divWithInfoPrevBgCover = $('.divWithInfoPrevBg button[data-original-title="Cover"]');
        this.divWithInfoPrevBgContain = $('.divWithInfoPrevBg button[data-original-title="Contain"]');
    };

};
globalCssEditor.getStyleItem();