/**
 * Created by devds on 05.02.16.
 */
var gulp = require('gulp');
var watch = require('gulp-watch');
const babel = require('gulp-babel');

gulp.task('default', function(){
        gulp.watch('js/gulpDir/GridChangeOption.js', function(changes){
            console.log(changes);
            return gulp.src("js/gulpDir/GridChangeOption.js")
                .pipe(babel({
                    presets: ['es2015']
                }))
                .pipe(gulp.dest("js/template-editor/"));
        });

    }
);
