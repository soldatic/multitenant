<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/template-editor/jquery-custom-scrollbar/jquery.mCustomScrollbar.css"/>
   <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->request->baseUrl; ?>/css/lightbox.css" />
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/libs/jquery-2.0.2.min.js"></script>
    <style type="text/css">
        .item-map {
            width: 100%;
            height: 300px;
        }
        .item-youtube,
        .item-youtube iframe{
            width: 100%;
            height: 100%;
        }
    </style>
    <title><?php echo $this->pageTitle; ?></title>
</head>
<body>
<?php echo $content; ?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<!-- BOOTSTRAP JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap/bootstrap.min.js"></script>
<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/select2/select2.min.js"></script>
<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<!--<script src="/js/app.min.js"></script>-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/admin_app.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/maps.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/lightbox/lightbox-plus-jquery.js"></script>
</body>
</html>
