<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>


<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
    <div class="well no-padding">
        <!--<form action="index.html" id="login-form" class="smart-form client-form" novalidate="novalidate">-->
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
//                'class' => 'smart-form client-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
                'htmlOptions'=>array(
                          'class'=>'smart-form client-form',
                        )
            ));
            ?>
            <header>
                Sign In
            </header>

            <fieldset>

                <section>
                    <label class="label">E-mail</label>
                    <label class="input"> <i class="icon-append fa fa-user"></i>
                        <div class="row">

                            <?php echo $form->textField($model, 'username', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;')); ?>
                            <?php echo $form->error($model, 'username'); ?>
                        </div>


                        <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address/username</b></label>
                </section>

                <section>
                    <label class="label">Password</label>
                    <label class="input"> <i class="icon-append fa fa-lock"></i>
                        <div class="row">

                            <?php echo $form->passwordField($model, 'password', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;')); ?>
                            <?php echo $form->error($model, 'password'); ?>

                        </div>
                        <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
                    <!--                    <div class="note">
                                            <a href="forgotpassword.html">Forgot password?</a>
                                        </div>-->
                </section>

                <section>
                    <!--                    <label class="checkbox">
                                            <input type="checkbox" name="remember" checked="">
                                            <label class="checkbox">
                                                <input type="checkbox" name="remember" checked="">-->


<!--                            <i></i>Stay signed in</label>-->

                    <div class="row rememberMe">
                        <label class="checkbox"> 
                            <?php echo $form->checkBox($model, 'rememberMe', array('name' => 'remember', 'checked' => '','style' => 'margin: 0px 0px 0px 15px; width: 94%;' )); ?>
                            <i></i>Stay signed in
                        </label>
                    </div> 
                </section>
            </fieldset>
            <footer>
<!--                <button type="submit" class="btn btn-primary">
                    Sign in
                </button>-->
                <?php echo CHtml::submitButton('Login',array('class' => 'btn btn-primary' ) ); ?>
            </footer>

            <?php $this->endWidget(); ?>
        <!--</form>-->

    </div>  

</div>

