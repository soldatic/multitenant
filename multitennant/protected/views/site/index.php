<?php
echo $dataPage['tpl_page_content'];
?>
<script>
    $(document).ready(function () {
        <?php
        $map = $dataPage['tpl_page_content_options']->map;
        foreach($map as $m){
        ?>
        mapsData.init(JSON.parse('<?php echo json_encode($m);?>'));
        <?php } ?>
    });
</script>
