<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <!--<a class="navbar-brand" href="#">Start Bootstrap</a>-->
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav editorUL">
        <?php
        foreach ($pages as $page) {
            if ($page['parent']->tpl_page_display_menu == 1 && $page['parent']->tpl_page_status == 1) {
                if (count($page['child']) > 0) {
                    ?>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/site/<?php echo $page['parent']->tpl_page_id; ?>"><?php echo $page['parent']->tpl_page_name; ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu editorUL">
                            <?php
                            foreach ($page['child'] as $p) {
                                ?>
                                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/site/<?php echo $p->tpl_page_id; ?>"><?php echo $p->tpl_page_name; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php
                } else {
                    ?>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/site/<?php echo $page['parent']->tpl_page_id; ?>"><?php echo $page['parent']->tpl_page_name; ?></a>
                    </li>
                    <?php
                }
            }
        }
        ?>
    </ul>
</div>
<!-- /.navbar-collapse -->
<!-- /.container -->

