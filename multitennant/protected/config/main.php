<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

return array(
    //Multi tenant module
    //startup before load site
    'behaviors' => array(
        'onBeginRequest' => array(
            'class' => 'multitenant.components.Startup'
        ),
    ),
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Superadmin site',
    // preloading 'log' component
    'preload' => array('log'),
    'import' => array(
        'application.models.*',
        'application.modules.multitenant.models.*', //Multi tenant models in module(onload)
        'application.components.*',
        'application.admin.modules.templates.models.*',
//        'bootstrap.helpers.TbHtml',
    ),
    'modules' => array(
// uncomment the following to enable the Gii tool
        'multitenant', //Multi tenant module
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '123',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        )
    ),
    // path aliases
//    'aliases' => array(
//        'bootstrap' => realpath(__DIR__ . '/../extensions/bootstrap'), // change this if necessary
//    ),
    // application components
    'components' => array(
        
        'bootstrap' => array(
//            'class' => 'bootstrap.components.TbApi',
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
            ),
        ),
        // uncomment the following to use a MySQL database
        'db' => require(dirname(__FILE__) . '/db.php'),
        'dbDefault' => require(dirname(__FILE__) . '/db.php'),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
// using Yii::app()->params['paramName']
    'params' => array(
// this is used in contact page
        'adminEmail' => 'webmaster@example.com',
        'baseUrl' => '',
        'baseDomain' => ($domain == 'multitenancy.dev') ? 'multitenancy.dev' : 'multitenancy.dev',
        'adminHome' => '/admin/'
    ),
);
