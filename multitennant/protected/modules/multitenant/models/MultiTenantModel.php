<?php

class MultiTenantModel extends CActiveRecord {

    public function tableName() {
        return 'cms_domains';
    }

    public function rules() {
        // define all model rules but make sure that ‘tenant_dbu’ is declared
        // safe because they’re handled in UserController
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Menus the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

// everything else that goes into the model
}
