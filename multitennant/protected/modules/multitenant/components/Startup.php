<?php

class Startup extends CBehavior {

    protected $serverName = '';

    public function events() {
        return array_merge(parent::events(), array(
            'onBeginRequest' => 'beginRequest',
        ));
    }

    public function beginRequest() {
//        if (Yii::app()->params['domain'] == 0) {
//        }
        //if server name does not match to default domain runs multitenan
        $this->serverName = CHttpRequest::getServerName();
        if ($this->serverName != Yii::app()->params['baseDomain']) {
            Yii::app()->params['domain'] = 1;
            //get db params in database
            $dataMTenant = MultiTenantModel::model()->find('domain_url=:domain and status=:status', array(
                ':domain' => $this->serverName,
                ':status' => '1'
            ));
            if (count($dataMTenant) > 0) {
                Yii::app()->params['domainId'] = $dataMTenant->domain_id;
//            $userData = DatabaseUsers::model()->find('db_user_id = :db_user_id', array(':db_user_id' => $dataMTenant->domain_db_user_id));
                //if connection is wrong set default params for db
                try {
                    Yii::app()->db->setActive(false);
                    Yii::app()->db->connectionString = 'mysql:host=' . $dataMTenant->domain_db_host . ';dbname=' . $dataMTenant->domain_db;
                    Yii::app()->db->username = $dataMTenant->domain_db_user;
                    Yii::app()->db->password = $dataMTenant->domain_db_user_pass;
                    Yii::app()->db->setActive(true);
                    Yii::app()->name = $dataMTenant->domain_name;
                } catch (Exception $ex) {
                    
                }
            } 
        }
    }

}
