<?php

class SiteController extends Controller {

    public $tplId = 1;
    public $pages = array();
    public $pageTitle = '';

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $tplPage = TemplatesPages::model()->with('tpl')->find('tpl_page_default=:act and tpl.tpl_id=:tplId', array(
            ':act' => 1,
            ':tplId' => $this->tplId
        ));
        $dataPage = array();
        if (count($tplPage) > 0) {
            $dataPage = array(
                'tpl_page_id' => $tplPage->tpl_page_id,
                'tpl_id' => $tplPage->tpl_id,
                'tpl_parent_page_id' => $tplPage->tpl_parent_page_id,
                'tpl_theme_id' => $tplPage->tpl->tpl_theme_id,
                'tpl_page_name' => $tplPage->tpl_page_name,
                'tpl_page_content' => $tplPage->tpl_page_content,
                'tpl_page_content_options' => json_decode($tplPage->tpl_page_content_options),
                'tpl_page_settings' => json_decode($tplPage->tpl_page_settings),
                'tpl_page_meta_settings' => json_decode($tplPage->tpl_page_meta_settings),
            );
            $this->pageTitle = $tplPage->tpl_page_title;
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'tpl.tpl_id=:tplId';
            $criteria->limit = 1;
            $criteria->params = array(':tplId' => $this->tplId);
            $criteria->order = 'tpl_page_position ASC';
            $tplPage = TemplatesPages::model()->with('tpl')->findAll($criteria);
            if (count($tplPage) > 0) {
                $tplPage = $tplPage[0];
                $dataPage = array(
                    'tpl_page_id' => $tplPage->tpl_page_id,
                    'tpl_id' => $tplPage->tpl_id,
                    'tpl_parent_page_id' => $tplPage->tpl_parent_page_id,
                    'tpl_theme_id' => $tplPage->tpl->tpl_theme_id,
                    'tpl_page_name' => $tplPage->tpl_page_name,
                    'tpl_page_content' => $tplPage->tpl_page_content,
                    'tpl_page_content_options' => json_decode($tplPage->tpl_page_content_options),
                    'tpl_page_settings' => json_decode($tplPage->tpl_page_settings),
                    'tpl_page_meta_settings' => json_decode($tplPage->tpl_page_meta_settings),
                );
                $this->pageTitle = $tplPage->tpl_page_title;
            }
        }

        $criteria = new CDbCriteria;
        $criteria->condition = 'tpl.tpl_id=:tplId';
        $criteria->condition = 'tpl_parent_page_id=0';
        $criteria->order = 'tpl_page_position ASC';
        $criteria->params = array(
            ':tplId' => $this->tplId,
        );

        $parentPages = TemplatesPages::model()->with('tpl')->findAll($criteria);
        foreach ($parentPages as $key => $pPage) {
            $criteria = new CDbCriteria;
            $criteria->condition = 'tpl.tpl_id=:tplId';
            $criteria->addCondition('tpl_parent_page_id=:parentId');
            $criteria->order = 'tpl_page_position ASC';
            $criteria->params = array(
                ':tplId' => $this->tplId,
                ':parentId' => $pPage->tpl_page_id
            );
            $pages = TemplatesPages::model()->with('tpl')->findAll($criteria);
            $this->pages[$key] = array(
                'parent' => $parentPages[$key],
                'child' => $pages
            );
        }

        $menu = $this->renderPartial('menu', array(
            'pages' => $this->pages
                ), TRUE);

        $this->render('index', array(
            'dataPage' => $dataPage,
            'menu' => $menu
        ));
    }

    /**
     *
     */
    public function actionView() {
        $idPage = (int) CHttpRequest::getParam('id');
        if (isset($idPage)) {
            $criteria = new CDbCriteria;
            $criteria->condition = 'tpl.tpl_id=:tplId';
            $criteria->condition = 'tpl_page_id=' . $idPage;
            $criteria->limit = 1;
            $criteria->params = array(
                ':tplId' => $this->tplId
            );
            $criteria->order = 'tpl_page_position ASC';
            $tplPage = TemplatesPages::model()->with('tpl')->findAll($criteria);
            if (count($tplPage) > 0) {
                $tplPage = $tplPage[0];
                $dataPage = array(
                    'tpl_page_id' => $tplPage->tpl_page_id,
                    'tpl_id' => $tplPage->tpl_id,
                    'tpl_parent_page_id' => $tplPage->tpl_parent_page_id,
                    'tpl_theme_id' => $tplPage->tpl->tpl_theme_id,
                    'tpl_page_name' => $tplPage->tpl_page_name,
                    'tpl_page_content' => $tplPage->tpl_page_content,
                    'tpl_page_content_options' => json_decode($tplPage->tpl_page_content_options),
                    'tpl_page_settings' => json_decode($tplPage->tpl_page_settings),
                    'tpl_page_meta_settings' => json_decode($tplPage->tpl_page_meta_settings),
                );
                $this->pageTitle = $tplPage->tpl_page_title;

                $criteria = new CDbCriteria;
                $criteria->condition = 'tpl.tpl_id=:tplId';
                $criteria->condition = 'tpl_parent_page_id=0';
                $criteria->order = 'tpl_page_position ASC';
                $criteria->params = array(
                    ':tplId' => $this->tplId,
                );

                $parentPages = TemplatesPages::model()->with('tpl')->findAll($criteria);
                foreach ($parentPages as $key => $pPage) {
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'tpl.tpl_id=:tplId';
                    $criteria->addCondition('tpl_parent_page_id=:parentId');
                    $criteria->order = 'tpl_page_position ASC';
                    $criteria->params = array(
                        ':tplId' => $this->tplId,
                        ':parentId' => $pPage->tpl_page_id
                    );
                    $pages = TemplatesPages::model()->with('tpl')->findAll($criteria);
                    $this->pages[$key] = array(
                        'parent' => $parentPages[$key],
                        'child' => $pages
                    );
                }

                $menu = $this->renderPartial('menu', array(
                    'pages' => $this->pages
                        ), TRUE);

                $this->render('index', array(
                    'dataPage' => $dataPage,
                    'menu' => $menu
                ));
            } else {
                $this->redirect('/site');
            }
        } else {
            $this->redirect('/site');
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

}
