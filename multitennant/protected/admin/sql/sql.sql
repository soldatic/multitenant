
CREATE TABLE IF NOT EXISTS `{DB}`.`cms_logs` (
`log_id` int(11) NOT NULL,
  `log_type` enum('info','error','warning') NOT NULL DEFAULT 'info',
  `log_action` varchar(255) NOT NULL,
  `log_info` varchar(255) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `{DB}`.`cms_roles` (
`role_id` int(11) NOT NULL,
  `role_name` varchar(32) NOT NULL,
  `role_access` enum('1','3','7') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `{DB}`.`cms_roles_users` (
`ru_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `{DB}`.`cms_settings` (
`sid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `comments` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;


INSERT INTO `{DB}`.`cms_settings` (`sid`, `title`, `value`, `comments`) VALUES
(1, 'activationStatusOnDefault', '0', ''),
(2, 'param', 'pam', ''),
(3, 'googleApiKey', 'AIzaSyAoZ9cYGr20CGlsxZaGmMjASEJOqGdzWwQ', '');

CREATE TABLE IF NOT EXISTS `{DB}`.`cms_users` (
`uid` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('superadministrator','administrator','moderator','user') NOT NULL DEFAULT 'user',
  `phone` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT  '0000-00-00 00:00:00',
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `forgot_key` varchar(255) DEFAULT NULL,
  `forgot_date` datetime DEFAULT NULL,
  `email_key` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

ALTER TABLE `{DB}`.`cms_logs`
 ADD PRIMARY KEY (`log_id`);

ALTER TABLE `{DB}`.`cms_roles`
 ADD PRIMARY KEY (`role_id`);

ALTER TABLE `{DB}`.`cms_roles_users`
 ADD PRIMARY KEY (`ru_id`), ADD KEY `uid` (`uid`);

ALTER TABLE `{DB}`.`cms_settings`
 ADD PRIMARY KEY (`sid`);

ALTER TABLE `{DB}`.`cms_users`
 ADD PRIMARY KEY (`uid`), ADD KEY `uid` (`uid`);

ALTER TABLE `{DB}`.`cms_logs`
MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `{DB}`.`cms_roles`
MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `{DB}`.`cms_roles_users`
MODIFY `ru_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `{DB}`.`cms_settings`
MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;

ALTER TABLE `{DB}`.`cms_users`
MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;

ALTER TABLE `{DB}`.`cms_roles_users`
ADD CONSTRAINT `cms_roles_users_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `{DB}`.`cms_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO `{DB}`.`cms_users` (`uid`, `first_name`, `last_name`, `email`, `password`, `role`, `phone`, `create_date`, `last_login`, `update_date`, `status`, `forgot_key`, `forgot_date`, `email_key`) VALUES
(NULL, '', '', '{EMAIL}', '{PASSWORD}', 'superadministrator', '', '{DATE}', '0000-00-00 00:00:00', '{DATE}', '1', '', '0000-00-00 00:00:00', '');

CREATE TABLE IF NOT EXISTS `{DB}`.`cms_languages` (
`lid` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `sort_position` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `{DB}`.`cms_languages_default` (
`id` int(11) NOT NULL,
  `key` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `{DB}`.`cms_languages`
 ADD PRIMARY KEY (`lid`);

ALTER TABLE `{DB}`.`cms_languages`
MODIFY `lid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `{DB}`.`cms_languages_default`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `{DB}`.`cms_languages_default`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



CREATE TABLE IF NOT EXISTS `{DB}`.`cms_templates` (
`tpl_id` int(11) NOT NULL,
  `tpl_theme_id` int(11) NOT NULL,
  `tpl_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;


INSERT INTO `{DB}`.`cms_templates` (`tpl_id`, `tpl_theme_id`, `tpl_status`) VALUES
(1, 1, '1');

CREATE TABLE IF NOT EXISTS `{DB}`.`cms_templates_pages` (
`tpl_page_id` int(11) NOT NULL,
  `tpl_id` int(11) NOT NULL,
  `tpl_parent_page_id` int(11) DEFAULT '0',
  `tpl_page_name` varchar(255) NOT NULL,
  `tpl_page_content` longtext NOT NULL,
  `tpl_page_header` text NOT NULL,
  `tpl_page_footer` text NOT NULL,
  `tpl_page_settings` text NOT NULL,
  `tpl_page_keywords` text NOT NULL,
  `ttpl_page_description` text NOT NULL,
  `tpl_page_style` int(11) NOT NULL,
  `tpl_page_status` enum('1','0') NOT NULL DEFAULT '1',
  `tpl_page_default` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


INSERT INTO `{DB}`.`cms_templates_pages` (`tpl_page_id`, `tpl_id`, `tpl_parent_page_id`, `tpl_page_name`, `tpl_page_content`, `tpl_page_header`, `tpl_page_footer`, `tpl_page_settings`, `tpl_page_keywords`, `ttpl_page_description`, `tpl_page_style`, `tpl_page_status`, `tpl_page_default`) VALUES
(1, 1, 0, '', '[]', '[{"x":0,"y":0,"width":12,"height":1,"data":"<p><span style=\\"color:#FFFFFF\\"><span style=\\"font-size:72px\\">Clean Blog</span></span></p>\\n","bgImage":"/upload/galleryManager/0c7c6d35c7d60b7f02bf5679ff228d85.jpg","padding":"150px"}]', '', '', '', '', 1, '1', 1);

CREATE TABLE IF NOT EXISTS `{DB}`.`cms_templates_uploads` (
`tpl_up_id` int(11) NOT NULL,
  `tpl_up_title` varchar(255) NOT NULL,
  `tpl_up_image` varchar(255) NOT NULL,
  `tpl_up_url` varchar(255) NOT NULL,
  `tpl_up_length` varchar(255) NOT NULL,
  `tpl_up_status` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

INSERT INTO `{DB}`.`cms_templates_uploads` (`tpl_up_id`, `tpl_up_title`, `tpl_up_image`, `tpl_up_url`, `tpl_up_length`, `tpl_up_status`) VALUES
(1, '', '6f3a41a6ad64ab9a3d4b562fc990a685.jpg', '', '115144', 0),
(2, '', '6d9c6bf5476a4a81687c0d74e15f93ef.jpg', '', '140909', 0),
(3, '', '0c7c6d35c7d60b7f02bf5679ff228d85.jpg', '', '172779', 0),
(4, '', '85c60fa9a6596b0a85aeb5cade3b72f7.jpg', '', '290070', 0),
(5, '', '8ae8e4f8119bacc60cd2ea29c6106ad8.jpg', '', '33097', 0);

ALTER TABLE `{DB}`.`cms_templates`
 ADD PRIMARY KEY (`tpl_id`);

ALTER TABLE `{DB}`.`cms_templates_pages`
 ADD PRIMARY KEY (`tpl_page_id`);

ALTER TABLE `{DB}`.`cms_templates_uploads`
 ADD PRIMARY KEY (`tpl_up_id`);

ALTER TABLE `{DB}`.`cms_templates`
MODIFY `tpl_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

ALTER TABLE `{DB}`.`cms_templates_uploads`
MODIFY `tpl_up_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;