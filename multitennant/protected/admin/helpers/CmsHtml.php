<?php

class CmsHtml {

    public function getAllLanguages() {
//        if (Yii::app()->params->domain == 0) {
            $defaultLanguage = Yii::app()->params->defaultLanguage;
            $allLanguages = Languages::model()->findAll(array('order'=>'sort_position ASC'));
            $defaultLangList = AllLanguages::model()->findAll('code2 = "' . $defaultLanguage . '"');
            $allLanguages = CHtml::listData($allLanguages, 'language.code2', 'language.nativeName');
            $allLanguages = array_merge($allLanguages, CHtml::listData($defaultLangList, 'code2', 'nativeName'));

            return CHtml::dropDownList('lang', Yii::app()->language, $allLanguages, array('style' => 'border: none; color: #3276b1; background: none;'));
        }
//    }

}
