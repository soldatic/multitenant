<?php

class SettingsHelper {

    public function getSettingValue($param) {
        $data = Settings::model()->find('title=:key', array(
            ':key' => $param,
        ));
        if (count($data) > 0) {
            return $data->value;
        } else {
            return false;
        }
    }

}
