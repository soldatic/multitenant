<?php

class MailHelper {

    public function sendMail($view, $subject, $email) {
        $message = new YiiMailMessage;
        $message->setBody($view, 'text/html');
        $message->subject = $subject;
        $message->addTo($email);
        $message->from = Yii::app()->params['adminEmail'];
        Yii::app()->mail->send($message);
    }

}
