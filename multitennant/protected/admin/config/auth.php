<?php

return array(
    'user' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'user',
        'bizRule' => null,
        'data' => null
    ),
    'moderator' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'moderator',
        'children' => array(
            'user', // унаследуемся от юзера
        ),
        'bizRule' => null,
        'data' => null
    ),
    'administrator' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'administrator',
        'children' => array(
            'moderator', // позволим модератору всё, что позволено модератору
        ),
        'bizRule' => null,
        'data' => null
    ),
    'superadministrator' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'superadministrator',
        'children' => array(
            'administrator', // позволим админу всё, что позволено админу
        ),
        'bizRule' => null,
        'data' => null
    ),
);
