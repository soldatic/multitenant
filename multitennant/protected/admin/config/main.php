<?php

$admin = dirname(dirname(__FILE__));
$frontend = dirname($admin);

Yii::setPathOfAlias('front', $frontend);
Yii::setPathOfAlias('admin', $admin);
Yii::setPathOfAlias('core', dirname(Yii::getPathOfAlias('front')));

return array(
    'behaviors' => array(
        'onBeginRequest' => array(
            'class' => 'application.modules.multitenant.components.Startup'
        ),
    ),
    'preload' => array('log'),
    'basePath' => $frontend,
    'controllerPath' => $admin . '/controllers',
    'modulePath' => $admin . '/modules',
    'viewPath' => $admin . '/views',
    'runtimePath' => $admin . '/runtime',
    'name' => 'Backend',
    'import' => array(
        'admin.models.*',
        'admin.components.*',
        'application.models.*',
        'admin.modules.users.models.*',
        'admin.modules.sites.models.*',
        'admin.modules.languages.models.*',
        'admin.modules.settings.models.*',
        'admin.helpers.*',
        'application.components.*',
        'application.modules.multitenant.models.*', //Multi tenant models in module(onload)
        'application.extensions.yii-mail-master.*',
    ),
    'modules' => array(
        'settings',
        'sites',
        'users',
        'templates',
        'languages',
    ),
    'components' => array(
        'authManager' => array(
            // Our authorization manager
            'class' => 'PhpAuthManager',
            // Role on default is guest.
            'defaultRoles' => array('guest'),
        ),
        'mail' => array(
            'class' => 'application.extensions.yii-mail-master.YiiMail',
            'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => 'smtp.gmail.com',
                'username' => 'petrusha.dv@gmail.com',
                'password' => 'jsud1614tpqb2202',
                'port' => '465',
                'encryption' => 'ssl',
            ),
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false
        ),
        'user' => array(
            'class' => 'WebUser',
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                'admin/ajax/<action:\w+>' => 'ajax/<action>',
                'admin/site/login' => 'index/login',
                'admin/forgot' => 'index/forgot',
                'admin/index/index' => 'index/index',
                'admin/index' => 'index/index',
                'admin' => 'index/index',
                'admin/login' => 'index/login',
                'admin/logout' => 'index/logout',
                'admin/<module>' => '<module>/default',
                'admin/<module>/<controller:database|domain|keys>' => '<module>/<controller>/index',
                'admin/<module>/<action:\w+>' => '<module>/default/<action>',
                'admin/<module>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                'admin/<controller:\w+>' => '<controller>/index',
                'admin/<controller:\w+>/<id:\d+>' => '<controller>/view',
                'admin/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                'admin/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'messages' => array(
            'basePath' => Yiibase::getPathOfAlias('admin.messages'),
            'forceTranslation' => TRUE
        ),
        'db' => require($frontend . '/config/db.php'),
        'dbDefault' => require($frontend . '/config/db.php'),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
    'sourceLanguage' => 'en',
    'language' => 'en',
    'params' => array(
        'adminEmail' => 'support@multitenancy.dev',
        'adminHome' => '/admin/',
        'defaultLanguage' => 'en',
        'baseDomain' => ($domain == 'multitenant.pol') ? 'multitenant.pol' : 'multitenancy.dev',
        'domain' => '0',
        'domainId' => 0,
        'allLanguages' => array(),
        'defaultLanguageList' => array()
    ),
);
