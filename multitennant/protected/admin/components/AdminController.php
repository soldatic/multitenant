<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class AdminController extends CController {

    public $menu = array();
    public $breadcrumbs = array();
    public $language = '';
    public $allLanguages = array();
    public $domainId;

    function init() {
        $defaultLanguage = Yii::app()->params->defaultLanguage;
        $this->domainId = Yii::app()->params['domainId'];
        $lang = CHttpRequest::getPost('lang');
        if ($lang != '') {
            $this->language = $lang;
            unset(Yii::app()->request->cookies['lang']);
            Yii::app()->request->cookies['lang'] = new CHttpCookie('lang', $lang);
        } else {
            $cookie = isset(Yii::app()->request->cookies['lang']) ? Yii::app()->request->cookies['lang']->value : false;
            if ($cookie && $cookie != '') {
                $this->language = $cookie;
            } else {
                $this->language = $defaultLanguage;
            }
        }

//        Yii::app()->language = $this->language;
        Yii::app()->setLanguage($this->language);
    }

}
