<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />-->
        <!--[if lt IE 8]>-->
        <!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />-->
        <!--<![endif]-->

        <!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />-->
        <!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />-->


        <!-- Basic Styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" />

        <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->request->baseUrl; ?>/css/smartadmin-production.min.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->request->baseUrl; ?>/css/smartadmin-skins.min.css" />

        <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->request->baseUrl; ?>/css/demo.min.css" />
        <!-- Ptivate css-->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ptivate.css" />
        <!-- pnotify css-->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->request->baseUrl; ?>/css/pnotify.custom.min.css" />

        <!-- FAVICONS -->
        <link rel="shortcut icon" href="img/favicon/favicon.ico" type="<?php echo Yii::app()->request->baseUrl; ?>/image/x-icon" />
        <link rel="icon" href="img/favicon/favicon.ico" type="<?php echo Yii::app()->request->baseUrl; ?>/image/x-icon" />

        <!-- GOOGLE FONT -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700" />

        <!-- Specifying a Webpage Icon for Web Clip 
                 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
        <link rel="apple-touch-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/img/splash/sptouch-icon-iphone.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo Yii::app()->request->baseUrl; ?>/img/splash/touch-icon-ipad.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo Yii::app()->request->baseUrl; ?>/img/splash/touch-icon-iphone-retina.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo Yii::app()->request->baseUrl; ?>/img/splash/touch-icon-ipad-retina.png" />

<!--            <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
            <script>
                if (!window.jQuery) {
                    document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');
                }
            </script>-->
        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>
        <?php // Yii::app()->bootstrap->register(); ?>

        <div class="container-fluid noPadding" id="page">

            <header id="header">
                <div id="logo-group">

                    <!-- PLACE YOUR LOGO HERE -->
                    <span id="logo"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/logo.png" alt="xGate" /> </span>
                    <!-- END LOGO PLACEHOLDER -->

                    <!-- Note: The activity badge color changes when clicked and resets the number to 0
                    Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->
                    <!--<span id="activity" class="activity-dropdown"> <i class="fa fa-user"></i> <b class="badge"> 21 </b> </span>-->

                    <!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->
                    <div class="ajax-dropdown">

                        <!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
                        <div class="btn-group btn-group-justified" data-toggle="buttons">
                            <label class="btn btn-default">
                                <input type="radio" name="activity" id="ajax/notify/mail.html">
                                    Msgs (14) </label>
                            <label class="btn btn-default">
                                <input type="radio" name="activity" id="ajax/notify/notifications.html">
                                    notify (3) </label>
                            <label class="btn btn-default">
                                <input type="radio" name="activity" id="ajax/notify/tasks.html">
                                    Tasks (4) </label>
                        </div>

                        <!-- notification content -->
                        <div class="ajax-notifications custom-scroll">

                            <div class="alert alert-transparent">
                                <h4>Click a button to show messages here</h4>
                                This blank page message helps protect your privacy, 
                                or you can show the first message here automatically.
                            </div>

                            <i class="fa fa-lock fa-4x fa-border"></i>

                        </div>
                        <!-- end notification content -->

                        <!-- footer: refresh area -->
                        <span> Last updated on: 12/12/2013 9:43AM
                            <button type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..." class="btn btn-xs btn-default pull-right">
                                <i class="fa fa-refresh"></i>
                            </button> 
                        </span>
                        <!-- end footer -->

                    </div>
                    <!-- END AJAX-DROPDOWN -->
                </div>

                <!-- projects dropdown -->
                <!--                                <div class="project-context hidden-xs">
                                
                                                    <span class="label">Projects:</span>
                                                    <span class="project-selector dropdown-toggle" data-toggle="dropdown">Recent projects <i class="fa fa-angle-down"></i></span>
                                
                                                     
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="javascript:void(0);">Online e-merchant management system - attaching integration with the iOS</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);">Notes on pipeline upgradee</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);">Assesment Report for merchant account</a>
                                                        </li>
                                                        <li class="divider"></li>
                                                        <li>
                                                            <a href="javascript:void(0);"><i class="fa fa-power-off"></i> Clear</a>
                                                        </li>
                                                    </ul>
                                                </div>-->
                <!-- end projects dropdown -->

                <!-- pulled right: nav area -->
                <div class="pull-right">

                    <!-- collapse menu button -->
                    <div id="hide-menu" class="btn-header pull-right">
                        <span> 
                            <a href="javascript:void(0);" data-action="toggleMenu" title="<?php echo Yii::t('main', 'Collapse Menu'); ?>">
                                <i class="fa fa-reorder"></i>
                            </a> 
                        </span>
                    </div>
                    <!-- end collapse menu -->

                    <!-- #MOBILE -->
                    <!-- Top menu profile link : this shows only when top menu is active -->
<!--                    <ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
                        <li class="">
                            <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown"> 
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/avatars/sunny.png" alt="John Doe" class="online" />  
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0">
                                        <i class="fa fa-cog"></i> Setting
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="profile.html" class="padding-10 padding-top-0 padding-bottom-0"> 
                                        <i class="fa fa-user"></i> 
                                        <u>P</u>rofile
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="toggleShortcut">
                                        <i class="fa fa-arrow-down"></i> 
                                        <u>S</u>hortcut
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="launchFullscreen">
                                        <i class="fa fa-arrows-alt"></i> 
                                        Full <u>S</u>creen
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="login.html" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout">
                                        <i class="fa fa-sign-out fa-lg"></i> 
                                        <strong><u>L</u>ogout</strong>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>-->

                    <!-- logout button -->
                    <div id="logout" class="btn-header transparent pull-right">
                        <span> 
                            <a href="/admin/logout" title="<?php echo Yii::t('main', 'Sign Out'); ?>" data-action="userLogout" data-logout-msg="<?php echo Yii::t('main', 'You can improve your security further after logging out by closing this opened browser'); ?>">
                                <i class="fa fa-sign-out"></i>
                            </a>                             

                        </span>
                    </div>
                    <!-- end logout button -->

                    <!-- search mobile button (this is hidden till mobile view port) -->
                    <!--                    <div id="search-mobile" class="btn-header transparent pull-right">
                                            <span> 
                                                <a href="javascript:void(0)" title="Search">
                                                    <i class="fa fa-search"></i>
                                                </a> 
                                            </span>
                                        </div>-->
                    <!-- end search mobile button -->

                    <!-- input: search field -->
                    <!--                    <form action="search.html" class="header-search pull-right">
                                            <input id="search-fld"  type="text" name="param" placeholder="Find reports and more" data-autocomplete='[
                                                   "ActionScript",
                                                   "AppleScript",
                                                   "Asp",
                                                   "BASIC",
                                                   "C",
                                                   "C++",
                                                   "Clojure",
                                                   "COBOL",
                                                   "ColdFusion",
                                                   "Erlang",
                                                   "Fortran",
                                                   "Groovy",
                                                   "Haskell",
                                                   "Java",
                                                   "JavaScript",
                                                   "Lisp",
                                                   "Perl",
                                                   "PHP",
                                                   "Python",
                                                   "Ruby",
                                                   "Scala",
                                                   "Scheme"]'>
                                                <button type="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                                <a href="javascript:void(0);" id="cancel-search-js" title="Cancel Search"><i class="fa fa-times"></i></a>
                                        </form>-->
                    <!-- end input: search field -->

                    <!-- fullscreen button -->
                    <div id="fullscreen" class="btn-header transparent pull-right">
                        <span> 
                            <a href="javascript:void(0);" data-action="launchFullscreen" title="<?php echo Yii::t('main', 'Full Screen'); ?>">
                                <i class="fa fa-arrows-alt"></i>
                            </a> 
                        </span>
                    </div>

                    <div class="header-dropdown-list smart-form client-form">
                        <form action="" method="post" id="changLang" class="dropDownCustom">
                            <?php
                            echo CmsHtml::getAllLanguages();
                            ?>
                        </form>
                    </div>
                    <!-- end fullscreen button -->

                    <!-- multiple lang dropdown : find all flags in the flags page -->
                    <!--                                        <ul class="header-dropdown-list hidden-xs">
                                                                                        <li>
                                                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                                                                                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/blank.gif" class="flag flag-us" alt="United States" /> 
                                                                                                <span> US </span> 
                                                                                                <i class="fa fa-angle-down"></i> 
                                                                                            </a>
                                                                                            <ul class="dropdown-menu pull-right">
                                                                                                <li class="active">
                                                                                                    <a href="javascript:void(0);">
                                                                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/blank.gif" class="flag flag-us" alt="United States" /> US
                                                                                                    </a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="javascript:void(0);">
                                                                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/blank.gif" class="flag flag-es" alt="Spanish" /> Spanish
                                                                                                    </a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="javascript:void(0);">
                                                                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/blank.gif" class="flag flag-de" alt="German" /> German
                                                                                                    </a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </li>
                                        
                                                            </ul>-->



                    <!-- end multiple lang -->

                </div>
                <!-- end pulled right: nav area -->

            </header>
            <!-- END HEADER -->

            <aside id="left-panel">

                <!-- User info -->
                <div class="login-info">
                    <span> <!-- User image size is adjusted inside CSS, it should stay as it --> 

                        <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/avatars/sunny.png" alt="me" class="online" /> 
                            <span>                                
                                <?php echo (Yii::app()->user->name); ?>
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </a> 

                    </span>
                </div>
                <!-- end user info -->

                <!-- NAVIGATION : This navigation is also responsive

                To make this navigation dynamic please make sure to link the node
                (the reference to the nav > ul) after page load. Or the navigation
                will not initialize.
                -->
                <nav>
                        <!-- NOTE: Notice the gaps after each icon usage <i></i>..
                        Please note that these links work a bit different than
                        traditional href="" links. See documentation for details.
                    -->

                    <?php
                    //param beforeHtml - set html code before tag A
                    //param afterHtml - set html code after tag A
                    $this->widget('zii.widgets.CMenu', array(
                        'id' => 'navlist',
                        'items' => array(
                            array(
                                'label' => Yii::t('main', 'Home'),
                                'url' => '/admin',
                                'visible' => !Yii::app()->user->isGuest,
                                'beforeHtml' => '<i class="fa fa-home"></i>',
                            ),
                            array(
                                'label' => Yii::t('main', 'Users'),
                                'url' => '#',
                                'visible' => !Yii::app()->user->isGuest,
                                'beforeHtml' => '<i class = "fa fa-users"></i>',
                                'items' => array(
                                    array('label' => Yii::t('main', 'List Users'), 'url' => array('/users/index')),
                                    array('label' => Yii::t('main', 'Create New User'), 'url' => array('/users/create')),
                                )
                            ),
                            array(
                                'label' => Yii::t('main', 'Domains'),
                                'url' => '#',
                                'visible' => (Yii::app()->params->domain == 0) ? !Yii::app()->user->isGuest : false,
                                'beforeHtml' => '<i class="fa fa-bookmark"></i>',
                                'items' => array(
                                    array('label' => Yii::t('main', 'List Domains'), 'url' => array('/sites/domain/')),
                                    array('label' => Yii::t('main', 'Create New Domain'), 'url' => array('/sites/domain/create')),
                                )
                            ),
//                            array(
//                                'label' => Yii::t('main', 'Database Users'),
//                                'url' => '#',
//                                'visible' => !Yii::app()->user->isGuest,
//                                'beforeHtml' => '<i class="fa fa-user"></i>',
//                                'items' => array(
//                                    array('label' => Yii::t('main', 'List DB Users'), 'url' => array('/sites/database')),
//                                    array('label' => Yii::t('main', 'Create New DB Users'), 'url' => array('/sites/database/create')),
//                                )
//                            ),
                            array(
                                'label' => Yii::t('main', 'Languages'),
                                'url' => '#',
                                'visible' => !Yii::app()->user->isGuest,
                                'beforeHtml' => '<i class="fa fa-user"></i>',
                                'items' => array(
                                    array('label' => Yii::t('main', 'List Languages'), 'url' => array('/languages/index')),
                                    array('label' => Yii::t('main', 'Add New Language'), 'url' => array('/languages/create')),
                                    array(
                                        'label' => Yii::t('main', 'List Language Keys'),
                                        'url' => array('/languages/keys/index'),
                                        'visible' => (Yii::app()->params->domain == 0) ? true : false
                                    ),
                                    array(
                                        'label' => Yii::t('main', 'Add New Language Key'),
                                        'url' => array('/languages/keys/create'),
                                        'visible' => (Yii::app()->params->domain == 0) ? true : false
                                    ),
                                )
                            ),
                            array(
                                'label' => Yii::t('main', 'Settings'),
                                'url' => array('/settings/index'),
                                'visible' => !Yii::app()->user->isGuest),
                            array(
                                'label' => Yii::t('main', 'Templates'),
                                'url' => array('/templates/index'),
                                'visible' => !Yii::app()->user->isGuest),
                            array(
                                'label' => Yii::t('main', 'Login'),
                                'url' => array('/index/login'),
                                'visible' => Yii::app()->user->isGuest),
                            array(
                                'label' =>
                                Yii::t('main', 'Logout ({username})', array(
                                    '{username}' => Yii::app()->user->name
                                )),
                                'url' => array('/index/logout'),
                                'visible' => !Yii::app()->user->isGuest),
                        ),
                    ));
                    ?>
                </nav>
<!--                <span class="minifyme" data-action="minifyMenu"> 
                    <i class="fa fa-arrow-circle-left hit"></i> 
                </span>-->

            </aside>
            <div id="main" role="main">

                <!-- RIBBON -->
                <div id="ribbon">



                    <!-- breadcrumb -->

                    <?php if (isset($this->breadcrumbs)) { ?>
                        <?php
                        $this->widget('zii.widgets.CBreadcrumbs', array(
                            'links' => $this->breadcrumbs,
                        ));
                        ?><!-- breadcrumbs -->
                    <?php } ?>



                    <span class="ribbon-button-alignment pull-right ">
                        <span> 

                        </span>    

                    </span> 

                </div>           

                <div id="content">
                    <?php echo $content; ?>
                </div>

            </div>

            <div class="page-footer">

                Copyright &copy; <?php echo date('Y'); ?> by My Company.
                All Rights Reserved.
                <?php echo Yii::powered(); ?>
            </div><!-- footer -->
        </div><!-- page -->
        <!--================================================== -->

        <div id="shortcut">
            <ul>
                <li>
                    <a href="#inbox.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
                </li>
                <li>
                    <a href="#calendar.html" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span>Calendar</span> </span> </a>
                </li>
                <li>
                    <a href="#gmap-xml.html" class="jarvismetro-tile big-cubes bg-color-purple"> <span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i> <span>Maps</span> </span> </a>
                </li>
                <li>
                    <a href="#invoice.html" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i> <span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
                </li>
                <li>
                    <a href="#gallery.html" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i> <span>Gallery </span> </span> </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="jarvismetro-tile big-cubes selected bg-color-pinkDark"> <span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span>My Profile </span> </span> </a>
                </li>
            </ul>
        </div>
        <!-- END SHORTCUT AREA -->

        <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
        <script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/pace/pace.min.js"></script>

        <!-- Link to Google CDN's jQuery + jQueryUI;
                            fall back to local -->
        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->
<!--        <script>
            if (!window.jQuery) {
                document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');
            }
        </script>-->

        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script>
            if (!window.jQuery.ui) {
                document.write('<script src="js/libs/jquery-ui-1.10.3.min.js"><\/script>');
            }
        </script>

        <!-- JS TOUCH : include this plugin for mobile drag / drop touch events
        <script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

        <!-- BOOTSTRAP JS -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap/bootstrap.min.js"></script>

        <!-- CUSTOM NOTIFICATION -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/notification/SmartNotification.min.js"></script>

        <!-- JARVIS WIDGETS -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/smartwidgets/jarvis.widget.min.js"></script>

        <!-- EASY PIE CHARTS -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

        <!-- SPARKLINES -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/sparkline/jquery.sparkline.min.js"></script>

        <!-- JQUERY VALIDATE -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/jquery-validate/jquery.validate.min.js"></script>

        <!-- JQUERY MASKED INPUT -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

        <!-- JQUERY SELECT2 INPUT -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/select2/select2.min.js"></script>

        <!-- JQUERY UI + Bootstrap Slider -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

        <!-- browser msie issue fix -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

        <!-- FastClick: For mobile devices -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/fastclick/fastclick.min.js"></script>

        <!--[if IE 8]>

        <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

        <![endif]-->

        <!-- Demo purpose only -->
        <!--<script src="js/demo.min.js"></script>-->
        <script>
            var titleSmart = '<?php echo Yii::t('main', 'Logout'); ?>';
            var yesButtonSmart = '<?php echo Yii::t('main', 'Yes'); ?>';
            var noButtonSmart = '<?php echo Yii::t('main', 'No'); ?>';
        </script>
        <!-- MAIN APP JS FILE -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/admin_app.js"></script>

        <!-- PAGE RELATED PLUGIN(S) -->

        <!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/flot/jquery.flot.cust.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/flot/jquery.flot.resize.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/flot/jquery.flot.tooltip.min.js"></script>

        <!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

        <!-- Full Calendar -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>

        <!-- pnotify -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/pnotify.custom.min.js"></script>

        <script>
            $(document).ready(function () {

                // DO NOT REMOVE : GLOBAL FUNCTIONS!
                pageSetUp();

                /*
                 * PAGE RELATED SCRIPTS
                 */

                $(".js-status-update a").click(function () {
                    var selText = $(this).text();
                    var $this = $(this);
                    $this.parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
                    $this.parents('.dropdown-menu').find('li').removeClass('active');
                    $this.parent().addClass('active');
                });

                /*
                 * TODO: add a way to add more todo's to list
                 */

                // initialize sortable
                $(function () {
                    $("#sortable1, #sortable2").sortable({
                        handle: '.handle',
                        connectWith: ".todo",
                        update: countTasks
                    }).disableSelection();
                });

                // check and uncheck
                $('.todo .checkbox > input[type="checkbox"]').click(function () {
                    var $this = $(this).parent().parent().parent();

                    if ($(this).prop('checked')) {
                        $this.addClass("complete");

                        // remove this if you want to undo a check list once checked
                        //$(this).attr("disabled", true);
                        $(this).parent().hide();

                        // once clicked - add class, copy to memory then remove and add to sortable3
                        $this.slideUp(500, function () {
                            $this.clone().prependTo("#sortable3").effect("highlight", {}, 800);
                            $this.remove();
                            countTasks();
                        });
                    } else {
                        // insert undo code here...
                    }

                })
                // count tasks
                function countTasks() {

                    $('.todo-group-title').each(function () {
                        var $this = $(this);
                        $this.find(".num-of-tasks").text($this.next().find("li").size());
                    });

                }

                /*
                 * RUN PAGE GRAPHS
                 */

                /* TAB 1: UPDATING CHART */
                // For the demo we use generated data, but normally it would be coming from the server

                var data = [], totalPoints = 200, $UpdatingChartColors = $("#updating-chart").css('color');

                function getRandomData() {
                    if (data.length > 0)
                        data = data.slice(1);

                    // do a random walk
                    while (data.length < totalPoints) {
                        var prev = data.length > 0 ? data[data.length - 1] : 50;
                        var y = prev + Math.random() * 10 - 5;
                        if (y < 0)
                            y = 0;
                        if (y > 100)
                            y = 100;
                        data.push(y);
                    }

                    // zip the generated y values with the x values
                    var res = [];
                    for (var i = 0; i < data.length; ++i)
                        res.push([i, data[i]])
                    return res;
                }

                // setup control widget
                var updateInterval = 1500;
                $("#updating-chart").val(updateInterval).change(function () {

                    var v = $(this).val();
                    if (v && !isNaN(+v)) {
                        updateInterval = +v;
                        $(this).val("" + updateInterval);
                    }

                });

                // setup plot
                var options = {
                    yaxis: {
                        min: 0,
                        max: 100
                    },
                    xaxis: {
                        min: 0,
                        max: 100
                    },
                    colors: [$UpdatingChartColors],
                    series: {
                        lines: {
                            lineWidth: 1,
                            fill: true,
                            fillColor: {
                                colors: [{
                                        opacity: 0.4
                                    }, {
                                        opacity: 0
                                    }]
                            },
                            steps: false

                        }
                    }
                };

//                var plot = $.plot($("#updating-chart"), [getRandomData()], options);

                /* live switch */
                $('input[type="checkbox"]#start_interval').click(function () {
                    if ($(this).prop('checked')) {
                        $on = true;
                        updateInterval = 1500;
                        update();
                    } else {
                        clearInterval(updateInterval);
                        $on = false;
                    }
                });

                function update() {
                    if ($on == true) {
                        plot.setData([getRandomData()]);
                        plot.draw();
                        setTimeout(update, updateInterval);

                    } else {
                        clearInterval(updateInterval)
                    }

                }

                var $on = false;

                /*end updating chart*/

                /* TAB 2: Social Network  */

                $(function () {
                    // jQuery Flot Chart
                    var twitter = [[1, 27], [2, 34], [3, 51], [4, 48], [5, 55], [6, 65], [7, 61], [8, 70], [9, 65], [10, 75], [11, 57], [12, 59], [13, 62]], facebook = [[1, 25], [2, 31], [3, 45], [4, 37], [5, 38], [6, 40], [7, 47], [8, 55], [9, 43], [10, 50], [11, 47], [12, 39], [13, 47]], data = [{
                            label: "Twitter",
                            data: twitter,
                            lines: {
                                show: true,
                                lineWidth: 1,
                                fill: true,
                                fillColor: {
                                    colors: [{
                                            opacity: 0.1
                                        }, {
                                            opacity: 0.13
                                        }]
                                }
                            },
                            points: {
                                show: true
                            }
                        }, {
                            label: "Facebook",
                            data: facebook,
                            lines: {
                                show: true,
                                lineWidth: 1,
                                fill: true,
                                fillColor: {
                                    colors: [{
                                            opacity: 0.1
                                        }, {
                                            opacity: 0.13
                                        }]
                                }
                            },
                            points: {
                                show: true
                            }
                        }];

                    var options = {
                        grid: {
                            hoverable: true
                        },
                        colors: ["#568A89", "#3276B1"],
                        tooltip: true,
                        tooltipOpts: {
                            //content : "Value <b>$x</b> Value <span>$y</span>",
                            defaultTheme: false
                        },
                        xaxis: {
                            ticks: [[1, "JAN"], [2, "FEB"], [3, "MAR"], [4, "APR"], [5, "MAY"], [6, "JUN"], [7, "JUL"], [8, "AUG"], [9, "SEP"], [10, "OCT"], [11, "NOV"], [12, "DEC"], [13, "JAN+1"]]
                        },
                        yaxes: {
                        }
                    };

//                    var plot3 = $.plot($("#statsChart"), data, options);
                });

                // END TAB 2

                // TAB THREE GRAPH //
                /* TAB 3: Revenew  */

                $(function () {

                    var trgt = [[1354586000000, 153], [1364587000000, 658], [1374588000000, 198], [1384589000000, 663], [1394590000000, 801], [1404591000000, 1080], [1414592000000, 353], [1424593000000, 749], [1434594000000, 523], [1444595000000, 258], [1454596000000, 688], [1464597000000, 364]], prft = [[1354586000000, 53], [1364587000000, 65], [1374588000000, 98], [1384589000000, 83], [1394590000000, 980], [1404591000000, 808], [1414592000000, 720], [1424593000000, 674], [1434594000000, 23], [1444595000000, 79], [1454596000000, 88], [1464597000000, 36]], sgnups = [[1354586000000, 647], [1364587000000, 435], [1374588000000, 784], [1384589000000, 346], [1394590000000, 487], [1404591000000, 463], [1414592000000, 479], [1424593000000, 236], [1434594000000, 843], [1444595000000, 657], [1454596000000, 241], [1464597000000, 341]], toggles = $("#rev-toggles"), target = $("#flotcontainer");

                    var data = [{
                            label: "Target Profit",
                            data: trgt,
                            bars: {
                                show: true,
                                align: "center",
                                barWidth: 30 * 30 * 60 * 1000 * 80
                            }
                        }, {
                            label: "Actual Profit",
                            data: prft,
                            color: '#3276B1',
                            lines: {
                                show: true,
                                lineWidth: 3
                            },
                            points: {
                                show: true
                            }
                        }, {
                            label: "Actual Signups",
                            data: sgnups,
                            color: '#71843F',
                            lines: {
                                show: true,
                                lineWidth: 1
                            },
                            points: {
                                show: true
                            }
                        }]

                    var options = {
                        grid: {
                            hoverable: true
                        },
                        tooltip: true,
                        tooltipOpts: {
                            //content: '%x - %y',
                            //dateFormat: '%b %y',
                            defaultTheme: false
                        },
                        xaxis: {
                            mode: "time"
                        },
                        yaxes: {
                            tickFormatter: function (val, axis) {
                                return "$" + val;
                            },
                            max: 1200
                        }

                    };

                    plot2 = null;

                    function plotNow() {
                        var d = [];
                        toggles.find(':checkbox').each(function () {
                            if ($(this).is(':checked')) {
                                d.push(data[$(this).attr("name").substr(4, 1)]);
                            }
                        });
                        if (d.length > 0) {
                            if (plot2) {
                                plot2.setData(d);
                                plot2.draw();
                            } else {
                                plot2 = $.plot(target, d, options);
                            }
                        }

                    }
                    ;

                    toggles.find(':checkbox').on('change', function () {
                        plotNow();
                    });
                    plotNow()

                });

                /*
                 * VECTOR MAP
                 */

                data_array = {
                    "US": 4977,
                    "AU": 4873,
                    "IN": 3671,
                    "BR": 2476,
                    "TR": 1476,
                    "CN": 146,
                    "CA": 134,
                    "BD": 100
                };

                $('#vector-map').vectorMap({
                    map: 'world_mill_en',
                    backgroundColor: '#fff',
                    regionStyle: {
                        initial: {
                            fill: '#c4c4c4'
                        },
                        hover: {
                            "fill-opacity": 1
                        }
                    },
                    series: {
                        regions: [{
                                values: data_array,
                                scale: ['#85a8b6', '#4d7686'],
                                normalizeFunction: 'polynomial'
                            }]
                    },
                    onRegionLabelShow: function (e, el, code) {
                        if (typeof data_array[code] == 'undefined') {
                            e.preventDefault();
                        } else {
                            var countrylbl = data_array[code];
                            el.html(el.html() + ': ' + countrylbl + ' visits');
                        }
                    }
                });

                /*
                 * FULL CALENDAR JS
                 */

                if ($("#calendar").length) {
                    var date = new Date();
                    var d = date.getDate();
                    var m = date.getMonth();
                    var y = date.getFullYear();

                    var calendar = $('#calendar').fullCalendar({
                        editable: true,
                        draggable: true,
                        selectable: false,
                        selectHelper: true,
                        unselectAuto: false,
                        disableResizing: false,
                        header: {
                            left: 'title', //,today
                            center: 'prev, next, today',
                            right: 'month, agendaWeek, agenDay' //month, agendaDay,
                        },
                        select: function (start, end, allDay) {
                            var title = prompt('Event Title:');
                            if (title) {
                                calendar.fullCalendar('renderEvent', {
                                    title: title,
                                    start: start,
                                    end: end,
                                    allDay: allDay
                                }, true // make the event "stick"
                                        );
                            }
                            calendar.fullCalendar('unselect');
                        },
                        events: [{
                                title: 'All Day Event',
                                start: new Date(y, m, 1),
                                description: 'long description',
                                className: ["event", "bg-color-greenLight"],
                                icon: 'fa-check'
                            }, {
                                title: 'Long Event',
                                start: new Date(y, m, d - 5),
                                end: new Date(y, m, d - 2),
                                className: ["event", "bg-color-red"],
                                icon: 'fa-lock'
                            }, {
                                id: 999,
                                title: 'Repeating Event',
                                start: new Date(y, m, d - 3, 16, 0),
                                allDay: false,
                                className: ["event", "bg-color-blue"],
                                icon: 'fa-clock-o'
                            }, {
                                id: 999,
                                title: 'Repeating Event',
                                start: new Date(y, m, d + 4, 16, 0),
                                allDay: false,
                                className: ["event", "bg-color-blue"],
                                icon: 'fa-clock-o'
                            }, {
                                title: 'Meeting',
                                start: new Date(y, m, d, 10, 30),
                                allDay: false,
                                className: ["event", "bg-color-darken"]
                            }, {
                                title: 'Lunch',
                                start: new Date(y, m, d, 12, 0),
                                end: new Date(y, m, d, 14, 0),
                                allDay: false,
                                className: ["event", "bg-color-darken"]
                            }, {
                                title: 'Birthday Party',
                                start: new Date(y, m, d + 1, 19, 0),
                                end: new Date(y, m, d + 1, 22, 30),
                                allDay: false,
                                className: ["event", "bg-color-darken"]
                            }, {
                                title: 'Smartadmin Open Day',
                                start: new Date(y, m, 28),
                                end: new Date(y, m, 29),
                                className: ["event", "bg-color-darken"]
                            }],
                        eventRender: function (event, element, icon) {
                            if (!event.description == "") {
                                element.find('.fc-event-title').append("<br/><span class='ultra-light'>" + event.description + "</span>");
                            }
                            if (!event.icon == "") {
                                element.find('.fc-event-title').append("<i class='air air-top-right fa " + event.icon + " '></i>");
                            }
                        }
                    });

                }
                ;

                /* hide default buttons */
                $('.fc-header-right, .fc-header-center').hide();

                // calendar prev
                $('#calendar-buttons #btn-prev').click(function () {
                    $('.fc-button-prev').click();
                    return false;
                });

                // calendar next
                $('#calendar-buttons #btn-next').click(function () {
                    $('.fc-button-next').click();
                    return false;
                });

                // calendar today
                $('#calendar-buttons #btn-today').click(function () {
                    $('.fc-button-today').click();
                    return false;
                });

                // calendar month
                $('#mt').click(function () {
                    $('#calendar').fullCalendar('changeView', 'month');
                });

                // calendar agenda week
                $('#ag').click(function () {
                    $('#calendar').fullCalendar('changeView', 'agendaWeek');
                });

                // calendar agenda day
                $('#td').click(function () {
                    $('#calendar').fullCalendar('changeView', 'agendaDay');
                });

                /*
                 * CHAT
                 */

                $.filter_input = $('#filter-chat-list');
                $.chat_users_container = $('#chat-container > .chat-list-body')
                $.chat_users = $('#chat-users')
                $.chat_list_btn = $('#chat-container > .chat-list-open-close');
                $.chat_body = $('#chat-body');

                /*
                 * LIST FILTER (CHAT)
                 */

                // custom css expression for a case-insensitive contains()
                jQuery.expr[':'].Contains = function (a, i, m) {
                    return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
                };

                function listFilter(list) {// header is any element, list is an unordered list
                    // create and add the filter form to the header

                    $.filter_input.change(function () {
                        var filter = $(this).val();
                        if (filter) {
                            // this finds all links in a list that contain the input,
                            // and hide the ones not containing the input while showing the ones that do
                            $.chat_users.find("a:not(:Contains(" + filter + "))").parent().slideUp();
                            $.chat_users.find("a:Contains(" + filter + ")").parent().slideDown();
                        } else {
                            $.chat_users.find("li").slideDown();
                        }
                        return false;
                    }).keyup(function () {
                        // fire the above change event after every letter
                        $(this).change();

                    });

                }

                // on dom ready
//                listFilter($.chat_users);
//
//                // open chat list
//                $.chat_list_btn.click(function () {
//                    $(this).parent('#chat-container').toggleClass('open');
//                })
//
//                $.chat_body.animate({
//                    scrollTop: $.chat_body[0].scrollHeight
//                }, 500);

            });

        </script>

        <!-- Your GOOGLE ANALYTICS CODE Below -->
        <script     type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>

        <script>
            $(document).ready(function () {
                $('#changLang select').change(function () {
                    $('#changLang').submit();
                });
            });
        </script>
    </body>
</html>
