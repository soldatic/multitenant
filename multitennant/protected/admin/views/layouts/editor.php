<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css"/>
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/smartadmin-production.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/smartadmin-skins.min.css"/>
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/demo.min.css"/>
    <!-- Ptivate css-->
    <link rel="stylesheet" type="text/css" media="screen"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/ptivate.css"/>
    <!-- pnotify css-->
    <link rel="stylesheet" type="text/css" media="screen"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/pnotify.custom.min.css"/>
    <!--Jqueri ui-->
    <link rel="stylesheet" type="text/css" media="screen"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/jquery-ui-1.11.4/jquery-ui.min.css"/>
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="img/favicon/favicon.ico"
          type="<?php echo Yii::app()->request->baseUrl; ?>/image/x-icon"/>
    <link rel="icon" href="img/favicon/favicon.ico" type="<?php echo Yii::app()->request->baseUrl; ?>/image/x-icon"/>
    <!-- GOOGLE FONT -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700"/>
    <!-- Specifying a Webpage Icon for Web Clip
             Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/img/splash/sptouch-icon-iphone.png"/>
    <link rel="apple-touch-icon" sizes="76x76"
          href="<?php echo Yii::app()->request->baseUrl; ?>/img/splash/touch-icon-ipad.png"/>
    <link rel="apple-touch-icon" sizes="120x120"
          href="<?php echo Yii::app()->request->baseUrl; ?>/img/splash/touch-icon-iphone-retina.png"/>
    <link rel="apple-touch-icon" sizes="152x152"
          href="<?php echo Yii::app()->request->baseUrl; ?>/img/splash/touch-icon-ipad-retina.png"/>
    <!--form editor-->
    <link rel="stylesheet" href="" />
    <link rel="stylesheet" type="text/css" href="/css/keyframes.css"/>
    <!--<link rel="stylesheet" type="text/css" href="/css/1434363560.css" />-->
    <!--<link rel="stylesheet" type="text/css" href="/css/formCustomizeStyle.css" />-->
    <link rel="stylesheet" type="text/css" href="/css/styleForEditPage.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/template-editor/dist/gridstack.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/tinymce/skins/lightgray/skin.min.css"/>
    <link rel="stylesheet"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/template-editor/template-tools.css"/>
    <link rel="stylesheet"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/template-editor/jquery-custom-scrollbar/jquery.mCustomScrollbar.css"/>
    <!-- bootstrap-colorpicker -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/bootstrap-colorpicker/bootstrap-colorpicker.min.css"/>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body class="xgate-template-editor">
<div id="header">
    <div id="logo-group" style="width: 300px">
        <span id="logo"> <img src="/img/logo.png" alt="XGATE"/> </span>
    </div>
    <div class="device-type">
        <a href="#" class="desktop active" title="View Desktop Version"><i class="fa fa-desktop"></i></a>
        <a href="#" class="tabletportrait" title="View Tablet"><i class="fa fa-tablet"></i></a>
        <a href="#" class="mobileportrait last" title="View Mobile"><i class="fa fa-mobile"></i></a>
        <span id="windSizeHeader">
            <div><img src="/img/triggerSidebarIco/winS.png" alt=""></div>
            <ul>
                <li><i class="fa fa-arrows-h"></i> <span id="iframeSizeWidth"></span></li>
                <li><i class="fa fa-arrows-v"></i> <span id="iframeSizeHeight"></li>
            </ul>
        </span>
    </div>
    <div class="save-page">
        <a href="javascript:void(0);" class="btn btn-danger" disabled="" id="savePageChanges"><i class="fa fa-save"></i>
            Save</a>
    </div>
</div>

<div class="container-fluid noPadding" id="page">
    <!--            <div id="header">
                    <div id="logo-group" style="width: 300px">
                         PLACE YOUR LOGO HERE
                        <span id="logo"> <img src="/img/logo.png" alt="XGATE" /> </span>
                    <span id="activity" class="activity-dropdown"> <i class="fa fa-globe"></i>  </span>

                    </div>

                     /.navbar-collapse
                     /.container
                                    <div class="responsive">
                                        <a href="#" class="desktop" title="View Desktop Version"></a>
                                        <a href="#" class="tabletlandscape" title="View Tablet Landscape (1024x768)"></a>
                                        <a href="#" class="tabletportrait" title="View Tablet Portrait (768x1024)"></a>
                                        <a href="#" class="mobilelandscape" title="View Mobile Landscape (480x320)"></a>
                                        <a href="#" class="mobileportrait last" title="View Mobile Portrait (320x480)"></a>
                                    </div>
                     pulled right: nav area

                     end pulled right: nav area
                </div>-->

    <div id="main" role="main">
        <div id="content">
            <?php echo $content; ?>
        </div>
    </div>
    <div id="left-panel">
        <nav>
            <ul>
                <li>
                    <a href="/admin" title="Back to Admin Panel"><i class="glyphicon glyphicon-arrow-left"></i>
                    </a>
                </li>
                <li data-link="panel" data-panel="insert-panel">
                    <a title="Insert" id="insert-pane"><i class="glyphicon glyphicon-plus "></i>
                    </a>
                </li>
                <li data-link="panel" data-panel="pages-panel">
                    <a title="Pages" id="pages-pane"><i class="glyphicon glyphicon-file "></i>
                    </a>
                </li>
<!--                <li data-ut="grid">-->
<!--                    <a title="Grid"><i class="glyphicon glyphicon-eye-open"></i>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li data-ut="snap">-->
<!--                    <a title="Snap"><i class="glyphicon glyphicon-magnet"></i>-->
<!--                    </a>-->
<!--                </li>-->
                <li data-link="panel">
                    <a title="Help"><i class="glyphicon glyphicon-question-sign"></i>
                    </a>
                </li>
                <li>
                    <a href="/admin/logout" title="<?php echo Yii::t('main', 'Sign Out'); ?> ">
                        <i class="glyphicon glyphicon-log-out"></i>
                    </a>
                </li>
               <!-- <li id="collapseLeftBar" class="triggerVisibleSidebarLeft_visible">
                    <i class="fa fa-arrow-circle-left"></i>
                </li>-->
            </ul>
        </nav>
    </div>

    <?php echo $this->panels['panelInsert']; ?>
    <?php echo $this->panels['panelPages']; ?>
    <div id="right-panel">
        <!--<div class="triggerVisibleSidebar triggerVisibleSidebar_visible">
            <i class="fa fa-arrow-circle-left"></i> <p>collapse sidebar</p>
        </div>-->
        <div class="devices changeDevVersion">
            Change device version
            <div class="responsive">
                <a href="#" class="desktop active" title="View Desktop Version">
                    <label for=""><i class="fa fa-desktop"></i></label>
                </a>
                <a href="#" class="tabletportrait active " title="View Tablet">
                    <label for=""><i class="fa fa-tablet"></i></label>
                </a>
                <a href="#" class="mobileportrait active last" title="View Mobile">
                    <label for=""><i class="fa fa-mobile"></i></label>
                </a>
            </div>
            <br>
            <p class="visibleOn">
                <span id="vistext">Visible on:&nbsp;</span>
                <span data-sizeicon="1" data-sizetext="hidden-sm hidden-xs" class="devMod" id="deskOnly">Desktop only</span>
                <span data-sizeicon="2" data-sizetext="hidden-xs" class="devMod" id="deskAndTab">Desktop and Tablet</span>
                <span data-sizeicon="3" data-sizetext="hidden-sm" class="devMod" id="deskAndMob">Desktop and Mobile</span>
                <span data-sizeicon="4" data-sizetext="hidden-xs hidden-lg hidden-md" class="devMod" id="tabOnly">Tablet only</span>
                <span data-sizeicon="5" data-sizetext="hidden-md hidden-lg" class="devMod" id="tabAndMob">Tablet and Mobile</span>
                <span data-sizeicon="6" data-sizetext="hidden-sm hidden-md hidden-lg" class="devMod" id="mobOnly">Mobile only</span>
                <span data-sizeicon="7" data-sizetext="hidden-md hidden-lg" class="devMod" id="mobAndTab">Mobile and Tablet</span>
                <span data-sizeicon="8" data-sizetext="visible-xs visible-sm visible-md visible-lg" class="devMod" id="allDevice" style="display:block">All Devices</span>
                <span data-sizeicon="9" data-sizetext="hidden-sm hidden-md hidden-lg hidden-xs" class="devMod" id="nullDev">Your imagination : )</span>
            </p>
        </div>
        <div class="anker-page">
            <fieldset>
                <section>
                    <label class="label">ID</label>
                    <label class="input">
                        <input type="text" id="anker-id">
                    </label>
                    <br>
                    <!--<span class="validationId" id="firstNumber">The first character should not be a number</span>
                    <span class="validationId" id="firstHash">The id should not be a hash</span>
                    <span class="validationId" id="allisColl">All is cool</span>-->
                </section>
            </fieldset>
        </div>
        <div class="info-page">
            <span>Page : <i id="current-page-name">Test page</i></span>
        </div>
        <div class="item-editor">
            <ul id="item-editor-tab" class="nav nav-tabs">
                <li class="active">
                    <a href="#item-editor-tab-s1" data-toggle="tab"><span class="glyphicon glyphicon-pencil"></span></a>
                </li>
                <li>
                    <a href="#item-editor-tab-s2" data-toggle="tab"><span class="glyphicon glyphicon-cog"></span></a>
                </li>
                <li>
                    <a href="#item-editor-tab-s3" data-toggle="tab"><span class="glyphicon glyphicon-tasks"></span></a>
                </li>
                <li>
                    <a href="#item-editor-tab-s4" data-toggle="tab"><span
                            class="glyphicon glyphicon-picture"></span></a>
                </li>
            </ul>
            <div id="item-editor-content" class="tab-content padding-10">
                <div class="tab-pane fade in active" id="item-editor-tab-s1">
                    <div class="non-select-tool">
                        <p>Please select an element in the canvas to activate this panel.</p>
                    </div>
                    <div class="tool-text tool-in-panel">
                        <div id="toolbarLocation">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <button title="Bold" class="btn testText btn-default" data-type="bold"><i class="fa fa-bold"></i></button>
                                    <button title="Italic" class="btn testText btn-default" data-type="italic"><i class="fa fa-italic"></i></button>
                                    <button title="Underline" class="btn testText btn-default" data-type="underline"><i class="fa fa-underline"></i></button>
                                    <button title="Strikethrough" class="btn testText btn-default" data-type="strikethrough"><i class="fa fa-strikethrough"></i></button>
                                    <button title="Subscript" class="btn testText btn-default" data-type="subscript"><i class="fa fa-subscript"></i></button>
                                    <button title="Superscript" class="btn testText btn-default" data-type="superscript"><i class="fa fa-superscript"></i></button>
                                </div>
                                <div class="contWithSpinner">
                                    <div class="spinnerFull fontSizeSpin">
                                        <label  title="Font Size" for="fontSizeSpin"><i class="fa fa-font"></i> <i class="fa fa-font"></i></label>
                                        <input onkeypress='return event.charCode >= 48 && event.charCode <= 57'  id="fontSizeSpin"  name="value">
                                        <div class="arrowContrl">
                                            <div class="upArrow"><i class="fa fa-arrow-up"></i></div>
                                            <div class="downArrow"><i class="fa fa-arrow-down"></i></div>
                                        </div>
                                        <div class="pxIcon">px</div>
                                    </div>
                                    <div class="spinnerFull LineHeightSpin">
                                        <label  title="Line Height" for="LineHeightSpin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/line-height.png" alt="line-height"></label>
                                        <input class="secondInputSpinner" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="LineHeightSpin" name="value">
                                        <div class="arrowContrl">
                                            <div class="upArrow"><i class="fa fa-arrow-up"></i></div>
                                            <div class="downArrow"><i class="fa fa-arrow-down"></i></div>
                                        </div>
                                        <div class="pxIcon">px</div>
                                    </div>
                                </div>







                                <div class="contWithSpinner contWithSpinnerSecond">
                                    <div class="spinnerFull WordSpaceSpin">
                                        <label title="Word Spacing" for="WordSpaceSpin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/wordSpace.png" alt="word-spacing"></label>
                                        <input onkeypress='return event.charCode >= 48 && event.charCode <= 57'  id="WordSpaceSpin"  name="value">
                                        <div class="arrowContrl">
                                            <div class="upArrow"><i class="fa fa-arrow-up"></i></div>
                                            <div class="downArrow"><i class="fa fa-arrow-down"></i></div>
                                        </div>
                                        <div class="pxIcon">px</div>
                                    </div>
                                    <div class="spinnerFull LetterSpaceSpin">
                                        <label title="Letter Spacing" for="LetterSpaceSpin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/letterSpace2.png" alt="line-height"></label>
                                        <input class="secondInputSpinner"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="LetterSpaceSpin" name="value">
                                        <div class="arrowContrl">
                                            <div class="upArrow"><i class="fa fa-arrow-up"></i></div>
                                            <div class="downArrow"><i class="fa fa-arrow-down"></i></div>
                                        </div>
                                        <div class="pxIcon">px</div>
                                    </div>
                                </div>


                                <div class="contWithSpinner contWithSpinnerSecond">
                                    <div class="spinnerFull WordSpaceSpin">
                                        <label class="colorPickerForJs colorPickerJsTextBG" title="Word Spacing" for="colorPickerJsTextBG">
                                            <i class="mce-ico mce-i-backcolor"></i>
                                        </label>
                                        <input type="text" id="colorPickerJsTextBG">
                                        <!--<div class="arrowContrl">
                                            <div class="upArrow"><i class="fa fa-arrow-up"></i></div>
                                            <div class="downArrow"><i class="fa fa-arrow-down"></i></div>
                                        </div>
                                        <div class="pxIcon">px</div>-->
                                    </div>
                                    <div class="spinnerFull LetterSpaceSpin">
                                        <label class="colorPickerForJs colorPickerJsText" title="Letter Spacing" for="contForColorText">
                                            <i class="mce-ico mce-i-forecolor"></i>
                                        </label>
                                        <input class="secondInputSpinner" id="contForColorText" name="value">
                                        <!--<div class="arrowContrl">
                                            <div class="upArrow"><i class="fa fa-arrow-up"></i></div>
                                            <div class="downArrow"><i class="fa fa-arrow-down"></i></div>
                                        </div>
                                        <div class="pxIcon">px</div>-->
                                    </div>
                                </div>



                                <div class="btn-group">
                                    <button  title="Justifyleft" class="btn testText btn-default" data-type="justifyleft"><i class="fa fa-align-left"></i></button>
                                    <button  title="Justifycenter" class="btn testText btn-default" data-type="justifycenter"><i class="fa fa-align-center"></i></button>
                                    <button  title="Justifyfull" class="btn testText btn-default" data-type="justifyfull"><i class="fa fa-align-justify"></i></button>
                                    <button  title="Justifyright" class="btn testText btn-default" data-type="justifyright"><i class="fa fa-align-right"></i></button>
                                    <button  title="Blockquote" class="btn formatBlockText btn-default" data-type="blockquote"><i class="fa fa-quote-left"></i></button>
                                    <button  title="Insert Unordered List" class="btn testText btn-default" data-type="insertUnorderedList"><i class="fa fa-list-ul"></i></button>
                                </div>
                                <div class="btn-group">
                                    <button  title="Cut" class="btn testText btn-default" data-type="cut"><i class="fa fa-scissors"></i></button>
                                    <button  title="Copy" class="btn testText btn-default" data-type="copy"><i class="fa fa-files-o"></i></button>
                                    <!--<button data-toggle="tooltip" data-placement="top" title="Paste" class="btn testText btn-default" data-type="paste"><i class="fa fa-clipboard"></i></button>-->
                                    <button  title="Insert Ordered List" class="btn testText btn-default" data-type="insertOrderedList"><i class="fa fa-list-ol"></i></button>
                                    <select class="textFontFamily form-control">
                                        <option>Font Family</option>
                                        <option data-type="andale mono" style="font-family: 'andale mono', monospace;" >Andala Mono</option>
                                        <option data-type="arial" style="font-family: arial, helvetica, sans-serif;" >Arial</option>
                                        <option data-type="arial black" style="font-family: 'arial black', sans-serif;" >Arial Black</option>
                                        <option data-type="book antiqua" style="font-family: 'book antiqua', palatino, serif;" >Book Antiqua</option>
                                        <option data-type="comic sans ms" style="font-family: 'comic sans ms', sans-serif;" >Comic Sans MS</option>
                                        <option data-type="courier new" style="font-family: 'courier new', courier, monospace;" >Courier New</option>
                                        <option data-type="georgia" style="font-family: georgia, palatino, serif;" >Georgia</option>
                                        <option data-type="helvetica" style="font-family: helvetica, arial, sans-serif;" >Helvetica</option>
                                        <option data-type="impact" style="font-family: impact, sans-serif;" >Impact</option>
                                        <option data-type="symbol" style="font-family: symbol;" >Symbol</option>
                                        <option data-type="tahoma" style="font-family: tahoma, arial, helvetica, sans-serif;" >Tahoma</option>
                                        <option data-type="terminal" style="font-family: terminal, monaco, monospace;" >Terminal</option>
                                        <option data-type="times new roman" style="font-family: 'times new roman', times, serif;" >Times New Roman</option>
                                        <option data-type="trebuchet ms" style="font-family: 'trebuchet ms', geneva, sans-serif;" >Trebuchet MS</option>
                                        <option data-type="verdana" style="font-family: verdana, geneva, sans-serif;" >Verdana</option>
                                        <option data-type="webdings" style="font-family: webdings;" >Webdings</option>
                                        <option data-type="wingdings" style="font-family: wingdings, 'zapf dingbats';" >Windings</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="hidenTolbar"></div>
                    </div>
                    <div class="tool-row tool-in-panel">
                        <h4>Row</h4>
                        <br />
                        <ul>
                            <li>Background</li>
                            <li class="tool-bg">
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_dark"
                                   style="background-color: #eff8f8; color: #000;">Darken</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_green"
                                   style="background-color: #169c78;">Green</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_red"
                                   style="background-color: #9c1b31;">Red</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_blue_light"
                                   style="background-color: #41b6ab;">Turquoise</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_blue"
                                   style="background-color: #34495e;">Dark Blue</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_orange"
                                   style="background-color: #f05442;">Orange</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_purple"
                                   style="background-color: #b163a3;">Purple</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_black"
                                   style="background-color: rgba(0, 0, 0, 0.9);">Black</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_parallax"
                                   style="background-size: cover;     background-image: url('/upload/bg/parallax_bg.jpg'); color: #000;">Sunflower</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_business_guy"
                                   style="background-size: cover;     background-image: url('/upload/bg/business_guy.jpg'); color: #000;">Business
                                    Guy</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_flower_field"
                                   style="background-size: cover;     background-image: url('/upload/bg/flower_field.jpg');">Flowers
                                    Field</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_landscape"
                                   style="background-size: cover;     background-image: url('/upload/bg/landscape.jpg');">Landscape</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_mountains"
                                   style="background-size: cover;     background-image: url('/upload/bg/mountains.jpg'); color: #000;">Mountains</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_greenfields"
                                   style="background-size: cover;     background-image: url('/upload/bg/greenfields.jpg');">Greenfields</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_aqua"
                                   style="background-size: cover;     background-image: url('/upload/bg/aqua.jpg')">Aqua</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_baby_blue"
                                   style="background-size: cover;     background-image: url('/upload/bg/baby_blue.jpg'); color: #000;">Baby
                                    Blue</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_color_splash"
                                   style="background-size: cover;     background-image: url('/upload/bg/color_splash.jpg');">Color
                                    Splash</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_mango"
                                   style="background-size: cover;     background-image: url('/upload/bg/mango.jpg');">Mango</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_orange_red"
                                   style="background-size: cover;     background-image: url('/upload/bg/orange_red.jpg');">Orange
                                    Red</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_flower"
                                   style="background-size: cover;     background-image: url('/upload/bg/flower.jpg');">Purple</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_velour"
                                   style="background-size: cover;     background-image: url('/upload/bg/velour.jpg');">Velour</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_wood"
                                   style="background-size: cover;     background-image: url('/upload/bg/wood.jpg'); color: #000;">Wood</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="bg_img_bg_yellow_green"
                                   style="background-size: cover;     background-image: url('/upload/bg/yellow_green.jpg');color: #000; ">Yellow
                                    Green</a>
                                <a href="javascript:void(0);" class="btn btn-primary tool-row-bg-remove"
                                   style="background-color: #fff; color: #000;">None</a>
                                <a href="javascript:void(0);" class="btn btn-primary"
                                   style="background-color: #fff; color: #000; font-weight: bold">Choose Image...</a>
                            </li>
                            <li>Possition of row</li>
                            <li class="tool-size">
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="none"><i
                                        class="fa fa-desktop"></i></i> Full Screen</a>
                                <a href="javascript:void(0);" class="btn btn-primary" data-type="content-center"><i
                                        class="fa fa-align-center"></i> Center</a>

                            </li>
                        </ul>
                    </div>
                    <div class="tool-column tool-in-panel">
                        <div class="row">
                            <div class="col-md-12">
                                <br>
                                <h4><i class="fa fa-desktop"></i> Desktop layout:</h4>
                            <ul class="gridOptin">
                                <li class="GridOptionFull" data-buttonId="1" id="GridOption1"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/6*6.png" alt=""></li>
                                <li class="GridOptionFull" data-buttonId="2" id="GridOption2"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/4*4*4.png" alt="">
                                </li>
                                <li class="GridOptionFull" data-buttonId="3" id="GridOption3"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/3*3*3*3.png" alt="">
                                </li>
                                <li class="GridOptionFull" data-buttonId="4" id="GridOption4"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/3*9.png" alt=""></li>
                                <li class="GridOptionFull" data-buttonId="5" id="GridOption5"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/9*3.png" alt=""></li>
                                <li class="GridOptionFull" data-buttonId="6" id="GridOption6"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/3*6*3.png" alt="">
                                </li>
                                <li class="GridOptionFull" data-buttonId="7" id="GridOption7"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/2*3*5*2.png" alt="">
                                </li>
                                <li class="GridOptionFull" data-buttonId="8" id="GridOption8"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/2*2*2*4*2.png" alt="">
                                </li>
                                <li class="GridOptionFull" data-buttonId="9" id="GridOption9"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/2*2*2*2*2*2.png"
                                        alt=""></li>
                                <li class="GridOptionFull" data-buttonId="10" id="GridOption10"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/custom.png" alt="">
                                </li>
                                <li style='display:none' class="GridOptionFull" data-buttonId="two" id="organized"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/custom.png" alt="">
                                </li>
                                <li style='display:none' class="GridOptionFull" data-buttonId="three" id="organized1"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/custom.png" alt="">
                                </li>
                                <li style='display:none' class="GridOptionFull" data-buttonId="four" id="organized2"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/custom.png" alt="">
                                </li>
                                <li style='display:none' class="GridOptionFull" data-buttonId="five" id="organized3"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/custom.png" alt="">
                                </li>
                                <li style='display:none' class="GridOptionFull" data-buttonId="six" id="organized4"><img
                                        src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/custom.png" alt="">
                                </li>
                            </ul>
                        <div class="contForGridSpinner"></div>
                            </div>
                            </div>
                        <br>
                        <h4><i class="fa fa-tablet"></i> Tablet layout:</h4>
                        <ul class="gridOptinTablet">
                            <li class="GridOptionFull" data-buttonId="0" style="display: inline-block;" id="GridOptionq"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/arr.png" alt=""></li>
                            <li class="GridOptionFull" data-buttonId="1" style="display: inline-block;" id="tabletGridOption1"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/6*6.png" alt=""></li>
                            <li class="GridOptionFull" data-buttonId="11" id="tabletGridOption11"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/2x2.png" alt="">
                            </li>
                            <li class="GridOptionFull" data-buttonId="12" id="tabletGridOption12"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/3X2.png" alt="">
                            </li>
                            <li class="GridOptionFull" data-buttonId="2" id="tabletGridOption2"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/4*4*4.png" alt="">
                            </li>
                            <li class="GridOptionFull" data-buttonId="3" id="tabletGridOption3"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/3*3*3*3.png" alt="">
                            </li>
                            <li class="GridOptionFull" data-buttonId="4" style="display: inline-block;" id="tabletGridOption4"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/3*9.png" alt=""></li>
                            <li class="GridOptionFull" data-buttonId="5" style="display: inline-block;" id="tabletGridOption5"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/9*3.png" alt=""></li>
                            <li class="GridOptionFull" data-buttonId="6" id="tabletGridOption6"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/3*6*3.png" alt="">
                            </li>
                            <li class="GridOptionFull" data-buttonId="7" id="tabletGridOption7"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/2*3*5*2.png" alt="">
                            </li>
                            <li class="GridOptionFull" data-buttonId="8" id="tabletGridOption8"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/2*2*2*4*2.png" alt="">
                            </li>
                            <li class="GridOptionFull" data-buttonId="9" id="tabletGridOption9"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/2*2*2*2*2*2.png"
                                    alt=""></li>
                            <li class="GridOptionFull" data-buttonId="10" id="tabletGridOption10"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/custom.png" alt="">
                            </li>
                        </ul>
                        <br>
                        <h4><i class="fa fa-mobile"></i> Phone layout:</h4>
                        <ul class="gridOptinTablet gridOptinPhone">
                            <li class="GridOptionFull" data-buttonId="0" style="display: inline-block;" id="GridOptionq"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/arr.png" alt=""></li>
                            <li class="GridOptionFull" data-buttonId="1" style="display: inline-block;" id="tabletGridOption1"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/6*6.png" alt=""></li>
                            <li class="GridOptionFull" data-buttonId="11" id="tabletGridOption11"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/2x2.png" alt="">
                            </li>
                            <li class="GridOptionFull" data-buttonId="12" id="tabletGridOption12"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/3X2.png" alt="">
                            </li>
                            <li class="GridOptionFull" data-buttonId="2" id="tabletGridOption2"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/4*4*4.png" alt="">
                            </li>
                            <li class="GridOptionFull" data-buttonId="3" id="tabletGridOption3"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/3*3*3*3.png" alt="">
                            </li>
                            <li class="GridOptionFull" data-buttonId="4" style="display: inline-block;" id="tabletGridOption4"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/3*9.png" alt=""></li>
                            <li class="GridOptionFull" data-buttonId="5" style="display: inline-block;" id="tabletGridOption5"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/9*3.png" alt=""></li>
                            <li class="GridOptionFull" data-buttonId="6" id="tabletGridOption6"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/3*6*3.png" alt="">
                            </li>
                            <li class="GridOptionFull" data-buttonId="7" id="tabletGridOption7"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/2*3*5*2.png" alt="">
                            </li>
                            <li class="GridOptionFull" data-buttonId="8" id="tabletGridOption8"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/2*2*2*4*2.png" alt="">
                            </li>
                            <li class="GridOptionFull" data-buttonId="9" id="tabletGridOption9"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/2*2*2*2*2*2.png"
                                    alt=""></li>
                            <li class="GridOptionFull" data-buttonId="10" id="tabletGridOption10"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/img/gridIcon/custom.png" alt="">
                            </li>
                        </ul>
                    </div>
                    <div>

                    </div>
                    <?php echo $this->panels['menuTabEdit']; ?>
                </div>
                <div class="tab-pane fade" id="item-editor-tab-s2">
                    <?php echo $this->plugins['cssEditor']; ?>
                </div>
                <div class="tab-pane fade" id="item-editor-tab-s3">
                    <div class="widget-body">
                    <br>
                        <div id="DOM-navigator" class="tree">
                            <ul id="DOM-navigator-main">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="item-editor-tab-s4">
                    <?php echo $this->panels['panelGallery']; ?>
                </div>

                <div class="overlayUniversal">
                    <div id="assetsBgModalClose"><i class="fa fa-times"></i></div>
                </div>
            </div>

        </div>

    </div>

    <div id="previewPopUp">
        <div class="container-fluid">
            <div class="row">
                <div class="panel-name">
                    <span>Image Preview</span>
                </div>
                <div class="panel-button">
                    <button class="btn btn-sm btn-primary" id="previewClose">Close</button>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive marginButom" id="imgPrevId" alt="" src="">
                </div>
            </div>
        </div>
    </div>


<div class="contMenu">
	<ul id="mainContMenu">
		<li id="SelParentElem">Select parent element
			<ul id="secondContMenu">
			</ul>
		</li>

		<li data-func='cut' class=''>
		Cut
		    <ul class='copyElem' id='cutElem'>
		    </ul>
		</li>
		<li data-func='copy' class=''>
		Copy <span class='ketHot'></span>
		    <ul class='copyElem' id='copyElem'>
		    </ul>
		</li>
		<li data-func='save'>Save</li>
		<li data-func='paste_inside'>Paste inside <span class='ketHot'></span></li>
		<li data-func='paste_inside_first'>Paste inside first</li>
		<li data-func='dublicate'>Dublicate <span class='ketHot'></span></li>
		<li data-func='delete'>Delete <span class='ketHot'></span></li>
	</ul>
</div>



    <?php echo $this->panels['dropzoneImg']; ?>
    <?php echo $this->panels['panelMenu']; ?>
    <?php echo $this->panels['modalIconMenu']; ?>
</div>
<div class="page-footer">
    Copyright &copy; <?php echo date('Y'); ?> by My Company.
    All Rights Reserved.
    <?php echo Yii::powered(); ?>
</div>
<!-- footer -->
</div><!-- page -->
<!--================================================== -->
<!-- END SHORTCUT AREA -->

<script src="/js/libs/jquery-2.0.2.min.js"></script>
<script src="/js/plugin/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/pace/pace.min.js"></script>
</script>
<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->
<!-- BOOTSTRAP JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap/bootstrap.min.js"></script>
<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/notification/SmartNotification.min.js"></script>
<!-- JARVIS WIDGETS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/smartwidgets/jarvis.widget.min.js"></script>
<!-- EASY PIE CHARTS -->
<script
    src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<!-- SPARKLINES -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/sparkline/jquery.sparkline.min.js"></script>
<!-- JQUERY VALIDATE -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<!-- JQUERY MASKED INPUT -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/select2/select2.min.js"></script>
<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- browser msie issue fix -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
<!-- FastClick: For mobile devices -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/fastclick/fastclick.min.js"></script>
<script>
    var titleSmart = '<?php echo Yii::t('main', 'Logout'); ?>';
    var yesButtonSmart = '<?php echo Yii::t('main', 'Yes'); ?>';
    var noButtonSmart = '<?php echo Yii::t('main', 'No'); ?>';
    var contents = $('#editor-iframe');
</script>
<!-- MAIN APP JS FILE -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/admin_app.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.nestable.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.sortElements.js"></script>
<!-- PAGE RELATED PLUGIN(S) -->
<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script
    src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- Full Calendar -->
<!--<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>-->
<!--PAGE RELATED PLUGIN(S)-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/dropzone/dropzone.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/superbox/superbox.min.js"></script>
<script>
    var totalCountItem = 1;
    var gridX = Math.floor($('#xgate-template-editor').width() / 12);
    var gridY = 10;
    var snap = false;
    $(window).resize(function () {
        gridX = Math.floor($('#xgate-template-editor').width() / 12);
    });
</script>
<!-- bootstrap-colorpicker -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/bootstrap-colorpicker/bootstrap-colorpicker.js"/></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/template-editor/jquery-custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/superbox/superbox.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.event.move.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.event.move.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
<!--css global editor-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/modernizr.min.js"></script>
<!--<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/pluginsCSSGlobal.js"></script>-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/updatedisplatcss.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jscolor.js"></script>
<!--form builder-->
<script type="text/javascript" src="/js/formeditor/script.js"></script>
<script type="text/javascript" src="/js/formeditor/script2.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/pnotify.custom.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/tinymce/tinymce.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/tinymce/jquery.tinymce.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/template-editor/lodash.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/template-editor/dist/gridstack.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/template-editor/dist/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/template-editor/form-editor.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/template-editor/contextMenu.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/template-editor/template-tools.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/template-editor/GridChangeOption.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/template-editor/cssEditor.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $('#changLang select').change(function () {
            $('#changLang').submit();
        });
        /*$( "#fontSizeSpin" ).spinner({
         min: 0,
         max: 100,
         icons: { down: "ui-icon-arrowthick-1-s", up: "ui-icon-arrowthick-1-n" }
         });*/
        $('label[for="LineHeightSpin"]').on("click", function () {
            reportColourAndFontSize();
        })
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

    });

</script>

<div class="loader">
    <div class="background"></div>
    <div class="icon"></div>
</div>
<!--<div class="smallDevAlert">
    <h3>Your browser is too small. Resize your window to fit this media query.</h3>
</div>-->
<?php echo $this->modals['modalForms']; ?>
<?php //echo $this->modals['modalMapSettings']; ?>
<?php echo $this->modals['modalItemSettings']; ?>
<?php //echo $this->modals['modalYoutubeSettings']; ?>
<?php echo $this->modals['modalImageLink']; ?>
<?php // echo $this->modals['modalChangePicture']; ?>
<?php // echo $this->modals['modalPages']; ?>
<?php // echo $this->modals['modalMenu']; ?>
<?php echo $this->modals['modalRemovePage']; ?>
<?php echo $this->modals['modalRemoveMenu']; ?>
</body>
</html>

