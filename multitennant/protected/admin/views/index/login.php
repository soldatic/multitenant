<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-offset-6 col-md-6 col-lg-offset-6 col-lg-6">
            <div class="well no-padding">
                <!--<form action="index.html" id="login-form" class="smart-form client-form" novalidate="novalidate">-->
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'login-form',
//                'class' => 'smart-form client-form',
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                    'htmlOptions' => array(
                        'class' => 'smart-form client-form',
                    )
                ));
                ?>
                <header>
                    <?php echo Yii::t('main', 'Sign In') ?>
                </header>

                <fieldset>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'E-mail') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">

                                <?php echo $form->textField($model, 'username', array('style' => 'margin: 0px 0px 0px 15px; width: 95%;')); ?>
                                <?php echo $form->error($model, 'username'); ?>
                            </div>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Password') ?></label>
                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                            <div class="row">

                                <?php echo $form->passwordField($model, 'password', array('style' => 'margin: 0px 0px 0px 15px; width: 95%;')); ?>
                                <?php echo $form->error($model, 'password'); ?>

                            </div>
                            <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> 
                        </label>
                        <div class="note">
                            <a href="/admin/forgot">Forgot password?</a>
                        </div>
                    </section>

                    <section>
                        <!--                    <label class="checkbox">
                                                <input type="checkbox" name="remember" checked="">
                                                <label class="checkbox">
                                                    <input type="checkbox" name="remember" checked="">-->


<!--                            <i></i>Stay signed in</label>-->

                        <!--                        <div class="row rememberMe">
                                                    <label class="checkbox"> 
                        <?php // echo $form->checkBox($model, 'rememberMe', array('name' => 'remember', 'checked' => '', 'style' => 'margin: 0px 0px 0px 15px; width: 95%;')); ?>
                                                        <i></i>Stay signed in
                                                    </label>
                                                </div> -->
                    </section>
                </fieldset>
                <footer>
                    <!--                <button type="submit" class="btn btn-primary">
                                        Sign in
                                    </button>-->
                    <?php echo CHtml::submitButton(Yii::t('main', 'Login'), array('class' => 'btn btn-primary')); ?>
                </footer>

                <?php $this->endWidget(); ?>
                <!--</form>-->

            </div>  

        </div>
    </div>
</div>

