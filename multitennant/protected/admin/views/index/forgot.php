<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Forgot password';
$this->breadcrumbs = array(
    'Forgot',
);
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-offset-6 col-md-6 col-lg-offset-6 col-lg-6">
            <div class="well no-padding">
                <!--<form action="index.html" id="login-form" class="smart-form client-form" novalidate="novalidate">-->
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'login-form',
//                'class' => 'smart-form client-form',
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                    'htmlOptions' => array(
                        'class' => 'smart-form client-form',
                    )
                ));
                ?>
                <header>
                    <?php echo Yii::t('main', 'Forgot password?') ?>
                </header>

                <fieldset>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'E-mail') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">

                                <?php echo $form->textField($model, 'username', array('style' => 'margin: 0px 0px 0px 15px; width: 95%;')); ?>
                                <?php echo $form->error($model, 'username'); ?>
                            </div>
                    </section>

                </fieldset>
                <footer>
                    <!--                <button type="submit" class="btn btn-primary">
                                        Sign in
                                    </button>-->
                    <?php echo CHtml::submitButton(Yii::t('main', 'Reset password'), array('class' => 'btn btn-primary')); ?>
                </footer>

                <?php $this->endWidget(); ?>
                <!--</form>-->

            </div>  

        </div>
    </div>
</div>

