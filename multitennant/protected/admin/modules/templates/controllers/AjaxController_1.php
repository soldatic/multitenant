<?php

Yii::import('admin.extensions.thumbnailImages');

class AjaxController extends AdminController
{

    protected $resultArray = array();

    public function actionIndex()
    {

    }

    public function actionSetElements()
    {
        if (isset($_POST['content']) && $_POST['content'] != '') {
            if (isset($_POST['pageId']) && $_POST['pageId'] != '') {
                if (isset($_POST['typeContent']) && $_POST['typeContent'] != '') {
                    $tplPage = TemplatesPages::model()->findByPk($_POST['pageId']);
                    switch ($_POST['typeContent']) {
                        case 'header':
                            $tplPage->tpl_page_header = $_POST['content'];
                            break;
                        case 'content':
                            $tplPage->tpl_page_content = $_POST['content'];
                            break;
                        case 'settings':
                            $tplPage->tpl_page_settings = $_POST['content'];
                            break;
                    }
                    $tplPage->save();
                } else {
                    $this->resultArray = array(
                        'error' => 'wrong page!'
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong page!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionGetElements()
    {
        if (isset($_POST['typeWidget']) && $_POST['typeWidget'] != '') {
            $typeWidget = $_POST['typeWidget'];
            switch ($typeWidget) {
                case 'row':
                    $this->resultArray = array(
                        'html' => array(),
                        'size' => array(
                            'w' => 12,
                            'h' => 266
                        )
                    );
                    break;
                case 'menu':
                    $pageId = (isset($_POST['pageId']) ? $_POST['pageId'] : 0);
                    $tplPage = $this->getPageData($pageId);
                    $idMenu = 'menu_' . rand(11111, 99999);
                    $dataMenu = array(
                        'menu_items' => array(
                            'it0' => array(
                                'menu_name' => $tplPage->tpl_page_name,
                                'menu_menu_type' => 'link', // link, content
                                'menu_link_type' => 'url', //url, page
                                'menu_page_id' => $tplPage->tpl_page_id,
                                'menu_url' => 'http://dengo-systems.com',
                                'menu_display' => 1,
                                'menu_image_position' => 'left', //left, right, top, bottom
                                'menu_image_url' => '',
                                'menu_target' => 1,
                            )
                        ),
                        'id_menu' => $idMenu
                    );
                    $this->resultArray = array(
                        'html' => array(
                            'html' => $this->renderPartial('/editor/menuRow', $dataMenu, TRUE),
                            'id_menu' => $idMenu,
                            'data_menu' => $dataMenu,
                        ),
                        'size' => array(
                            'w' => 12,
                            'h' => 64
                        ),
                        'typeWidget' => 'menu'
                    );
                    break;
                case 'form':
                    $this->resultArray = array(
                        'html' => array(
                            0 => array(
                                'html' => '[{"id":"foli1",'
                                    . '"count":1,'
                                    . '"type":"firstname",'
                                    . '"label":"First Name",'
                                    . '"required":1,'
                                    . '"size":"medium",'
                                    . '"duplicate":0,'
                                    . '"defaultValue":"",'
                                    . '"choices":[],'
                                    . '"date_format":0,'
                                    . '"columns":"",'
                                    . '"likert":{},'
                                    . '"formid":"",'
                                    . '"surveyid":"",'
                                    . '"uniqueCheck":0,'
                                    . '"errorMessage":"Please fill in your information"},'
                                    . '{"id":"foli2",'
                                    . '"count":2,'
                                    . '"type":"lastname",'
                                    . '"label":"Last Name",'
                                    . '"required":1,'
                                    . '"size":"medium",'
                                    . '"duplicate":0,'
                                    . '"defaultValue":"",'
                                    . '"choices":[],'
                                    . '"date_format":0,'
                                    . '"columns":"",'
                                    . '"likert":{},'
                                    . '"formid":"",'
                                    . '"surveyid":"",'
                                    . '"uniqueCheck":0,'
                                    . '"errorMessage":"Please fill in your information"},'
                                    . '{"id":"foli3",'
                                    . '"count":3,'
                                    . '"type":"email",'
                                    . '"label":"Email",'
                                    . '"required":1,'
                                    . '"size":"medium",'
                                    . '"duplicate":0,'
                                    . '"defaultValue":"",'
                                    . '"choices":[],'
                                    . '"date_format":0,'
                                    . '"columns":"",'
                                    . '"likert":{},'
                                    . '"formid":"",'
                                    . '"surveyid":"",'
                                    . '"validErrorMsg":"Please fill valid information",'
                                    . '"uniqueCheck":0,"errorMessage":"Please fill in your information"},'
                                    . '{"id":"foli4",'
                                    . '"count":4,'
                                    . '"type":"textarea",'
                                    . '"label":"Message",'
                                    . '"required":1,'
                                    . '"size":"small",'
                                    . '"duplicate":0,'
                                    . '"defaultValue":"",'
                                    . '"choices":[],'
                                    . '"date_format":0,'
                                    . '"columns":"",'
                                    . '"likert":{},'
                                    . '"formid":"",'
                                    . '"surveyid":"",'
                                    . '"uniqueCheck":0,'
                                    . '"length":"1000",'
                                    . '"errorMessage":"Please fill in your information"}]',
                                'size' => array(
                                    'w' => 4,
                                    'h' => 240
                                ),
                                'position' => array(
                                    'x' => 4,
                                    'y' => 33
                                ),
                                'typeWidget' => 'form'
                            )
                        ),
                        'size' => array(
                            'w' => 12,
                            'h' => 300
                        ),
                        'typeWidget' => 'form'
                    );
                    break;
                case 'formItem':
                    $this->resultArray = array(
                        'html' => '[{"id":"foli1",'
                            . '"count":1,'
                            . '"type":"firstname",'
                            . '"label":"First Name",'
                            . '"required":1,'
                            . '"size":"medium",'
                            . '"duplicate":0,'
                            . '"defaultValue":"",'
                            . '"choices":[],'
                            . '"date_format":0,'
                            . '"columns":"",'
                            . '"likert":{},'
                            . '"formid":"",'
                            . '"surveyid":"",'
                            . '"uniqueCheck":0,'
                            . '"errorMessage":"Please fill in your information"},'
                            . '{"id":"foli2",'
                            . '"count":2,'
                            . '"type":"lastname",'
                            . '"label":"Last Name",'
                            . '"required":1,'
                            . '"size":"medium",'
                            . '"duplicate":0,'
                            . '"defaultValue":"",'
                            . '"choices":[],'
                            . '"date_format":0,'
                            . '"columns":"",'
                            . '"likert":{},'
                            . '"formid":"",'
                            . '"surveyid":"",'
                            . '"uniqueCheck":0,'
                            . '"errorMessage":"Please fill in your information"},'
                            . '{"id":"foli3",'
                            . '"count":3,'
                            . '"type":"email",'
                            . '"label":"Email",'
                            . '"required":1,'
                            . '"size":"medium",'
                            . '"duplicate":0,'
                            . '"defaultValue":"",'
                            . '"choices":[],'
                            . '"date_format":0,'
                            . '"columns":"",'
                            . '"likert":{},'
                            . '"formid":"",'
                            . '"surveyid":"",'
                            . '"validErrorMsg":"Please fill valid information",'
                            . '"uniqueCheck":0,"errorMessage":"Please fill in your information"},'
                            . '{"id":"foli4",'
                            . '"count":4,'
                            . '"type":"textarea",'
                            . '"label":"Message",'
                            . '"required":1,'
                            . '"size":"small",'
                            . '"duplicate":0,'
                            . '"defaultValue":"",'
                            . '"choices":[],'
                            . '"date_format":0,'
                            . '"columns":"",'
                            . '"likert":{},'
                            . '"formid":"",'
                            . '"surveyid":"",'
                            . '"uniqueCheck":0,'
                            . '"length":"1000",'
                            . '"errorMessage":"Please fill in your information"}]',
                        'size' => array(
                            'w' => 5,
                            'h' => 300
                        ),
                        'typeWidget' => 'form'
                    );
                    break;
                case 'textItem':
                    $this->resultArray = array(
                        'html' => '<h2 style="text-align: center;"><strong>Product Name</strong></h2>
                                            See more snippets like these online store reviews at Bootsnipp - http://bootsnipp.com.
                                            Want to make these reviews work? Check out this building a review system tutorial over at maxoffsky.com!
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                        'size' => array(
                            'w' => 10,
                            'h' => 150
                        ),
                        'typeWidget' => 'text'
                    );
                    break;
                case 'titleItem':
                    $this->resultArray = array(
                        'html' => '<h1 style="text-align: center;"><strong>Your Website Title</strong></h1>
                                            <h4 style="text-align: center">And a great subtitle too</h4>',
                        'size' => array(
                            'w' => 10,
                            'h' => 150
                        ),
                        'typeWidget' => 'text'
                    );
                    break;
                case 'imageItem':
                    $this->resultArray = array(
                        'html' => '<div class="item-image"></div><div class="item-over"></div>',
                        'size' => array(
                            'w' => 5,
                            'h' => 250
                        ),
                        'typeWidget' => 'image'
                    );
                    break;
                case 'text':
                    $this->resultArray = array(
                        'html' => array(
                            0 => array(
                                'html' => '<h2 style="text-align: center;"><strong>Product Name</strong></h2>
                                            See more snippets like these online store reviews at Bootsnipp - http://bootsnipp.com.
                                            Want to make these reviews work? Check out this building a review system tutorial over at maxoffsky.com!
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                                'size' => array(
                                    'w' => 10,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 1,
                                    'y' => 33
                                ),
                                'typeWidget' => 'text'
                            )
                        ),
                        'size' => array(
                            'w' => 12,
                            'h' => 266
                        )
                    );
                    break;
                case 'title':
                    $this->resultArray = array(
                        'html' => array(
                            0 => array(
                                'html' => '<h1 style="text-align: center;"><strong>Your Website Title</strong></h1>
                                            <h4 style="text-align: center">And a great subtitle too</h4>',
                                'size' => array(
                                    'w' => 10,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 1,
                                    'y' => 33
                                ),
                                'typeWidget' => 'text'
                            )
                        ),
                        'size' => array(
                            'w' => 12,
                            'h' => 266
                        )
                    );
                    break;
                case 'big-text':
                    $this->resultArray = array(
                        'html' => array(
                            0 => array(
                                'html' => '<h1 style="text-align: left;" data-mce-style="text-align: left;">'
                                    . '<strong>Title Message</strong></h1>'
                                    . '<h4 style="text-align: left;" data-mce-style="text-align: left;">2015.09.27&nbsp;16:11</h4>'
                                    . '<p style="text-align: left;" data-mce-style="text-align: left;"> Lorem ipsum dolor sit amet, '
                                    . 'consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '
                                    . 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo '
                                    . 'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat '
                                    . 'nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt '
                                    . 'mollit anim id est laborum&nbsp;<span style="line-height: 1.42857;">Lorem ipsum dolor sit amet, '
                                    . 'consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '
                                    . 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo '
                                    . 'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat '
                                    . 'nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt '
                                    . 'mollit anim id est laborum</span></p>',
                                'size' => array(
                                    'w' => 10,
                                    'h' => 280
                                ),
                                'position' => array(
                                    'x' => 1,
                                    'y' => 33
                                ),
                                'typeWidget' => 'text'
                            )
                        ),
                        'size' => array(
                            'w' => 12,
                            'h' => 266
                        )
                    );
                    break;
                case 'image-text':
                    $this->resultArray = array(
                        'html' => array(
                            0 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 5,
                                    'h' => 250
                                ),
                                'position' => array(
                                    'x' => 0,
                                    'y' => 0
                                ),
                                'typeWidget' => 'image'
                            ),
                            1 => array(
                                'html' => '<h2>Product Name</h2>
                                            See more snippets like these online store reviews at Bootsnipp - http://bootsnipp.com.
                                            Want to make these reviews work? Check out this building a review system tutorial over at maxoffsky.com!
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                                'size' => array(
                                    'w' => 7,
                                    'h' => 250
                                ),
                                'position' => array(
                                    'x' => 5,
                                    'y' => 0
                                ),
                                'typeWidget' => 'text'
                            )
                        ),
                        'size' => array(
                            'w' => 12,
                            'h' => 266
                        )
                    );
                    break;
                case 'banner':
                    $this->resultArray = array(
                        'html' => array(
                            0 => array(
                                'html' => '<h2><strong>Banner Text</strong></h2>
                                            <p>Tell custommer something very important in that area</p>',
                                'size' => array(
                                    'w' => 5,
                                    'h' => 160
                                ),
                                'position' => array(
                                    'x' => 1,
                                    'y' => 100
                                ),
                                'typeWidget' => 'text'
                            ),
                            1 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 4,
                                    'h' => 250
                                ),
                                'position' => array(
                                    'x' => 7,
                                    'y' => 10
                                ),
                                'typeWidget' => 'image'
                            ),
                        ),
                        'size' => array(
                            'w' => 12,
                            'h' => 300
                        ),
                        'src' => 'bg_img_bg_mango'
                    );
                    break;
                case 'text-image':
                    $this->resultArray = array(
                        'html' => array(
                            0 => array(
                                'html' => '<h2>Product Name</h2>
                                            See more snippets like these online store reviews at Bootsnipp - http://bootsnipp.com.
                                            Want to make these reviews work? Check out this building a review system tutorial over at maxoffsky.com!
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                                'size' => array(
                                    'w' => 7,
                                    'h' => 250
                                ),
                                'position' => array(
                                    'x' => 0,
                                    'y' => 0
                                ),
                                'typeWidget' => 'text'
                            ),
                            1 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 5,
                                    'h' => 250
                                ),
                                'position' => array(
                                    'x' => 7,
                                    'y' => 0
                                ),
                                'typeWidget' => 'image'
                            ),
                        ),
                        'size' => array(
                            'w' => 12,
                            'h' => 266
                        )
                    );
                    break;
                case 'image':
                    $this->resultArray = array(
                        'html' => '<div class="item-image"></div><div class="item-over"></div>',
                        'size' => array(
                            'w' => 5,
                            'h' => 10
                        )
                    );
                    break;
                case 'features':
                    $this->resultArray = array(
                        'html' => array(
                            0 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 1,
                                    'h' => 100
                                ),
                                'position' => array(
                                    'x' => 2,
                                    'y' => 10
                                ),
                                'typeWidget' => 'image'
                            ),
                            1 => array(
                                'html' => '<h2 style="text-align: center;"><strong>First Feature</strong></h2>
                                            <p style="text-align: center">Tell what is the value for the customer for this feature.</p>',
                                'size' => array(
                                    'w' => 3,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 1,
                                    'y' => 110
                                ),
                                'typeWidget' => 'text'
                            ),
                            2 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 1,
                                    'h' => 100
                                ),
                                'position' => array(
                                    'x' => 6,
                                    'y' => 10
                                ),
                                'typeWidget' => 'image'
                            ),
                            3 => array(
                                'html' => '<h2 style="text-align: center;"><strong>Second Feature</strong></h2>
                                            <p style="text-align: center">Write what the customer would like to know, not what you want to show.</p>',
                                'size' => array(
                                    'w' => 3,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 5,
                                    'y' => 110
                                ),
                                'typeWidget' => 'text'
                            ),
                            4 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 1,
                                    'h' => 100
                                ),
                                'position' => array(
                                    'x' => 10,
                                    'y' => 10
                                ),
                                'typeWidget' => 'image'
                            ),
                            5 => array(
                                'html' => '<h2 style="text-align: center;"><strong>Third Feature</strong></h2>
                                            <p style="text-align: center">A small explanation of this great feature, in clear words.</p>',
                                'size' => array(
                                    'w' => 3,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 9,
                                    'y' => 110
                                ),
                                'typeWidget' => 'text'
                            )
                        ),
                        'size' => array(
                            'w' => 12,
                            'h' => 400
                        )
                    );
                    break;
                case 'photo':
                    $this->resultArray = array(
                        'html' => array(
                            0 => array(
                                'html' => '<h2 style="text-align: center;"><strong>Some Nice Headline</strong></h2>',
                                'size' => array(
                                    'w' => 12,
                                    'h' => 100
                                ),
                                'position' => array(
                                    'x' => 0,
                                    'y' => 10
                                ),
                                'typeWidget' => 'text'
                            ),
                            1 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 10,
                                    'h' => 500
                                ),
                                'position' => array(
                                    'x' => 1,
                                    'y' => 120
                                ),
                                'typeWidget' => 'image'
                            ),
                            2 => array(
                                'html' => '<h4 style="text-align: center;"><strong>A Small Subtitle</strong></h4> 
                                           <p style="text-align: center">Choose a vibrant image and write an inspiring paragraph about it. It does not have to be long, but it should reinforce your image.</p>',
                                'size' => array(
                                    'w' => 8,
                                    'h' => 100
                                ),
                                'position' => array(
                                    'x' => 2,
                                    'y' => 630
                                ),
                                'typeWidget' => 'text'
                            ),
                        ),
                        'size' => array(
                            'w' => 12,
                            'h' => 850
                        )
                    );
                    break;
                case 'columns':
                    $this->resultArray = array(
                        'html' => array(
                            0 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 2,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 2,
                                    'y' => 10
                                ),
                                'typeWidget' => 'image'
                            ),
                            1 => array(
                                'html' => '<h2>Feature One</h2>
                                            Adapt these three columns to fit you design need. To duplicate, delete or move columns, select the column and use the top icons to perform your action.',
                                'size' => array(
                                    'w' => 2,
                                    'h' => 170
                                ),
                                'position' => array(
                                    'x' => 2,
                                    'y' => 180
                                ),
                                'typeWidget' => 'text'
                            ),
                            2 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 2,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 5,
                                    'y' => 10
                                ),
                                'typeWidget' => 'image'
                            ),
                            3 => array(
                                'html' => '<h2>Feature Two</h2>
                                            To add a fourth column, reduce the size of these three columns using the right icon of each block. Then, duplicate one of the column to create a new one as a copy.',
                                'size' => array(
                                    'w' => 2,
                                    'h' => 170
                                ),
                                'position' => array(
                                    'x' => 5,
                                    'y' => 180
                                ),
                                'typeWidget' => 'text'
                            ),
                            4 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 2,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 8,
                                    'y' => 10
                                ),
                                'typeWidget' => 'image'
                            ),
                            5 => array(
                                'html' => '<h2>Feature Three</h2>
                                            Delete the above image or replace it with a picture that illustrates your message.',
                                'size' => array(
                                    'w' => 2,
                                    'h' => 170
                                ),
                                'position' => array(
                                    'x' => 8,
                                    'y' => 180
                                ),
                                'typeWidget' => 'text'
                            )
                        ),
                        'size' => array(
                            'w' => 12,
                            'h' => 400
                        )
                    );
                    break;
                case 'gallery':
                    $this->resultArray = array(
                        'html' => array(
                            0 => array(
                                'html' => '<h2 style="text-align: center;"><strong>Check Some Nice Pictures</strong></h2>
                                            <p style="text-align: center"> Smoller title for more details</p>',
                                'size' => array(
                                    'w' => 12,
                                    'h' => 100
                                ),
                                'position' => array(
                                    'x' => 0,
                                    'y' => 10
                                ),
                                'typeWidget' => 'text'
                            ),
                            1 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 4,
                                    'h' => 320
                                ),
                                'position' => array(
                                    'x' => 1,
                                    'y' => 170
                                ),
                                'typeWidget' => 'image'
                            ),
                            2 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 2,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 6,
                                    'y' => 170
                                ),
                                'typeWidget' => 'image'
                            ),
                            3 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 2,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 9,
                                    'y' => 170
                                ),
                                'typeWidget' => 'image'
                            ),
                            4 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 2,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 6,
                                    'y' => 340
                                ),
                                'typeWidget' => 'image'
                            ),
                            5 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 2,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 9,
                                    'y' => 340
                                ),
                                'typeWidget' => 'image'
                            ),
                            6 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 2,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 1,
                                    'y' => 510
                                ),
                                'typeWidget' => 'image'
                            ),
                            7 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 4,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 4,
                                    'y' => 510
                                ),
                                'typeWidget' => 'image'
                            ),
                            8 => array(
                                'html' => '<div class="item-image"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 2,
                                    'h' => 150
                                ),
                                'position' => array(
                                    'x' => 9,
                                    'y' => 510
                                ),
                                'typeWidget' => 'image'
                            ),
                        ),
                        'size' => array(
                            'w' => 12,
                            'h' => 700
                        )
                    );
                    break;
                case 'map':
                    $rnd = rand(1111, 9999);
                    $this->resultArray = array(
                        'html' => array(
                            0 => array(
                                'html' => '<div class="item-map" id="item-map-' . $rnd . '"></div><div class="item-over"></div>',
                                'size' => array(
                                    'w' => 12,
                                    'h' => 266
                                ),
                                'typeWidget' => 'map',
                                'position' => array(
                                    'x' => 0,
                                    'y' => 0
                                ),
                                'id' => 'item-map-' . $rnd,
                                'title' => 'Map',
                                'options' => array(
                                    'center' => '',
                                    'lat' => '',
                                    'lng' => '',
                                    'address' => 'Wroclaw, Karpacka 25',
                                    'zoom' => 10,
                                    'panControl' => false,
                                    'scaleControl' => false,
                                    'streetViewControl' => true,
                                    'overviewMapControl' => true,
                                    'mapTypeControl' => true,
                                    'zoomControl' => true
                                )
                            )
                        ),
                        'size' => array(
                            'w' => 12,
                            'h' => 266
                        ),
                    );
                    break;
                case 'youtube':
                    $videoFrame = '<iframe src="https://www.youtube.com/embed/ks8a4uhLBAI" frameborder="0" allowfullscreen></iframe><div class="item-over"></div>';
                    $this->resultArray = array(
                        'html' => array(
                            0 => array(
                                'html' => '<div class="item-youtube">' . $videoFrame . '</div>',
                                'size' => array(
                                    'w' => 6,
                                    'h' => 240
                                ),
                                'typeWidget' => 'youtube',
                                'position' => array(
                                    'x' => 3,
                                    'y' => 0
                                )
                            )
                        ),
                        'size' => array(
                            'w' => 12,
                            'h' => 266
                        )
                    );
                    break;
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionGetPageData()
    {
        if (isset($_POST['page']) && $_POST['page'] != '') {
            $tplPage = $this->getPageData($_POST['page']);
            if ($tplPage) {
                $this->resultArray = array(
                    'tpl_page_id' => $tplPage->tpl_page_id,
                    'tpl_id' => $tplPage->tpl_id,
                    'tpl_parent_page_id' => $tplPage->tpl_parent_page_id,
                    'tpl_theme_id' => $tplPage->tpl->tpl_theme_id,
                    'tpl_page_name' => $tplPage->tpl_page_name,
                    'tpl_page_content' => $tplPage->tpl_page_content,
                    'tpl_page_settings' => $tplPage->tpl_page_settings,
                    'tpl_page_meta_settings' => $tplPage->tpl_page_meta_settings,
                );
            } else {
                $this->resultArray = array(
                    'error' => 'wrong page!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong page!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    protected function getPageData($pageId)
    {
        $tplPage = TemplatesPages::model()->with('tpl')->findByPk($pageId);
        if (count($tplPage) > 0) {
            return $tplPage;
        }

        return false;
    }

    public function actionUploadFiles()
    {
        $corePath = Yii::getPathOfAlias('core');
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_FILES['file'])) {
                $images = CUploadedFile::getInstancesByName('file');
                if (isset($images) && count($images) > 0) {
                    $resultImages = array();
                    // go through each uploaded image
                    foreach ($images as $image => $pic) {
                        $fileName = $pic->getName();
                        $ext = strtolower(array_pop(explode(".", $fileName)));
                        $newFileName = md5($fileName . date('Y-m-s')) . '.' . $ext;
                        if ($pic->saveAs($corePath . '/upload/' . $_POST['dId'] . '/' . $newFileName)) {
                            // add it to the main model now

                            $objImgl = new thumbnailImages;
                            $objImgl->PathImgOld = $corePath . '/upload/' . $_POST['dId'] . '/' . $newFileName;
                            $objImgl->PathImgNew = $corePath . '/upload/' . $_POST['dId'] . '/thumb_' . $newFileName;
                            $objImgl->NewWidth = 160;
                            $objImgl->NewHeight = 230;
                            if ($objImgl->create_thumbnail_images()) {
                                $model = new TemplatesUploads();
                                $model->tpl_up_title = '';
                                $model->tpl_up_image = $newFileName;
                                $model->tpl_up_url = '';
                                $model->tpl_up_length = $pic->getSize();
                                $model->tpl_up_status = 1;
                                $model->save();

                                $resultImages[] = array(
                                    'fileName' => $newFileName,
                                    'imageId' => $model->tpl_up_id
                                );
                            }
                        }
                    }
                    $this->resultArray = array(
                            'images' => $resultImages
                        );
                } else {
                    $this->resultArray = array(
                        'error' => 'error saving!'
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong request!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionRemoveFiles()
    {
        $corePath = Yii::getPathOfAlias('core');
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['imageId'])) {
                $tplImages = TemplatesUploads::model()->find('tpl_up_id = :id', array(':id' => $_POST['imageId']));
                if (count($tplImages) > 0) {
                    if ($tplImages->tpl_up_status == 1) {
                        if (file_exists($corePath . '/upload/' . $_POST['dId'] . '/' . $tplImages->tpl_up_image)) {
                            unlink($corePath . '/upload/' . $_POST['dId'] . '/' . $tplImages->tpl_up_image);
                            unlink($corePath . '/upload/' . $_POST['dId'] . '/thumb_' . $tplImages->tpl_up_image);
                            $tplImages->delete();
                        }
                    } else {
                        $tplImages->delete();
                    }
                    $this->resultArray = array(
                        'result' => 1
                    );
                } else {
                    $this->resultArray = array(
                        'error' => 'image is not found!'
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong imageId!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionDownloadImage()
    {
        $corePath = Yii::getPathOfAlias('core');
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['imageUrl'])) {
                $url = $_POST['imageUrl'];
                $ext = strtolower(array_pop(explode(".", $url)));
                if ($ext != 'png' || $ext != 'jpg' || $ext != 'jpeg' || $ext != 'gif')
                    $ext = 'jpg';
                $newFileName = md5($url . date('Y-m-s')) . '.' . $ext;
                $img = $corePath . '/upload/' . $_POST['dId'] . '/' . $newFileName;
                file_put_contents($img, file_get_contents($url));
                $objImgl = new thumbnailImages;
                $objImgl->PathImgOld = $img;
                $objImgl->PathImgNew = $corePath . '/upload/' . $_POST['dId'] . '/thumb_' . $newFileName;
                $objImgl->NewWidth = 160;
                $objImgl->NewHeight = 230;
                if ($objImgl->create_thumbnail_images()) {
                    $model = new TemplatesUploads();
                    $model->tpl_up_title = '';
                    $model->tpl_up_image = $newFileName;
                    $model->tpl_up_url = '';
                    $model->tpl_up_length = 0;
                    $model->tpl_up_status = 1;
                    $model->save();

                    $this->resultArray = array(
                        'fileName' => $newFileName,
                        'imageId' => $model->tpl_up_id
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong request!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionGetPage()
    {
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['pageId']) && $_POST['pageId'] != '') {
                $pages = TemplatesPages::model()->with('tpl')->findByPk($_POST['pageId']);
                $this->resultArray = array(
                    'result' => 1,
                    'pageId' => $_POST['pageId'],
                    'html' => $pages
                );
            } else {
                $this->resultArray = array(
                    'error' => 'wrong request!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }

        return $this->renderJSON($this->resultArray);
    }

    public function actionSetPage()
    {
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['pageName']) && $_POST['pageName'] != '') {
                $criteria = new CDbCriteria;
                $criteria->condition = 'tpl.tpl_id=:tplId';
                $criteria->order = 'tpl_page_id ASC';
                $criteria->params = array(
                    ':tplId' => $_POST['tplId'],
                );
                $tplPage = new TemplatesPages();
                $tplPage->tpl_page_default = $_POST['setHomePage'];
                $tplPage->tpl_page_content = json_encode(array());
                $tplPage->tpl_page_settings = json_encode(array());
                $tplPage->tpl_page_meta_settings = $_POST['pageSettings'];
                $tplPage->tpl_page_name = $_POST['pageName'];
                $tplPage->tpl_page_title = (isset($_POST['pageTitle'])) ? $_POST['pageTitle'] : '';
                $tplPage->tpl_page_keywords = (isset($_POST['pageKeywords'])) ? $_POST['pageKeywords'] : '';
                $tplPage->tpl_page_description = (isset($_POST['pageDescription'])) ? $_POST['pageDescription'] : '';
                $tplPage->tpl_page_display_menu = 1;
//                $tplPage->tpl_page_position = $countParents + 1;
                $tplPage->tpl_page_status = 1;
                $tplPage->tpl_id = $_POST['tplId'];
                if ($tplPage->save()) {
                    if ($_POST['setHomePage'] == 1) {
                        TemplatesPages::model()->updateAll(array('tpl_page_default' => 0), 'tpl_page_id!="' . $tplPage->tpl_page_id . '"');
                    }
                    $pages = TemplatesPages::model()->with('tpl')->findAll($criteria);
                    $this->resultArray = array(
                        'result' => 1,
                        'pageId' => $tplPage->tpl_page_id,
                        'html' => $this->renderPartial('/default/listPages', array(
                            'pages' => $pages
                        ), TRUE)
                    );
                } else {
                    $this->resultArray = array(
                        'error' => 'wrong request!'
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong request!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionRemovePage()
    {
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['pageId'])) {
                $allPages = TemplatesPages::model()->findAll();
                $countAllPages = count($allPages);
                if ($countAllPages > 1) {
                    $error = 0;
                    $tplPage = TemplatesPages::model()->findByPk($_POST['pageId']);
                    if ($tplPage->tpl_parent_page_id == 0) {
                        $criteria = new CDbCriteria;
                        $criteria->condition = 'tpl_parent_page_id=:tplPageId';
                        $criteria->params = array(':tplPageId' => $tplPage->tpl_page_id);
                        $childPages = TemplatesPages::model()->findAll($criteria);
                        if (count($childPages) > 0) {
                            $error = 1;
                        }
                    }
                    if ($error == 0) {
                        if ($tplPage->delete()) {
                            $this->resultArray = array(
                                'result' => 1,
                                'html' => $this->reloadMenu()
                            );
                        } else {
                            $this->resultArray = array(
                                'error' => 'error db!'
                            );
                        }
                    } else {
                        $this->resultArray = array(
                            'error' => 'please remove child pages!'
                        );
                    }
                } else {
                    $this->resultArray = array(
                        'error' => 'wrong count!'
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong page!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionUpdatePage()
    {
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['pageId'])) {
                $tplPage = TemplatesPages::model()->findByPk($_POST['pageId']);
                $tplPage->tpl_page_default = $_POST['setHomePage'];
                $tplPage->tpl_page_meta_settings = $_POST['pageSettings'];
                $tplPage->tpl_page_name = $_POST['pageName'];
                $tplPage->tpl_page_title = (isset($_POST['pageTitle'])) ? $_POST['pageTitle'] : '';
                $tplPage->tpl_page_keywords = (isset($_POST['pageKeywords'])) ? $_POST['pageKeywords'] : '';
                $tplPage->tpl_page_description = (isset($_POST['pageDescription'])) ? $_POST['pageDescription'] : '';
                if ($tplPage->save()) {
                    if ($_POST['setHomePage'] == 1) {
                        TemplatesPages::model()->updateAll(array('tpl_page_default' => 0), 'tpl_page_id!="' . $tplPage->tpl_page_id . '"');
                    }
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'tpl.tpl_id=:tplId';
                    $criteria->order = 'tpl_page_id ASC';
                    $criteria->params = array(
                        ':tplId' => $_POST['tplId'],
                    );
                    $pages = TemplatesPages::model()->with('tpl')->findAll($criteria);
                    $this->resultArray = array(
                        'result' => 1,
                        'pageId' => $tplPage->tpl_page_id,
                        'html' => $this->renderPartial('/default/listPages', array(
                            'pages' => $pages
                        ), TRUE)
                    );
                } else {
                    $this->resultArray = array(
                        'error' => 'wrong request!'
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong page!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionDuplicatePage()
    {
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['pageId'])) {
                $tplPage = TemplatesPages::model()->findByPk($_POST['pageId']);
                $tplPage->tpl_page_default = 0;
                $tplPage->tpl_page_id = null;
                $tplPage->isNewRecord = true;
                if ($tplPage->save()) {
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'tpl.tpl_id=:tplId';
                    $criteria->order = 'tpl_page_id ASC';
                    $criteria->params = array(
                        ':tplId' => $_POST['tplId'],
                    );
                    $pages = TemplatesPages::model()->with('tpl')->findAll($criteria);
                    $this->resultArray = array(
                        'result' => 1,
                        'pageId' => $tplPage->tpl_page_id,
                        'html' => $this->renderPartial('/default/listPages', array(
                            'pages' => $pages
                        ), TRUE)
                    );
                } else {
                    $this->resultArray = array(
                        'error' => 'wrong request!'
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong request!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionSortPages()
    {
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['data'])) {
                $data = json_decode($_POST['data']);
                foreach ($data as $key => $d) {
                    $parentId = $d->id;
                    $tplPage = TemplatesPages::model()->findByPk($parentId);
                    $tplPage->tpl_parent_page_id = 0;
                    $tplPage->tpl_page_position = $key + 1;
                    $tplPage->save();
                    if (isset($d->children)) {
                        foreach ($d->children as $k => $c) {
                            $childId = $c->id;
                            $tplPage = TemplatesPages::model()->findByPk($childId);
                            $tplPage->tpl_parent_page_id = $parentId;
                            $tplPage->tpl_page_position = $k + 1;
                            $tplPage->save();
                        }
                    }
                }
                $this->resultArray = array(
                    'result' => 1,
                    'html' => $this->reloadMenu()
                );
            } else {
                $this->resultArray = array(
                    'error' => 'wrong data!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    protected function reloadMenu()
    {
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            $pagesAll = array();
            $criteria = new CDbCriteria;
            $criteria->condition = 'tpl.tpl_id=:tplId';
            $criteria->condition = 'tpl_parent_page_id=0';
            $criteria->order = 'tpl_page_position ASC';
            $criteria->params = array(
                ':tplId' => $_POST['tplId']
            );

            $parentPages = TemplatesPages::model()->with('tpl')->findAll($criteria);
            foreach ($parentPages as $key => $pPage) {
                $criteria = new CDbCriteria;
                $criteria->condition = 'tpl.tpl_id=:tplId';
                $criteria->addCondition('tpl_parent_page_id=:parentId');
                $criteria->order = 'tpl_page_position ASC';
                $criteria->params = array(
                    ':tplId' => $_POST['tplId'],
                    ':parentId' => $pPage->tpl_page_id
                );
                $pages = TemplatesPages::model()->with('tpl')->findAll($criteria);
                $pagesAll[$key] = array(
                    'parent' => $parentPages[$key],
                    'child' => $pages
                );
            }

            $menu = $this->renderPartial('/editor/menu', array(
                'pages' => $pagesAll
            ), TRUE);

            return $menu;
        }
        return false;
    }

    protected function renderJSON($data)
    {
        header('Content-type: application/json');
        echo CJSON::encode($data);

        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute) {
                $route->enabled = false; // disable any weblogroutes
            }
        }
        Yii::app()->end();
    }

}
