<?php

class DefaultController extends AdminController {

    public $tplId = 1;
    public $gallery = array();
    public $pages = array();
    public $modals = array();
    public $plugins = array();
    public $panels = array();

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
//            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'password'),
                'roles' => array('user'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'password', 'update'),
                'roles' => array('moderator'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'password', 'update', 'create'),
                'roles' => array('administrator'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('index', 'view', 'password', 'admin', 'delete', 'create', 'update', 'password'),
                'roles' => array('superadministrator'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
//    public function actionCreate() {
//        $model = new Templates;
//
//        // Uncomment the following line if AJAX validation is needed
//        // $this->performAjaxValidation($model);
//
//        if (isset($_POST['Templates'])) {
//            $model->attributes = $_POST['Templates'];
//            if ($model->save())
//                $this->redirect(array('view', 'id' => $model->tpl_id));
//        }
//
//        $this->render('create', array(
//            'model' => $model,
//        ));
//    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
//    public function actionUpdate($id) {
//        $model = $this->loadModel($id);
//
//        // Uncomment the following line if AJAX validation is needed
//        // $this->performAjaxValidation($model);
//
//        if (isset($_POST['Templates'])) {
//            $model->attributes = $_POST['Templates'];
//            if ($model->save())
//                $this->redirect(array('view', 'id' => $model->tpl_id));
//        }
//
//        $this->render('update', array(
//            'model' => $model,
//        ));
//    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
//    public function actionDelete($id) {
//        $this->loadModel($id)->delete();
//
//        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//        if (!isset($_GET['ajax']))
//            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//    }


    /**
     * Manages all models.
     */
    public function actionIndex() {
        $tplPage = TemplatesPages::model()->with('tpl')->find('tpl_page_default=:act and tpl.tpl_id=:tplId', array(
            ':act' => 1,
            ':tplId' => $this->tplId
        ));
        $dataPage = array();
        if (count($tplPage) > 0) {
            $dataPage = array(
                'tpl_page_id' => $tplPage->tpl_page_id,
                'tpl_id' => $tplPage->tpl_id,
                'tpl_parent_page_id' => $tplPage->tpl_parent_page_id,
                'tpl_theme_id' => $tplPage->tpl->tpl_theme_id,
                'tpl_page_name' => $tplPage->tpl_page_name,
                'tpl_page_content' => json_decode($tplPage->tpl_page_content),
            );
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'tpl.tpl_id=:tplId';
            $criteria->limit = 1;
            $criteria->params = array(':tplId' => $this->tplId);
            $criteria->order = 'tpl_page_position ASC';
            $tplPage = TemplatesPages::model()->with('tpl')->findAll($criteria);
            if (count($tplPage) > 0) {
                $tplPage = $tplPage[0];
                $dataPage = array(
                    'tpl_page_id' => $tplPage->tpl_page_id,
                    'tpl_id' => $tplPage->tpl_id,
                    'tpl_parent_page_id' => $tplPage->tpl_parent_page_id,
                    'tpl_theme_id' => $tplPage->tpl->tpl_theme_id,
                    'tpl_page_name' => $tplPage->tpl_page_name,
                    'tpl_page_content' => json_decode($tplPage->tpl_page_content),
                );
            }
        }

        $criteria = new CDbCriteria;
        $criteria->condition = 'tpl.tpl_id=:tplId';
        $criteria->order = 'tpl_page_id ASC';
        $criteria->params = array(
            ':tplId' => $this->tplId,
        );

        $this->pages = TemplatesPages::model()->with('tpl')->findAll($criteria);

        $this->gallery = TemplatesUploads::model()->findAll();

        $contentRowButtonHTML = $this->renderPartial('contentRowButtonHTML', array(), TRUE);
        $this->panels['menuTabEdit'] = $this->renderPartial('tabEdit', array(), TRUE);
        $this->panels['panelGallery'] = $this->renderPartial('panelGallery', array(
            'images' => $this->gallery,
            'domainId' => $this->domainId,
            'tplId' => $this->tplId
                ), TRUE);
        $this->plugins['cssEditor'] = $this->renderPartial('cssEditor', array(
            'images' => $this->gallery,
            'domainId' => $this->domainId,
            'tplId' => $this->tplId
        ), TRUE);
        $this->panels['dropzoneImg'] = $this->renderPartial('dropzoneImg', array(
            'images' => $this->gallery,
            'domainId' => $this->domainId,
            'tplId' => $this->tplId
                ), TRUE);
        $this->modals['modalItemSettings'] = $this->renderPartial('modalItemSettings', array(), TRUE);
        $this->modals['modalImageLink'] = $this->renderPartial('modalImageLink', array(), TRUE);
        $this->modals['modalForms'] = $this->renderPartial('modalForms', array(), TRUE);
        $this->modals['modalRemovePage'] = $this->renderPartial('modalRemovePage', array(), TRUE);
        $this->modals['modalRemoveMenu'] = $this->renderPartial('modalRemoveMenu', array(), TRUE);
        $this->panels['panelPages'] = $this->renderPartial('panelPages', array(
            'listPages' => $this->renderPartial('listPages', array(
                'pages' => $this->pages
                    ), TRUE)
                ), TRUE);
        $this->panels['modalIconMenu'] = $this->renderPartial('modalIconMenu', array(), TRUE);
        $this->panels['panelInsert'] = $this->renderPartial('panelInsert', array(), TRUE);
        $this->panels['panelMenu'] = $this->renderPartial('panelMenu', array(), TRUE);
        $this->plugins['globalSettings'] = $this->renderPartial('globalSettings', array(), TRUE);


        $this->layout = '//layouts/editor';
        $this->render('admin', array(
            'dataPage' => $dataPage,
            'contentRowButtonHTML' => $contentRowButtonHTML
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Templates the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Templates::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Templates $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'templates-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
