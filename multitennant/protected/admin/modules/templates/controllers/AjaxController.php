<?php

Yii::import('admin.extensions.thumbnailImages');

class AjaxController extends AdminController
{

    protected $resultArray = array();

    public function actionIndex()
    {

    }

    public function actionSetElements()
    {
        if (isset($_POST['content']) && $_POST['content'] != '') {
            if (isset($_POST['pageId']) && $_POST['pageId'] != '') {
                if (isset($_POST['typeContent']) && $_POST['typeContent'] != '') {
                    $tplPage = TemplatesPages::model()->findByPk($_POST['pageId']);
                    switch ($_POST['typeContent']) {
                        case 'content':
                            $content = $_POST['content'];
                            $content = str_replace('edit-item', '', $content);
                            $content = str_replace('id="edit-text"', '', $content);
                            $content = str_replace('mce-content-body', '', $content);
                            $content = str_replace('mce-edit-focus', '', $content);
                            $content = str_replace('mce-edit-focus', '', $content);
                            $content = str_replace('spellcheck="false"', '', $content);
                            $content = str_replace('contenteditable="true"', '', $content);

                            $tplPage->tpl_page_content = $content;
                            $tplPage->tpl_page_content_options = $_POST['c_options'];
                            break;
                        case 'settings':
                            $tplPage->tpl_page_settings = $_POST['content'];
                            break;
                    }
                    $tplPage->save();
                } else {
                    $this->resultArray = array(
                        'error' => 'wrong page!'
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong page!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionGetElements()
    {
        if (isset($_POST['typeWidget']) && $_POST['typeWidget'] != '') {
            $typeWidget = $_POST['typeWidget'];
            switch ($typeWidget) {
                case 'row':
                    $this->resultArray = array(
                        'html' => '<div class="row" data-e-id="' . rand(1111111, 9999999) . '">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 column"></div>
                        </div>',
                    );
                    break;
                case 'menu':
                    $pageId = (isset($_POST['pageId']) ? $_POST['pageId'] : 0);
                    $tplPage = $this->getPageData($pageId);
                    $idMenu = 'menu_' . rand(1111111, 9999999);
                    $dataMenu = array(
                        'menu_items' => array(
                            'it0' => array(
                                'menu_name' => $tplPage->tpl_page_name,
                                'menu_menu_type' => 'link', // link, content
                                'menu_link_type' => 'url', //url, page
                                'menu_page_id' => $tplPage->tpl_page_id,
                                'menu_url' => 'http://dengo-systems.com',
                                'menu_display' => 1,
                                'menu_image_position' => 'left', //left, right, top, bottom
                                'menu_image_url' => '',
                                'menu_target' => 1,
                            )
                        ),
                        'id_menu' => $idMenu
                    );
                    $this->resultArray = array(
                        'html' => $this->renderPartial('/editor/menuRow', $dataMenu, TRUE),
                        'options' => array(
                            'typeElement' => 'menu',
                            'id_menu' => $idMenu,
                            'data_menu' => $dataMenu,
                        )
                    );
                    break;
                case 'form':
                    $this->resultArray = array(
                        'html' => '<div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2  column">
                            <fieldset>
                            <legend style="background:none">Contact Us</legend>
				<div class="form-group">
					<label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label">Name</label>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
					<input class="form-control" placeholder="Enter Your Name" type="text">
				</div>
				</div>
				<div class="form-group">
					<label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label">Your email</label>
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
						<input class="form-control" placeholder="Enter Your Email" type="text">
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label">Your message</label>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
							<textarea class="form-control" placeholder="Enter your message" rows="4"></textarea>
						</div>
					</div>
				<div class="col-md-12" style="text-align: right">
				<button class="btn btn-default" type="submit">Cancel</button>
				<button class="btn btn-primary" type="submit"><i class="fa fa-save"></i>Submit</button>
                                </div>
                            </fieldset>
                        </div>
                        </div>'
                    );
                    break;
                case 'textItem':
                    $this->resultArray = array(
                        'html' => '<h1 data-e-id="' . rand(1111111, 9999999) . '">Product Name</h1>
                        <p class="lead" data-e-id="' . rand(1111111, 9999999) . '">See more snippets like these online store reviews at Bootsnipp - http://bootsnipp.com.
                        Want to make these reviews work? Check out this building a review system tutorial over at maxoffsky.com!
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                        mollit anim id est laborum</p>'
                    );
                    break;
                case 'titleItem':
                    $this->resultArray = array(
                        'html' => '<header class="jumbotron hero-spacer">
                        <h1 data-e-id="' . rand(1111111, 9999999) . '">A Warm Welcome!</h1>
                        <p data-e-id="' . rand(1111111, 9999999) . '">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, ipsam, eligendi, in quo sunt possimus non incidunt odit vero aliquid similique quaerat nam nobis illo aspernatur vitae fugiat numquam repellat.</p>
                        <p data-e-id="' . rand(1111111, 9999999) . '"><a class="btn btn-primary btn-large">Call to action!</a>
                        </p>
                        </header>'
                    );
                    break;
                case 'imageItem':
                    $this->resultArray = array(
                        'html' => '<img data-e-id="' . rand(1111111, 9999999) . '" data-srcthumb="/upload/0/thumb_2dead82e195484e678d2bb35bdd84557.jpg" class="img-responsive" src="/upload/0/2dead82e195484e678d2bb35bdd84557.jpg" alt="">'
                    );
                    break;
                case 'listOlItem':
                    $this->resultArray = array(
                        'html' => '<h3>Project Details</h3>
                                 <ol>
                                <li>Lorem Ipsum</li>
                                <li>Dolor Sit Amet</li>
                                <li>Consectetur</li>
                                <li>Adipiscing Elit</li>
                                </ol>'
                    );
                    break;
                case 'listUlItem':
                    $this->resultArray = array(
                        'html' => '<h3>Project Details</h3>
                                <ul>
                                <li>Lorem Ipsum</li>
                                <li>Dolor Sit Amet</li>
                                <li>Consectetur</li>
                                <li>Adipiscing Elit</li>
                                </ul>'
                    );
                    break;
                case 'labelItem':
                    $this->resultArray = array(
                        'html' => '<label class="control-label">Text field</label>'
                    );
                    break;
                case 'inputItem':
                    $this->resultArray = array(
                        'html' => '<input class="form-control" placeholder="Enter your data" type="text">'
                    );
                    break;
                case 'textareaItem':
                    $this->resultArray = array(
                        'html' => '<textarea class="form-control" placeholder="Textarea" rows="4"></textarea>'
                    );
                    break;
                case 'checkboxItem':
                    $this->resultArray = array(
                        'html' => '<div class="checkbox">
                                    <label><input type="checkbox">Checkbox 1 </label>
				</div>
				<div class="checkbox">
                                    <label><input type="checkbox">Checkbox 2 </label>
				</div>
				<div class="checkbox">
                                    <label><input type="checkbox">Checkbox 3 </label>
				</div>'
                    );
                    break;
                case 'radiobuttonItem':
                    $this->resultArray = array(
                        'html' => '<div class="radio"><label><input type="radio">Radiobox 1 </label></div>
					<div class="radio"><label><input type="radio">Radiobox 2 </label></div>
					<div class="radio"><label><input type="radio">Radiobox 3 </label></div>'
                    );
                    break;
                case 'selectItem':
                    $this->resultArray = array(
                        'html' => '<select class="form-control" id="select-1">
                                            <option>Amsterdam</option>
                                            <option>Atlanta</option>
                                            <option>Baltimore</option>
                                        </select>'
                    );
                    break;
                case 'text':
                    $this->resultArray = array(
                        'html' => '<div data-e-id="' . rand(1111111, 9999999) . '" data-sizeicon="8" class="row changeRowHidden  visible-xs visible-sm visible-md visible-lg">
                        <div class="col-lg-12 text-center column" data-e-id="' . rand(1111111, 9999999) . '">
                        <h1 data-e-id="' . rand(1111111, 9999999) . '">Product Name</h1>
                        <p data-e-id="' . rand(1111111, 9999999) . '" class="lead">See more snippets like these online store reviews at Bootsnipp - http://bootsnipp.com.
                        Want to make these reviews work? Check out this building a review system tutorial over at maxoffsky.com!
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo c
                        onsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                        </div>
                        </div>'
                    );
                    break;
                case 'title':
                    $this->resultArray = array(
                        'html' => '<div data-e-id="' . rand(1111111, 9999999) . '" data-sizeicon="8" class="row text-center changeRowHidden  visible-xs visible-sm visible-md visible-lg">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 column" data-e-id="' . rand(1111111, 9999999) . '">
                        <h1 data-e-id="' . rand(1111111, 9999999) . '">One Page Wonder</h1>
                        <h3 data-e-id="' . rand(1111111, 9999999) . '">Will Knock Your Socks Off</h3>
                        </div>
                        </div>'
                    );
                    break;
                case 'big-text':
                    $this->resultArray = array(
                        'html' => '<div data-e-id="' . rand(1111111, 9999999) . '" data-sizeicon="8" class="row text-center changeRowHidden  visible-xs visible-sm visible-md visible-lg">
                      <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 column">
                      <h2 data-e-id="' . rand(1111111, 9999999) . '"><a href="#">Blog Post Title</a></h2>
                      <p data-e-id="' . rand(1111111, 9999999) . '" class="lead"><a href="index.php">Start Bootstrap</a></p>
                      <hr>
                      <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/2dead82e195484e678d2bb35bdd84557.jpg" data-srcthumb="/upload/0/thumb_2dead82e195484e678d2bb35bdd84557.jpg" alt="">
                      <hr>
                      <p data-e-id="' . rand(1111111, 9999999) . '">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, veritatis, tempora, necessitatibus inventore nisi quam quia repellat ut tempore laborum possimus eum dicta id animi corrupti debitis ipsum officiis rerum.</p>
                      <a data-e-id="' . rand(1111111, 9999999) . '" class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
                      </div>
                      </div>'
                    );
                    break;
                case 'image-text':
                    $this->resultArray = array(
                        'html' => '<div data-e-id="' . rand(1111111, 9999999) . '" data-sizeicon="8" class="row changeRowHidden  visible-xs visible-sm visible-md visible-lg">
                                 <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-5 col-md-5 col-sm-5 col-xs-5 column">
                                     <a href="#">
                                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/1df6f0dcb04a1600869d5ea5103e3d76.jpg" data-srcthumb="/upload/0/thumb_1df6f0dcb04a1600869d5ea5103e3d76.jpg" alt="">
                                     </a>
                                </div>
                            <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-7 col-md-7 col-sm-7 col-xs-7 column">
                        <h3 data-e-id="' . rand(1111111, 9999999) . '">Product Name</h3>
                        <h4 data-e-id="' . rand(1111111, 9999999) . '">Subheading</h4>
                        <p data-e-id="' . rand(1111111, 9999999) . '">See more snippets like these online store reviews at Bootsnipp - http://bootsnipp.com.
                         Want to make these reviews work? Check out this building a review system tutorial over at maxoffsky.com!
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum, </p>
                        </div>

                        </div>'
                    );
                    break;
                case 'banner':
                    $this->resultArray = array(
                        'html' => '<div data-e-id="' . rand(1111111, 9999999) . '" data-sizeicon="8" class="row changeRowHidden  visible-xs visible-sm visible-md visible-lg activeItem">
                            <div data-e-id="' . rand(1111111, 9999999) . '"  class="col-lg-5 col-md-5 col-sm-6 col-xs-6 column">
                                     <a href="#">
                                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/0d0d5f365c09687d5f43758926675660.jpg" data-srcthumb="/upload/0/thumb_0d0d5f365c09687d5f43758926675660.jpg" alt="">
                                     </a>
                             </div>
                            <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-7 col-md-7 col-sm-12 col-xs-12 column">
                          <h1 data-e-id="' . rand(1111111, 9999999) . '">Hello, world!</h1>
                          <p data-e-id="' . rand(1111111, 9999999) . '">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi numquam placeat voluptatibus! Eligendi est hic itaque magnam, natus nesciunt omnis repellendus saepe voluptate voluptatibus? Ab animi asperiores dicta dolorem dolorum ducimus facere facilis, labore modi molestiae molestias necessitatibus nemo nostrum pariatur quasi qui repellendus reprehenderit saepe sunt veniam?</p>
                          <p data-e-id="' . rand(1111111, 9999999) . '"><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>
                        </div>
                        </div>'
                    );
                    break;
                case 'text-image':
                    $this->resultArray = array(
                        'html' => '<div data-e-id="' . rand(1111111, 9999999) . '" data-sizeicon="8" class="row changeRowHidden  visible-xs visible-sm visible-md visible-lg">
                            <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-7 col-md-7 col-sm-7 col-xs-7 column">
                        <h3 data-e-id="' . rand(1111111, 9999999) . '">Product Name</h3>
                        <h4 data-e-id="' . rand(1111111, 9999999) . '">Subheading</h4>
                        <p data-e-id="' . rand(1111111, 9999999) . '">See more snippets like these online store reviews at Bootsnipp - http://bootsnipp.com.
                         Want to make these reviews work? Check out this building a review system tutorial over at maxoffsky.com!
                        </div>

                            <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-5 col-md-5 col-sm-5 col-xs-5 column">
                                     <a href="#">
                                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/0d95ffc6f03e15ee0f323e287110cf1a.jpg" data-srcthumb="/upload/0/thumb_0d95ffc6f03e15ee0f323e287110cf1a.jpg" alt="">
                                     </a>
                             </div>
                        </div>'
                    );
                    break;
                case 'image':
                    $this->resultArray = array(
                        'html' => '<div data-e-id="' . rand(1111111, 9999999) . '" data-sizeicon="8" class="row changeRowHidden  visible-xs visible-sm visible-md visible-lg">
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-8 col-md-8 col-sm-12 col-xs-12 column">
                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="http://placehold.it/750x500"  alt="">
                        </div>
                        </div>'
                    );
                    break;
                case 'features':
                    $this->resultArray = array(
                        'html' => '<div data-e-id="' . rand(1111111, 9999999) . '" data-sizeicon="8" class="row changeRowHidden  visible-xs visible-sm visible-md visible-lg">
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 column">
                       <h3 data-e-id="' . rand(1111111, 9999999) . '">Latest Features</h3>
                       </div>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="row text-center">
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hero-feature column">
                       <div class="thumbnail">
                       <img data-e-id="' . rand(1111111, 9999999) . '" src="/upload/0/5d9109c506e543a02569985912f19495.jpg" data-srcthumb="/upload/0/thumb_5d9109c506e543a02569985912f19495.jpg" alt="">
                       <div class="caption">
                       <h3 data-e-id="' . rand(1111111, 9999999) . '">Feature Label</h3>
                       <p data-e-id="' . rand(1111111, 9999999) . '">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                       <p>
                       <a data-e-id="' . rand(1111111, 9999999) . '" href="#" class="btn btn-primary">Buy Now!</a> <a href="#" class="btn btn-default">More Info</a>
                       </p>
                       </div>
                       </div>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hero-feature column">
                       <div class="thumbnail">
                       <img data-e-id="' . rand(1111111, 9999999) . '" src="/upload/0/6cd6af0a7591d5bbd5949919a17a0379.jpg" data-srcthumb="/upload/0/thumb_6cd6af0a7591d5bbd5949919a17a0379.jpg" alt="">
                       <div class="caption">
                       <h3 data-e-id="' . rand(1111111, 9999999) . '">Feature Label</h3>
                       <p data-e-id="' . rand(1111111, 9999999) . '">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                       <p>
                       <a data-e-id="' . rand(1111111, 9999999) . '" href="#" class="btn btn-primary">Buy Now!</a> <a href="#" class="btn btn-default">More Info</a>
                       </p>
                       </div>
                       </div>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hero-feature column">
                       <div class="thumbnail">
                       <img data-e-id="' . rand(1111111, 9999999) . '" src="/upload/0/6d19948465b3079352a6e21fe1673904.jpg" data-srcthumb="/upload/0/thumb_6d19948465b3079352a6e21fe1673904.jpg" alt="">
                       <div class="caption">
                       <h3 data-e-id="' . rand(1111111, 9999999) . '">Feature Label</h3>
                       <p data-e-id="' . rand(1111111, 9999999) . '">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                       <p>
                       <a data-e-id="' . rand(1111111, 9999999) . '" href="#" class="btn btn-primary">Buy Now!</a> <a href="#" class="btn btn-default">More Info</a>
                       </p>
                       </div>
                       </div>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hero-feature column">
                       <div class="thumbnail">
                       <img data-e-id="' . rand(1111111, 9999999) . '" src="/upload/0/7cd620252e4684d5cb09d9df8eb753da.jpg" data-srcthumb="/upload/0/thumb_7cd620252e4684d5cb09d9df8eb753da.jpg" alt="">
                       <div class="caption">
                       <h3 data-e-id="' . rand(1111111, 9999999) . '">Feature Label</h3>
                       <p data-e-id="' . rand(1111111, 9999999) . '">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                       <p>
                       <a data-e-id="' . rand(1111111, 9999999) . '" href="#" class="btn btn-primary">Buy Now!</a> <a href="#" class="btn btn-default">More Info</a>
                       </p>
                       </div>
                       </div>
                       </div>
                       </div>
                       </div>'
                    );
                    break;
                case 'photo':
                    $this->resultArray = array(
                        'html' => '<div data-e-id="' . rand(1111111, 9999999) . '" data-sizeicon="8" class="row changeRowHidden  visible-xs visible-sm visible-md visible-lg">
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 column">
                        <h1 data-e-id="' . rand(1111111, 9999999) . '">The Big Picture</h1>
                        <p data-e-id="' . rand(1111111, 9999999) . '">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, iusto, unde, sunt incidunt id sapiente rerum soluta voluptate harum veniam fuga odit ea pariatur vel eaque sint sequi tenetur eligendi.</p>
                        </div>
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 column">
                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img img-responsive img-center" src="/upload/0/2f55b5caa9a2219c100bd5c243314fb1.jpg" data-srcthumb="/upload/0/thumb_2f55b5caa9a2219c100bd5c243314fb1.jpg" alt="">
                        </div>
                        </div>'
                    );
                    break;
                case 'columns':
                    $this->resultArray = array(
                        'html' => '<div data-e-id="' . rand(1111111, 9999999) . '" data-sizeicon="8" class="row changeRowHidden  visible-xs visible-sm visible-md visible-lg">
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 column">
                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img-circle img-responsive img-center" src="/upload/0/4bb23764706e83ca3ea82718bb8964e9.jpg" data-srcthumb="/upload/0/thumb_4bb23764706e83ca3ea82718bb8964e9.jpg" alt="">
                        <h2 data-e-id="' . rand(1111111, 9999999) . '">Marketing Box #1</h2>
                        <p data-e-id="' . rand(1111111, 9999999) . '">These marketing boxes are a great place to put some information. These can contain summaries of what the company does, promotional information, or anything else that is relevant to the company. These will usually be below-the-fold.</p>
                        </div>
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 column">
                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img-circle img-responsive img-center" src="/upload/0/3f34c08f16a5c81ea703a9f25c586ece.jpg" data-srcthumb="/upload/0/thumb_3f34c08f16a5c81ea703a9f25c586ece.jpg" alt="">
                        <h2 data-e-id="' . rand(1111111, 9999999) . '">Marketing Box #2</h2>
                        <p data-e-id="' . rand(1111111, 9999999) . '">The images are set to be circular and responsive. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.</p>
                        </div>
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 column">
                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img-circle img-responsive img-center" src="/upload/0/4e26239d31cb28735d09326185626291.jpg" data-srcthumb="/upload/0/thumb_4e26239d31cb28735d09326185626291.jpg" alt="">
                        <h2 data-e-id="' . rand(1111111, 9999999) . '">Marketing Box #3</h2>
                        <p data-e-id="' . rand(1111111, 9999999) . '">Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.</p>
                        </div>
                        </div>
                        </div>'
                    );
                    break;
                case 'gallery':
                    $this->resultArray = array(
                        'html' => '<div data-e-id="' . rand(1111111, 9999999) . '" data-sizeicon="8" class="row changeRowHidden  visible-xs visible-sm visible-md visible-lg">
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 column">
                       <h1 data-e-id="' . rand(1111111, 9999999) . '" class="page-header">Page Heading
                       <small>Secondary Text</small>
                       </h1>
                       </div>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="row">
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 portfolio-item column">
                       <a href="#">
                       <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/9fc531b48140f27680b219aea4a43a0e.jpg" data-srcthumb="/upload/0/thumb_9fc531b48140f27680b219aea4a43a0e.jpg" alt="">
                       </a>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 portfolio-item column">
                       <a href="#">
                       <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/12b83f1593d24398600fd7fa86ce2d02.jpg" data-srcthumb="/upload/0/thumb_12b83f1593d24398600fd7fa86ce2d02.jpg" alt="">
                       </a>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 portfolio-item column">
                       <a href="#">
                       <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/12d6faef0de4d5ab9e2d06ff87ad1499.jpg" data-srcthumb="/upload/0/thumb_12d6faef0de4d5ab9e2d06ff87ad1499.jpg" alt="">
                       </a>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 portfolio-item column">
                       <a href="#">
                       <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/18d5450c592444aa6f89e132836757c8.jpg" data-srcthumb="/upload/0/thumb_18d5450c592444aa6f89e132836757c8.jpg" alt="">
                       </a>
                       </div>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="row">
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 portfolio-item column">
                       <a href="#">
                       <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/18de32a3a8333cd00dcdff877ec7da0d.jpg" data-srcthumb="/upload/0/thumb_18de32a3a8333cd00dcdff877ec7da0d.jpg" alt="">
                       </a>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 portfolio-item column">
                       <a href="#">
                       <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/28b75c9fb716d454c85a61b151be5731.jpg" data-srcthumb="/upload/0/thumb_28b75c9fb716d454c85a61b151be5731.jpg" alt="">
                       </a>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-md-3 col-sm-6 col-xs-12 portfolio-item column">
                       <a href="#">
                       <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/45d5f9947c2b02342c7e87db6921440e.jpg" data-srcthumb="/upload/0/thumb_45d5f9947c2b02342c7e87db6921440e.jpg" alt="">
                       </a>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 portfolio-item column">
                       <a href="#">
                       <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/82ada2a0c3bfe1be52eb5e6b2aad6ff9.jpg" data-srcthumb="/upload/0/thumb_82ada2a0c3bfe1be52eb5e6b2aad6ff9.jpg" alt="">
                       </a>
                       </div>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="row">
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 portfolio-item column">
                       <a href="#">
                       <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/54ab607b38b17adb79ac7eefc81c76ed.jpg" data-srcthumb="/upload/0/thumb_54ab607b38b17adb79ac7eefc81c76ed.jpg" alt="">
                       </a>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 portfolio-item column">
                       <a href="#">
                       <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/64e448463a3002c2cbbe66396c117e2f.jpg" data-srcthumb="/upload/0/thumb_64e448463a3002c2cbbe66396c117e2f.jpg" alt="">
                       </a>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 portfolio-item column">
                       <a href="#">
                       <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/81cdb3ed0bba2b6a9b6d56538e4f2f39.jpg" data-srcthumb="/upload/0/thumb_81cdb3ed0bba2b6a9b6d56538e4f2f39.jpg" alt="">
                       </a>
                       </div>
                       <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 portfolio-item column">
                       <a href="#">
                       <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/84efd42b509dd7815d9dc725f253292a.jpg" data-srcthumb="/upload/0/thumb_84efd42b509dd7815d9dc725f253292a.jpg" alt="">
                       </a>
                       </div>
                       </div>
                       </div>'
                    );
                    break;
                case 'gallery2':
                    $this->resultArray = array(
                        'html' => '<div data-e-id="' . rand(1111111, 9999999) . '" data-sizeicon="8" class="row changeRowHidden  visible-xs visible-sm visible-md visible-lg">
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-12 col-sm-12 col-md-12 col-xs-12 column">
                        <h1 data-e-id="' . rand(1111111, 9999999) . '" class="page-header">Thumbnail Gallery</h1>
                        </div>
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-6 col-sm-6 col-md-6 col-xs-12 column">
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-12 col-md-12 col-xs-12 thumb column">
                        <a class="thumbnail" href="#">
                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/54ab607b38b17adb79ac7eefc81c76ed.jpg" data-srcthumb="/upload/0/thumb_54ab607b38b17adb79ac7eefc81c76ed.jpg" alt="">
                        </a>
                        </div>
                        </div>
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-6 col-sm-6 col-md-6 col-xs-12 column">
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-6 col-sm-6 col-md-6 col-xs-6 thumb column">
                        <a class="thumbnail" href="#">
                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/84efd42b509dd7815d9dc725f253292a.jpg" data-srcthumb="/upload/0/thumb_84efd42b509dd7815d9dc725f253292a.jpg" alt="">
                        </a>
                        </div>
                        </div>
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-6 col-sm-6 col-md-6 col-xs-6 thumb column">
                        <a class="thumbnail" href="#">
                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/82ada2a0c3bfe1be52eb5e6b2aad6ff9.jpg" data-srcthumb="/upload/0/thumb_82ada2a0c3bfe1be52eb5e6b2aad6ff9.jpg" alt="">
                        </a>
                        </div>
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-6 col-sm-6 col-md-6 col-xs-6 thumb column">
                        <a class="thumbnail" href="#">
                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/18de32a3a8333cd00dcdff877ec7da0d.jpg" data-srcthumb="/upload/0/thumb_18de32a3a8333cd00dcdff877ec7da0d.jpg" alt="">
                        </a>
                        </div>
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-6 col-sm-6 col-md-6 col-xs-6 thumb column">
                        <a class="thumbnail" href="#">
                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/12d6faef0de4d5ab9e2d06ff87ad1499.jpg" data-srcthumb="/upload/0/thumb_12d6faef0de4d5ab9e2d06ff87ad1499.jpg" alt="">
                        </a>
                        </div>
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-3 col-md-3 col-sm-3  col-xs-3 thumb column">
                        <a class="thumbnail" href="#">
                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/12b83f1593d24398600fd7fa86ce2d02.jpg" data-srcthumb="/upload/0/thumb_12b83f1593d24398600fd7fa86ce2d02.jpg" alt="">
                        </a>
                        </div>
                        <div data-e-id="' . rand(1111111, 9999999) . '" class="col-lg-9 col-md-9 col-sm-9 col-xs-9 thumb column">
                        <a class="thumbnail" href="#">
                        <img data-e-id="' . rand(1111111, 9999999) . '" class="img-responsive" src="/upload/0/18d5450c592444aa6f89e132836757c8.jpg" data-srcthumb="/upload/0/thumb_18d5450c592444aa6f89e132836757c8.jpg" alt="">
                        </a>
                        </div>
                        </div>'
                    );
                    break;
                case 'map':
                    $rnd = rand(1111, 9999);
                    $this->resultArray = array(
                        'html' => '<div data-e-id="' . rand(1111111, 9999999) . '" data-sizeicon="8"  class="item-map" id="item-map-' . $rnd . '"></div>',
                        'options' => array(
                            'typeElement' => 'map',
                            'id' => 'item-map-' . $rnd,
                            'title' => 'Map',
                            'options' => array(
                                'center' => '',
                                'lat' => '',
                                'lng' => '',
                                'address' => 'Wroclaw, Karpacka 25',
                                'zoom' => 10,
                                'panControl' => false,
                                'scaleControl' => false,
                                'streetViewControl' => true,
                                'overviewMapControl' => true,
                                'mapTypeControl' => true,
                                'zoomControl' => true
                            )
                        )
                    );
                    break;
                case 'youtube':
                    $videoFrame = '<iframe src = "https://www.youtube.com/embed/ks8a4uhLBAI" frameborder = "0" allowfullscreen></iframe><div class="item-youtube-over item-over"></div>';
                    $this->resultArray = array(
                        'html' => '<div data-e-id="' . rand(1111111, 9999999) . '" class="item-youtube">' . $videoFrame . '</div>'
                    );
                    break;
                case 'slider':
                    $this->resultArray = array(
                        'html' => ' <div data-sizeicon="8" class="row changeRowHidden  visible-xs visible-sm visible-md visible-lg">
                            <header id="tabCaorusel" class="carousel shade">
        <ol class="carousel-indicators">
            <li data-target="#tabCaorusel" data-slide-to="0" class="active"></li>
            <li data-target="#tabCaorusel" data-slide-to="1"></li>
            <li data-target="#tabCaorusel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
               <img src="/upload/0/2f04a962a722b4128f5ead7d91bd6baa.jpg" data-srcthumb="/upload/0/thumb_2f04a962a722b4128f5ead7d91bd6baa.jpg" alt="">
               <div class="carousel-caption">
                    <h2>Caption 1</h2>
                </div>
            </div>
            <div class="item">
                <img src="/upload/0/4bb23764706e83ca3ea82718bb8964e9.jpg" data-srcthumb="/upload/0/thumb_4bb23764706e83ca3ea82718bb8964e9.jpg" alt="">
                <div class="carousel-caption">
                    <h2>Caption 2</h2>
                </div>
            </div>
            <div class="item">
                <img src="/upload/0/1ee3d08664537947e54b3f3861d39274.jpg" data-srcthumb="/upload/0/thumb_1ee3d08664537947e54b3f3861d39274.jpg" alt="">
                <div class="carousel-caption">
                    <h2>Caption 3</h2>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#tabCaorusel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#tabCaorusel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
    </div>'
                    );
                    break;
                case 'button':
                    $this->resultArray = array(
                        'html' => '<div data-sizeicon="8" class="row changeRowHidden  visible-xs visible-sm visible-md visible-lg">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 column jumbotron hero-spacer">
                              <h1>A Warm Welcome!</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, ipsam, eligendi, in quo sunt possimus non incidunt odit vero aliquid similique quaerat nam nobis illo aspernatur vitae fugiat numquam repellat.</p>
                            <p><a class="btn btn-primary btn-large">Call to action!</a>
                            </p>
                            </div>
                            </div>'
                    );
                    break;
                default:
                    $this->resultArray = array(
                        'html' => ''
                    );
                    break;
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionGetPageData()
    {
        if (isset($_POST['page']) && $_POST['page'] != '') {
            $tplPage = $this->getPageData($_POST['page']);
            if ($tplPage) {
                $this->resultArray = array(
                    'tpl_page_id' => $tplPage->tpl_page_id,
                    'tpl_id' => $tplPage->tpl_id,
                    'tpl_parent_page_id' => $tplPage->tpl_parent_page_id,
                    'tpl_theme_id' => $tplPage->tpl->tpl_theme_id,
                    'tpl_page_name' => $tplPage->tpl_page_name,
                    'tpl_page_content' => $tplPage->tpl_page_content,
                    'tpl_page_content_options' => $tplPage->tpl_page_content_options,
                    'tpl_page_settings' => $tplPage->tpl_page_settings,
                    'tpl_page_meta_settings' => $tplPage->tpl_page_meta_settings,
                );
            } else {
                $this->resultArray = array(
                    'error' => 'wrong page!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong page!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    protected function getPageData($pageId)
    {
        $tplPage = TemplatesPages::model()->with('tpl')->findByPk($pageId);
        if (count($tplPage) > 0) {
            return $tplPage;
        }

        return false;
    }

    public function actionUploadFiles()
    {
        $corePath = Yii::getPathOfAlias('core');
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_FILES['file'])) {
                $images = CUploadedFile::getInstancesByName('file');
                if (isset($images) && count($images) > 0) {
                    $resultImages = array();
                    // go through each uploaded image
                    foreach ($images as $image => $pic) {
                        $fileName = $pic->getName();
                        $ext = strtolower(array_pop(explode(".", $fileName)));
                        $newFileName = md5($fileName . date('Y-m-s')) . '.' . $ext;
                        if ($pic->saveAs($corePath . '/upload/' . $_POST['dId'] . '/' . $newFileName)) {
                            // add it to the main model now

                            $objImgl = new thumbnailImages;
                            $objImgl->PathImgOld = $corePath . '/upload/' . $_POST['dId'] . '/' . $newFileName;
                            $objImgl->PathImgNew = $corePath . '/upload/' . $_POST['dId'] . '/thumb_' . $newFileName;
                            $objImgl->NewWidth = 160;
                            $objImgl->NewHeight = 230;
                            if ($objImgl->create_thumbnail_images()) {
                                $model = new TemplatesUploads();
                                $model->tpl_up_title = '';
                                $model->tpl_up_image = $newFileName;
                                $model->tpl_up_url = '';
                                $model->tpl_up_length = $pic->getSize();
                                $model->tpl_up_status = 1;
                                $model->save();

                                $resultImages[] = array(
                                    'fileName' => $newFileName,
                                    'imageId' => $model->tpl_up_id
                                );
                            }
                        }
                    }
                    $this->resultArray = array(
                        'images' => $resultImages
                    );
                } else {
                    $this->resultArray = array(
                        'error' => 'error saving!'
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong request!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionRemoveFiles()
    {
        $corePath = Yii::getPathOfAlias('core');
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['imageId'])) {
                $tplImages = TemplatesUploads::model()->find('tpl_up_id = :id', array(':id' => $_POST['imageId']));
                if (count($tplImages) > 0) {
                    if ($tplImages->tpl_up_status == 1) {
                        if (file_exists($corePath . '/upload/' . $_POST['dId'] . '/' . $tplImages->tpl_up_image)) {
                            unlink($corePath . '/upload/' . $_POST['dId'] . '/' . $tplImages->tpl_up_image);
                            unlink($corePath . '/upload/' . $_POST['dId'] . '/thumb_' . $tplImages->tpl_up_image);
                            $tplImages->delete();
                        }
                    } else {
                        $tplImages->delete();
                    }
                    $this->resultArray = array(
                        'result' => 1
                    );
                } else {
                    $this->resultArray = array(
                        'error' => 'image is not found!'
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong imageId!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionDownloadImage()
    {
        $corePath = Yii::getPathOfAlias('core');
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['imageUrl'])) {
                $url = $_POST['imageUrl'];
                $ext = strtolower(array_pop(explode(".", $url)));
                if ($ext != 'png' || $ext != 'jpg' || $ext != 'jpeg' || $ext != 'gif')
                    $ext = 'jpg';
                $newFileName = md5($url . date('Y-m-s')) . '.' . $ext;
                $img = $corePath . '/upload/' . $_POST['dId'] . '/' . $newFileName;
                file_put_contents($img, file_get_contents($url));
                $objImgl = new thumbnailImages;
                $objImgl->PathImgOld = $img;
                $objImgl->PathImgNew = $corePath . '/upload/' . $_POST['dId'] . '/thumb_' . $newFileName;
                $objImgl->NewWidth = 160;
                $objImgl->NewHeight = 230;
                if ($objImgl->create_thumbnail_images()) {
                    $model = new TemplatesUploads();
                    $model->tpl_up_title = '';
                    $model->tpl_up_image = $newFileName;
                    $model->tpl_up_url = '';
                    $model->tpl_up_length = 0;
                    $model->tpl_up_status = 1;
                    $model->save();

                    $this->resultArray = array(
                        'fileName' => $newFileName,
                        'imageId' => $model->tpl_up_id
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong request!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionGetPage()
    {
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['pageId']) && $_POST['pageId'] != '') {
                $pages = TemplatesPages::model()->with('tpl')->findByPk($_POST['pageId']);
                $this->resultArray = array(
                    'result' => 1,
                    'pageId' => $_POST['pageId'],
                    'html' => $pages
                );
            } else {
                $this->resultArray = array(
                    'error' => 'wrong request!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }

        return $this->renderJSON($this->resultArray);
    }

    public function actionSetPage()
    {
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['pageName']) && $_POST['pageName'] != '') {
                $criteria = new CDbCriteria;
                $criteria->condition = 'tpl.tpl_id = :tplId';
                $criteria->order = 'tpl_page_id ASC';
                $criteria->params = array(
                    ':tplId' => $_POST['tplId'],
                );
                $tplPage = new TemplatesPages();
                $tplPage->tpl_page_default = $_POST['setHomePage'];
                $tplPage->tpl_page_content = '';
                $tplPage->tpl_page_content_options = json_encode(array(
                    'map' => array(),
                    'menu' => array()
                ));
                $tplPage->tpl_page_settings = json_encode(array());
                $tplPage->tpl_page_meta_settings = $_POST['pageSettings'];
                $tplPage->tpl_page_name = $_POST['pageName'];
                $tplPage->tpl_page_title = (isset($_POST['pageTitle'])) ? $_POST['pageTitle'] : '';
                $tplPage->tpl_page_keywords = (isset($_POST['pageKeywords'])) ? $_POST['pageKeywords'] : '';
                $tplPage->tpl_page_description = (isset($_POST['pageDescription'])) ? $_POST['pageDescription'] : '';
                $tplPage->tpl_page_display_menu = 1;
//                $tplPage->tpl_page_position = $countParents + 1;
                $tplPage->tpl_page_status = 1;
                $tplPage->tpl_id = $_POST['tplId'];
                if ($tplPage->save()) {
                    if ($_POST['setHomePage'] == 1) {
                        TemplatesPages::model()->updateAll(array('tpl_page_default' => 0), 'tpl_page_id!="' . $tplPage->tpl_page_id . '"');
                    }
                    $pages = TemplatesPages::model()->with('tpl')->findAll($criteria);
                    $this->resultArray = array(
                        'result' => 1,
                        'pageId' => $tplPage->tpl_page_id,
                        'html' => $this->renderPartial('/default/listPages', array(
                            'pages' => $pages
                        ), TRUE)
                    );
                } else {
                    $this->resultArray = array(
                        'error' => 'wrong request!'
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong request!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionRemovePage()
    {
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['pageId'])) {
                $allPages = TemplatesPages::model()->findAll();
                $countAllPages = count($allPages);
                if ($countAllPages > 1) {
                    $error = 0;
                    $tplPage = TemplatesPages::model()->findByPk($_POST['pageId']);
                    if ($tplPage->tpl_parent_page_id == 0) {
                        $criteria = new CDbCriteria;
                        $criteria->condition = 'tpl_parent_page_id = :tplPageId';
                        $criteria->params = array(':tplPageId' => $tplPage->tpl_page_id);
                        $childPages = TemplatesPages::model()->findAll($criteria);
                        if (count($childPages) > 0) {
                            $error = 1;
                        }
                    }
                    if ($error == 0) {
                        if ($tplPage->delete()) {
                            $this->resultArray = array(
                                'result' => 1,
                                'html' => $this->reloadMenu()
                            );
                        } else {
                            $this->resultArray = array(
                                'error' => 'error db!'
                            );
                        }
                    } else {
                        $this->resultArray = array(
                            'error' => 'please remove child pages!'
                        );
                    }
                } else {
                    $this->resultArray = array(
                        'error' => 'wrong count!'
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong page!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionUpdatePage()
    {
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['pageId'])) {
                $tplPage = TemplatesPages::model()->findByPk($_POST['pageId']);
                $tplPage->tpl_page_default = $_POST['setHomePage'];
                $tplPage->tpl_page_meta_settings = $_POST['pageSettings'];
                $tplPage->tpl_page_name = $_POST['pageName'];
                $tplPage->tpl_page_title = (isset($_POST['pageTitle'])) ? $_POST['pageTitle'] : '';
                $tplPage->tpl_page_keywords = (isset($_POST['pageKeywords'])) ? $_POST['pageKeywords'] : '';
                $tplPage->tpl_page_description = (isset($_POST['pageDescription'])) ? $_POST['pageDescription'] : '';
                if ($tplPage->save()) {
                    if ($_POST['setHomePage'] == 1) {
                        TemplatesPages::model()->updateAll(array('tpl_page_default' => 0), 'tpl_page_id!="' . $tplPage->tpl_page_id . '"');
                    }
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'tpl.tpl_id = :tplId';
                    $criteria->order = 'tpl_page_id ASC';
                    $criteria->params = array(
                        ':tplId' => $_POST['tplId'],
                    );
                    $pages = TemplatesPages::model()->with('tpl')->findAll($criteria);
                    $this->resultArray = array(
                        'result' => 1,
                        'pageId' => $tplPage->tpl_page_id,
                        'html' => $this->renderPartial('/default/listPages', array(
                            'pages' => $pages
                        ), TRUE)
                    );
                } else {
                    $this->resultArray = array(
                        'error' => 'wrong request!'
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong page!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionDuplicatePage()
    {
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['pageId'])) {
                $tplPage = TemplatesPages::model()->findByPk($_POST['pageId']);
                $tplPage->tpl_page_default = 0;
                $tplPage->tpl_page_id = null;
                $tplPage->isNewRecord = true;
                if ($tplPage->save()) {
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'tpl.tpl_id = :tplId';
                    $criteria->order = 'tpl_page_id ASC';
                    $criteria->params = array(
                        ':tplId' => $_POST['tplId'],
                    );
                    $pages = TemplatesPages::model()->with('tpl')->findAll($criteria);
                    $this->resultArray = array(
                        'result' => 1,
                        'pageId' => $tplPage->tpl_page_id,
                        'html' => $this->renderPartial('/default/listPages', array(
                            'pages' => $pages
                        ), TRUE)
                    );
                } else {
                    $this->resultArray = array(
                        'error' => 'wrong request!'
                    );
                }
            } else {
                $this->resultArray = array(
                    'error' => 'wrong request!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    public function actionSortPages()
    {
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            if (isset($_POST['data'])) {
                $data = json_decode($_POST['data']);
                foreach ($data as $key => $d) {
                    $parentId = $d->id;
                    $tplPage = TemplatesPages::model()->findByPk($parentId);
                    $tplPage->tpl_parent_page_id = 0;
                    $tplPage->tpl_page_position = $key + 1;
                    $tplPage->save();
                    if (isset($d->children)) {
                        foreach ($d->children as $k => $c) {
                            $childId = $c->id;
                            $tplPage = TemplatesPages::model()->findByPk($childId);
                            $tplPage->tpl_parent_page_id = $parentId;
                            $tplPage->tpl_page_position = $k + 1;
                            $tplPage->save();
                        }
                    }
                }
                $this->resultArray = array(
                    'result' => 1,
                    'html' => $this->reloadMenu()
                );
            } else {
                $this->resultArray = array(
                    'error' => 'wrong data!'
                );
            }
        } else {
            $this->resultArray = array(
                'error' => 'wrong request!'
            );
        }
        return $this->renderJSON($this->resultArray);
    }

    protected function reloadMenu()
    {
        if (isset($_POST['dId']) && isset($_POST['tplId'])) {
            $pagesAll = array();
            $criteria = new CDbCriteria;
            $criteria->condition = 'tpl.tpl_id = :tplId';
            $criteria->condition = 'tpl_parent_page_id = 0';
            $criteria->order = 'tpl_page_position ASC';
            $criteria->params = array(
                ':tplId' => $_POST['tplId']
            );

            $parentPages = TemplatesPages::model()->with('tpl')->findAll($criteria);
            foreach ($parentPages as $key => $pPage) {
                $criteria = new CDbCriteria;
                $criteria->condition = 'tpl.tpl_id = :tplId';
                $criteria->addCondition('tpl_parent_page_id = :parentId');
                $criteria->order = 'tpl_page_position ASC';
                $criteria->params = array(
                    ':tplId' => $_POST['tplId'],
                    ':parentId' => $pPage->tpl_page_id
                );
                $pages = TemplatesPages::model()->with('tpl')->findAll($criteria);
                $pagesAll[$key] = array(
                    'parent' => $parentPages[$key],
                    'child' => $pages
                );
            }

            $menu = $this->renderPartial('/editor/menu', array(
                'pages' => $pagesAll
            ), TRUE);

            return $menu;
        }
        return false;
    }

    protected function renderJSON($data)
    {
        header('Content-type: application/json');
        echo CJSON::encode($data);

        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute) {
                $route->enabled = false; // disable any weblogroutes
            }
        }
        Yii::app()->end();
    }

}
