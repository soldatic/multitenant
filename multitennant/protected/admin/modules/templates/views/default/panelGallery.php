<div class="container-fluid">
    <div class="row">
        <br>
        <div class="col-lg-12">
            <!--            <div class="fileUpload btn btn-sm btn-primary" id="menu-panel-img">
                            <span><i class="fa fa-cloud-upload"></i> Upload</span>
                            <input type="file" class="upload" />
                        </div>-->
            <button class="btn btn-sm btn-primary" id="uploadImg"><span><i class="fa fa-cloud-upload"></i> Upload</span></button>
        </div>
        <!-- <div class="col-lg-6">
            <button class="btn btn-sm btn-danger disabled" id="imgRemowe" ><span class="glyphicon glyphicon-trash page-remove"></span> Remove</button>
        </div> -->
    </div>
    <div class="row" id="contWithUploadImg" style="margin-top: 8px;">
        <?php
        foreach ($images as $gallery) {
            $folder = $domainId;
            if ($gallery->tpl_up_status == 0) {
                $folder = 'galleryManager';
            }
            ?>
            <div class="preview">
                <span class="iconOnPrew iconOnPrewImgZoom"><span class="glyphicon glyphicon-zoom-in"></span></span>
                <span class="iconOnPrew iconOnPrewImgTrash"><span class="glyphicon glyphicon-trash"></span></span>
                <img class="img-responsive showPreview" alt='Alt picture'
                     src="<?php echo Yii::app()->request->baseUrl . '/upload/' . $folder . '/thumb_' . $gallery->tpl_up_image; ?>"
                     data-img="<?php echo Yii::app()->request->baseUrl . '/upload/' . $folder . '/' . $gallery->tpl_up_image; ?>"
                     data-id="<?php echo $gallery->tpl_up_id; ?>" />
            </div>
        <?php } ?>
    </div>
</div>

