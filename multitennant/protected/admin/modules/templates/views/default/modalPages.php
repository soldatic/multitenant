<div class="modal fade" id="pagesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ChangeImageOnHeaderModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Pages</h4>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-pages" data-widget-load="ajax/demowidget.php" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" role="widget">

                    <header role="heading"><div class="jarviswidget-ctrls" role="menu"></div>
                        <h2><strong>Edit Page</strong> <i>Widget</i></h2>

                    </header>

                    <!-- widget div-->
                    <div role="content">

                        <!-- widget content -->
                        <div class="widget-body">

                            <!-- widget body text-->

                            <div class="row hidden-mobile">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <h1 class="page-title txt-color-blueDark">
                                        <i class="fa-fw fa fa-copy"></i>
                                        Edit Pages </h1>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-align-right">
                                    <div class="page-title">
                                        <button type="button" class="btn btn-default" id="addNewListItem">
                                            Add new page
                                        </button>
                                    </div>
                                </div>
                            </div>


                            <div class="row" >
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                    <div class="dd" id="nestable3" style="max-width: 100%">
                                        <ol class="dd-list dd-main">
                                            <?php
                                            foreach ($pages as $page) {
                                                $data = $page['parent'];
                                                ?>
                                                <li class="dd-item dd3-item" data-id="<?php echo $data->tpl_page_id; ?>">
                                                    <div class="dd-handle dd3-handle">
                                                        Drag
                                                    </div>
                                                    <div class="dd3-content">
                                                        <span class="classOutput"><?php echo $data->tpl_page_name; ?></span>
                                                        <span class="pull-right">
                                                            <span class="onoffswitch">
                                                                <input type="checkbox" name="start_interval" class="onoffswitch-checkbox" id="id<?php echo $data->tpl_page_id; ?>" <?php if ($data->tpl_page_status == 1) { ?> checked="checked" <?php } ?>>
                                                                <label class="onoffswitch-label" for="id<?php echo $data->tpl_page_id; ?>">
                                                                    <span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span>
                                                                    <span class="onoffswitch-switch"></span>
                                                                </label>
                                                            </span>
                                                            <span>
                                                                <a href="javascript:void(0);" class="editMenu marginEditButton" rel="tooltip" title="" data-placement="bottom" data-original-title="Edit Title"><i class="fa fa-cog "></i></a>
                                                                <a href="javascript:void(0);" class="deleteMenu" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a>
                                                            </span>
                                                        </span>
                                                        <div class="pageDetails">
                                                            <form class="smart-form">
                                                                <div class="row">

                                                                    <section class="col col-5">
                                                                        <header>
                                                                            Edit Page
                                                                        </header>
                                                                        <section>
                                                                            <label class="label" style="color: #000">Page Name</label>
                                                                            <label class="input">
                                                                                <input type="text" name="pageName" class="classInput" value="<?php echo $data->tpl_page_name; ?>">
                                                                            </label>
                                                                        </section>
                                                                        <section>
                                                                            <label class="toggle">
                                                                                <input type="checkbox" name="setHomePage" <?php if ($data->tpl_page_default == 1) { ?> checked="checked" <?php } ?>>
                                                                                <i data-swchon-text="ON" data-swchoff-text="OFF"></i>Set as homepage
                                                                            </label>
                                                                        </section>
                                                                        <section>
                                                                            <label class="toggle">
                                                                                <input type="checkbox" name="setHidePage" <?php if ($data->tpl_page_display_menu == 0) { ?> checked="checked" <?php } ?>>
                                                                                <i data-swchon-text="ON" data-swchoff-text="OFF"></i>Hide page in navigation
                                                                            </label>
                                                                        </section>


                                                                    </section>

                                                                    <section class="col col-1">
                                                                    </section>

                                                                    <section class="col col-5">
                                                                        <header>
                                                                            Basic SEO
                                                                        </header>
                                                                        <section>
                                                                            <label class="label" style="color: #000">Page Title</label>
                                                                            <label class="input">
                                                                                <input type="text" name="pageTitle" value="<?php echo $data->tpl_page_title; ?>">
                                                                            </label>
                                                                        </section>

                                                                        <section>
                                                                            <label class="label">Description</label>
                                                                            <label class="textarea">
                                                                                <textarea rows="3" name="pageDescription" class="custom-scroll"><?php echo $data->tpl_page_description; ?></textarea>
                                                                            </label>
                                                                        </section>

                                                                        <section>
                                                                            <label class="label" style="color: #000">Keywords</label>
                                                                            <label class="input">
                                                                                <input type="text" name="pageKeywords" value="<?php echo $data->tpl_page_keywords; ?>">
                                                                            </label>
                                                                        </section>
                                                                    </section>
                                                                </div>
                                                                <div class="row">
                                                                    <section class="col col-11 pull-right">
                                                                        <button type="button" class="btn btn-primary savePageChanges">
                                                                            Save changes
                                                                        </button>
                                                                    </section>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    if (count($page['child']) > 0) {
                                                        ?>
                                                        <ol class="dd-list">
                                                            <?php
                                                            foreach ($page['child'] as $data) {
                                                                ?>
                                                                <li class="dd-item dd3-item" data-id="<?php echo $data->tpl_page_id; ?>">
                                                                    <div class="dd-handle dd3-handle">
                                                                        Drag
                                                                    </div>
                                                                    <div class="dd3-content">
                                                                        <span class="classOutput"><?php echo $data->tpl_page_name; ?></span>
                                                                        <span class="pull-right">
                                                                            <span class="onoffswitch">
                                                                                <input type="checkbox" name="start_interval" id="id<?php echo $data->tpl_page_id; ?>" class="onoffswitch-checkbox" <?php if ($data->tpl_page_status == 1) { ?> checked="checked" <?php } ?>>
                                                                                <label class="onoffswitch-label" for="id<?php echo $data->tpl_page_id; ?>">
                                                                                    <span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span>
                                                                                    <span class="onoffswitch-switch"></span>
                                                                                </label>
                                                                            </span>
                                                                            <span>
                                                                                <a href="javascript:void(0);" class="editMenu marginEditButton" rel="tooltip" title="" data-placement="bottom" data-original-title="Edit Title"><i class="fa fa-cog "></i></a>
                                                                                <a href="javascript:void(0);" class="deleteMenu" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a>
                                                                            </span>
                                                                        </span>
                                                                        <div class="pageDetails">
                                                                            <form class="smart-form">
                                                                                <div class="row">

                                                                                    <section class="col col-5">
                                                                                        <header>
                                                                                            Edit Page
                                                                                        </header>
                                                                                        <section>
                                                                                            <label class="label" style="color: #000">Page Name</label>
                                                                                            <label class="input">
                                                                                                <input type="text" name="pageName" class="classInput" value="<?php echo $data->tpl_page_name; ?>">
                                                                                            </label>
                                                                                        </section>
                                                                                        <section>
                                                                                            <label class="toggle">
                                                                                                <input type="checkbox" name="setHomePage" <?php if ($data->tpl_page_default == 1) { ?> checked="checked" <?php } ?>>
                                                                                                <i data-swchon-text="ON" data-swchoff-text="OFF"></i>Set as homepage
                                                                                            </label>
                                                                                        </section>
                                                                                        <section>
                                                                                            <label class="toggle">
                                                                                                <input type="checkbox" name="setHidePage" <?php if ($data->tpl_page_display_menu == 0) { ?> checked="checked" <?php } ?>>
                                                                                                <i data-swchon-text="ON" data-swchoff-text="OFF"></i>Hide page in navigation
                                                                                            </label>
                                                                                        </section>


                                                                                    </section>

                                                                                    <section class="col col-1">
                                                                                    </section>

                                                                                    <section class="col col-5">
                                                                                        <header>
                                                                                            Basic SEO
                                                                                        </header>
                                                                                        <section>
                                                                                            <label class="label" style="color: #000">Page Title</label>
                                                                                            <label class="input">
                                                                                                <input type="text" name="pageTitle" value="<?php echo $data->tpl_page_title; ?>">
                                                                                            </label>
                                                                                        </section>

                                                                                        <section>
                                                                                            <label class="label">Description</label>
                                                                                            <label class="textarea">
                                                                                                <textarea rows="3" name="pageDescription" class="custom-scroll"><?php echo $data->tpl_page_description; ?></textarea>
                                                                                            </label>
                                                                                        </section>

                                                                                        <section>
                                                                                            <label class="label" style="color: #000">Keywords</label>
                                                                                            <label class="input">
                                                                                                <input type="text" name="pageKeywords" value="<?php echo $data->tpl_page_keywords; ?>">
                                                                                            </label>
                                                                                        </section>
                                                                                    </section>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <section class="col col-11 pull-right">
                                                                                        <button type="button" class="btn btn-primary savePageChanges">
                                                                                            Save changes
                                                                                        </button>
                                                                                    </section>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <?php
                                                            }
                                                            ?>
                                                        </ol>
                                                    <?php } ?>
                                                </li>

                                                <?php
                                            }
                                            ?>
                                        </ol>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>