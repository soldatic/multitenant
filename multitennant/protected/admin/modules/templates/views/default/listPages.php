<?php
foreach ($pages as $page) {
    ?>
    <li data-id="<?php echo $page->tpl_page_id; ?>">
        <a data-href="" class="pull-left page pageJS"><?php echo $page->tpl_page_name; ?></a>
        <div class="pages-edit-button">
            <a class=" pull-right"><span class="glyphicon glyphicon-trash page-remove"></span></a>
            <a class=" pull-right"><span class="glyphicon glyphicon-cog page-settings"></span></a>
            <a class=" pull-right"><span class="glyphicon glyphicon-duplicate page-duplicate"></span></a>
            <?php if ($page->tpl_page_default == 1) { ?>
                <span class="glyphicon glyphicon-home home pull-right"></span>
            <?php } ?>
        </div>
    </li>
<?php
}