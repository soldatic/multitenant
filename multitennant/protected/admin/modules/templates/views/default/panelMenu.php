<div id="menu-panel-np">
    <div class="container-fluid">
        <div class="row">
            <div class="panel-name">Edit Menu Item</div>
            <div class="panel-button">
                <button class="btn btn-sm btn-danger" id="menu-np-close"><span class="glyphicon glyphicon-remove"></span>Cancel</button>
                <button class="btn btn-sm btn-primary" id="menu-np-save"><span class="glyphicon glyphicon-ok"></span>Save</button>
            </div>
        </div>
        <div class="row">    
            <div class="panel-body">
                <div class="panel-body-form">
                    <form class="smart-form">
                        <ul>
                            <li>
                                <label class="label">Item Name</label>
                                <label class="input">
                                    <input type="text" name="menuName" id="getValueOfMenuName" class="" value="">
                                </label>
                            </li>
                            <li>
                                <label class="label">Choose Typy Of Menu</label>
                                <label class="select">
                                    <select id="contentType" name="menuTypeMenu">
                                        <option value="0">Choose Type</option>
                                        <option value="content" id="menuContent">Content</option>
                                        <option value="link" id="menuLink">Link</option>      
                                    </select> <i></i> 
                                </label>
                            </li>
                        </ul>
                    </form>
                </div>
                <div id="contenttab"></div>
                <div id="linktab">
                    <h5 class="marginMenuFloat">Choose link type</h5>
                    <form class="smart-form">
                        <ul>
                            <li>
                                <div class="row">
                                    <div class="col-lg-2 marginMenuFloat">
                                        <label class="radio">
                                            <input type="radio" name="menuLinkType" checked="checked" value="url">
                                            <i></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-8">
                                        <label class="label colorWhite">Website URL</label>
                                        <label class="input"> <i class="icon-append fa fa-globe"></i>
                                            <input type="text" name="menuLinkUrl" placeholder="URL" class="urlFromImg"> 
                                        </label>  
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="row">
                                    <div class="col-lg-2 marginMenuFloat">
                                        <label class="radio">
                                            <input type="radio" name="menuLinkType" value="page">
                                            <i></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-8">
                                        <label class="label colorWhite">Site Page</label>
                                        <label class="select">
                                            <select name="pageList" class="linkToSitePage">

                                            </select> <i></i> 
                                        </label>
                                    </div>
                                </div>

                            </li>

<!--                            <li>
                                <div class="row marginButom">
                                    <div class="col-lg-2 marginMenuFloat">
                                        <label class="radio">
                                            <input type="radio" name="radio">
                                            <i></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-8">
                                        <label class="label colorWhite">Anchor to</label>
                                        <label class="select"><select name="siteList" class="linkToSitePage">
                                                <option value="0" selected="selected">Anchor Page</option>
                                                <option value="/index.html">Anchor Home</option>
                                                <option value="/about.html">Anchor About</option>
                                                <option value="/post.html">Anchor Sample Post</option>
                                                <option value="/contact.html">Anchor Contacts</option>
                                            </select> <i></i> 
                                        </label>
                                    </div>
                                </div>
                            </li>-->
                        </ul>
                    </form>

                </div>
                <div>
                    <form class="smart-form">
                        <ul>
                            <li>                                                                        
                                <label class="label marginMenuFloat colorWhite">Add Image To Menu List</label>
                                <div class="shortcuts">
                                    <a href="#">
                                        <div class="left menu-left-image" data-position="left">
                                            <span class="sideText">
                                                Left
                                            </span>
                                            <span class="sidePictogram">
                                                <i class="fa fa-file-image-o"></i>
                                                <i class="fa fa-align-left"></i>
                                            </span>
                                        </div>
                                    </a>
                                    <a  href="#">
                                        <div class="top menu-top-image" data-position="top">
                                            <span class="sideText">
                                                Top
                                            </span>
                                            <span class="heightPictogram3">
                                                <i class="fa fa-file-image-o"></i>
                                            </span>
                                            <span class="heightPictogram4">                                                                                        
                                                <i class="fa fa-align-left"></i>
                                            </span>
                                        </div>
                                    </a>
                                    <a  href="#">
                                        <div class="insertImgInMenu">
                                            Click to select picture
                                        </div>
                                    </a>
                                    <a  href="#">
                                        <div class="right menu-right-image" data-position="right">
                                            <span class="sideText">
                                                Right
                                            </span>
                                            <span class="sidePictogram">
                                                <i class="fa fa-align-left"></i>
                                                <i class="fa fa-file-image-o"></i>
                                            </span>
                                        </div>
                                    </a>
                                    <a  href="#">
                                        <div class="bottom menu-bottom-image" data-position="bottom">
                                            <span class="sideText">
                                                Bottom
                                            </span>
                                            <span class="heightPictogram">
                                                <i class="fa fa-align-left"></i>
                                            </span>
                                            <span class="heightPictogram2">
                                                <i class="fa fa-file-image-o"></i>
                                            </span>
                                        </div>
                                    </a>
                                </div>
                            </li>
                        </ul>

                    </form>
                </div>
                <div>
                    <form class="smart-form">
                        <ul >
                            <li>                                                                        
                                <!--<label class="checkbox"><input type="checkbox" name="subscription" id="homePageSet"><i></i>Set as homepage</label>-->    
                                <label class="checkbox colorWhite"><input type="checkbox" name="subscription" id="separateWindow"><i></i>Open in separate window</label>    
                                <label class="checkbox colorWhite"><input type="checkbox" name="subscription" id="dontShowInMenu"><i></i>Do not show this item in menu</label>    
                            </li>
                        </ul>

                    </form>
                </div>

            </div>
        </div>
    </div>
   
</div>
