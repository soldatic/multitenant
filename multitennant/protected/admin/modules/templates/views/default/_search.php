<?php
/* @var $this TemplatesController */
/* @var $model Templates */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <div class="row">
<?php echo $form->label($model, 'tpl_id'); ?>
<?php echo $form->textField($model, 'tpl_id'); ?>
    </div>

    <div class="row">
<?php echo $form->label($model, 'tpl_name'); ?>
<?php echo $form->textField($model, 'tpl_name', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
<?php echo $form->label($model, 'tpl_description'); ?>
<?php echo $form->textField($model, 'tpl_description'); ?>
    </div>

    <div class="row">
<?php echo $form->label($model, 'tpl_html'); ?>
<?php echo $form->textArea($model, 'tpl_html', array('rows' => 6, 'cols' => 50)); ?>
    </div>

    <div class="row">
<?php echo $form->label($model, 'tpl_blocks'); ?>
<?php echo $form->textArea($model, 'tpl_blocks', array('rows' => 6, 'cols' => 50)); ?>
    </div>

    <div class="row">
<?php echo $form->label($model, 'tpl_status'); ?>
<?php echo $form->textField($model, 'tpl_status', array('size' => 1, 'maxlength' => 1)); ?>
    </div>

    <div class="row buttons">
    <?php echo CHtml::submitButton('Search'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->