<div class="modal fade" id="menuModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ChangeImageOnHeaderModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Menu</h4>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-pages" data-widget-load="ajax/demowidget.php" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" role="widget">

                    <header role="heading"><div class="jarviswidget-ctrls" role="menu"></div>
                        <h2><strong>Edit Menu</strong> <i>Widget</i></h2>

                    </header>

                    <!-- widget div-->
                    <div role="content">

                        <!-- widget content -->
                        <div class="widget-body">

                            <!-- widget body text-->

                            <div class="row hidden-mobile">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <h1 class="page-title txt-color-blueDark">
                                        <i class="fa-fw fa fa-copy"></i>
                                        Edit Menu </h1>
                                </div>

                            </div>


                            <div class="jarviswidget jarviswidget-sortable" id="wid-id-9" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" role="widget">
                                <header role="heading">
                                    <ul id="widget-tab-1" class="nav nav-tabs pull-left">
                                        <li class="active">
                                            <a data-toggle="tab" href="#hr20"> <i class="fa fa-lg fa-arrow-circle-o-down"></i> <span class="hidden-mobile hidden-tablet"> Menu </span> </a>
                                        </li>
                                        <li class="">
                                            <a data-toggle="tab" href="#hr21"> <i class="fa fa-lg fa-arrow-circle-o-down"></i> <span class="hidden-mobile hidden-tablet"> Pages </span> </a>
                                        </li>

                                        <li class="">
                                            <a data-toggle="tab" href="#hr22"> <i class="fa fa-lg fa-arrow-circle-o-up"></i> <span class="hidden-mobile hidden-tablet"> CSS of Menu </span></a>
                                        </li>
                                    </ul>	
                                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                                </header>
                                <div role="content" style="width: 100%">
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                                    </div>
                                    <!-- end widget edit box -->
                                    <!-- widget content -->
                                    <div class="widget-body no-padding" style="margin : 0">
                                        <!-- widget body text-->
                                        <div class="tab-content ">
                                            <div class="tab-pane fade active in" id="hr20">

                                                <div class="container-fluid">
                                                    <div class="row" >
                                                        <div class="col-sm-6 col-lg-4">

                                                            <h6>Menu List</h6>

                                                            <div class="dd" id="nestable3">
                                                                <ol class="dd-list">
                                                                    <li class="dd-item dd3-item" data-id="131" id="menuItem131">
                                                                        <div class="dd-handle dd3-handle">
                                                                            Drag
                                                                        </div>
                                                                        <div class="dd3-content">
                                                                            <span class="changeTextInMenu"> Home </span>
                                                                            <span class="pull-right"> 

                                                                                <span class="onoffswitch">
                                                                                    <input type="checkbox" name="start_interval" class="onoffswitch-checkbox" id="start_interval">
                                                                                    <label class="onoffswitch-label" for="start_interval"> 
                                                                                        <span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span> 
                                                                                        <span class="onoffswitch-switch"></span>
                                                                                    </label> 
                                                                                </span> 
                                                                                <span>                                                                                    
                                                                                    <a href="javascript:void(0);" class="deleteMenu" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a>
                                                                                </span>
                                                                            </span>



                                                                        </div>
                                                                    </li>

                                                                    <li class="dd-item dd3-item" data-id="141" id="menuItem141">
                                                                        <div class="dd-handle dd3-handle">
                                                                            Drag
                                                                        </div>
                                                                        <div class="dd3-content">
                                                                            <span class="changeTextInMenu"> About </span>
                                                                            <span class="pull-right"> 

                                                                                <span class="onoffswitch">
                                                                                    <input type="checkbox" name="start_interval" class="onoffswitch-checkbox" id="start_interval1">
                                                                                    <label class="onoffswitch-label" for="start_interval1"> 
                                                                                        <span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span> 
                                                                                        <span class="onoffswitch-switch"></span>
                                                                                    </label> 
                                                                                </span> 
                                                                                <span>                                                                                    
                                                                                    <a href="javascript:void(0);" class="deleteMenu" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a>
                                                                                </span>
                                                                            </span>
                                                                        </div>
                                                                    </li>
                                                                    <li class="dd-item dd3-item" data-id="142" id="menuItem142">
                                                                        <div class="dd-handle dd3-handle">
                                                                            Drag
                                                                        </div>
                                                                        <div class="dd3-content">
                                                                            <span class="changeTextInMenu"> Contacts </span>
                                                                            <span class="pull-right"> 

                                                                                <span class="onoffswitch">
                                                                                    <input type="checkbox" name="start_interval" class="onoffswitch-checkbox" id="start_interval2">
                                                                                    <label class="onoffswitch-label" for="start_interval2"> 
                                                                                        <span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span> 
                                                                                        <span class="onoffswitch-switch"></span>
                                                                                    </label> 
                                                                                </span> 
                                                                                <span>                                                                                    
                                                                                    <a href="javascript:void(0);" class="deleteMenu" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a>
                                                                                </span>
                                                                            </span>
                                                                        </div>
                                                                    </li>


                                                                </ol>
                                                            </div>
                                                            <br>
                                                            <div class="page-title">
                                                                <button type="button" class="btn btn-default" id="addNewMenuItem">
                                                                    Add new page
                                                                </button>
                                                            </div>

                                                        </div>

                                                        <div class="">
                                                            <form class="smart-form">                       
                                                                <fieldset style="padding: 25px 0px 5px;">
                                                                    <section>
                                                                        <label class="label">Item Name</label>
                                                                        <label class="input">
                                                                            <input type="text" name="Home" placeholder="" id="getValueOfMenuName"><b class="tooltip tooltip-bottom-right">Title of item</b> 
                                                                        </label>
                                                                    </section>
                                                                    <br>
                                                                    <section>

                                                                        <div class="inline-group">
                                                                            <label class="radio">
                                                                                <input type="radio" name="radio-inline" checked="checked" id="menuContent">
                                                                                <i></i>Content
                                                                            </label>
                                                                            <label class="radio">
                                                                                <input type="radio" name="radio-inline" id="menuLink">
                                                                                <i></i>Link
                                                                            </label>

                                                                        </div>
                                                                    </section>
                                                                </fieldset>
                                                            </form>
                                                            <div id="contenttab"></div>
                                                            <div id="linktab">
                                                                <div class="jarviswidget jarviswidget-sortable" id="wid-id-21"  data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" role="widget">
                                                                    <header role="heading">
                                                                        <h2><strong>Choose Link</strong> <i>Type</i></h2>
                                                                        <div class="widget-toolbar" role="menu"></div>
                                                                    </header>
                                                                    <div role="content">
                                                                        <div class="jarviswidget-editbox"></div>
                                                                        <div class="widget-body">
                                                                            <form class="smart-form">
                                                                                <fieldset>
                                                                                    <section>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-2">
                                                                                                <label class="radio">
                                                                                                    <input type="radio" name="radio" checked="checked">
                                                                                                    <i></i>
                                                                                                </label>
                                                                                            </div>
                                                                                            <div class="col-lg-10">
                                                                                                <label class="label">Website URL</label>
                                                                                                <label class="input"> <i class="icon-append fa fa-globe"></i>
                                                                                                    <input type="text" name="url" placeholder="URL" class="urlFromImg"><b class="tooltip tooltip-bottom-right">Needed to enter the website</b> 
                                                                                                </label>  
                                                                                            </div>
                                                                                        </div>



                                                                                    </section>

                                                                                    <section>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-2">
                                                                                                <label class="radio">
                                                                                                    <input type="radio" name="radio">
                                                                                                    <i></i>
                                                                                                </label>
                                                                                            </div>
                                                                                            <div class="col-lg-10">
                                                                                                <label class="label">Site Page</label>
                                                                                                <label class="select"><select name="siteList" class="linkToSitePage">
                                                                                                        <option value="0" selected="selected">Choose Page</option>
                                                                                                        <option value="/index.html">Home</option>
                                                                                                        <option value="/about.html">About</option>
                                                                                                        <option value="/post.html">Sample Post</option>
                                                                                                        <option value="/contact.html">Contacts</option>
                                                                                                    </select> <i></i> 
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>

                                                                                    </section>

                                                                                    <section>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-2">
                                                                                                <label class="radio">
                                                                                                    <input type="radio" name="radio">
                                                                                                    <i></i>
                                                                                                </label>
                                                                                            </div>
                                                                                            <div class="col-lg-10">
                                                                                                <label class="label">Anchor to</label>
                                                                                                <label class="select"><select name="siteList" class="linkToSitePage">
                                                                                                        <option value="0" selected="selected">Anchor Page</option>
                                                                                                        <option value="/index.html">Anchor Home</option>
                                                                                                        <option value="/about.html">Anchor About</option>
                                                                                                        <option value="/post.html">Anchor Sample Post</option>
                                                                                                        <option value="/contact.html">Anchor Contacts</option>
                                                                                                    </select> <i></i> 
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </section>
                                                                                </fieldset>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <form class="smart-form">
                                                                <fieldset style="padding: 25px 0px 5px;">
                                                                    <section>                                                                        
                                                                        <label class="label">Add Image To Menu List</label>
                                                                        <div class="shortcuts">
                                                                            <a  href="#">
                                                                                <div class="left">
                                                                                    <span class="sideText">
                                                                                        Left
                                                                                    </span>
                                                                                    <span class="sidePictogram">
                                                                                        <i class="fa fa-file-image-o"></i>
                                                                                        <i class="fa fa-align-left"></i>
                                                                                    </span>
                                                                                </div>
                                                                            </a>
                                                                            <a  href="#">
                                                                                <div class="top">
                                                                                    <span class="sideText">
                                                                                        Top
                                                                                    </span>
                                                                                    <span class="heightPictogram3">
                                                                                        <i class="fa fa-file-image-o"></i>
                                                                                    </span>
                                                                                    <span class="heightPictogram4">                                                                                        
                                                                                        <i class="fa fa-align-left"></i>
                                                                                    </span>
                                                                                </div>
                                                                            </a>
                                                                            <a  href="#">
                                                                                <div class="insertImgInMenu">
                                                                                    Click to select picture
                                                                                </div>
                                                                            </a>
                                                                            <a  href="#">
                                                                                <div class="right">
                                                                                    <span class="sideText">
                                                                                        Right
                                                                                    </span>
                                                                                    <span class="sidePictogram">
                                                                                        <i class="fa fa-align-left"></i>
                                                                                        <i class="fa fa-file-image-o"></i>
                                                                                    </span>
                                                                                </div>
                                                                            </a>
                                                                            <a  href="#">
                                                                                <div class="bottom">
                                                                                    <span class="sideText">
                                                                                        Bottom
                                                                                    </span>
                                                                                    <span class="heightPictogram">
                                                                                        <i class="fa fa-align-left"></i>
                                                                                    </span>
                                                                                    <span class="heightPictogram2">
                                                                                        <i class="fa fa-file-image-o"></i>
                                                                                    </span>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    </section>
                                                                </fieldset>

                                                            </form>

                                                            <form class="smart-form">
                                                                <fieldset style="padding: 25px 0px 5px;">
                                                                    <section>                                                                        
                                                                        <label class="checkbox"><input type="checkbox" name="subscription" id="homePageSet"><i></i>Set as homepage</label>    
                                                                        <label class="checkbox"><input type="checkbox" name="subscription" id="separateWindow"><i></i>Open link in a separate window</label>    
                                                                        <label class="checkbox"><input type="checkbox" name="subscription" id="dontShowInMenu"><i></i>Do not show this item in menu</label>    
                                                                    </section>
                                                                </fieldset>

                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>


                                            </div>



                                            <div class="tab-pane fade" id="hr21">

                                                <div class="container-fluid">
                                                    <div class="row" >
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                                            <div class="dd" id="nestable4" style="max-width: 100%">
                                                                <ol class="dd-list dd-main">
                                                                    <?php
                                                                    foreach ($pages as $page) {
                                                                        $data = $page['parent'];
                                                                        ?>
                                                                        <li class="dd-item dd3-item" data-id="<?php echo $data->tpl_page_id; ?>">
                                                                            <div class="dd-handle dd3-handle">
                                                                                Drag
                                                                            </div>
                                                                            <div class="dd3-content">
                                                                                <span class="classOutput"><?php echo $data->tpl_page_name; ?></span>
                                                                                <span class="pull-right">
                                                                                    <span class="onoffswitch">
                                                                                        <input type="checkbox" name="start_interval" class="onoffswitch-checkbox" id="id<?php echo $data->tpl_page_id; ?>" <?php if ($data->tpl_page_status == 1) { ?> checked="checked" <?php } ?>>
                                                                                        <label class="onoffswitch-label" for="id<?php echo $data->tpl_page_id; ?>">
                                                                                            <span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span>
                                                                                            <span class="onoffswitch-switch"></span>
                                                                                        </label>
                                                                                    </span>
                                                                                    <span>
                                                                                        <a href="javascript:void(0);" class="editMenu marginEditButton" rel="tooltip" title="" data-placement="bottom" data-original-title="Edit Title"><i class="fa fa-cog "></i></a>
                                                                                        <a href="javascript:void(0);" class="deleteMenu" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a>
                                                                                    </span>
                                                                                </span>
                                                                                <div class="pageDetails">
                                                                                    <form class="smart-form">
                                                                                        <div class="row">     
                                                                                            <header>
                                                                                                Basic SEO
                                                                                            </header>

                                                                                            <section class="col col-5">
                                                                                                <br>
                                                                                                <section>
                                                                                                    <label class="label" style="color: #000">Page Name</label>
                                                                                                    <label class="input">
                                                                                                        <input type="text" name="pageName" class="classInput" value="<?php echo $data->tpl_page_name; ?>">
                                                                                                    </label>
                                                                                                </section>

                                                                                                <section>
                                                                                                    <label class="label">Description</label>
                                                                                                    <label class="textarea">
                                                                                                        <textarea rows="3" name="pageDescription" class="custom-scroll"><?php echo $data->tpl_page_description; ?></textarea>
                                                                                                    </label>
                                                                                                </section>


                                                                                            </section>

                                                                                            <section class="col col-1">
                                                                                            </section>

                                                                                            <section class="col col-5">
                                                                                                <br>
                                                                                                <section>
                                                                                                    <label class="label" style="color: #000">Page Title</label>
                                                                                                    <label class="input">
                                                                                                        <input type="text" name="pageTitle" value="<?php echo $data->tpl_page_title; ?>">
                                                                                                    </label>
                                                                                                </section>



                                                                                                <section>
                                                                                                    <label class="label" style="color: #000">Keywords</label>
                                                                                                    <label class="input">
                                                                                                        <input type="text" name="pageKeywords" value="<?php echo $data->tpl_page_keywords; ?>">
                                                                                                    </label>
                                                                                                </section>
                                                                                            </section>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <section class="col col-11 pull-right">
                                                                                                <button type="button" class="btn btn-primary savePageChanges">
                                                                                                    Save changes
                                                                                                </button>
                                                                                            </section>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            if (count($page['child']) > 0) {
                                                                                ?>
                                                                                <ol class="dd-list">
                                                                                    <?php
                                                                                    foreach ($page['child'] as $data) {
                                                                                        ?>
                                                                                        <li class="dd-item dd3-item" data-id="<?php echo $data->tpl_page_id; ?>">
                                                                                            <div class="dd-handle dd3-handle">
                                                                                                Drag
                                                                                            </div>
                                                                                            <div class="dd3-content">
                                                                                                <span class="classOutput"><?php echo $data->tpl_page_name; ?></span>
                                                                                                <span class="pull-right">
                                                                                                    <span class="onoffswitch">
                                                                                                        <input type="checkbox" name="start_interval" id="id<?php echo $data->tpl_page_id; ?>" class="onoffswitch-checkbox" <?php if ($data->tpl_page_status == 1) { ?> checked="checked" <?php } ?>>
                                                                                                        <label class="onoffswitch-label" for="id<?php echo $data->tpl_page_id; ?>">
                                                                                                            <span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span>
                                                                                                            <span class="onoffswitch-switch"></span>
                                                                                                        </label>
                                                                                                    </span>
                                                                                                    <span>
                                                                                                        <a href="javascript:void(0);" class="editMenu marginEditButton" rel="tooltip" title="" data-placement="bottom" data-original-title="Edit Title"><i class="fa fa-cog "></i></a>
                                                                                                        <a href="javascript:void(0);" class="deleteMenu" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a>
                                                                                                    </span>
                                                                                                </span>
                                                                                                <div class="pageDetails">
                                                                                                    <form class="smart-form">
                                                                                                        <div class="row">

                                                                                                            <section class="col col-5">
                                                                                                                <header>
                                                                                                                    Edit Page
                                                                                                                </header>
                                                                                                                <section>
                                                                                                                    <label class="label" style="color: #000">Page Name</label>
                                                                                                                    <label class="input">
                                                                                                                        <input type="text" name="pageName" class="classInput" value="<?php echo $data->tpl_page_name; ?>">
                                                                                                                    </label>
                                                                                                                </section>
                                                                                                                <section>
                                                                                                                    <label class="toggle">
                                                                                                                        <input type="checkbox" name="setHomePage" <?php if ($data->tpl_page_default == 1) { ?> checked="checked" <?php } ?>>
                                                                                                                        <i data-swchon-text="ON" data-swchoff-text="OFF"></i>Set as homepage
                                                                                                                    </label>
                                                                                                                </section>
                                                                                                                <section>
                                                                                                                    <label class="toggle">
                                                                                                                        <input type="checkbox" name="setHidePage" <?php if ($data->tpl_page_display_menu == 0) { ?> checked="checked" <?php } ?>>
                                                                                                                        <i data-swchon-text="ON" data-swchoff-text="OFF"></i>Hide page in navigation
                                                                                                                    </label>
                                                                                                                </section>


                                                                                                            </section>

                                                                                                            <section class="col col-1">
                                                                                                            </section>

                                                                                                            <section class="col col-5">
                                                                                                                <header>
                                                                                                                    Basic SEO
                                                                                                                </header>
                                                                                                                <section>
                                                                                                                    <label class="label" style="color: #000">Page Title</label>
                                                                                                                    <label class="input">
                                                                                                                        <input type="text" name="pageTitle" value="<?php echo $data->tpl_page_title; ?>">
                                                                                                                    </label>
                                                                                                                </section>

                                                                                                                <section>
                                                                                                                    <label class="label">Description</label>
                                                                                                                    <label class="textarea">
                                                                                                                        <textarea rows="3" name="pageDescription" class="custom-scroll"><?php echo $data->tpl_page_description; ?></textarea>
                                                                                                                    </label>
                                                                                                                </section>

                                                                                                                <section>
                                                                                                                    <label class="label" style="color: #000">Keywords</label>
                                                                                                                    <label class="input">
                                                                                                                        <input type="text" name="pageKeywords" value="<?php echo $data->tpl_page_keywords; ?>">
                                                                                                                    </label>
                                                                                                                </section>
                                                                                                            </section>
                                                                                                        </div>
                                                                                                        <div class="row">
                                                                                                            <section class="col col-11 pull-right">
                                                                                                                <button type="button" class="btn btn-primary savePageChanges">
                                                                                                                    Save changes
                                                                                                                </button>
                                                                                                            </section>
                                                                                                        </div>
                                                                                                    </form>
                                                                                                </div>
                                                                                            </div>
                                                                                        </li>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </ol>
                                                                            <?php } ?>
                                                                        </li>

                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </ol>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="page-title">
                                                        <button type="button" class="btn btn-default" id="addNewListItem">
                                                            Add new page
                                                        </button>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">

                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="hr22">
                                                <br>
                                                <div class="topic"><h2>Menu CSS Style</h2></div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="input-group"><span class="input-group-addon">Text Color</span><input class="form-control color {hash:true}" id="popt-color" value="#000000" type="text" onchange="updateDisplayPopUp()" autocomplete="off" style="color: rgb(255, 255, 255); background-image: none; background-color: rgb(0, 0, 0);"></div>
                                                    </div>                            
                                                    <div class="col-md-2">
                                                        <div class="input-group"><span class="input-group-addon">Text Color</span><input class="form-control color {hash:true}" id="popHoveredT-color" value="#000000" type="text" onchange="updateDisplayPopUp()" autocomplete="off" style="color: rgb(255, 255, 255); background-image: none; background-color: rgb(0, 0, 0);"></div>
                                                    </div>                            

                                                    <div class="col-md-2">
                                                        <p>
                                                            <label for="popfont-name">Font: </label>
                                                            <select class="form-control" onchange="updateDisplayPopUp()" id="popfont-name">
                                                                <option value=""></option>
                                                                <option value="arial">Arial</option>
                                                                <option value="'arial black'">Arial Black</option>
                                                                <option value="'comic sans ms'">Comic Sans MS</option>
                                                                <option value="'courier new'">Courier New</option>
                                                                <option value="georgia">Georgia</option>
                                                                <option value="helvetica">Helvetica</option>
                                                                <option value="impact">Impact</option>
                                                                <option value="'times new roman'">Times New Roman</option>
                                                                <option value="'trebuchet ms'">Trebuchet MS</option>
                                                                <option value="verdana">Verdana</option>
                                                            </select>
                                                        </p>
                                                    </div>   
                                                    <div class="col-md-2">
                                                        <p>
                                                            <label for="popfont-style">Font Style: </label>
                                                            <select class="form-control" onchange="updateDisplayPopUp()" id="popfont-style">
                                                                <option value=""></option>
                                                                <option value="normal">Normal</option>
                                                                <option value="italic">Italic</option>
                                                                <option value="oblique">Oblique</option>
                                                            </select>
                                                        </p>
                                                    </div>   
                                                    <div class="col-md-2">
                                                        <p>
                                                            <label for="popfont-weight">Font Weight: </label>
                                                            <select class="form-control" onchange="updateDisplayPopUp()" id="popfont-weight">
                                                                <option value=""></option>
                                                                <option value="normal">Normal</option>
                                                                <option value="bold">Bold</option>
                                                                <option value="bolder">Bolder</option>
                                                                <option value="lighter">Lighter</option>
                                                            </select>
                                                        </p>
                                                    </div>   
                                                    <div class="col-md-2">
                                                        <p>
                                                            <label for="popfont-size">Font Size:</label>
                                                            <strong class="text-info"><span id="popfont-size">0</span>px</strong>
                                                        </p>
                                                        <div id="popslider_font-size" class="slider-pop fontsize ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 0%;"></div><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a></div>

                                                    </div>   
                                                    <div class="col-md-2">
                                                        <p>
                                                            <label for="popfont-variant">Font Variant: </label>
                                                            <select class="form-control" onchange="updateDisplayPopUp()" id="popfont-variant">
                                                                <option value=""></option>
                                                                <option value="normal">Normal</option>
                                                                <option value="small-caps">small-caps</option>
                                                            </select>
                                                        </p>
                                                    </div>  
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <p>
                                                                <label for="popline-height">Line Height:</label>
                                                                <strong class="text-info"><span id="popline-height">0</span>px</strong>
                                                            </p>
                                                            <div id="popslider_line-height" class="slider-pop padding ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 0%;"></div><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a></div>

                                                        </div>   
                                                        <div class="col-md-2">
                                                            <p>
                                                                <label for="poptext-align">Text-Align: </label>
                                                                <select class="form-control" onchange="updateDisplayPopUp()" id="poptext-align">
                                                                    <option value=""></option>
                                                                    <option value="left">left</option>
                                                                    <option value="right">right</option>
                                                                    <option value="center">center</option>
                                                                    <option value="justify">justify</option>
                                                                </select>
                                                            </p>
                                                        </div>   
                                                        <div class="col-md-2">
                                                            <p>
                                                                <label for="poptext-decoration">Text-Decoration: </label>
                                                                <select class="form-control" onchange="updateDisplayPopUp()" id="poptext-decoration">
                                                                    <option value=""></option>
                                                                    <option value="none">none</option>
                                                                    <option value="underline">underline</option>
                                                                    <option value="overline">overline</option>
                                                                    <option value="underline overline">underline overline</option>
                                                                    <option value="line-through">line through</option>
                                                                    <option value="blink">blink</option>
                                                                </select>
                                                            </p> 
                                                        </div>   
                                                        <div class="col-md-2">
                                                            <p>
                                                                <label for="poptext-indent">Text-Indent:</label>
                                                                <strong class="text-info"><span id="poptext-indent">0</span>px</strong>
                                                            </p>
                                                            <div id="popslider_text-indent" class="slider-pop fontsize ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 0%;"></div><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a></div>

                                                        </div>   
                                                        <div class="col-md-2">
                                                            <p>
                                                                <label for="popletter-spacing">Letter-Spacing:</label>
                                                                <strong class="text-info"><span id="popletter-spacing">0</span>px</strong>
                                                            </p>
                                                            <div id="popslider_letter-spacing" class="slider-pop border ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 0%;"></div><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a></div>

                                                        </div>   
                                                        <div class="col-md-2">
                                                            <p>
                                                                <label for="popword-spacing">Word-Spacing:</label>
                                                                <strong class="text-info"><span id="popword-spacing">0</span>px</strong>
                                                            </p>
                                                            <div id="popslider_word-spacing" class="slider-pop border ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 0%;"></div><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a></div>

                                                        </div>   
                                                        <div class="col-md-2">
                                                            <p>
                                                                <label for="poptext-transform">Text-Transform: </label>
                                                                <select class="form-control" onchange="updateDisplayPopUp()" id="poptext-transform">
                                                                    <option value=""></option>
                                                                    <option value="none">none</option>
                                                                    <option value="capitalize">capitalize</option>
                                                                    <option value="lowercase">lowercase</option>
                                                                    <option value="uppercase">uppercase</option>
                                                                </select>
                                                            </p>
                                                        </div>   
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- end widget body text-->
                                    </div>
                                    <!-- end widget content -->
                                </div>
                            </div>



                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>