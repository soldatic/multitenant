<?php
/* @var $this TemplatesController */
/* @var $model Templates */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'templates-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>


    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'tpl_name'); ?>
        <?php echo $form->textField($model, 'tpl_name', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'tpl_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'tpl_description'); ?>
        <?php echo $form->textField($model, 'tpl_description', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'tpl_description'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'tpl_html'); ?>
        <?php $this->widget('application.extensions.ckeditor.CKEditor', array('model' => $model, 'attribute' => 'tpl_html'));
        ?>
        <?php echo $form->error($model, 'tpl_html'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'tpl_blocks'); ?>
        <?php echo $form->textArea($model, 'tpl_blocks', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'tpl_blocks'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'tpl_status'); ?>
        <?php echo $form->textField($model, 'tpl_status', array('size' => 1, 'maxlength' => 1)); ?>
        <?php echo $form->error($model, 'tpl_status'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->