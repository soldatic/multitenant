<div id="pages-panel" class="panel">
    <div class="panel-header">
        <div class="panel-name">
            <span>Pages</span>
            <span class="glyphicon glyphicon-remove panel-close"></span>
        </div>
        <div class="panel-button">
            <button class="btn btn-sm btn-primary" id="page-add"><span class="glyphicon glyphicon-plus"></span>Add new page</button>
        </div>
    </div>
    <div class="panel-body">
        <nav>
            <ul>
                <?php echo $listPages; ?>
            </ul>
        </nav>
    </div>
</div>
<div id="pages-panel-np">
    <div class="panel-header">
        <div class="panel-name">
            <span>Add new page</span>
        </div>
        <div class="panel-button">
            <button class="btn btn-sm btn-danger" id="page-np-close"><span class="glyphicon glyphicon-remove"></span>Cancel</button>
            <button class="btn btn-sm btn-primary" id="page-np-save"><span class="glyphicon glyphicon-ok"></span>Save</button>
        </div>
    </div>
    <div class="panel-body default-skin">
        <div class="panel-body-form">
            <form class="smart-form">
                <ul>
                    <li class="label-block">Main</li>
                    <li>
                        <label class="label">Page Name</label>
                        <label class="input">
                            <input type="text" name="pageName" class="classInput" value="" />
                        </label>
                    </li>
                    <li>
                        <label class="toggle">
                            <input type="checkbox" name="setHomePage" >
                            <i data-swchon-text="ON" data-swchoff-text="OFF"></i>Set as homepage
                        </label>
                    </li>
                    <li class="label-block">SEO Settings</li>
                    <li>
                        <label class="label">Page Title</label>
                        <label class="input">
                            <input type="text" name="pageTitle" value="" />
                        </label>
                    </li>
                    <li>
                        <label class="label">Description</label>
                        <label class="textarea">
                            <textarea rows="3" name="pageDescription" class="custom-scroll" style="height: 100px;"></textarea>
                        </label>
                    </li>
                    <li>
                        <label class="label">Keywords</label>
                        <label class="input">
                            <input type="text" name="pageKeywords" value="" />
                        </label>
                    </li>
                    <li class="label-block">Open Graph Settings<br />
                        <small>
                            The information that is presented when sharing content on Facebook, Twitter, and LinkedIn, Pinterest, and Google+.
                        </small>
                    </li>
                    <li>
                        <label class="label">OG Title:</label>
                        <label class="input">
                            <input type="text" name="pageOGTitle" value="" />
                        </label>
                    </li>
                    <li>
                        <label class="label">OG Description</label>
                        <label class="textarea">
                            <textarea rows="3" name="pageOGDescription" class="custom-scroll" style="height: 100px;"></textarea>
                        </label>
                    </li>
                    <li>
                        <label class="label">OG Image URL:</label>
                        <label class="input">
                            <input type="text" name="pageOGImage" value="" />
                        </label>
                    </li>
                    <li class="label-block">Custom Code<br />
                        <small>
                            Custom code and scripts will only appear on the published site. The code included here will only apply to this page, and will be appear after any site-wide custom code.
                        </small>
                    </li>
                    <li>
                        <label class="label"><span>Inside &lt;head&gt; tag:</span></label>
                        <label class="textarea">
                            <textarea rows="3" name="pageHeadCode" class="custom-scroll" style="height: 100px;"></textarea>
                        </label>
                    </li>
                    <li>
                        <label class="label">Before &lt;/body&gt; tag:</label>
                        <label class="textarea">
                            <textarea rows="3" name="pageBodyCode" class="custom-scroll" style="height: 100px;"></textarea>
                        </label>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</div>