<div class="buttons-block out">\n\
    <div class="btn-group">\n\
        <div class="btn btn-primary row-item-move" aria-label="Move">\n\
            <span class="glyphicon glyphicon-move" aria-hidden="true"></span>\n\
        </div>\n\
        <div class="dropdown">\n\
            <button  data-toggle="dropdown" class="dropdown-toggle btn btn-primary" type="button" aria-label="Duplicate">\n\
                <span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span>\n\
            </button>\n\
            <ul class="dropdown-menu">\n\
                <li><a class="row-item-duplicate" data-edit="up">Duplicate up</a></li>\n\
                <li><a class="row-item-duplicate" data-edit="down">Duplicate down</a></li>\n\
            </ul>\n\
        </div>\n\
        <button type="button" class="btn btn-primary row-item-remove" aria-label="Remove">\n\
            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>\n\
        </button>\n\
    </div>\n\
</div>