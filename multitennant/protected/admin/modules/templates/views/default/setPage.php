<li class="dd-item" data-id="<?php echo $pageId; ?>">
    <div class="dd-handle dd3-handle">
        Drag
        </div>
    <div class="dd3-content">
         <span class="classOutput">New Page</span>
         <span class="pull-right">
             <span class="onoffswitch">
                 <input type="checkbox" name="start_interval" id="id<?php echo $pageId; ?>" class="onoffswitch-checkbox">
                 <label class="onoffswitch-label" for="id<?php echo $pageId; ?>">
                     <span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span>
                     <span class="onoffswitch-switch"></span>
                     </label>
                 </span>
             <span>
                 <a href="javascript:void(0);" class="editMenu marginEditButton" rel="tooltip" title="" data-placement="bottom" data-original-title="Edit Title"><i class="fa fa-cog "></i></a>
                 <a href="javascript:void(0);" class="deleteMenu" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a>
                 </span>
             </span>
         <div class="pageDetails">
             <form class="smart-form">
                 <div class="row">
                     <section class="col col-5">
                         <header>
                             Edit Page
                             </header>
                         <section>
                             <label class="label" style="color: #000">Page Name</label>
                             <label class="input">
                                 <input type="text" name="pageName" maxlength="10" class="classInput" value="New Page">
                                 </label>
                             </section>
                         <section>
                             <label class="toggle">
                                 <input type="checkbox" name="setHomePage">
                                 <i data-swchon-text="ON" data-swchoff-text="OFF"></i>Set as homepage
                                 </label>
                             </section>
                         <section>
                             <label class="toggle">
                                 <input type="checkbox" name="setHidePage">
                                 <i data-swchon-text="ON" data-swchoff-text="OFF"></i>Hide page in navigation
                                 </label>
                             </section>
                         </section>
                     <section class="col col-1">
                         </section>
                     <section class="col col-5">
                         <header>
                             Basic SEO
                             </header>
                         <section>
                             <label class="label" style="color: #000">Page Title</label>
                             <label class="input">
                                 <input type="text" name="pageTitle" value="New Page">
                                 </label>
                             </section>
                         <section>
                             <label class="label">Description</label>
                             <label class="textarea">
                                 <textarea rows="3" name="pageDescription" class="custom-scroll"></textarea>
                                 </label>
                             </section>
                         <section>
                             <label class="label" style="color: #000">Keywords</label>
                             <label class="input">
                                 <input type="text" name="pageKeywords" value="">
                                 </label>
                             </section>
                         </section>
                     </div>
                 <div class="row">
                     <section class="col col-11 pull-right">
                         <button type="button" class="btn btn-primary savePageChanges">
                             Save changes
                             </button>
                         </section>
                     </div>
                 </form>
             </div>
         </div>
     </li>