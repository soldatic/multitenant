<?php
/* @var $this TemplatesController */
/* @var $model Templates */

if (count($dataPage) > 0) {
    ?>
    <!--<h1>Manage Templates</h1>-->
    <script>
        var baseUrl = '<?php echo Yii::app()->request->baseUrl; ?>';
        var pageId = <?php echo $dataPage['tpl_page_id']; ?>;
        var themeId = <?php echo $dataPage['tpl_theme_id']; ?>;
        var templateId = <?php echo $dataPage['tpl_id']; ?>;
        var domainId = <?php echo $this->domainId; ?>;
        var contentRowButtonHTML = '<?php echo $contentRowButtonHTML; ?>';
    </script>
    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="xgate-template-editor-body">
            <div class="template-content" id="template-content">
                <iframe  src="" id="editor-iframe"></iframe>
            </div>

        </article>
        <!-- WIDGET END -->

    </div>
    <!-- end row -->

    <!--    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/clean-blog/css/clean-blog.css" rel="stylesheet" />
                <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/clean-blog/js/clean-blog.js"></script>-->
<?php } else { ?>
    <script>
        var baseUrl = '<?php echo Yii::app()->request->baseUrl; ?>';
        var pageId = 0;
        var themeId = 1;
        var templateId = 1;
        var domainId = '<?php echo $this->domainId; ?>';
    </script>
    <h2>Please add page in your template</h2>
    <?php
}
