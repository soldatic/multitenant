<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalItemSettings" class="modal fade" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                    ×
                </button>
                <h4 id="myModalLabel" class="modal-title">Item Settings</h4>
            </div>
            <div class="modal-body">
                <form class="smart-form">
                    <fieldset>
                        <div class="row" data-settings="off" id="image-settings">
                            <section>
                                <label>Border radius</label>
                                <div class="row">
                                    <div class="col col-6">
                                        <label class="input">
                                            <input type="text" id="" placeholder="test" />
                                        </label>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">
                    Cancel
                </button>
                <button class="btn btn-primary" id="itemSettingsUpdate" type="button">
                    Update
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>