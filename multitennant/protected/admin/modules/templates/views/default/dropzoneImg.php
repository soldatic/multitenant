<div id="menu-panel-img">
    <div class="container-fluid">
        <div class="row">
            <div class="panel-name">
                <span>Add new picture</span>
            </div>
            <div class="panel-button">
                <button class="btn btn-sm btn-danger" id="menu-img-close"><span class="glyphicon glyphicon-remove"></span>Cancel</button>
                <button class="btn btn-sm btn-primary" id="menu-img-save"><span class="glyphicon glyphicon-ok"></span>Save</button>
            </div>
        </div>
        <div class="row">    
            <div class="jarviswidget jarviswidget-sortable">

                            <header role="heading">
                                	

                                <ul id="widget-tab-1" class="nav nav-tabs pull-left ">

                                    <li class="active">

                                        <a data-toggle="tab" href="#hr3"> <i class="fa fa-lg fa-arrow-circle-o-down"></i> <span class="hidden-mobile hidden-tablet"> PC </span> </a>

                                    </li>

                                    <li class="">
                                        <a data-toggle="tab" href="#hr4"> <i class="fa fa-lg fa-arrow-circle-o-up"></i> <span class="hidden-mobile hidden-tablet"> URL </span></a>
                                    </li>

                                </ul>	

                                <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                            </header>

                            <!-- widget div-->
                            <div role="content" class="greyBackground noBorder">

                                <!-- widget edit box -->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->

                                </div>
                                <!-- end widget edit box -->

                                <!-- widget content -->
                                <div class="widget-body no-padding greyBackground">

                                    <!-- widget body text-->

                                    <div class="tab-content padding-10 paddingNones greyBackground">
                                        <div class="tab-pane fade active in" id="hr3" >
                                             <form action="/admin/templates/ajax/uploadFiles" enctype="multipart/form-data"  id="mydropzone1">
                                                <div class="dz-default dz-message">
                                                    <span>Drop files here to upload</span>
                                                </div>
                                                <input type="hidden" name="dId" value="<?php echo $domainId; ?>" />
                                                <input type="hidden" name="tplId" value="<?php echo $tplId; ?>" />

                                                <!-- If you want control over the fallback form, just add it here: -->
                                                <div class="fallback"> <!-- This div will be removed if the fallback is not necessary -->
                                                    <input type="file" name="youfilename" />
                                                </div>
                                            </form>

                                        </div>
                                        <div class="tab-pane fade greyBackground" id="hr4">
                                               
                                            <h1 class="">Insert URL of Picture</h1>
                                                
                                            <form id="review-form" class="smart-form greyBackground" novalidate="novalidate">
                                                <fieldset class="greyBackground">
                                                    <div class="row">
                                                        <section class="col col-12">
                                                            <label class="input"> <i class="icon-append fa fa-globe"></i>
                                                                <input type="text" name="name" id="inputWithUrlImg" placeholder="URL of picture">
                                                            </label>
                                                        </section>
                                                        <button   class="btn btn-sm btn-primary downloadImgWithUrl" id="ok-youtube"><span class="glyphicon glyphicon-ok"></span>Ok</button>
                                                    </div>
                                                </fieldset>

                                            </form>
                                        </div>
                                    </div>


                                  <!-- <div class="widget-footer text-right">
                                  
                                        <button type="button" class="btn btn-default" id="backGallery">
                                            Back to gallery
                                        </button>
                                        <button type="submit" class="btn btn-primary asdasdasd">
                                            Save Picture
                                        </button>
                                  
                                                                  </div> -->

                                </div>

                            </div>



                        </div>

            </div>
        </div>
    </div>
</div>