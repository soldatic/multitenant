<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="imageLinkModal" class="modal fade" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                    ×
                </button>
                <h4 id="myModalLabel" class="modal-title">Link for image</h4>
            </div>
            <div class="modal-body">
                <form class="smart-form">
                    <fieldset>
                        <div class="row">
                            <section>
                                <label class="input">
                                    <input type="text" id="imageLinkTitle" placeholder="Link Title" />
                                </label>
                            </section>
                        </div>
                        <div class="row">
                            <section>
                                <label class="input">
                                    <input type="text" id="imageLinkWeb" placeholder="Website URL" />
                                </label>
                                <div class="note">
                                    Needed to enter the website
                                </div>
                            </section>
                        </div>
                        <div class="row">
                            <section>
                                <label class="select">
                                    <select id="imageLinkPage">
                                        <option value="0">Select page</option>
                                    </select>
                                    <i></i>
                                </label>
                                <div class="note">
                                    Site Pages
                                </div>
                            </section>
                        </div>
                        <div class="row">
                            <div class="col col-6">
                                <label class="toggle">
                                    <input type="checkbox" name="checkbox-toggle" id="imageLinkTarget"/>
                                    <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Target in new window
                                </label>
                                <label class="toggle">
                                    <input type="checkbox" name="checkbox-toggle" id="imageLinkLightbox"/>
                                    <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Lightbox
                                </label>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">
                    Cancel
                </button>
                <button class="btn btn-primary" id="imageLinkRemove" type="button">
                    Remove Link
                </button>
                <button class="btn btn-primary" id="imageLinkSave" type="button">
                    Save Link
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>