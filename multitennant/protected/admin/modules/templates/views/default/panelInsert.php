<div id="insert-panel" class="panel">
    <div class="panel-header">
        <div class="panel-name">
            <span>Tools</span>
            <span class="glyphicon glyphicon-remove panel-close"></span>
        </div>
    </div>
    <div class="panel-body tools-s">
        <ul>
            <li>Structure</li>
            <li>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools" data-type="row"><i
                        class="fa fa-bar-chart-o"></i> Row</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools" data-type="banner"><i
                        class="fa fa-bar-chart-o"></i> Banner</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools"
                   data-type="text-image"><i class="fa fa-align-left"></i><i class="fa fa-file-image-o"></i> Text Image</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools"
                   data-type="image-text"><i class="fa fa-file-image-o"></i><i class="fa fa-align-right"></i> Image Text</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools"
                   data-type="big-text"><i class="fa fa-comment"></i> Big Message</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools" data-type="text"><i
                        class="fa fa-text-height"></i> Text Block</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools" data-type="title"><i
                        class="fa fa-font"></i> Title</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools"
                   data-type="features"><i class="fa fa-th"></i> Features</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools" data-type="photo"><i
                        class="fa fa-camera"></i> Big Photo</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools" data-type="columns"><i
                        class="fa fa-th-list"></i> Three Columns</a>
            </li>
            <li>Feature</li>
            <li>
                <a href="javascript:void(0);" class="btn btn-success xgate-template-editor-tools" data-type="gallery"><i
                        class="fa fa-th-large"></i> Image Gallery</a>
                <a href="javascript:void(0);" class="btn btn-success xgate-template-editor-tools disabled" data-type="slider"><i
                        class="fa fa-play"></i><i class="fa fa-picture-o"></i> Image Slider</a>
                <a href="javascript:void(0);" class="btn btn-success xgate-template-editor-tools disabled" data-type="button"><i
                        class="fa fa-keyboard-o"></i> Button</a>
                <a href="javascript:void(0);" class="btn btn-success xgate-template-editor-tools" data-type="map"><i
                        class="fa  fa-map-marker"></i> Map</a>
                <a href="javascript:void(0);" class="btn btn-success xgate-template-editor-tools disabled" data-type="form"><i
                        class="fa fa-indent"></i> Form</a>
                <a href="javascript:void(0);" class="btn btn-success xgate-template-editor-tools" data-type="youtube"><i
                        class="fa fa-youtube-play"></i> Youtube Video</a>
            </li>
            <li>Other</li>
            <li>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools-i"
                   data-type="menu"><i class="fa fa-align-left"></i> Menu</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools-i"
                   data-type="textItem"><i class="fa fa-text-height"></i> Text</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools-i"
                   data-type="titleItem"><i class="fa fa-font"></i> Title</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools-i disabled"
                   data-type="button"><i class="fa fa-keyboard-o"></i> Button</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools-i"
                   data-type="imageItem"><i class="fa fa-camera"></i> Image</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools-i disabled"
                   data-type="link"><i class="fa fa-link"></i> Link</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools-i disabled"
                   data-type="listOlItem"><i class="fa fa-list-ol"></i> Ordered List</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools-i disabled"
                   data-type="listUlItem"><i class="fa fa-list-ul"></i> Unordered List</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools-i disabled"
                   data-type="labelItem"><i class="fa fa-file-text"></i> Label</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools-i disabled"
                   data-type="textareaItem"><i class="fa fa-text-height"></i> Text Area</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools-i disabled"
                   data-type="checkboxItem"><i class="fa fa-check-square-o"></i> Check Box</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools-i disabled"
                   data-type="radiobuttonItem"><i class="fa fa-check-circle-o"></i> Radio Button</a>
                <a href="javascript:void(0);" class="btn btn-primary xgate-template-editor-tools-i disabled"
                   data-type="selectItem"><i class="fa fa-sort-desc"></i> Select</a>
            </li>
        </ul>
    </div>
</div>