<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalFormBuilder" class="modal fade" style="display: none;">
    <div class="modal-dialog ChangeImageOnHeaderModal">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                    ×
                </button>
                <h4 id="myModalLabel" class="modal-title">Form builder</h4>
            </div>

            <!--form builder-->
            <div class="modal-body">
                <script type="text/javascript">

                    /*<![CDATA[*/

//                    var desktopNotifications =
//                            {
//                                notify: function (image, title, body)
//                                {
//
//                                    return false;
//                                },
//                                isSupported: function ()
//                                {
//                                    if (typeof window.webkitNotifications != 'undefined')
//                                    {
//                                        return true
//                                    }
//                                    else
//                                    {
//                                        return false
//                                    }
//                                },
//                                requestAutorization: function ()
//                                {
//                                    if (typeof window.webkitNotifications != 'undefined')
//                                    {
//                                        if (window.webkitNotifications.checkPermission() == 1)
//                                        {
//                                            window.webkitNotifications.requestPermission();
//                                        }
//                                        else if (window.webkitNotifications.checkPermission() == 2)
//                                        {
//                                            alert('You have blocked desktop notifications for this browser.');
//                                        }
//                                        else
//                                        {
//                                            alert('You have already activated desktop notifications for Chrome');
//                                        }
//                                    }
//                                    else
//                                    {
//                                        alert('This is only available in Chrome.');
//                                    }
//                                }
//                            };
//
//                    $(document).ready(function ()
//                    {
//                        $('#FlashMessageBar').jnotifyInizialize(
//                                {
//                                    oneAtTime: false,
//                                    appendType: 'append'
//                                }
//                        );
//                    }
//                    );
                    /*]]>*/
                </script>
                <div id="WebformsPageView" class="ZurmoDefaultPageView ZurmoPageView PageView"><div id="ZurmoDefaultView"><div class="GridView"><div class="AppContainer clearfix GridView">
                                <div class="AppContent GridView">
                                    <div id="WebformTabView" class="SecuredActionBarForSearchAndListView ActionBarForSearchAndListView ConfigurableMetadataView MetadataView">
                                        <div class="view-toolbar-container clearfix">

                                        </div>

                                    </div>
                                    <div id="WebformDragAndDropEditView" class="SecuredEditAndDetailsView EditAndDetailsView DetailsView ModelView ConfigurableMetadataView MetadataView">
                                        <div class="wrapper">
                                            <div class="doc_page" style="min-width: 760px; width: 100%">    
                                                <div class="g-unit">        
                                                    <div class="adsui-module adsui-module-primary ltas-body-module">            
                                                        <div id="errTB" style="padding-bottom: 3px; padding-top: 5px;">            
                                                        </div>            
                                                        <div class="ltas-home-contents" style="padding: 5px 0px;">                
                                                            <table style="width: 100%; min-width: 740px;">                    
                                                                <tbody> </tbody>
                                                            </table>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">                                    
                                                                <tbody>                                        
                                                                    <tr>                                            
                                                                        <td>                                                
                                                                            <div class="hasFields tooLong" id="container" style="min-width:1050px;">
                                                                                <div class="afi" id="stage">                                                        
                                                                                    <div id="side">                                                            
                                                                                        <ul id="tabs">                                                                
                                                                                            <li id="afi"><a title="Add    fields to your form." onmousedown="showFields(true);">Add a Field</a></li>                                                              
                                                                                            <li id="cfi"><a title="Change the properties of a field here." onmousedown="showFields(false);">Field Settings</a> </li>  
                                                                                        </ul>                                                            
                                                                                        <form onsubmit="return false;" action="#" id="formProperties" style="display: none;" class=""> 
                                                                                            <ul style="margin-top: 0px;" id="formSettings"> 
                                                                                                <li class="clear"><label class="desc formSettings">Form Question</label>   
                                                                                                    <div style="width: 290px; padding-bottom: 5px;">   
                                                                                                        <input value="1" name="formQuestion" id="formQuestion" onchange="updateHideQuestion(this.checked)" checked="checked" type="checkbox"> 
                                                                                                        <!--Hide Question Number--> 
                                                                                                    </div>                                                                    
                                                                                                </li>       
                                                                                                <li class="clear"><label class="desc formSettings">Alert Error Message</label>  
                                                                                                    <div style="width: 290px; padding-bottom: 5px;">
                                                                                                        <input value="1" name="alertErrorMessage" id="alertErrorMessage" onchange="updateAlertErrorMessage(this.checked)" type="checkbox"> 
                                                                                                        <!--Error Message Alert Box-->         
                                                                                                    </div>
                                                                                                </li>
                                                                                                <li class="clear"><label class="desc formSettings">Add Footer and Header</label>                                                                      
                                                                                                    <div style="width: 290px; padding-bottom: 5px;">      
                                                                                                        <input value="1" name="headerAndFooter" id="headerAndFooter" onchange="updateheaderAndFooter(this.checked)" type="checkbox">  
                                                                                                        <!--Footer and Header-->         
                                                                                                    </div>
                                                                                                </li>
                                                                                                <li class="clear"><label class="desc formSettings">SMS Trigger</label>                                                                      
                                                                                                    <div style="width: 290px; padding-bottom: 5px;">                                                                            
                                                                                                        <input value="1" name="useSmsTrigger" id="useSmsTrigger" onchange="updateSMSTrigger(this.checked)" type="checkbox"> Use SMS Trigger     
                                                                                                    </div>    
                                                                                                </li>    
                                                                                                <li id="smspanel" class="clear" style="display:none;">
                                                                                                    <label class="desc formSettings">SMS Content</label>
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="290px">  
                                                                                                        <tbody>
                                                                                                            <tr>  
                                                                                                                <td>
                                                                                                                    <div style="font-size: 11px;">Insert Unique Code &nbsp;&nbsp;<a href="javascript:%20void(0);" onclick="clickCodeSetting('https://dms.xgate.com.hk/account/codeGenSetting.php?link=aHR0cHM6Ly9kbXMueGdhdGUuY29tLmhrL3dlYi93ZWJGcm0ucGhw')">[Setting]</a> 
                                                                                                                    </div>
                                                                                                                </td>                                &nb                                                <td align="right"><input name="btRCInsert" value="Unique Code" onclick="insertAtCaret('smsContent', '{#RandomCode#}');" type="button">                                                                          
                                                                                                                </td> 
                                                                                                            </tr>  
                                                                                                            <tr>   
                                                                                                                <td>   
                                                                                                                    <div style="font-size: 11px; padding-bottom: 5px;">   
                                                                                                                        <select name="cstmField" id="cstmField" style="width: 200px;">                                                                                       
                                                                                                                            <option selected="selected" value="">Q7-
                                                                                                                            </option>
                                                                                                                        </select>                                                                                    </div>                                                                                </td>                                                                                <td align="right">                                                                                    <div style="padding-bottom: 5px;">                                                                                        
                                                                                                                        <input name="btCstmInsert" value="Custom Field" onclick="insertCstmField('smsContent');" type="button">      
                                                                                                                    </div>                                                                                
                                                                                                                </td>     
                                                                                                            </tr>    
                                                                                                        </tbody>
                                                                                                    </table> 
                                                                                                    <div style="width: 290px; padding-bottom: 5px;">    
                                                                                                        <textarea style="height: 55px;" id="smsContent" name="smsContent" class="textarea" onkeydown="savePos()" onkeyup="savePos();
                                                                                                                updateSMSContentLength();" onmousedown="savePos()" onmouseup="savePos()" onfocus="savePos()"></textarea>   
                                                                                                        Word Count 
                                                                                                        <input name="wordcount" id="wordcount" class="text" style="width: 50px;" value="0" readonly="readonly" type="text">  
                                                                                                        <div class="shadow" style="position: absolute; top: -50000px; left: -50000px; visibility: hidden; width: 98px; font-size: 11px; font-family: &quot;Lucida Grande&quot;,Tahoma,Arial,sans-serif; line-height: 15px; resize: none;">a
                                                                                                        </div>

                                                                                                    </div>
                                                                                                </li>                                                                    
                                                                                                <li class="clear" style="display:block;">
                                                                                                    <label class="desc formSettings">Official Rule</label> 
                                                                                                    <div style="width: 290px; padding-bottom: 5px;">  
                                                                                                        <input value="1" name="useOfficialRule" id="useOfficialRule" onchange="updateOfficialRuleTrigger(this.checked)" type="checkbox"> 
                                                                                                        Use Official Rule 
                                                                                                    </div> 
                                                                                                </li> 
                                                                                                <li id="officialRuleContentpanel" class="clear" style="display:none;">
                                                                                                    <label class="desc">Official Rule Content</label>
                                                                                                    <div style="width: 290px; padding-bottom: 5px;">  
                                                                                                        <textarea style="height: 55px;" id="officialRuleContent" name="officialRuleContent" class="textarea"></textarea>
                                                                                                        <div class="shadow" style="position: absolute; top: -50000px; left: -50000px; visibility: hidden; width: 98px; font-size: 11px; font-family: &quot;Lucida Grande&quot;,Tahoma,Arial,sans-serif; line-height: 15px; resize: none;">a
                                                                                                        </div>

                                                                                                    </div> 
                                                                                                </li>
                                                                                                <li id="officialRuleWordpanel" class="clear" style="display:none;">
                                                                                                    <label class="desc">Official Rule Word</label>  
                                                                                                    <div style="width: 290px; padding-bottom: 5px;"> 
                                                                                                        <input name="officialRuleWord" id="officialRuleWord" class="text" style="width: 150px;" value="Official" rules="" type="text">
                                                                                                    </div>
                                                                                                </li> 
                                                                                                <li id="officialrulemandatorypanel" class="clear" style="display:none;">
                                                                                                    <label class="desc">Mandatory  Official Rule</label> 
                                                                                                    <div style="width: 290px; padding-bottom: 5px;">  
                                                                                                        <input value="1" name="mandatoryOfficialRule" id="mandatoryOfficialRule" onchange="updateOfficialRuleRequire(this.checked);" type="checkbox">Required  Mandatory Official Rule 
                                                                                                    </div> 
                                                                                                </li>
                                                                                            </ul> 
                                                                                        </form>   
                                                                                        <div class="clearfix" id="addFields" style="margin-top: 0px; display: block;">
                                                                                            <div id="shake"> 
                                                                                                <h3 class="stand">Standard</h3>  
                                                                                                <ul id="col1">  
                                                                                                    <li class="dragfld 1 bigger ui-draggable" id="drag1_textarea" style="position: relative;">
                                                                                                        <a href="#" class="button" id="ml">  <b></b> Paragraph Text </a> 
                                                                                                    </li>  
                                                                                                    <li class="dragfld 2 bigger ui-draggable" id="drag1_radio" style="position: relative;">
                                                                                                        <a href="#" class="button" id="mc"> <b></b> Multiple Choice</a> 
                                                                                                    </li> 
                                                                                                    <li class="dragfld 3 bigger ui-draggable" id="drag1_plaintext" style="position: relative;">
                                                                                                        <a href="#" class="button" id="pt"> <b></b> Plain Text             </a>    
                                                                                                    </li>     
                                                                                                    <li class="dragfld 5 ui-draggable" id="drag3_pagebreak" style="position: relative;">
                                                                                                        <a href="#" class="button" id="pageb"> <b></b> Page Break                     </a>      
                                                                                                    </li>                                                                        
                                                                                                    <li class="dragfld 0 ui-draggable" id="drag1_text" style="position: relative;">
                                                                                                        <a href="#" class="button" id="sl"> <b></b> Single Line Text                               </a>
                                                                                                    </li> 
                                                                                                    <li class="dragfld 0 ui-draggable" id="drag1_likert" style="position: relative;">
                                                                                                        <a href="#" class="button" id="lt"> <b></b> Likert             </a>
                                                                                                    </li>   
                                                                                                    <li class="dragfld 5" id="drag1_hiddentext" style="position: relative;">
                                                                                                        <a href="#" class="button" id="ht"> <b></b> Hidden Text</a>
                                                                                                    </li>
                                                                                                </ul>                                                                    
                                                                                                <ul id="col2">
                                                                                                    <li class="dragfld 0 ui-draggable" id="drag2_number" style="position: relative;">
                                                                                                        <a href="#" class="button" id="nu"> <b></b> Number        </a>
                                                                                                    </li>  
                                                                                                    <li class="dragfld 1 bigger ui-draggable" id="drag2_checkbox" style="position: relative;">
                                                                                                        <a href="#" class="button" id="cb"> <b></b> Checkboxes                          </a>   
                                                                                                    </li>                                                                        
                                                                                                    <li class="dragfld 2 ui-draggable" id="drag2_select" style="position: relative;">
                                                                                                        <a href="#" class="button" id="dd"> <b></b> Dropdown                            </a>   
                                                                                                    </li>                                                                        
                                                                                                    <li class="dragfld 4 ui-draggable" id="drag4_limit" style="position: relative;">
                                                                                                        <a href="#" class="button" id="lc"> <b></b> Quota Capping                                 </a> 
                                                                                                    </li>                                                                        
                                                                                                    <li class="dragfld 4 ui-draggable" id="drag4_sketch" style="position: relative;">
                                                                                                        <a href="#" class="button" id="sk"> <b></b> Signature                                   </a>  
                                                                                                    </li>                                                                    
                                                                                                </ul>                                                                    
                                                                                                <h3 class="fancy">Lead/Contact information</h3>                                                                   
                                                                                                <ul id="col3">                                                                        
                                                                                                    <li class="dragfld 2 ui-draggable" id="drag3_email" style="position: relative;">
                                                                                                        <a href="#" class="button" id="em"> <b></b> Email                         </a>
                                                                                                    </li>                                                                        
                                                                                                    <li class="dragfld 5 ui-draggable" id="drag4_firstname" style="position: relative;">
                                                                                                        <a href="#" class="button" id="fn"> <b></b> First Name                       </a>   
                                                                                                    </li>                                                                        
                                                                                                    <li class="dragfld 3 ui-draggable" id="drag3_phone" style="position: relative;">
                                                                                                        <a href="#" class="button" id="ph"> <b></b> Mobile     </a> 
                                                                                                    </li>                                                                       
                                                                                                    <li class="dragfld 3 ui-draggable" id="drag4_birthmonth" style="position: relative;">
                                                                                                        <a href="#" class="button" id="bm"> <b></b> Birth Month "mm"                           </a>   
                                                                                                    </li>                                                                        
                                                                                                    <li class="dragfld 3 ui-draggable" id="drag4_birth" style="position: relative;">
                                                                                                        <a href="#" class="button" id="bi"> <b></b> Birth Day                 </a>  
                                                                                                    </li>                                                                        
                                                                                                    <li class="dragfld 3 ui-draggable" id="drag4_membergate" style="position: relative;">
                                                                                                        <a href="#" class="button" id="mg"> <b></b> Member Gate          </a>    
                                                                                                    </li>                                                                   
                                                                                                </ul>                                                                   
                                                                                                <ul id="col4">    
                                                                                                    <li class="dragfld 5 ui-draggable" id="drag3_shortname" style="position: relative;">
                                                                                                        <a href="#" class="button" id="na"> <b></b> Full Name      </a> 
                                                                                                    </li>                                                                        
                                                                                                    <li class="dragfld 5 ui-draggable" id="drag4_lastname" style="position: relative;">
                                                                                                        <a href="#" class="button" id="ln"> <b></b> Last Name   </a>        
                                                                                                    </li>                                                                        
                                                                                                    <li class="dragfld 1 ui-draggable" id="drag4_url" style="position: relative;">
                                                                                                        <a href="#" class="button" id="ws"> <b></b> Web Site              </a>
                                                                                                    </li>                                                                        
                                                                                                    <li class="dragfld 3 ui-draggable" id="drag4_birthdate" style="position: relative;">
                                                                                                        <a href="#" class="button" id="bd"> <b></b> Birth Date "dd"                        </a>       
                                                                                                    </li>                                                                       
                                                                                                    <li class="dragfld 1 ui-draggable" id="drag4_agerange" style="position: relative;">
                                                                                                        <a href="#" class="button" id="ar"> <b></b> Age Range    </a>
                                                                                                    </li>                                                                        
                                                                                                    <li class="dragfld 1 ui-draggable" id="drag4_redeemcode" style="position: relative;">
                                                                                                        <a href="#" class="button" id="rc"> <b></b> Redeem Code </a>                                                                       
                                                                                                    </li>                                                                    
                                                                                                </ul>                                                                
                                                                                            </div>                                                           
                                                                                        </div>                                                            
                                                                                        <form action="#" id="fieldProperties" onsubmit="return false;" style="display: none; margin-top: 0px;">  
                                                                                            <ul id="allProps" style="display: block; margin-top: 0px;">
                                                                                                <li style="display: block;" id="listTitle"><label class="desc">Field Label
                                                                                                    </label>
                                                                                                    <div>
                                                                                                        <textarea onmouseup="updateProperties(this.value, 'Title')" onkeyup="updateProperties(this.value, 'Title')" cols="35" rows="10" class="textarea" id="fieldTitle"></textarea>
                                                                                                    </div>
                                                                                                </li>
                                                                                                <li class="clear noheight" id="listDateFormat"></li>
                                                                                                <li style="display: block;" class="clear" id="listTextDefault">
                                                                                                    <fieldset>
                                                                                                        <legend>Options</legend>
                                                                                                        <input id="fieldRequired" value="" class="checkbox" onclick="(this.checked) ? checkVal = '1' : checkVal = '0';
                                                                                                                updateProperties(checkVal, 'IsRequired')" type="checkbox">
                                                                                                        <label for="fieldRequired" class="choice">&nbsp;&nbsp;Required</label>
                                                                                                    </fieldset>
                                                                                                </li>
                                                                                            </ul>                                                            
                                                                                        </form>                                                        
                                                                                    </div>   
                                                                                    <div class="noI ui-droppable" id="haupt" style="WIDTH: 800px;">         
                                                                                        <form action="#" method="post" class="survey_form" id="formPreview">              
                                                                                            <div style="height:40px;">             
                                                                                            </div>             
                                                                                            <ul class="" id="formFields">            
                                                                                                <div style="display: block;" class="form-group-edit col-sm-12 col-xs-12 nopad ui-draggable ui-droppable" id="itemOnPage7">
                                                                                                    <ul>
                                                                                                        <li title="Click to edit. Drag to reorder." style="cursor: pointer; top: 0px; left: 0px;" class="form-group-edit col-sm-12 col-xs-12 nopad" id="foli7">
                                                                                                            <div class="floatDiv" onclick="settingFieldOnLeftPanel('7');">

                                                                                                            </div>
                                                                                                            <label for="Field7" id="title7" class="frmquestion cstm_frmquestion">
                                                                                                                <br><span class="req" id="req_7">

                                                                                                                </span>
                                                                                                            </label>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </ul>         
                                                                                        </form>                                                         
                                                                                    </div>                                                    
                                                                                </div>                                                
                                                                            </div>                                            
                                                                        </td>                                        
                                                                    </tr>                                    
                                                                </tbody>                                
                                                            </table>                                                    
                                                            <div>&nbsp;</div>               
                                                            <form id="xgWebForm" name="xgWebForm" method="post">                   
                                                                <input id="externalRequestToken" name="externalRequestToken" value="03a5a02f90475fd" type="hidden"> 
                                                                <input id="hidwebLogo" name="hidwebLogo" value="0" type="hidden">
                                                                <input id="hidwebLogoAlign" name="hidwebLogoAlign" value="" type="hidden"> 
                                                                <input id="hidwebBgcolor" name="hidwebBgcolor" value="#FFFFFF" type="hidden"> 
                                                                <input id="hidwebBordercolor" name="hidwebBordercolor" value="#FFFFFF" type="hidden">
                                                                <input id="hidwebHidQuestionNo" name="hidwebHidQuestionNo" value="1" type="hidden"> 
                                                                <input id="hidalertErrorMessage" name="hidalertErrorMessage" value="0" type="hidden">  
                                                                <input id="hiduseHeaderAndFooter" name="hiduseHeaderAndFooter" value="0" type="hidden">
                                                                <input id="hidwebval" name="hidwebval" type="hidden"> 
                                                                <input id="hiduseSmsTrigger" name="hiduseSmsTrigger" value="" type="hidden">
                                                                <input id="hidsmsContent" name="hidsmsContent" value="" type="hidden"> 
                                                                <input id="hiduseOfficialRule" name="hiduseOfficialRule" value="" type="hidden">
                                                                <input id="hidofficialRuleWord" name="hidofficialRuleWord" value="Official" rules="" type="hidden">
                                                                <input id="hidmandatoryOfficialRule" name="hidmandatoryOfficialRule" value="" type="hidden">
                                                                <input name="mode" id="mode" type="hidden">
                                                                <div style="display: none;">
                                                                    <textarea style="height: 15px;" id="hidname" name="hidname">data</textarea>\
                                                                    <textarea style="height: 15px;" id="hidwebSubmitContent" name="hidwebSubmitContent"></textarea>
                                                                    <textarea style="height: 15px;" id="hidinviteMessage" name="hidinviteMessage"></textarea>
                                                                    <textarea style="height: 15px;" id="hidofficialRuleContent" name="hidofficialRuleContent"></textarea>
                                                                    <div class="shadow" style="position: absolute; top: -50000px; left: -50000px; visibility: hidden; width: 0px; font-size: 13px; font-family: monospace; line-height: 15px; resize: none;">data</div><div class="shadow" style="position: absolute; top: -50000px; left: -50000px; visibility: hidden; width: 0px; font-size: 13px; font-family: monospace; line-height: 15px; resize: none;">a
                                                                    </div>
                                                                    <div class="shadow" style="position: absolute; top: -50000px; left: -50000px; visibility: hidden; width: 0px; font-size: 13px; font-family: monospace; line-height: 15px; resize: none;">a</div><div class="shadow" style="position: absolute; top: -50000px; left: -50000px; visibility: hidden; width: 0px; font-size: 13px; font-family: monospace; line-height: 15px; resize: none;">a
                                                                    </div>

                                                                </div>                   
                                                                <!--                                                                                            <div class="float-bar">                   
                                                                                                                <div class="view-toolbar-container clearfix">                   
                                                                                                                <div class="form-toolbar">                       
                                                                                                                <a href="http://runxin.xgate.com/app/index.php/webforms/default">
                                                                                                                <span class="z-label">Cancel</span></a>                       
                                                                                                                <a id="saveyt0" class="attachLoading z-button" href="#" name="save" onclick="clickSave('Save')">
                                                                                                                <span class="z-spinner"></span><span class="z-icon"></span><span class="z-label">Save</span></a>
                                                                                                                <a id="saveyt0" class="attachLoading z-button" href="#" name="save" onclick="clickSave('SaveNext')"> 
                                                                                                                <span class="z-spinner"></span><span class="z-icon"></span><span class="z-label">Save and Next</span></a> 
                                                                                                                </div>                  
                                                                                                                </div>                   
                                                                                                                </div>               -->
                                                            </form>               
                                                        </div>            
                                                    </div>   
                                                </div>
                                            </div>
                                        </div>

                                        <script type="text/javascript" language="javascript">
                                            var total_count = 1;
                                            var currently_editing = "";
                                            var fields = new Array();
//                                                                                                                                        init("https://dms.xgate.com.hk");
                                            init(baseUrl);

                                            var code_length = 6;
                                            var content_endpt = 0;

//                                                                                                                                        function insertAtCaret(objid, text) {
//                                                                                                                                            var textBox = document.getElementById(objid);
//                                                                                                                                            var pre = textBox.value.substr(0, content_endpt);
//                                                                                                                                            var post = textBox.value.substr(content_endpt, (textBox.value.length - content_endpt));
//                                                                                                                                            textBox.value = pre + text + post;
//                                                                                                                                            content_endpt += text.length;
//                                                                                                                                            var msgend = content_endpt;
//                                                                                                                                            setCursor(document.getElementById("smsContent"), content_endpt, content_endpt);
//                                                                                                                                            content_endpt = msgend;
//                                                                                                                                            updateSMSContentLength();
//                                                                                                                                        }
//
//                                                                                                                                        function insertCstmField(objid) {
//                                                                                                                                            var selectedField = document.getElementById('cstmField').options.selectedIndex;
//                                                                                                                                            var selectedText = document.getElementById('cstmField').options[selectedField].text;
//                                                                                                                                            insertAtCaret(objid, '{$' + selectedText + '}');
//                                                                                                                                        }
//
//                                                                                                                                        function setCursor(el, st, content_endpt) {
//                                                                                                                                            if (el.setSelectionRange) {
//                                                                                                                                                el.focus();
//                                                                                                                                                el.setSelectionRange(st, content_endpt);
//                                                                                                                                            } else {
//                                                                                                                                                if (el.createTextRange) {
//                                                                                                                                                    range = el.createTextRange();
//                                                                                                                                                    range.collapse(true);
//                                                                                                                                                    range.moveEnd('character', content_endpt);
//                                                                                                                                                    range.moveStart('character', st);
//                                                                                                                                                    range.select();
//                                                                                                                                                }
//                                                                                                                                            }
//                                                                                                                                        }
//
//                                                                                                                                        function savePos() {
//                                                                                                                                            var textarea = document.getElementById('smsContent');
//                                                                                                                                            textarea.focus();
//                                                                                                                                            // get selection in firefox, opera, ?K
//                                                                                                                                            if (typeof (textarea.selectionStart) == 'number')
//                                                                                                                                            {
//                                                                                                                                                content_endpt = textarea.selectionStart;
//                                                                                                                                                //alert(textarea.selectionStart);
//                                                                                                                                            } else if (document.selection) {
//                                                                                                                                                var selection_range = document.selection.createRange().duplicate();
//                                                                                                                                                if (selection_range.parentElement() == textarea) { // Check that the selection is actually in our textarea
//                                                                                                                                                    // Create three ranges, one containing all the text before the selection,
//                                                                                                                                                    // one containing all the text in the selection (this already exists), and one containing all
//                                                                                                                                                    // the text after the selection.
//                                                                                                                                                    var before_range = document.body.createTextRange();
//                                                                                                                                                    before_range.moveToElementText(textarea); // Selects all the text
//                                                                                                                                                    before_range.setEndPoint("EndToStart", selection_range); // Moves the end where we need it
//
//                                                                                                                                                    var after_range = document.body.createTextRange();
//                                                                                                                                                    after_range.moveToElementText(textarea); // Selects all the text
//                                                                                                                                                    after_range.setEndPoint("StartToEnd", selection_range); // Moves the start where we need it
//
//                                                                                                                                                    var before_finished = false, selection_finished = false, after_finished = false;
//                                                                                                                                                    var before_text, untrimmed_before_text, selection_text, untrimmed_selection_text, after_text, untrimmed_after_text;
//
//                                                                                                                                                    // Load the text values we need to compare
//                                                                                                                                                    before_text = untrimmed_before_text = before_range.text;
//                                                                                                                                                    selection_text = untrimmed_selection_text = selection_range.text;
//                                                                                                                                                    after_text = untrimmed_after_text = after_range.text;
//
//                                                                                                                                                    // Check each range for trimmed newlines by shrinking the range by 1 character and seeing
//                                                                                                                                                    // if the text property has changed. If it has not changed then we know that IE has trimmed
//                                                                                                                                                    // a \r\n from the end.
//                                                                                                                                                    do {
//                                                                                                                                                        if (!before_finished) {
//                                                                                                                                                            if (before_range.compareEndPoints("StartToEnd", before_range) == 0) {
//                                                                                                                                                                before_finished = true;
//                                                                                                                                                            } else {
//                                                                                                                                                                before_range.moveEnd("character", -1)
//                                                                                                                                                                if (before_range.text == before_text) {
//                                                                                                                                                                    untrimmed_before_text += "\r\n";
//                                                                                                                                                                } else {
//                                                                                                                                                                    before_finished = true;
//                                                                                                                                                                }
//                                                                                                                                                            }
//                                                                                                                                                        }
//                                                                                                                                                        if (!selection_finished) {
//                                                                                                                                                            if (selection_range.compareEndPoints("StartToEnd", selection_range) == 0) {
//                                                                                                                                                                selection_finished = true;
//                                                                                                                                                            } else {
//                                                                                                                                                                selection_range.moveEnd("character", -1)
//                                                                                                                                                                if (selection_range.text == selection_text) {
//                                                                                                                                                                    untrimmed_selection_text += "\r\n";
//                                                                                                                                                                } else {
//                                                                                                                                                                    selection_finished = true;
//                                                                                                                                                                }
//                                                                                                                                                            }
//                                                                                                                                                        }
//                                                                                                                                                        if (!after_finished) {
//                                                                                                                                                            if (after_range.compareEndPoints("StartToEnd", after_range) == 0) {
//                                                                                                                                                                after_finished = true;
//                                                                                                                                                            } else {
//                                                                                                                                                                after_range.moveEnd("character", -1)
//                                                                                                                                                                if (after_range.text == after_text) {
//                                                                                                                                                                    untrimmed_after_text += "\r\n";
//                                                                                                                                                                } else {
//                                                                                                                                                                    after_finished = true;
//                                                                                                                                                                }
//                                                                                                                                                            }
//                                                                                                                                                        }
//                                                                                                                                                    } while ((!before_finished || !selection_finished || !after_finished));
//
//                                                                                                                                                    // Untrimmed success test to make sure our results match what is actually in the textarea
//                                                                                                                                                    // This can be removed once you??re confident it??s working correctly
//                                                                                                                                                    var untrimmed_text = untrimmed_before_text + untrimmed_selection_text + untrimmed_after_text;
//                                                                                                                                                    var untrimmed_successful = false;
//                                                                                                                                                    if (textarea.value == untrimmed_text) {
//                                                                                                                                                        untrimmed_successful = true;
//                                                                                                                                                    }
//                                                                                                                                                    // ** END Untrimmed success test
//                                                                                                                                                    var startPoint = untrimmed_before_text.length;
//                                                                                                                                                    content_endpt = startPoint;
//                                                                                                                                                }
//                                                                                                                                            }
//                                                                                                                                        }
//
//                                                                                                                                        function str_pad(str, length, pad_string, pad_type)
//                                                                                                                                        {
//                                                                                                                                            while (str.length < length)
//                                                                                                                                            {
//                                                                                                                                                if (pad_type == "left")
//                                                                                                                                                {
//                                                                                                                                                    str = pad_string + str;
//                                                                                                                                                }
//                                                                                                                                                else if (pad_type == "right")
//                                                                                                                                                {
//                                                                                                                                                    str = str + pad_string;
//                                                                                                                                                }
//                                                                                                                                            }
//                                                                                                                                            return str;
//                                                                                                                                        }
//
//                                                                                                                                        function getY(el)
//                                                                                                                                        {
//                                                                                                                                            var ot = el.offsetTop;
//                                                                                                                                            while ((el = el.offsetParent) != null) {
//                                                                                                                                                ot += el.offsetTop;
//                                                                                                                                            }
//                                                                                                                                            return ot;
//                                                                                                                                        }
//
//                                                                                                                                        $(function () {
//                                                                                                                                            var fieldsObj = $("#addFields");
//                                                                                                                                            var settingObj = $("#allProps");
//                                                                                                                                            var basicObj = $("#formSettings");
//                                                                                                                                            var formObj = $("#formPreview");
//                                                                                                                                            var offset = fieldsObj.offset();
//                                                                                                                                            var topPadding = 25;
//                                                                                                                                            var orgY = getY(document.getElementById("formPreview"));
//                                                                                                                                            $(window).scroll(function () {
//
//                                                                                                                                                if (($(window).scrollTop() - formObj.offset().top + fieldsObj.height() + topPadding) < formObj.height()) {
//                                                                                                                                                    if ($(window).scrollTop() > orgY) {
//                                                                                                                                                        var stageWidth = $("#stage").width();
//                                                                                                                                                        if (stageWidth > 938) {
//                                                                                                                                                            fieldsObj.stop().animate({
//                                                                                                                                                                marginTop: $(window).scrollTop() - offset.top + topPadding
//                                                                                                                                                            });
//                                                                                                                                                        }
//                                                                                                                                                    }
//                                                                                                                                                    else
//                                                                                                                                                    {
//                                                                                                                                                        fieldsObj.stop().animate({
//                                                                                                                                                            marginTop: 0
//                                                                                                                                                        });
//                                                                                                                                                    }
//                                                                                                                                                }
//                                                                                                                                                if (($(window).scrollTop() - formObj.offset().top + $("#allProps").height() + topPadding) < formObj.height()) {
//                                                                                                                                                    if ($(window).scrollTop() > orgY) {
//                                                                                                                                                        var stageWidth = $("#stage").width();
//                                                                                                                                                        if (stageWidth > 938) {
//                                                                                                                                                            settingObj.stop().animate({
//                                                                                                                                                                marginTop: $(window).scrollTop() - offset.top + topPadding
//                                                                                                                                                            });
//                                                                                                                                                        }
//                                                                                                                                                    }
//                                                                                                                                                    else
//                                                                                                                                                    {
//                                                                                                                                                        settingObj.stop().animate({
//                                                                                                                                                            marginTop: 0
//                                                                                                                                                        });
//                                                                                                                                                    }
//                                                                                                                                                }
//
//                                                                                                                                                if (($(window).scrollTop() - formObj.offset().top + $("#formSettings").height() + topPadding) < formObj.height()) {
//                                                                                                                                                    if ($(window).scrollTop() > orgY) {
//                                                                                                                                                        var stageWidth = $("#stage").width();
//                                                                                                                                                        if (stageWidth > 938) {
//                                                                                                                                                            basicObj.stop().animate({
//                                                                                                                                                                marginTop: $(window).scrollTop() - offset.top + topPadding
//                                                                                                                                                            });
//                                                                                                                                                        }
//                                                                                                                                                    }
//                                                                                                                                                    else
//                                                                                                                                                    {
//                                                                                                                                                        basicObj.stop().animate({
//                                                                                                                                                            marginTop: 0
//                                                                                                                                                        });
//                                                                                                                                                    }
//                                                                                                                                                }
//                                                                                                                                            });
//                                                                                                                                        });
//
//                                                                                                                                        function showLogo(theForm, checked)
//                                                                                                                                        {
//                                                                                                                                            if (checked)
//                                                                                                                                            {
//                                                                                                                                                document.getElementById("logoalign").disabled = false;
//                                                                                                                                            }
//                                                                                                                                            else
//                                                                                                                                            {
//                                                                                                                                                document.getElementById("logoalign").disabled = true;
//                                                                                                                                            }
//                                                                                                                                        }
//
//                                                                                                                                        function clickCodeSetting(val)
//                                                                                                                                        {
//                                                                                                                                            var isPass = false;
//                                                                                                                                            isPass = window.confirm("Please save the form first, otherwise all data will be lost");
//                                                                                                                                            if (isPass)
//                                                                                                                                            {
//                                                                                                                                                window.location = val;
//                                                                                                                                            }
//                                                                                                                                        }
//
//                                                                                                                                        function clickBack(theForm)
//                                                                                                                                        {
//                                                                                                                                            window.location = "index.php";
//                                                                                                                                        }
//
//
//                                                                                                                                        function clickSave(part)
//                                                                                                                                        {
//                                                                                                                                            theForm = document.getElementById('xgWebForm');
//                                                                                                                                            var allWrong = true;
//                                                                                                                                            var isPass = true;
//                                                                                                                                            var this_val = "";
//                                                                                                                                            var alertMsg = "";
//
//                                                                                                                                            this_val = document.getElementById("hidname").value;
//                                                                                                                                            if (isEmpty(this_val))
//                                                                                                                                            {
//                                                                                                                                                isPass = false;
//                                                                                                                                                alertMsg += "Web Form Name";
//                                                                                                                                            }
//                                                                                                                                            else
//                                                                                                                                            {
//                                                                                                                                                allWrong = false;
//                                                                                                                                            }
//
//
//                                                                                                                                            this_val = document.getElementById("hiduseSmsTrigger").value;
//                                                                                                                                            if (this_val == 1)
//                                                                                                                                            {
//                                                                                                                                                this_val = document.getElementById("hidsmsContent").value;
//                                                                                                                                                if (isEmpty(this_val))
//                                                                                                                                                {
//                                                                                                                                                    if (!isPass)
//                                                                                                                                                    {
//                                                                                                                                                        alertMsg += ", ";
//                                                                                                                                                    }
//                                                                                                                                                    alertMsg += "SMS Trigger Content";
//                                                                                                                                                    isPass = false;
//                                                                                                                                                }
//                                                                                                                                            }
//
//                                                                                                                                            // Modify By Wood
//                                                                                                                                            // javascript check the official rule validation
//                                                                                                                                            this_val = document.getElementById("hiduseOfficialRule").value;
//                                                                                                                                            if (this_val == 1)
//                                                                                                                                            {
//                                                                                                                                                this_val = document.getElementById("officialRuleContent").value;
//                                                                                                                                                if (isEmpty(this_val))
//                                                                                                                                                {
//                                                                                                                                                    if (!isPass)
//                                                                                                                                                    {
//                                                                                                                                                        alertMsg += ", ";
//                                                                                                                                                    }
//                                                                                                                                                    alertMsg += "Official Rules";
//                                                                                                                                                    isPass = false;
//                                                                                                                                                }
//                                                                                                                                                else
//                                                                                                                                                {
//                                                                                                                                                    document.getElementById("hidofficialRuleContent").value = this_val;
//                                                                                                                                                }
//                                                                                                                                                this_val = document.getElementById("officialRuleWord").value;
//                                                                                                                                                if (isEmpty(this_val))
//                                                                                                                                                {
//                                                                                                                                                    if (!isPass)
//                                                                                                                                                    {
//                                                                                                                                                        alertMsg += ", ";
//                                                                                                                                                    }
//                                                                                                                                                    alertMsg += "Official Rules Word";
//                                                                                                                                                    isPass = false;
//                                                                                                                                                }
//                                                                                                                                                else
//                                                                                                                                                {
//                                                                                                                                                    document.getElementById("hidofficialRuleWord").value = this_val;
//                                                                                                                                                }
//                                                                                                                                            }
//                                                                                                                                            // End Modify By Wood
//
//                                                                                                                                            // if(document.getElementById("useweblogo").checked)
//                                                                                                                                            // {
//                                                                                                                                            // if(document.getElementById("logoalign").value == "")
//                                                                                                                                            // {
//                                                                                                                                            // isPass = false;
//                                                                                                                                            // alertMsg += "Logo Alignment";
//                                                                                                                                            // }
//                                                                                                                                            // else
//                                                                                                                                            // {
//                                                                                                                                            // document.getElementById("hidwebLogo").value = "1";
//                                                                                                                                            // document.getElementById("hidwebLogoAlign").value = document.getElementById("logoalign").value
//                                                                                                                                            // }
//                                                                                                                                            // }
//                                                                                                                                            // else
//                                                                                                                                            // {
//                                                                                                                                            // document.getElementById("hidwebLogo").value = "0";
//                                                                                                                                            // document.getElementById("hidwebLogoAlign").value = "";
//                                                                                                                                            // }
//
//
//                                                                                                                                            this_val = JSON.stringify(fields);
//                                                                                                                                            if (this_val == "[]")
//                                                                                                                                            {
//                                                                                                                                                if (!isPass)
//                                                                                                                                                {
//                                                                                                                                                    alertMsg += ", ";
//                                                                                                                                                }
//                                                                                                                                                alertMsg += "Question(s)";
//                                                                                                                                                isPass = false;
//                                                                                                                                            }
//                                                                                                                                            else
//                                                                                                                                            {
//                                                                                                                                                document.getElementById("hidwebval").value = this_val;
//                                                                                                                                                allWrong = false;
//                                                                                                                                            }
//
//                                                                                                                                            if (isPass)
//                                                                                                                                            {
//                                                                                                                                                document.getElementById("errTB").innHTML = "";
//                                                                                                                                                document.getElementById("mode").value = part;
//                                                                                                                                                theForm.submit();
//                                                                                                                                            }
//                                                                                                                                            else
//                                                                                                                                            {
//                                                                                                                                                var errmsg = "                    <table cellspacing=0 cellpadding=0 border=0 width='100%'>\r\n";
//                                                                                                                                                errmsg += "                        <tr>\r\n";
//                                                                                                                                                errmsg += "                            <td valign=\"middle\" width=11>\r\n";
//                                                                                                                                                errmsg += "                                <div class=\"errorleftbox\">&nbsp;</div>\r\n";
//                                                                                                                                                errmsg += "                            </td>\r\n";
//                                                                                                                                                errmsg += "                            <td valign=\"middle\" class=\"errormidbox\">\r\n";
//                                                                                                                                                errmsg += "                                <div style=\"float:left;\">\r\n";
//                                                                                                                                                errmsg += "                                    <div class=\"head_error\">\r\n";
//                                                                                                                                                errmsg += "                                        Please enter a " + alertMsg + ".\r\n";
//                                                                                                                                                errmsg += "                                    </div>\r\n";
//                                                                                                                                                errmsg += "                                </div>\r\n";
//                                                                                                                                                errmsg += "                            </td>\r\n";
//                                                                                                                                                errmsg += "                            <td valign=middle width=11>\r\n";
//                                                                                                                                                errmsg += "                                <div class=\"errorrightbox\">&nbsp;</div>\r\n";
//                                                                                                                                                errmsg += "                            </td>\r\n";
//                                                                                                                                                errmsg += "                        </tr>\r\n";
//                                                                                                                                                errmsg += "                    </table>\r\n";
//                                                                                                                                                document.getElementById("errTB").innerHTML = errmsg;
//                                                                                                                                            }
//                                                                                                                                        }
//                                                                                                                                        updateSMSContentLength();
                                        </script>
                                        <style type="text/css"> </style> 
                                        <script type="text/javascript" language="javascript">
//                                                                                                                                        var customizeFieldArr = new Array();
//                                                                                                                                        customizeFieldArr = ["department", "firstname", "jobtitle", "lastname", "mobilephone", "officephone", "officefax", "emailaddress", "isinvalid", "optout", "companyname", "description", "website", "googlewebtrackingid", "lang", "isdeleted", "emailoptout", "mobileoptout", "salutation", "extid", "newfield1cstm", "newfield2cstm", "shoptelcstm", "expirydatecstm", "offercodecstm", "openidcstm", "nicknamecstm", "sexcstm", "languagecstm", "provincecstm", "headimgurlcstm", "remarkcstm", "membershipidcstm", "customercstm", "personidcstm", "legacyidcstm", "birthdatecstm", "stcstm", "sincecstm", "origjoiningdcstm", "fromdatecstm", "todatecstm", "rectsourcecstm", "homephonecstm", "telephonebuzcstm", "maritalstatucstm", "occupationcstm", "householdinccstm", "personalincocstm", "noofperosnscstm", "startdtcstm", "customercstmcstm", "mobile", "email", "deleted", "firstname", "lastname", "birthdate", "gender", "address", "country", "countrycode", "optin", "city", "createdate", "modifieddate", "attachmentcstm", "newfield2cstm", "regctrcstm", "shoptelcstm", "vipcodecstm", "namecstm", "campaginmemberidcstm"];
//
//                                                                                                                                        window.onload = function ()
//                                                                                                                                        {
//
//                                                                                                                                        };
                                        </script>
                                    </div></div><div id="FlashMessageView">
                                    <div class="notify-wrapper" id="FlashMessageBar">                
                                    </div>                
                                </div>           
                            </div><div id="ModalContainerView">
                                <div id="modalContainer"></div></div><div id="ModalGameNotificationContainerView"></div>
                        </div></div></div>
                <script type="text/javascript">


                    /*<![CDATA[*/
//                                                                                                                        jQuery(function ($) {
//                                                                                                                            $("#globalSearchScope").bind("multiselectclick", function (event, ui) {
//                                                                                                                                if ($("#globalSearchScope").multiselect("widget").find(":checkbox:checked").length == 0)
//                                                                                                                                {
//                                                                                                                                    $("#globalSearchScope").multiselect("widget").find(":checkbox").each(function () {
//                                                                                                                                        if (this.value == "All" && !this.checked)
//                                                                                                                                        {
//                                                                                                                                            this.click();
//                                                                                                                                        }
//                                                                                                                                    });
//                                                                                                                                }
//                                                                                                                                if (ui.value == "All" && ui.checked)
//                                                                                                                                {
//                                                                                                                                    $("#globalSearchScope").multiselect("widget").find(":checkbox").each(function () {
//                                                                                                                                        if (this.value != "All" && this.checked)
//                                                                                                                                        {
//                                                                                                                                            this.click();
//                                                                                                                                        }
//                                                                                                                                    });
//                                                                                                                                }
//                                                                                                                                else if (ui.value != "All" && ui.checked)
//                                                                                                                                {
//                                                                                                                                    $("#globalSearchScope").multiselect("widget").find(":checkbox").each(function () {
//                                                                                                                                        if (this.value == "All" && this.checked)
//                                                                                                                                        {
//                                                                                                                                            this.click();
//                                                                                                                                        }
//                                                                                                                                    });
//                                                                                                                                }
//                                                                                                                            });

//                                                                                                                            $('#globalSearchScope').multiselect({'selectedText': '', 'noneSelectedText': '', 'header': false, 'position': {'my': 'right top', 'at': 'right bottom'}});
//                                                                                                                            $(this).setupCheckboxStyling($('#globalSearchScope').parent())
//                                                                                                                            $("#globalSearchInput").bind("focus", function (event, ui) {
//                                                                                                                                $("#globalSearchInput").autocomplete("option", "source", "/app/index.php/zurmo/default/globalSearchAutoComplete?" + $.param($("#globalSearchScope").serializeArray()));
//                                                                                                                            });

//                                                                                                                            jQuery('#globalSearchInput').autocomplete({'select': function (event, ui) {
//                                                                                                                                    if (ui.item.href.length > 0) {
//                                                                                                                                        window.location = ui.item.href;
//                                                                                                                                    }
//                                                                                                                                    return false;
//                                                                                                                                }, 'search': function (event, ui) {
//                                                                                                                                    $(this).makeOrRemoveTogglableSpinner(true, "#app-search")
//                                                                                                                                }, 'open': function (event, ui) {
//                                                                                                                                    $(this).makeOrRemoveTogglableSpinner(false, "#app-search")
//                                                                                                                                }, 'position': {'my': 'right top', 'at': 'right bottom'}, 'source': '/app/index.php/zurmo/default/globalSearchAutoComplete'});
//                                                                                                                            $("#globalSearchInput").data("autocomplete")._renderItem = function (ul, item) {
//                                                                                                                                return $("<li></li>").data("item.autocomplete", item)
//                                                                                                                                        .append("<a><span class=" + item.iconClass + "></span><span>" + item.label + "</span></a>")
//                                                                                                                                        .appendTo(ul);
//                                                                                                                            };
//                                                                                                                            $(".nav li").hover(
//                                                                                                                                    function () {
//                                                                                                                                        if ($(this).hasClass("parent")) {
//                                                                                                                                            $(this).addClass("over");
//                                                                                                                                        }
//                                                                                                                                    },
//                                                                                                                                    function () {
//                                                                                                                                        $(this).removeClass("over");
//                                                                                                                                    }
//                                                                                                                            );


//                                                                                                                            jQuery("body").ajaxComplete(
//                                                                                                                                    function (event, request, options)
//                                                                                                                                    {
//                                                                                                                                        if (request.responseText == "sessionTimeout")
//                                                                                                                                        {
//                                                                                                                                            $.cookie("sessionTimeoutCookie", 1,
//                                                                                                                                                    {
//                                                                                                                                                        expires: 1,
//                                                                                                                                                        path: "/"
//                                                                                                                                                    });
//                                                                                                                                            window.location.reload(true);
//                                                                                                                                        }
//                                                                                                                                    }
//                                                                                                                            );

//                                                                                                                        });
                    /*]]>*/
                </script>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">
                    Cancel
                </button>
                <button class="btn btn-primary" id="formSave" type="button">
                    Save
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>