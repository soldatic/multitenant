<?php
/* @var $this TemplatesController */
/* @var $model Templates */

$this->breadcrumbs = array(
    'Templates' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'Create Templates', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#templates-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Templates</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
echo CHtml::form();
echo CHtml::submitButton('Activate', array('name' => 'Activate'));
echo CHtml::submitButton('Disactivate', array('name' => 'Disactivate'));
?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'templates-grid',
    'selectableRows' => 2,
    'dataProvider' => $model->search(),
//    'filter' => $model,
    'columns' => array(
        'tpl_id',
        array('class' => 'CCheckBoxColumn',
            'id' => 'tpl_id',
        ),
        'tpl_name',
        'tpl_description',
        'tpl_html',
        'tpl_blocks',
        'tpl_status' => array(
            'name' => 'tpl_status',
            'value' => '($data->tpl_status==0)?"Not_Active":"Active"',
            'filter' => array(0 => "Active", 1 => "Not Active"),
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>

<?php
echo CHtml::endForm();
