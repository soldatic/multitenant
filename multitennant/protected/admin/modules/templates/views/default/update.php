<?php
/* @var $this TemplatesController */
/* @var $model Templates */

$this->breadcrumbs = array(
    'Templates' => array('index'),
    $model->tpl_id => array('view', 'id' => $model->tpl_id),
    'Update',
);

$this->menu = array(
    array('label' => 'List Templates', 'url' => array('admin')),
    array('label' => 'Create Templates', 'url' => array('create')),
    array('label' => 'View Templates', 'url' => array('view', 'id' => $model->tpl_id)),
//	array('label'=>'Manage Templates', 'url'=>array('admin')),
);
?>

<h1>Update Templates <?php echo $model->tpl_id; ?></h1>

<?php
$this->renderPartial('_form', array('model' => $model));
