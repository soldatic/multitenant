<div id="accordion">
    <div class="tool-menu tool-in-panel">
        <h4> Menu</h4>
        <br/>
        <div class="dd" id="nestable-menu">
            <ol class="dd-list">
                
            </ol>
        </div>
        <div class="page-title">
            <button class="btn btn-sm btn-primary" id="menu-add"><span class="glyphicon glyphicon-plus colorWhite"></span> Add New Menu Item</button>
        </div>
    </div>


    <div class="tool-image tool-in-panel">
        <h4>Image Properties</h4>
        <br/>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h6 >Change picture</h6>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="btn btn-sm btn-primary buttonForGallery" ><i class="fa fa-picture-o"></i> Gallery</div>
                </div>
               <!--  <div class="col-lg-6">
                   <div  class="btn btn-primary"><i class="fa fa-cloud-upload"></i> Upload</div>
               <div class="fileUpload btn btn-sm btn-primary">
                                           <span><i class="fa fa-cloud-upload"></i> Upload</span>
                                           <input type="file" class="upload" />
                                       </div>
                   <button class="btn btn-sm btn-primary" id="uploadImgFloat"><span><i class="fa fa-cloud-upload"></i> Upload</span></button>
               </div> -->
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="preview">
                        <img class="img-responsive" alt='' id="imgPropThumb" src="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <form class="smart-form" id="imgSettings">
                        <ul style="list-style: none">
                           <!--  <li>
                                       <label class="label">Image Title</label>
                                       <label class="input">
                                           <input type="text" name="pageName" id="" class="image title" value="" style="height: 20px;   padding: 0px 3px;">
                                       </label>
                                   </li>
                                   <li>
                                       <label class="label">Size : 312 kb</label>
                                   </li>
                                    -->
                             <li>
                                <label class="label">Resolution : <span class="imgWidthForPreviewProp"></span> * <span class="imgHeightForPreviewProp"></span></label>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div id="linktabImg">
                        <form class="smart-form">
                            <ul style="list-style: none">
                                <li>
                                    <label class="toggle colorWhite">
                                        <input type="checkbox" name="imageLink" >
                                        <i data-swchoff-text="OFF" data-swchon-text="ON"></i><h6>Link On Image</h6>
                                    </label>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="radio">
                                                <input type="radio" name="typeImageHref" checked="checked" value="url">
                                                <i></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-8">
                                            <label class="label colorWhite">Website URL</label>
                                            <label class="input"> 
                                                <input type="text" class="input-xs" name="imageUrl" placeholder="URL" class="urlFromImg">
                                            </label>  
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="radio">
                                                <input type="radio" name="typeImageHref" value="page">
                                                <i></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-8">
                                            <label class="label colorWhite">Site Page</label>
                                            <label class="select">
                                                <select name="imagePageUrl" class="linkToSitePage">

                                                </select> <i></i> 
                                            </label>
                                        </div>
                                    </div>

                                </li>

<!--                                <li>-->
<!--                                    <div class="row marginButom">-->
<!--                                        <div class="col-lg-3">-->
<!--                                            <label class="radio">-->
<!--                                                <input type="radio" name="radio">-->
<!--                                                <i></i>-->
<!--                                            </label>-->
<!--                                        </div>-->
<!--                                        <div class="col-lg-8">-->
<!--                                            <label class="label colorWhite">Anchor to</label>-->
<!--                                            <label class="select"><select name="siteList" class="linkToSitePage">-->
<!--                                                    <option value="0" selected="selected">Anchor Page</option>-->
<!--                                                    <option value="/index.html">Anchor Home</option>-->
<!--                                                    <option value="/about.html">Anchor About</option>-->
<!--                                                    <option value="/post.html">Anchor Sample Post</option>-->
<!--                                                    <option value="/contact.html">Anchor Contacts</option>-->
<!--                                                </select> <i></i> -->
<!--                                            </label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </li>-->
                                <li>
                                    <label class="toggle colorWhite">
                                        <input type="checkbox" name="imageTargetUrl">
                                        <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Target in new window
                                    </label>
                                </li>
                                <li class="ligthBar">
                                    <label class="toggle colorWhite">
                                        <input type="checkbox" name="imageLightbox">
                                        <i data-swchoff-text="OFF" data-swchon-text="ON"></i><h6>Lightbox</h6>
                                    </label>
                                </li>
                            </ul>
                        </form>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tool-map tool-in-panel">
        <h4>Map Settings</h4>
        <form class="smart-form">

            <div class="paddingInTab">
                <section>
                    <label class="input">
                        <input type="text" class="input-xs" id="mapTitle" placeholder="Title">
                    </label>
                </section>
                <!--</div>-->
                <!--<div class="row">-->
                <section>
                    <label class="input">
                        <input type="text" class="input-xs" id="mapAddress" placeholder="Address">
                    </label>
                </section>
                <!--</div>-->
                <!--<div class="row">-->
                <section>
                    <label>Detailed locations</label>
                    <div class="row">
                        <div class="col col-6">
                            <label class="input">
                                <input type="text" class="input-xs" class="input-xs" id="mapLocationsLat" placeholder="Latitude">
                            </label>
                        </div>
                        <div class="col col-6">
                            <label class="input">
                                <input type="text" class="input-xs" class="input-xs" id="mapLocationsLng" placeholder="Longitude">
                            </label>
                        </div>
                    </div>
                </section>
                <!--</div>-->
                <!--<div class="row">-->
                <section id="mapOptions">
                    <label>Options</label>
                    <ul style="list-style: none">
<!--                        <li>-->
<!--                            <label class="toggle colorWhite">-->
<!--                                <input type="checkbox" name="checkbox-toggle" id="mapPanControl" checked="checked">-->
<!--                                <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Panoram Control-->
<!--                            </label>-->
<!--                        </li>-->
                        <li>
                            <label class="toggle colorWhite">
                                <input type="checkbox" name="checkbox-toggle" id="mapStreetViewControl" checked="checked">
                                <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Street View Control
                            </label>
                        </li>
                        <li>
                            <label class="toggle colorWhite">
                                <input type="checkbox" name="checkbox-toggle" id="mapMapTypeControl" checked="checked">
                                <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Map Type Control
                            </label>
                        </li>
                        <li>
                            <label class="toggle colorWhite">
                                <input type="checkbox" name="checkbox-toggle" id="mapScaleControl" checked="checked">
                                <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Scale Control
                            </label>
                        </li>
<!--                        <li>-->
<!--                            <label class="toggle colorWhite">-->
<!--                                <input type="checkbox" name="checkbox-toggle" id="mapOverviewMapControl" checked="checked">-->
<!--                                <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Overview Map Control-->
<!--                            </label>-->
<!--                        </li>-->
                        <li>
                            <label class="toggle colorWhite">
                                <input type="checkbox" name="checkbox-toggle" id="mapZoomControl" checked="checked">
                                <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Zoom Control
                            </label>
                        </li>
                        <li>
                            <label class="label">Zoom</label>
                            <label class="select">
                                <select id="mapOptionsZoom">
                                    <option value="1">1(far)</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17(close)</option>
                                </select>
                                <i></i>
                            </label>
                        </li>
                    </ul>
                </section>
            </div>
        </form>
    </div>

    <div class="tool-youtube tool-in-panel">
        <h4>YouTube Video Settings</h4>
        <div class="paddingInTab">
            <section>
                <label class="input">
                    <input type="text" class="input-xs" id="youtubeSettings" placeholder="URL">
                </label>
            </section>
        </div>
    </div>


    <div class="tool-features tool-in-panel">
        <h4> GRID Option</h4>
        <br/>
        <ul>            
            <li>
                <a href="javascript:void(0);" class=" smallBtn" data-type="twocol"><img src="/img/collumns/twocol.png" /></a>
                <a href="javascript:void(0);" class=" smallBtn" data-type="threecol"><img src="/img/collumns/threecol.png" /></a>                                           
                <a href="javascript:void(0);" class="smallBtn" data-type="fourcol"><img src="/img/collumns/fourcol.png" /></a>
                <a href="javascript:void(0);" class=" smallBtn" data-type="title"><img src="/img/collumns/smallleft.png" /></a>
                <a href="javascript:void(0);" class="smallBtn" data-type="title"><img src="/img/collumns/smallleftright.png" /></a>
                <a href="javascript:void(0);" class=" smallBtn" data-type="title"><img src="/img/collumns/smallright.png" /></a>
            </li>   
        </ul>
    </div>
    <div class="tool-gallery-options tool-in-panel">
        <h4> Gallery Option</h4>
        <br/>
        <ul>            
            <li>
                <a href="javascript:void(0);" class=" middleBtn" data-type="onepic"><img src="/img/gallery/onepic.png" /></a>
                <a href="javascript:void(0);" class=" middleBtn" data-type="twopic"><img src="/img/gallery/2pic.png" /></a>                                           
                <a href="javascript:void(0);" class="middleBtn" data-type="threepic"><img src="/img/gallery/3pic.png" /></a>
                <a href="javascript:void(0);" class=" middleBtn" data-type="sixpic"><img src="/img/gallery/6pic.png" /></a>
                <a href="javascript:void(0);" class="middleBtn" data-type="mosaic"><img src="/img/gallery/mosaic.png" /></a>
                <a href="javascript:void(0);" class=" middleBtn" data-type="mosaictwo"><img src="/img/gallery/mosaic2.png" /></a>
            </li>   
        </ul>
    </div>
</div>



