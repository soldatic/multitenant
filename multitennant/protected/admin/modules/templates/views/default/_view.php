<?php
/* @var $this TemplatesController */
/* @var $data Templates */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('tpl_id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->tpl_id), array('view', 'id' => $data->tpl_id)); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('tpl_name')); ?>:</b>
    <?php echo CHtml::encode($data->tpl_name); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('tpl_description')); ?>:</b>
    <?php echo CHtml::encode($data->tpl_description); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('tpl_html')); ?>:</b>
    <?php echo CHtml::encode($data->tpl_html); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('tpl_blocks')); ?>:</b>
    <?php echo CHtml::encode($data->tpl_blocks); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('tpl_status')); ?>:</b>
    <?php echo CHtml::encode($data->tpl_status); ?>
    <br />


</div>