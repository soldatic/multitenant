<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="mapSettings" class="modal fade" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                    ×
                </button>
                <h4 id="myModalLabel" class="modal-title">Map Settings</h4>
            </div>
            <div class="modal-body">
                <form class="smart-form">
                    <fieldset>
                        <div class="row">
                            <section>
                                <label class="input">
                                    <input type="text" id="mapTitle" placeholder="Title" />
                                </label>
                            </section>
                        </div>
                        <div class="row">
                            <section>
                                <label class="input">
                                    <input type="text" id="mapAddress" placeholder="Address" />
                                </label>
                            </section>
                        </div>
                        <div class="row">
                            <section>
                                <label>Detailed locations</label>
                                <div class="row">
                                    <div class="col col-6">
                                        <label class="input">
                                            <input type="text" id="mapLocationsLat" placeholder="Latitude" />
                                        </label>
                                    </div>
                                    <div class="col col-6">
                                        <label class="input">
                                            <input type="text" id="mapLocationsLng" placeholder="Longitude" />
                                        </label>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="row">
                            <section id="mapOptions">
                                <label>Options</label>
                                <div class="row">
                                    <div class="col col-6">
                                        <label class="toggle">
                                            <input type="checkbox"  name="checkbox-toggle" id="mapPanControl"/>
                                            <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Panoram Control
                                        </label>
                                        <label class="toggle">
                                            <input type="checkbox" checked="checked" name="checkbox-toggle" id="mapStreetViewControl"/>
                                            <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Street View Control
                                        </label>
                                        <label class="toggle">
                                            <input type="checkbox" checked="checked" name="checkbox-toggle" id="mapMapTypeControl"/>
                                            <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Map Type Control
                                        </label>
                                    </div>
                                    <div class="col col-6">
                                        <label class="toggle">
                                            <input type="checkbox" checked="checked" name="checkbox-toggle" id="mapScaleControl"/>
                                            <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Scale Control
                                        </label>
                                        <label class="toggle">
                                            <input type="checkbox" checked="checked" name="checkbox-toggle" id="mapOverviewMapControl"/>
                                            <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Overview Map Control
                                        </label>
                                        <label class="toggle">
                                            <input type="checkbox" checked="checked" name="checkbox-toggle" id="mapZoomControl"/>
                                            <i data-swchoff-text="OFF" data-swchon-text="ON"></i>Zoom Control
                                        </label>
                                        <label class="label">Zoom</label>
                                        <label class="select">
                                            <select id="mapOptionsZoom">
                                                <option value="1">1(far)</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17(close)</option>
                                            </select>
                                            <i></i>
                                        </label>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">
                    Cancel
                </button>
                <button class="btn btn-primary" id="mapSettingsSave" type="button">
                    Save & Update map
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>