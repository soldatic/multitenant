<div class="examples">
    <!--<div class="topic"><h2> Global CSS Style Options</h2></div>-->

    <div class="row">
        <div class="col-md-12 optionDisplay contWirthBtnForDisp">
            <h4>Position</h4>
            <p> Display Option</p>
            <!--<a href="javascript:void(0);" class="btn btn-default" rel="tooltip" data-placement="top" data-original-title="Tooltip Top"><i class="fa fa-angle-double-up"></i> Tooltip Top</a>-->
            <button href="javascript:void(0);" class=" tabBtnSM" data-position="block" id="block"   rel="tooltip" data-placement="top" data-original-title="Block"><i class="fa fa-desktop"></i></button>
            <button href="javascript:void(0);" class=" tabBtnSM" data-position="inline-block" id="inline-block"  rel="tooltip" data-placement="top" data-original-title="Inline Block"><i class="fa fa-table"></i></button>
            <button href="javascript:void(0);" class=" tabBtnSM" data-position="inline" id=inline" rel="tooltip" data-placement="top" data-original-title="Inline"><i class="fa fa-font"></i></button>
            <button href="javascript:void(0);" class=" tabBtnSM" data-position="none" id="none" rel="tooltip" data-placement="top" data-original-title="None" style="margin-left: 20px"><i class="fa fa-eye-slash"></i></button>
        </div>
        <div class="col-md-12">
            <p> Margin / Padding</p>
            <div class="marginBox">
                <p class="marginLable">margin</p>
                <p class="paddingLable">padding</p>
                <!--<div class="kit-button"></div>-->
                <!--<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-down" type="button">-</button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo3" type="text" value="" name="demo3" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn"><button class="btn btn-default bootstrap-touchspin-up" type="button">+</button></span></div>-->
                <div class="wrapper">
                    <div class="WithSpinnerEditToolTop">
                        <div class="spinnerFullEditTool LineHeightSpinEditTool">
                            <input id="inputMarginTop" data-margin="margin-top">
                            <div class="arrowContrlEditTool">
                                <div class="upArrow"><i class="fa fa-angle-double-up"></i></div>
                                <div class="downArrow"><i class="fa fa-angle-double-down"></i></div>
                            </div>
                            <!--<div class="pxIconEditTool">px</div>-->
                        </div>
                    </div>
                </div>

                <div class="box">
                    <div class="wrapper">
                        <div class="WithSpinnerEditToolTopInner">
                            <div class="spinnerFullEditTool LineHeightSpinEditToolInner">
                                <input id="inputPaddingTop" data-margin="padding-top">
                                <div class="arrowContrlEditTool">
                                    <div class="upArrow"><i class="fa fa-angle-double-up"></i></div>
                                    <div class="downArrow"><i class="fa fa-angle-double-down"></i></div>
                                </div>
                                <!--<div class="pxIconEditTool">px</div>-->
                            </div>
                        </div>
                    </div>
                    <div class="wrapper">
                        <div class="WithSpinnerEditToolButtomInner">
                            <div class="spinnerFullEditTool LineHeightSpinEditButtomlInner">
                                <input id="inputPaddingBottom" data-margin="padding-bottom">
                                <div class="arrowContrlEditTool">
                                    <div class="upArrow"><i class="fa fa-angle-double-up"></i></div>
                                    <div class="downArrow"><i class="fa fa-angle-double-down"></i></div>
                                </div>
                                <!--<div class="pxIconEditTool">px</div>-->
                            </div>
                        </div>
                    </div>
                    <div class="wrapper">
                        <div class="WithSpinnerEditToolLeftInner">
                            <div class="spinnerFullEditTool LineHeightSpinEditLeftInner">
                                <input id="inputPaddingLeft" data-margin="padding-left">
                                <div class="arrowContrlEditTool">
                                    <div class="upArrow"><i class="fa fa-angle-double-up"></i></div>
                                    <div class="downArrow"><i class="fa fa-angle-double-down"></i></div>
                                </div>
                                <!--<div class="pxIconEditTool">px</div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wrapper">
                    <div class="WithSpinnerEditToolRightInner">
                        <div class="spinnerFullEditTool LineHeightSpinEditRightInner">
                            <input id="inputPaddingRight" data-margin="padding-right">
                            <div class="arrowContrlEditTool">
                                <div class="upArrow"><i class="fa fa-angle-double-up"></i></div>
                                <div class="downArrow"><i class="fa fa-angle-double-down"></i></div>
                            </div>
                            <!--<div class="pxIconEditTool">px</div>-->
                        </div>
                    </div>
                </div>
            </div>


            <div class="wrapper">
                <div class="WithSpinnerEditToolBottom">
                    <div class="spinnerFullEditTool LineHeightSpinEditToolBottom">
                        <input id="inputMarginBottom" data-margin="margin-bottom">
                        <div class="arrowContrlEditTool">
                            <div class="upArrow"><i class="fa fa-angle-double-up"></i></div>
                            <div class="downArrow"><i class="fa fa-angle-double-down"></i></div>
                        </div>
                        <!--<div class="pxIconEditTool">px</div>-->
                    </div>
                </div>
            </div>
            <div class="wrapper">
                <div class="WithSpinnerEditToolLeft">
                    <div class="spinnerFullEditTool LineHeightSpinEditToolLeft">
                        <input id="inputMarginLeft"  data-margin="margin-left">
                        <div class="arrowContrlEditTool">
                            <div class="upArrow"><i class="fa fa-angle-double-up"></i></div>
                            <div class="downArrow"><i class="fa fa-angle-double-down"></i></div>
                        </div>
                        <!--<div class="pxIconEditTool">px</div>-->
                    </div>
                </div>
            </div>
            <div class="wrapper">
                <div class="WithSpinnerEditToolRight">
                    <div class="spinnerFullEditTool LineHeightSpinEditToolRight">
                        <input id="inputMarginRight" data-margin="margin-right">
                        <div class="arrowContrlEditTool">
                            <div class="upArrow"><i class="fa fa-angle-double-up"></i></div>
                            <div class="downArrow"><i class="fa fa-angle-double-down"></i></div>
                        </div>
                        <!--<div class="pxIconEditTool">px</div>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 optionDisplay contWirthBtnForFloat">
            <p> Float</p>

            <button href="javascript:void(0);" class=" tabBtnSM" id="noneFloat" data-float="none"  rel="tooltip" data-placement="top" data-original-title="None"><i class="fa fa-times"></i></button>
            <button href="javascript:void(0);" class=" tabBtnSM" id="leftFloat" data-float="left" rel="tooltip" data-placement="top" data-original-title="Left"><i class="fa fa-align-left"></i></button>
            <button href="javascript:void(0);" class=" tabBtnSM" id="rightFloat"data-float="right" rel="tooltip" data-placement="top" data-original-title="Right"><i class="fa fa-align-right"></i></button>
        </div>
        <div class="col-md-12 optionDisplay contWirthBtnForClear">

            <p> Clear</p>

            <button href="javascript:void(0);" class=" tabBtnSM" id="noneClear" data-clear="none"  rel="tooltip" data-placement="top" data-original-title="none"><i class="fa fa-times"></i></button>
            <button href="javascript:void(0);" class=" tabBtnSM" id="leftClear" data-clear="left" rel="tooltip" data-placement="top" data-original-title="left"><i class="fa fa-arrow-left"></i></button>
            <button href="javascript:void(0);" class=" tabBtnSM" id="rightClear"data-clear="right" rel="tooltip" data-placement="top" data-original-title="right"><i class="fa fa-arrow-right"></i></button>
            <button href="javascript:void(0);" class=" tabBtnSM" id="bothClear" data-clear="both" rel="tooltip" data-placement="top" data-original-title="both" style="padding: 6px 6px"><i class="fa fa-arrow-left"></i><i class="fa fa-arrow-right"></i></button>
        </div>
        <div class="col-md-12  contWirthBtnForOverflow">

            <p> Overflow</p>

            <button href="javascript:void(0);" class=" tabBtnSM" id="visibleOverflow" data-overflow="visible"  rel="tooltip" data-placement="top" data-original-title="visible"><i class="fa fa-eye"></i></button>
            <button href="javascript:void(0);" class=" tabBtnSM" id="hiddenOverflow"  data-overflow="hidden" rel="tooltip" data-placement="top" data-original-title="hidden"><i class="fa fa-eye-slash"></i></button>
            <button href="javascript:void(0);" class=" tabBtnSM" id="scrollOverflow" data-overflow="scroll" rel="tooltip" data-placement="top" data-original-title="scroll"><i class="fa fa-list-alt"></i></button>
            <button href="javascript:void(0);" class=" tabBtnSM" id="autoOverflow" data-overflow="auto" rel="tooltip" data-placement="top" data-original-title="auto" ><i class="fa fa-font"></i></button>
        </div>
        <div class="col-md-12 optionDisplay contWirthBtnForPosition">

            <p> Position</p>

            <button href="javascript:void(0);" class=" tabBtnSM" id="staticPosition" data-position="static" rel="tooltip" data-placement="top" data-original-title="static"><i class="fa fa-file"></i></button>
            <button href="javascript:void(0);" class=" tabBtnSM" id="relativePosition" data-position="relative" rel="tooltip" data-placement="top" data-original-title="relat ive"><i class="fa fa-files-o"></i></button>
            <button href="javascript:void(0);" class=" tabBtnSM" id="absolutePosition" data-position="absolute" rel="tooltip" data-placement="top" data-original-title="absol ute"><i class="fa fa-paste "></i></button>
            <button href="javascript:void(0);" class=" tabBtnSM" id="fixedPosition" data-position="fixed" rel="tooltip" data-placement="top" data-original-title="fixed"><i class="fa fa-paperclip"></i></button>
        </div>

        <div class="wrapperHiddenPositionSettings">
            <div class="positionFloatIcon">            
                <p style="float: left"> Top</p> 
                <div class="contWithInput chekInput" id="inputWithSpinner-1" style="margin-left: -14px;">
                    <input data-position="px" data-style="top" id="inputPositionTop" class="inputSpinners" type="text">
                    <div class="arrowInSpinner">
                        <div class="upArrowInSpinner"><i class="fa fa-arrow-up"></i></div>
                        <div class="downArrowInSpinner"><i class="fa fa-arrow-down"></i></div>
                    </div>
                    <span class="listOfpx">px</span>
                    <ul class="hiddenDropdaunInSpinner">
                        <li>px</li>
                        <li>auto</li>
                        <li>%</li>
                    </ul>
                </div>
            </div>
            <div class="positionFloatIcon">            
                <p style="float: left"> Bottom</p> 
                <div class="contWithInput chekInput" id="inputWithSpinner-2">
                    <input data-position="px" data-style="bottom" id="inputPositionBottom" class="inputSpinners" type="text">
                    <div class="arrowInSpinner">
                        <div class="upArrowInSpinner"><i class="fa fa-arrow-up"></i></div>
                        <div class="downArrowInSpinner"><i class="fa fa-arrow-down"></i></div>
                    </div>
                    <span class="listOfpx">px</span>
                    <ul class="hiddenDropdaunInSpinner">
                        <li>px</li>
                        <li>auto</li>
                        <li>%</li>
                    </ul>
                </div>
            </div>
            <div class="positionFloatIcon" style="margin-top: 15px;">            
                <p> Left</p>
                <div class="contWithInput chekInput" id="inputWithSpinner-3" style="margin-left: -14px;margin-top: -25px;">
                    <input data-position="px" data-style="left" id="inputPositionLeft" class="inputSpinners" type="text">
                    <div class="arrowInSpinner">
                        <div class="upArrowInSpinner"><i class="fa fa-arrow-up"></i></div>
                        <div class="downArrowInSpinner"><i class="fa fa-arrow-down"></i></div>
                    </div>
                    <span class="listOfpx">px</span>
                    <ul class="hiddenDropdaunInSpinner">
                        <li>px</li>
                        <li>auto</li>
                        <li>%</li>
                    </ul>
                </div>

            </div>
            <div class="positionFloatIcon" style="margin-top: 15px;">            
                <p> Right</p>
                <div class="contWithInput chekInput" id="inputWithSpinner-4" style="margin-top: -25px;">
                    <input data-position="px" data-style="right" id="inputPositionRight" class="inputSpinners" type="text">
                    <div class="arrowInSpinner">
                        <div class="upArrowInSpinner"><i class="fa fa-arrow-up"></i></div>
                        <div class="downArrowInSpinner"><i class="fa fa-arrow-down"></i></div>
                    </div>
                    <span class="listOfpx">px</span>
                    <ul class="hiddenDropdaunInSpinner">
                        <li>px</li>
                        <li>auto</li>
                        <li>%</li>
                    </ul>
                </div>
            </div>

            <div class="positionFloatIcon positionZIndex">            
                <p style="margin-left: -20px;"> Z-Index</p>
                <div class="contWithInput chekInput" id="inputWithSpinner-5" style="margin-left: -14px; margin-top: -25px;">
                    <input data-position="px" data-style="zindex" id="zIndex" class="inputSpinners" type="text" data-zindex="z-index">
                    <div class="arrowInSpinner">
                        <div class="upArrowInSpinner zindexArrowUP"><i class="fa fa-arrow-up"></i></div>
                        <div class="downArrowInSpinner"><i class="fa fa-arrow-down"></i></div>
                    </div>
<!--                    <span class="listOfpx">px</span>
                    <ul class="hiddenDropdaunInSpinner">
                        <li>px</li>
                        <li>auto</li>
                        <li>%</li>
                    </ul>-->
                </div>
            </div>
        </div>
    </div>
    <br>

    <!--Background-->
    <div class="row">
        <div class="col-md-12">
            <h4>Background</h4>
        </div>
        <div class="col-md-12">
            <p>Image:</p>
        </div>
        <div class="col-md-12">
            <div class="col-md-5">
                <button id="assetsBgModal" class="btn bg-color-blueDark btn-sm"><i class="fa fa-picture-o"></i> Assets</button>
            </div>
            <div class="col-md-5">
                <button class="btn uploadImgBg bg-color-blueDark btn-sm"><i class="fa fa-download"></i> Upload</button>
            </div>

            <!--Modal with img bg-->
            <div class="modalWithBgImg">
               <div class="contWithprevBg">
                   <?php
                   foreach ($images as $gallery) {
                       $folder = $domainId;
                       if ($gallery->tpl_up_status == 0) {
                           $folder = 'galleryManager';
                       }
                       ?>
                       <div class="preview">
                           <span class="iconOnPrew iconOnPrewImgZoom"><span class="glyphicon glyphicon-zoom-in"></span></span>
                           <span class="iconOnPrew iconOnPrewImgTrash"><span class="glyphicon glyphicon-trash"></span></span>
                           <img class="img-responsive showPreview" alt='Alt picture'
                                src="<?php echo Yii::app()->request->baseUrl . '/upload/' . $folder . '/thumb_' . $gallery->tpl_up_image; ?>"
                                data-img="<?php echo Yii::app()->request->baseUrl . '/upload/' . $folder . '/' . $gallery->tpl_up_image; ?>"
                                data-id="<?php echo $gallery->tpl_up_id; ?>" />
                       </div>
                   <?php } ?>
               </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-5">
                <img class="bgPrevievImg" src="<?php echo Yii::app()->request->baseUrl; ?>/upload/0/be152cff2d95e47fcc5ed1672e0d3501.jpg" alt="">
            </div>
            <div class="col-md-6">
                <div class="divWithInfoPrevBg">
                    <p id="bgPrevSize"><i class="fa fa-expand"></i> Size: <span></span></p>
                    <p id="bgPrevWeight"><i class="fa fa-cube"></i> Weight: <span></span></p>
                    <button class="bg-color-blueDark btn-sm btn" rel="tooltip" data-placement="top" data-original-title="Cover"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/backgroundIcon/coverI1.png" alt=""></button>
                    <button class="bg-color-blueDark btn-sm btn" rel="tooltip" data-placement="top" data-original-title="Contain"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/backgroundIcon/containI.png" alt=""></button>
                </div>
            </div>
        </div>
        <br>
        <div class="col-md-12">
            <br>
        </div>
        <div class="col-md-12">
            <p>Size:</p>
        </div>
        <div class="col-md-12 sizeBgEdit">
            <div class="col-md-1">
                <p class="titleBordWidth">Width:</p>
            </div>
            <div class="col-md-5">
                <div class="contWithInput chekInput" id="inputWithSpinner-BgWidth">
                    <!--<label for="inputSpinner1"><i class="fa fa-desktop"></i></label>-->
                    <input data-position="px" id="BgWidthinputSpinner1" class="inputSpinners" type="text">
                    <div class="arrowInSpinner">
                        <div class="upArrowInSpinner"><i class="fa fa-arrow-up"></i></div>
                        <div class="downArrowInSpinner"><i class="fa fa-arrow-down"></i></div>
                    </div>
                    <span class="listOfpx">px</span>
                    <ul class="hiddenDropdaunInSpinner">
                        <li>px</li>
                        <li>auto</li>
                        <li>%</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-1">
                <p class="titleBordWidth titleBgWidth">Tile:</p>
            </div>
        <div class="col-md-5">
            <div class="colWithBottom bgRepeatsbut">
                <button class="bg-color-blueDark btn-sm btn borderStyles" id="" data-border="None" rel="tooltip" data-placement="top" data-original-title="no-repeat">
                    <i class="fa fa-times"></i></button>
                <button class="bg-color-blueDark btn-sm btn borderStyles" id="" data-border="Dotted" rel="tooltip" data-placement="top" data-original-title="Repeat">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/backgroundIcon/repeat.png" alt=""></button>
                <button class="bg-color-blueDark btn-sm btn borderStyles" id="" data-border="Dashed" rel="tooltip" data-placement="top" data-original-title="repeat-x">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/backgroundIcon/repeatH.png" alt=""></button>
                <button class="bg-color-blueDark btn-sm btn borderStyles" id="" data-border="solid" rel="tooltip" data-placement="top" data-original-title="repeat-y">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/backgroundIcon/repeatV.png" alt=""></button>
            </div>
        </div>
            <div class="col-md-1">
                <p class="titleBordWidth titleBgHeight">Height:</p>
            </div>
            <div class="col-md-5">
                <div class="contWithInput chekInput" id="inputWithSpinner-BgHeight">
                    <!--<label for="inputSpinner1"><i class="fa fa-desktop"></i></label>-->
                    <input data-position="px" id="BgHeightinputSpinner1" class="inputSpinners" type="text">
                    <div class="arrowInSpinner">
                        <div class="upArrowInSpinner"><i class="fa fa-arrow-up"></i></div>
                        <div class="downArrowInSpinner"><i class="fa fa-arrow-down"></i></div>
                    </div>
                    <span class="listOfpx">px</span>
                    <ul class="hiddenDropdaunInSpinner">
                        <li>px</li>
                        <li>auto</li>
                        <li>%</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <br>
        </div>
        <div class="col-md-12">
            <p>Preset:</p>
        </div>
        <div class="col-md-12">
            <div class="col-md-6 presetColBg">
                <div class="col-md-2 firstColumnPreset">
                    <button data-positionh="0" data-positionv="0" class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                    <button data-positionh="0" data-positionv="50" class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                    <button data-positionh="0" data-positionv="100" class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                </div>
                <div class="col-md-2 secondColumnPreset">
                    <button data-positionh="50" data-positionv="0" class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                    <button data-positionh="50" data-positionv="50" class="bg-color-blueDark btn-sm btn"></button>
                    <button data-positionh="50" data-positionv="100" class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                </div>
                <div class="col-md-2 ThreeColumnPreset">
                    <button data-positionh="100" data-positionv="0" class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                    <button data-positionh="100" data-positionv="50" class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                    <button data-positionh="100" data-positionv="100" class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                </div>
            </div>
            <div class="resultPositionvVal">
                <button class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                <span id="posBgValV">0%</span>
                <button class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                <span id="posBgValH">0%</span>
            </div>
            <div class="col-md-5 adjustpostBg">
                <p class="titleBordWidth titleBgHeight">Adjust position:</p>
                <div class="col-md-1 firstColumnPreset">
                    <button class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                    <button id="adjustLeft"  class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                    <button class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                </div>
                <div class="col-md-1 secondColumnPreset">
                    <button id="adjustTop" class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                    <button class="bg-color-blueDark btn-sm btn"></button>
                    <button id="adjustBottom" class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                </div>
                <div class="col-md-1 ThreeColumnPreset">
                    <button class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                    <button id="adjustRight"  class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                    <button class="bg-color-blueDark btn-sm btn"><i class="fa fa-long-arrow-left"></i></button>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-7 BgColorAndFix">
                <p class="titleBordColor titleBGColor">Color:</p>
                <div class="contWithInput chekInput" id="inputWithSpinner-BGColor">
                    <input data-position="px" id="inputSpinnerColorBg" class="inputSpinners" type="text">
                </div>
            </div>
            <!--<div class="col-md-4 checkAttachmengBg">
                <label class="toggle">
                    <input type="checkbox" name="checkbox-toggle" checked="checked">
                    <i data-swchon-text="ON" data-swchoff-text="OFF"></i>Fixed
                </label>
            </div>-->
        </div>
    </div>




    <br>






    <!--Border-->
    <div class="row">
        <div class="col-md-12 optionDisplay">
            <h4>Border</h4>
        </div>

        <div class="col-md-12">
            <div class="col-md-1">
                <p class="titleBordWidth">Width:</p>
            </div>
            <div class="col-md-5">
                <div class="contWithInput chekInput" id="inputWithSpinner-borderWidth">
                    <!--<label for="inputSpinner1"><i class="fa fa-desktop"></i></label>-->
                    <input data-position="px" id="inputSpinner1" class="inputSpinners" type="text">
                    <div class="arrowInSpinner">
                        <div class="upArrowInSpinner"><i class="fa fa-arrow-up"></i></div>
                        <div class="downArrowInSpinner"><i class="fa fa-arrow-down"></i></div>
                    </div>
                    <span class="listOfpx">px</span>
                    <ul class="hiddenDropdaunInSpinner">
                        <li>px</li>
                        <li>auto</li>
                        <li>%</li>
                    </ul>
                </div>
                <p class="titleBordColor">Color:</p>
                <div class="contWithInput chekInput" id="inputWithSpinner-borderColor">
                    <input data-position="px" id="inputSpinnerColo" class="inputSpinners" type="text">
                </div>
            </div>
            <div class="col-md-5">
                <p class="titleBordStyle">Style:</p>
                <div class="colWithBottom">
                    <button class="bg-color-blueDark btn-sm btn borderStyles" id="" data-border="None" rel="tooltip" data-placement="top" data-original-title="None"><i class="fa fa-times"></i></button>
                    <button class="bg-color-blueDark btn-sm btn borderStyles" id="" data-border="Dotted" rel="tooltip" data-placement="top" data-original-title="Dotted"><span></span></button>
                    <button class="bg-color-blueDark btn-sm btn borderStyles" id="" data-border="Dashed" rel="tooltip" data-placement="top" data-original-title="Dashed"><span></span></button>
                    <button class="bg-color-blueDark btn-sm btn borderStyles" id="" data-border="solid" rel="tooltip" data-placement="top" data-original-title="solid"><span></span></button>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="borderCont">
                <div class="borderEdit">
                    <div data-borderdef="border-top-left-radius:50px; border-bottom-left-radius:50px;" data-border="TL-BL" class="leftRadiusBorder" rel="tooltip" data-placement="top" data-original-title="Oval Border"></div>
                    <div data-borderdef="border-top-left-radius:50px; border-top-right-radius:50px;" data-border="TL-TR" class="TopRadiusBorder" rel="tooltip" data-placement="top" data-original-title="Oval Border"></div>
                    <div data-borderdef="border-bottom-right-radius:50px; border-top-right-radius:50px;" data-border="TR-BR" class="RightRadiusBorder" rel="tooltip" data-placement="top" data-original-title="Oval Border"></div>
                    <div data-borderdef="border-bottom-left-radius:50px; border-bottom-right-radius:50px;" data-border="BR-BL" class="BottomRadiusBorder" rel="tooltip" data-placement="top" data-original-title="Oval Border"></div>
                    <div data-borderdef="border-top-left-radius:0px; border-bottom-left-radius:0px;" data-border="TL-BL" class="innerBorderLeft" rel="tooltip" data-placement="top" data-original-title="Square Border"></div>
                    <div data-borderdef="border-bottom-right-radius:0px; border-top-right-radius:0px;" data-border="TR-BR" class="innerBorderRight" rel="tooltip" data-placement="top" data-original-title="Square Border"></div>
                    <div data-borderdef="border-top-left-radius:0px; border-top-right-radius:0px;"  data-border="TL-TR" class="innerBorderTop" rel="tooltip" data-placement="top" data-original-title="Square Border"></div>
                    <div data-borderdef="border-bottom-left-radius:0px; border-bottom-right-radius:0px;" data-border="BR-BL" class="innerBorderBottom" rel="tooltip" data-placement="top" data-original-title="Square Border"></div>
                    <div data-borderdef="border-top-left-radius:50px;" data-border="TL" class="innerBorderLeftTopAngle" rel="tooltip" data-placement="top" data-original-title="Oval Corner"></div>
                    <div data-borderdef="border-top-right-radius:50px;" data-border="TR" class="innerBorderRightTopAngle" rel="tooltip" data-placement="top" data-original-title="Oval Corner"></div>
                    <div data-borderdef="border-bottom-right-radius:50px;" data-border="BR" class="innerBorderRightBottomAngle" rel="tooltip" data-placement="top" data-original-title="Oval Corner"></div>
                    <div data-borderdef="border-bottom-left-radius:50px;" data-border="BL" class="innerBorderLeftBottomAngle" rel="tooltip" data-placement="top" data-original-title="Oval Corner"></div>
                    <span class="prevSizeSquare activeSize" rel="tooltip" data-placement="top" data-original-title="Square"></span>
                    <h5 class="prevInnerBorder">Preview:</h5>
                    <span class="prevSizeRectangle" rel="tooltip" data-placement="top" data-original-title="Recta ngle"></span>
                    <div class="prevDivBorder" style=" " id="prevDivBorder"></div>
                </div>
                <div id="sliderBorderRad" style="height:168px;"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 optionDisplay" style="min-height: 250px;">
            <h4>Shadow</h4>
            <div class="row">
                <p style="position: absolute;top: 39px; left: 20px"> Type</p>
                <fieldset class="requiresjcrop">
                    <div class="tabShadowGroup">
                        <button class="tabBtnShadow" id="shadowOutside" data-shadow="">Outside</button>
                        <button class="tabBtnShadow" id="shadowInside" data-shadow="inset">Inside</button>
                    </div>
                </fieldset>
            </div>
            <div class="row">
                <p style="position: absolute;top: 80px; left: 20px;"> Color</p>
                <div class="colorpickerShadow" id="colorBoxShadow">
                    <input id="inputColorPickerShadowBox" class="inputSpinners" type="text">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p style="position: absolute;top: 10px; left: 20px;">Horizontal Length</p>
                    <div class="customSlider">
                        <div id="sliderShadowHorizntalLenght" class=""></div>
                    </div>
                    <div class="contWithInput chekInput" style="margin-left: 92px;">
                        <input id="inputShadowHorizntalLenght" class="inputSpinners" type="text">
                        <span class="listOfpx">px</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p style="position: absolute;top: 10px; left: 20px;">Vertical Length</p>
                    <div class="customSlider">
                        <div id="sliderShadowVerticalLenght" class=""></div>
                    </div>
                    <div class="contWithInput chekInput" style="margin-left: 92px;">
                        <input id="inputShadowVerticalLenght" class="inputSpinners" type="text">
                        <span class="listOfpx">px</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p style="position: absolute;top: 10px; left: 20px;">Blur Radius</p>
                    <div class="customSlider">
                        <div id="sliderShadowBlur" class=""></div>
                    </div>
                    <div class="contWithInput chekInput" style="margin-left: 92px;">
                        <input id="inputShadowBlur" class="inputSpinners" type="text">
                        <span class="listOfpx">px</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p style="position: absolute;top: 10px; left: 20px;">Spread Radius</p>
                    <div class="customSlider">
                        <div id="sliderShadowSpread" class=""></div>
                    </div>
                    <div class="contWithInput chekInput" style="margin-left: 92px;">
                        <input id="inputShadowSpread" class="inputSpinners" type="text">
                        <span class="listOfpx">px</span>
                    </div>
                </div>
            </div>
<!--            <div class="row">
                <div class="col-md-12">
                    <p style="position: absolute;top: 10px; left: 20px;">Opacity</p>
                    <div class="customSlider">
                        <div id="sliderShadowOpacity" class=""></div>
                    </div>
                    <div class="contWithInput chekInput" style="margin-left: 92px;">
                        <input id="inputShadowOpacity" class="inputSpinners" type="text">
                        <span class="listOfpx">px</span>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
    <!--<hr>-->
</div>