<div class="modal fade" id="changePicture1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ChangeImageOnHeaderModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Choose a picture for header</h4>
            </div>
            <div class="modal-body">
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-uploadPicture" data-widget-load="ajax/demowidget.php" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" role="widget">

                    <header role="heading">
                        <h2><strong>Change Header picture</strong> <i>Widget</i></h2>	

                        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                    </header>

                    <!-- widget div-->
                    <div role="content">

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">

                            <!-- widget body text-->

                            <div class="row hidden-mobile">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <h1 class="page-title txt-color-blueDark">
                                        <i class="fa-fw fa fa-picture-o"></i> 
                                        Gallery </h1>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-align-right">
                                    <div class="page-title">                                                
                                        <button type="button" class="btn btn-default" id="dropPicture">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <!-- SuperBox -->
                                <div class="superbox col-sm-12 selectPicture">
                                    <?php
                                    foreach ($images as $gallery) {
                                        $folder = $domainId;
                                        if ($gallery->tpl_up_status == 0) {
                                            $folder = 'galleryManager';
                                        }
                                        ?>
                                        <div class="superbox-list">
                                            
                                            <img src="<?php echo Yii::app()->request->baseUrl . '/upload/' . $folder . '/thumb_' . $gallery->tpl_up_image; ?>" 
                                                 data-img="<?php echo Yii::app()->request->baseUrl . '/upload/' . $folder . '/' . $gallery->tpl_up_image; ?>" 
                                                 class="superbox-img" data-id="<?php echo $gallery->tpl_up_id; ?>"/>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="superbox-float"></div>
                                </div>
                                <!-- /SuperBox -->

                                <div class="superbox-show" style="height:300px; display: none"></div>

                            </div>

                            <!-- end widget body text-->

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!--drop zone-->

                <div class="jarviswidget jarviswidget-sortable" id="wid-id-dropzone" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" role="widget">

                    <header role="heading">
                        <h2><strong> Drop / Give a link to Picture</strong> <i>Widget</i></h2>	

                        <ul id="widget-tab-1" class="nav nav-tabs pull-right">

                            <li class="active">

                                <a data-toggle="tab" href="#hrImg1"> <i class="fa fa-lg fa-arrow-circle-o-down"></i> <span class="hidden-mobile hidden-tablet"> PC </span> </a>

                            </li>

                            <li class="">
                                <a data-toggle="tab" href="#hrImg2"> <i class="fa fa-lg fa-arrow-circle-o-up"></i> <span class="hidden-mobile hidden-tablet"> URL </span></a>
                            </li>

                        </ul>	

                        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                    <!-- widget div-->
                    <div role="content">

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body no-padding">

                            <!-- widget body text-->

                            <div class="tab-content padding-10">
                                <div class="tab-pane fade active in" id="hrImg1">
                                    <form action="/admin/templates/ajax/uploadFiles" enctype="multipart/form-data" class="dropzone dz-clickable" id="mydropzone">
                                        <div class="dz-default dz-message">
                                            <span>Drop files here to upload</span>
                                        </div>
                                        <input type="hidden" name="dId" value="<?php echo $domainId; ?>" />
                                        <input type="hidden" name="tplId" value="<?php echo $tplId; ?>" />

                                        <!-- If you want control over the fallback form, just add it here: -->
                                        <div class="fallback"> <!-- This div will be removed if the fallback is not necessary -->
                                            <input type="file" name="youfilename" />
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="hrImg2">
                                    <form id="review-form" class="smart-form" novalidate="novalidate">
                                        <header>
                                            <h1>Insert URL of Picture</h1>
                                        </header>

                                        <fieldset>
                                            <div class="row">
                                                <section class="col col-12">
                                                    <label class="input"> <i class="icon-append fa fa-globe"></i>
                                                        <input type="text" name="name" id="urlToDownloadImage" placeholder="URL of picture" />
                                                    </label>
                                                    <button type="button" class="btn btn-primary downloadImage">
                                                        Download
                                                    </button>
                                                </section>
                                            </div>
                                        </fieldset>

                                    </form>
                                </div>
                            </div>

                            <!-- end widget body text-->  

                            <div class="widget-footer text-right">

                                <button type="button" class="btn btn-default" id="backGallery">
                                    Back to gallery
                                </button>
                                <!--                                        <button type="submit" class="btn btn-primary">
                                                                Save Picture
                                                                </button>-->

                            </div>

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->



                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>