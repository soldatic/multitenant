<button data-toggle="dropdown" id="glogalSettings" class="dropdown-toggle btn btn-success btn-lg" data-toggle="modal" data-target="#modalGlobalSettings">Settings</button>
<ul class="dropdown-menu">
    <li class="dropdown-submenu bg-menu">
        <a>Background <i class="icon-arrow-right"></i></a>
        <ul class="dropdown-menu sub-menu">
            <li class="dropdown-submenu">                                
                <a>Uniform Color <i class="icon-arrow-right"></i></a>
                <ul class="dropdown-menu sub-menu sub-menu2 uniform-color">
                    <li data-value="bg_dark"><a>Darken</a></li>
                    <li data-value="bg_green"><a>Green</a></li>
                    <li data-value="bg_red"><a>Red</a></li>
                    <li data-value="bg_blue_light"><a>Turquoise</a></li>
                    <li data-value="bg_blue"><a>Dark Blue</a></li>
                    <li data-value="bg_orange"><a>Orange</a></li>
                    <li data-value="bg_purple"><a>Purple</a></li>
                    <li data-value="bg_black"><a>Black</a></li>
                </ul>
            </li>
            <li class="dropdown-submenu">                                
                <a>People <i class="icon-arrow-right"></i></a>
                <ul class="dropdown-menu sub-menu sub-menu2 uniform-color">
                    <li data-value="bg_img_bg_parallax"><a>Sunflower</a></li>
                    <li data-value="bg_img_bg_business_guy"><a>Business Guy</a></li>
                </ul>
            </li>
            <li class="dropdown-submenu">
                <a>Landscape <i class="icon-arrow-right"></i></a>
                <ul class="dropdown-menu sub-menu sub-menu2 uniform-color">
                    <li data-value="bg_img_bg_flower_field"><a>Flowers Field</a></li>
                    <li data-value="bg_img_bg_landscape"><a>Landscape</a></li>
                    <li data-value="bg_img_bg_mountains"><a>Mountains</a></li>
                    <li data-value="bg_img_bg_greenfields"><a>Greenfields</a></li>
                </ul>
            </li>
            <li class="dropdown-submenu">
                <a>Various <i class="icon-arrow-right"></i></a>
                <ul class="dropdown-menu sub-menu sub-menu2 uniform-color">
                    <li data-value="bg_img_bg_aqua"><a>Aqua</a></li>
                    <li data-value="bg_img_bg_baby_blue"><a>Baby Blue</a></li>
                    <li data-value="bg_img_bg_black"><a>Black</a></li>
                    <li data-value="bg_img_bg_color_splash"><a>Color Splash</a></li>
                    <li data-value="bg_img_bg_mango"><a>Mango</a></li>
                    <li data-value="bg_img_bg_orange_red"><a>Orange Red</a></li>
                    <li data-value="bg_img_bg_flower"><a>Purple</a></li>
                    <li data-value="bg_img_bg_velour"><a>Velour</a></li>
                    <li data-value="bg_img_bg_wood"><a>Wood</a></li>
                    <li data-value="bg_img_bg_yellow_green"><a>Yellow Green</a></li>
                </ul>
            </li>
            <li><a class="body-bg-remove">None</a></li>
            <li><a style="background: none; padding: 5px; border-top: 1px solid #ddd;"></a></li>
            <li data-value="bg_custom_img" class="body-bg-choose"><a><b>Choose an image...</b></a></li>
        </ul>
    </li>
    <!--                            <li class="dropdown-submenu">
                                <a>Size <i class="icon-arrow-right"></i></a>
                                <ul class="dropdown-menu sub-menu size-menu">
                                <li data-value="none"><a>Full</a></li>
                                <li data-value="content-center"><a>Center</a></li>
                                </ul>
                                </li>-->
    <!--<li><a class="body-css" data-toggle="modal" data-target="#modalGlobalCss">Global CSS</a></li>-->
    <li><a class="body-css">Global CSS</a></li>
    <li><a class="body-settings">Global Settings</a></li>
</ul>