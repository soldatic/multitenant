<nav data-e-id="<?php echo rand(1111111, 9999999); ?>" class="navbar navbar-default menu">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#<?php echo $id_menu; ?>" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse menuJS" id="<?php echo $id_menu; ?>">
        <ul class="nav navbar-nav editorUL">
            <?php
            foreach ($menu_items as $page) {
                if ($page['menu_display'] == 1) {
                    if (isset($page['child']) && count($page['child']) > 0) {
                        ?>
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle"><?php echo $page['menu_name']; ?> <b
                                    class="caret"></b></a>
                            <ul class="dropdown-menu editorUL">
                                <?php
                                foreach ($page['child'] as $p) {
                                    if ($p['menu_menu_type'] == 'link') {
                                        if(is_numeric($p['menu_page_id']) && $p['menu_page_id'] > 0) {
                                            ?>
                                            <li>
                                                <a data-id="<?php echo $p['menu_page_id']; ?>"
                                                   <?php if ($p['menu_target'] == 1) { ?>target="_blank"<?php } ?>><?php echo $p['menu_name']; ?></a>
                                            </li>
                                            <?php
                                        } else {
                                            ?>
                                            <li>
                                                <a data-href="<?php echo $p['menu_url']; ?>"
                                                   <?php if ($p['menu_target'] == 1) { ?>target="_blank"<?php } ?>><?php echo $p['menu_name']; ?></a>
                                            </li>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <li>
                                            <a data-href="#"><?php echo $p['menu_name']; ?></a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </li>
                        <?php
                    } else {
                        if ($page['menu_menu_type'] == 'link') {
                            if(is_numeric($page['menu_page_id']) && $page['menu_page_id'] > 0) {
                                ?>
                                <li>
                                    <a data-id="<?php echo $page['menu_page_id']; ?>"
                                       <?php if ($page['menu_target'] == 1) { ?>target="_blank"<?php } ?>><?php echo $page['menu_name']; ?></a>
                                </li>
                                <?php
                            } else {
                                ?>
                                <li>
                                    <a data-href="<?php echo $page['menu_url']; ?>"
                                       <?php if ($page['menu_target'] == 1) { ?>target="_blank"<?php } ?>><?php echo $page['menu_name']; ?></a>
                                </li>
                                <?php
                            }
                        } else {
                            ?>
                            <li>
                                <a data-href="#"><?php echo $page['menu_name']; ?></a>
                            </li>
                            <?php
                        }
                        ?>
                        <?php
                    }
                }
            }
            ?>
        </ul>
    </div>
</nav>