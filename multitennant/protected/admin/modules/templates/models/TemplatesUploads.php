<?php

class TemplatesUploads extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cms_templates_uploads';
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'tpl_up_id' => 'Image ID',
            'tpl_up_title' => 'Image Title',
            'tpl_up_image' => 'Name File',
            'tpl_up_url' => 'Image Url',
            'tpl_up_length' => 'Image Size',
            'tpl_up_status' => 'Image Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('tpl_up_id', $this->tpl_up_id);
        $criteria->compare('tpl_up_title', $this->tpl_up_title, true);
        $criteria->compare('tpl_up_image', $this->tpl_up_image, true);
        $criteria->compare('tpl_up_url', $this->tpl_up_url, true);
        $criteria->compare('tpl_up_length', $this->tpl_up_length, true);
        $criteria->compare('tpl_up_status', $this->tpl_up_status, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Templates the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
