<?php

/**
 * This is the model class for table "cms_templates".
 *
 * The followings are the available columns in table 'cms_templates':
 */
class TemplatesPages extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cms_templates_pages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('tpl_page_status', 'length', 'max' => 1),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'tpl' => array(self::BELONGS_TO, 'Templates', 'tpl_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'tpl_page_id' => 'Page ID',
            'tpl_id' => 'Tpl ID',
            'tpl_parent_page_id' => 'Parent Page ID',
            'tpl_page_name' => 'Page Name',
            'tpl_page_title' => 'Page Title',
            'tpl_page_cpu' => 'Page Cpu',
            'tpl_page_content' => 'Page Content',
            'tpl_page_content_options' => 'Page Content Options',
            'tpl_page_settings' => 'Page Settings',
            'tpl_page_meta_settings' => 'Page Meta Settings',
            'tpl_page_keywords' => 'Page Keywords',
            'tpl_page_description' => 'Page Description',
            'tpl_page_position' => 'Page Position',
            'tpl_page_status' => 'Page Status',
            'tpl_page_display_menu' => 'Page Visible in Menu',
            'tpl_page_default' => 'Default Page Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('tpl_page_id', $this->tpl_page_id);
        $criteria->compare('tpl_id', $this->tpl_id);
        $criteria->compare('tpl_parent_page_id', $this->tpl_parent_page_id);
        $criteria->compare('tpl_page_name', $this->tpl_page_name);
        $criteria->compare('tpl_page_title', $this->tpl_page_title);
        $criteria->compare('tpl_page_cpu', $this->tpl_page_cpu);
        $criteria->compare('tpl_page_content', $this->tpl_page_content);
        $criteria->compare('tpl_page_content_options', $this->tpl_page_content_options);
        $criteria->compare('tpl_page_settings', $this->tpl_page_settings);
        $criteria->compare('tpl_page_meta_settings', $this->tpl_page_settings);
        $criteria->compare('tpl_page_keywords', $this->tpl_page_keywords);
        $criteria->compare('tpl_page_description', $this->tpl_page_description);
        $criteria->compare('tpl_page_position', $this->tpl_page_position);
        $criteria->compare('tpl_page_status', $this->tpl_page_status, true);
        $criteria->compare('tpl_page_display_menu', $this->tpl_page_display_menu, true);
        $criteria->compare('tpl_page_default', $this->tpl_page_default, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Templates the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
