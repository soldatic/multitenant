<?php

class DatabaseController extends AdminController {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
//            'postOnly', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'password'),
                'roles' => array('user'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'password'),
                'roles' => array('moderator'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'password'),
                'roles' => array('administrator'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('index', 'view', 'password', 'admin', 'delete', 'create', 'update', 'password'),
                'roles' => array('superadministrator'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new DatabaseUsers;

        $this->performAjaxValidation($model);

        if (isset($_POST['DatabaseUsers'])) {
            $model->attributes = $_POST['DatabaseUsers'];
            if ($model->save()) {
                if ($model->setDatabaseUser()) {
                    $this->redirect(array('view', 'id' => $model->db_user_id));
                }
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $model = $this->loadModel($id);
        if ($model->dropUsers()) {
            $model->delete();
        }

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('/database'));
    }

    /**
     * Manages all models.
     */
    public function actionIndex() {
        if (!empty($_POST)) {
            if (isset($_POST['Activate'])) {
                $model = Domain::model()->updateByPk($_POST['db_user_id'][0], array('status' => 1));
            }

            if (isset($_POST['Disactivate'])) {
                $model = Domain::model()->updateByPk($_POST['db_user_id'][0], array('status' => 0));
            }
        }

        $model = new DatabaseUsers('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['DatabaseUsers']))
            $model->attributes = $_GET['DatabaseUsers'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Multi the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = DatabaseUsers::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Multi $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'db-users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
