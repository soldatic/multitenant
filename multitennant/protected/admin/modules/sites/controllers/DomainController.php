<?php

class DomainController extends AdminController {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
//            'postOnly', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        if (Yii::app()->params->domain == 0) {
            return array(
                array('allow', // allow all users to perform 'index' and 'view' actions
                    'actions' => array('index', 'password'),
                    'roles' => array('user'),
                ),
                array('allow', // allow authenticated user to perform 'create' and 'update' actions
                    'actions' => array('index', 'view', 'password'),
                    'roles' => array('moderator'),
                ),
                array('allow', // allow authenticated user to perform 'create' and 'update' actions
                    'actions' => array('index', 'view', 'password'),
                    'roles' => array('administrator'),
                ),
                array('allow', // allow admin user to perform 'admin' and 'delete' actions
                    'actions' => array('index', 'view', 'password', 'admin', 'delete', 'create', 'update', 'password'),
                    'roles' => array('superadministrator'),
                ),
                array('deny', // deny all users
                    'users' => array('*'),
                ),
            );
        } else {
            return array(
                array('deny', // deny all users
                    'users' => array('*'),
                )
            );
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Domain;
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'domain-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Domain'])) {
            $model->attributes = $_POST['Domain'];
            if ($model->save()) {
                if ($model->setNewDatabaseAndUser()) {
                    $corePath = Yii::getPathOfAlias('core');
                    $url = $corePath . '/upload/' . $model->domain_id;
                    if (mkdir($url, 0777)) {
                        $view = $this->renderPartial('//mails/domain', array(
                            'domainName' => $_POST['Domain']['domain_name'],
                            'domainUrl' => $_POST['Domain']['domain_url'],
                            'domainDB' => $_POST['Domain']['domain_db'],
                            'domainDBUser' => $_POST['Domain']['domain_db_user'],
                            'domainDBUserPassword' => $_POST['Domain']['domain_db_user_pass'],
                            'domainAdminEmail' => $_POST['Domain']['domain_user'],
                            'domainAdminPassword' => $_POST['Domain']['domain_user_pass'],
                                ), true);
                        MailHelper::sendMail($view, Yii::t('main', 'Create Your Domain'), $_POST['Domain']['domain_user']);
                        $this->redirect(array('view', 'id' => $model->domain_id));
                    }
                }
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
//    public function actionUpdate($id) {
//        $model = $this->loadModel($id);
//        if (isset($_POST['ajax']) && $_POST['ajax'] === 'domain-form') {
//            echo CActiveForm::validate($model);
//            Yii::app()->end();
//        }
//        if (isset($_POST['Domain'])) {
//            $model->attributes = $_POST['Domain'];
//            if ($model->save()) {
//                if ($model->setNewDatabaseAndUser())
//                    $this->redirect(array('view', 'id' => $model->domain_id));
//            }
//        }
//
//        $this->render('update', array(
//            'model' => $model,
//            'list' => CHtml::listData(DatabaseUsers::model()->findAll(), 'db_user_id', 'db_user'),
//            'listUsers' => CHtml::listData(Users::model()->findAll("status = :status", array(":status" => "1")), 'uid', 'email')
//        ));
//    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $model = $this->loadModel($id);
        $corePath = Yii::getPathOfAlias('core');
        $url = $corePath . '/upload/' . $model->domain_id;
        if (rmdir($url)) {
            if ($model->dropDatabase()) {
                $model->delete();
            }
        }
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('/domain'));
    }

    /**
     * Manages all models.
     */
    public function actionIndex() {
        if (!empty($_POST)) {
            if (isset($_POST['Activate'], $_POST['domain_id'])) {
                foreach ($_POST['domain_id'] as $value) {
                    $model = Domain::model()->updateByPk($value, array('status' => 1));
                }
            }

            if (isset($_POST['Disactivate'], $_POST['domain_id'])) {
                foreach ($_POST['domain_id'] as $value) {
                    $model = Domain::model()->updateByPk($value, array('status' => 0));
                }
            }
        }

        $model = new Domain('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Domain']))
            $model->attributes = $_GET['Domain'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Multi the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Domain::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Multi $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'domain-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
