<?php

/**
 * This is the model class for table "cms_domains".
 *
 * The followings are the available columns in table 'cms_domains':
 * @property integer $domain_id
 * @property string $domain_name
 * @property string $domain_url
 * @property string $domain_db
 * @property string $domain_db_host
 */
class Domain extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cms_domains';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('domain_name, domain_url, domain_db, domain_db_user, domain_db_user_pass,'
                . 'domain_db_host, domain_user, domain_user_pass', 'required'),
            array('domain_name, domain_url, domain_db', 'unique'),
            array('domain_user', 'email'),
            array('domain_name, domain_url, domain_db, domain_db_host', 'length', 'max' => 255),
            // The following rule is used by search().
// @todo Please remove those attributes that should not be searched.
            array('domain_id, domain_name, domain_url, domain_db, domain_db_user, domain_db_host, domain_user', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'domain_id' => Yii::t('main', 'Domain'),
            'domain_name' => Yii::t('main', 'Domain Name'),
            'domain_url' => Yii::t('main', 'Domain Url'),
            'domain_db' => Yii::t('main', 'Domain Connection DB'),
            'domain_db_user' => Yii::t('main', 'Domain Connection DB User'),
            'domain_db_user_pass' => Yii::t('main', 'Domain Connection DB User Password'),
            'domain_db_host' => Yii::t('main', 'Domain Connection DB Host'),
            'domain_user' => Yii::t('main', 'Domain Admin Email'),
            'domain_user_pass' => Yii::t('main', 'Domain Admin Password'),
            'status' => Yii::t('main', 'Status'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
// @todo Please modify the following code to remove attributes that should not be searched.
//check string domain
        $domain = str_replace(array('/', ':', ' ', 'http', '?'), '', $this->domain_url);
        $db = str_replace(array('/', ':', ' ', 'http', '?', '.', ','), '', $this->domain_db);

        $criteria = new CDbCriteria;
        $criteria->compare('domain_id', $this->domain_id);
        $criteria->compare('domain_name', $this->domain_db, true);
        $criteria->compare('domain_url', $domain, true);
        $criteria->compare('domain_db', $db, true);
        $criteria->compare('domain_db_user', $this->domain_db_user, true);
        $criteria->compare('domain_db_user_pass', $this->domain_db_user_pass, true);
        $criteria->compare('domain_db_host', $this->domain_db_host, true);
        $criteria->compare('domain_user', $this->domain_user, true);
        $criteria->compare('domain_user_pass', $this->domain_user_pass, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Multi the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /*
     * Create new database and new user for new domain
     * @return  boolean GRANT ALL PRIVILEGES ON `:db`.`*` TO ':user'@':host' 
      IDENTIFIED BY ':password' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0
      MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
     */

    public function setNewDatabaseAndUser() {
        try {
            $checkUser = $this->checkExistUserDb();
            if (!$checkUser) {
                $this->setDatabaseUser();
            }
            $url = Yiibase::getPathOfAlias('admin') . '/sql/sql.sql';
            $date = date('Y-m-d H:i:s');
            $sqlContent = file_get_contents($url);
            $sqlContent = str_replace('{DB}', $this->domain_db, $sqlContent);
            $sqlContent = str_replace('{EMAIL}', $this->domain_user, $sqlContent);
            $sqlContent = str_replace('{PASSWORD}', $this->domain_user_pass, $sqlContent);
            $sqlContent = str_replace('{DATE}', $date, $sqlContent);

//            $userData = DatabaseUsers::model()->find('db_user_id = :user_id', array(':user_id' => $this->domain_db_user_id));
            $sql = "CREATE DATABASE IF NOT EXISTS `" . $this->domain_db . "` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci; "
                    . "GRANT ALL PRIVILEGES ON " . $this->domain_db . ".* TO " . $this->domain_db_user . "@" . $this->domain_db_host . " 
      IDENTIFIED BY '" . $this->domain_db_user_pass . "' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0
      MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;";
            $sql .= $sqlContent;
            
            $command = Yii::app()->db->createCommand($sql);
            $command->execute();

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function dropDatabase() {
        try {
            $sql = "DROP DATABASE `" . $this->domain_db . "`";

            $command = Yii::app()->db->createCommand($sql);

            $command->execute();
            
            $this->dropUsers();

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function setDatabaseUser() {
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $sql = "CREATE USER :user@:host IDENTIFIED BY :password;";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":user", $this->domain_db_user);
            $command->bindValue(":host", $this->domain_db_host);
            $command->bindValue(":password", $this->domain_db_user_pass);
            $command->execute();
            $transaction->commit();
            return true;
        } catch (Exception $ex) {
            $transaction->rollback();
            return false;
        }
    }

    public function checkExistUserDb() {
        $sql = "SELECT COUNT(*) as cnt FROM mysql.user WHERE User  = :user AND Host = :host";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":user", $this->domain_db_user);
        $command->bindValue(":host", $this->domain_db_host);
        $result = $command->query()->read();
        if ($result['cnt'] != 0) {
//            $this->addError($attribute, 'Wrong user ' . $this->domain_db_user . '@' . $this->domain_db_host);
            return false;
        }

        return true;
    }
//
//    public function checkUseUserDb() {
//        $sql = "SELECT COUNT(*) as cnt FROM `cms_domains` WHERE domain_db_user  = :user AND domain_db_host = :host";
//        $command = Yii::app()->db->createCommand($sql);
//        $command->bindValue(":user", $this->domain_db_user);
//        $command->bindValue(":host", $this->domain_db_host);
//        $result = $command->query()->read();
//
//        return $result['cnt'];
//    }

    public function dropUsers() {
        try {
            $sql = "DROP USER '" . $this->domain_db_user . "'@'" . $this->domain_db_host . "'";

            $command = Yii::app()->db->createCommand($sql);

            $command->execute();

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function beforeSave() {
        $this->domain_user_pass = md5($this->domain_user_pass);
        return parent::beforeSave();
    }

}
