<?php

/**
 * This is the model class for table "cms_database_users".
 *
 * The followings are the available columns in table 'cms_database_users':
 * @property integer $db_user_id
 * @property string $db_user
 * @property string $db_user_pass
 * @property string $db_host
 */
class DatabaseUsers extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cms_database_users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('db_user, db_user_pass, db_host', 'required'),
            array('db_user', 'unique'),
            array('db_user', 'checkExistUserDb'),
            array('db_user, db_user_pass', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('db_user_id, db_user, db_host', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'domain' => array(self::HAS_MANY, 'Domain', 'db_user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'db_user_id' => Yii::t('main','DB User ID'),
            'db_user' => Yii::t('main','Database User'),
            'db_user_pass' => Yii::t('main','Database User Password'),
            'db_host' => Yii::t('main','Database Host'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.
        //check string domain
        $criteria = new CDbCriteria;
        $criteria->compare('db_user_id', $this->db_user_id);
        $criteria->compare('db_user', $this->db_user, true);
        $criteria->compare('db_host', $this->db_host, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DatabaseUsers the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function setDatabaseUser() {
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $sql = "CREATE USER :user@:host IDENTIFIED BY :password;";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":user", $this->db_user);
            $command->bindValue(":host", $this->db_host);
            $command->bindValue(":password", $this->db_user_pass);
            $command->execute();
            $transaction->commit();
            return true;
        } catch (Exception $ex) {
            $transaction->rollback();
            return false;
        }
    }

    public function checkExistUserDb($attribute) {
        $sql = "SELECT COUNT(*) as cnt FROM mysql.user WHERE User  = :user AND Host = :host";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":user", $this->db_user);
        $command->bindValue(":host", $this->db_host);
        $result = $command->query()->read();
        if ($result['cnt'] != 0) {
            $this->addError($attribute, 'Wrong user ' . $this->db_user . '@' . $this->db_host);
            return false;
        }

        return true;
    }

    public function dropUsers() {
        try {
            $sql = "DROP USER '" . $this->db_user . "'@'" . $this->db_host . "'";

            $command = Yii::app()->db->createCommand($sql);

            $command->execute();

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

}
