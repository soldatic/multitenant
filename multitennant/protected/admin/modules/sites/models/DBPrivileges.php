<?php

/**
 * This is the model class for table "cms_db_privileges".
 *
 * The followings are the available columns in table 'cms_db_privileges':
 * @property integer $id
 * @property string $value_privileges
 * @property string $name_privileges
 */
class DBPrivileges extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cms_db_privileges';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array();
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Multi the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
