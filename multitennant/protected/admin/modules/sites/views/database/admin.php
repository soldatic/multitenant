<?php
/* @var $this DomainController */
/* @var $model Domain */

$this->breadcrumbs = array(
    Yii::t('main', 'Database Users') => array('index'),
    Yii::t('main', 'Manage'),
);

$this->menu = array(
    array('label' => Yii::t('main', 'Create DB User'), 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#multi-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('main', 'Manage Database Users') ?> </h1>


<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->
<?php
$this->beginWidget('zii.widgets.CPortlet', array(
    'title' => '',
));
$this->widget('zii.widgets.CMenu', array(
    'items' => $this->menu,
    'htmlOptions' => array('class' => 'param'),
));
$this->endWidget();
?>
<?php
echo CHtml::form();

echo '<div class="pull-left buttonintabbles">';
echo CHtml::link(Yii::t('main', 'Search'), '#', array('class' => 'search-button btn btn-primary '));
echo '</div>';
echo '<div class="pull-left buttonintabbles">';
//echo CHtml::submitButton('Activate', array('name' => 'Activate', 'class' => 'ui-pg-button ui-corner-all btn btn-sm btn-success'));
echo '</div>';
echo '<div class="pull-left buttonintabbles">';
//echo CHtml::submitButton('Disactivate', array('name' => 'Disactivate', 'class' => 'ui-pg-button ui-corner-all btn btn-sm btn-danger'));
echo '</div>';
?>

<?php
echo CHtml::form();

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'multi-grid',
//    'selectableRows' => 2,
    'dataProvider' => $model->search(),
//    'filter' => $model,
    'columns' => array(
        'db_user_id',
//        array(
//        'class' => 'CCheckBoxColumn',
//         'id' => 'db_user_id',
//        ),
        'db_user',
        'db_user_pass',
        'db_host',
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}{delete}'
        ),
    ),
));

echo CHtml::endForm();
