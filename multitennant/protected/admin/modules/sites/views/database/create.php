<?php
/* @var $this DatabaseUsersController */
/* @var $model DatabaseUsers */

$this->breadcrumbs = array(
    Yii::t('main', 'Database Users') => array('index'),
    Yii::t('main','Create'),
);

?>

<h1><?php echo Yii::t('main', 'Add New Database User') ?> </h1>

<?php
$this->renderPartial('_form', array('model' => $model));
