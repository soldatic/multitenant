<?php
/* @var $this DatabaseUsersController */
/* @var $data DatabaseUsers */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('db_user_id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->db_user_id), array('view', 'id' => $data->db_user_id)); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('db_user')); ?>:</b>
    <?php echo CHtml::encode($data->db_user); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('db_user_pass')); ?>:</b>
    <?php echo CHtml::encode($data->db_user_pass); ?>
    <br />


</div>