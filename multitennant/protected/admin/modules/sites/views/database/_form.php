<?php
/* @var $this DatabaseUsersController */
/* @var $model DatabaseUsers */
/* @var $form CActiveForm */
?>
<script>
    var keylist = "abcdefghijklmnopqrstuvwxyz123456789";
    var temp = '';

    function generatepass(plength) {
        temp = '';
        for (i = 0; i < plength; i++)
            temp += keylist.charAt(Math.floor(Math.random() * keylist.length));
        return temp;
    }

    function populateform() {
        document.getElementById('DatabaseUsers_db_user_pass').value = generatepass(10);
    }

    $(document).ready(function () {
<?php
if ($model->isNewRecord) {
    ?>
            populateform();
    <?php
}
?>
    })
</script>


<div class="row">
    <div class="widget-body  col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="widget-body updateForm col-xs-12 col-sm-12 col-md-5 col-lg-4">
            <div class="form smart-form">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'db-users-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => true
                    ),
                ));
                ?>
    <!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

                <fieldset>
                    <?php echo $form->errorSummary($model); ?>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Database User Name') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">
                                <?php echo $form->textField($model, 'db_user', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;',)); ?>
                                <?php echo $form->error($model, 'db_user'); ?>
                            </div>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'User Password') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">

                                <?php echo $form->textField($model, 'db_user_pass', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;',)); ?>
                                <?php echo CHtml::htmlButton(Yii::t('main', 'Generate Password'), array('onclick' => 'populateform();', 'class' => 'btn btn-primary pull-right', 'style' => 'margin: 5px 15px 0px 0px;')); ?>
                                <?php echo $form->error($model, 'db_user'); ?>
                            </div>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Database Host') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">

                                <?php echo $form->textField($model, 'db_host', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;',)); ?>
                                <?php echo $form->error($model, 'db_host'); ?>
                            </div>
                    </section>


                    <footer>
                        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), array('class' => 'btn btn-primary')); ?>
                    </footer>

                    <?php $this->endWidget(); ?>
                </fieldset>

            </div><!-- form -->
        </div><!-- widget-body no-padding -->
    </div>
</div>