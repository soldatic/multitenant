<?php
/* @var $this DatabaseUsersController */
/* @var $model DatabaseUsers */

$this->breadcrumbs = array(
    Yii::t('main', 'Database Users') => array('index'),
    $model->db_user_id,
);

$this->menu = array(
    array('label' =>Yii::t('main', 'List'), 'url' => array('index')),
//    array('label' => 'Add New Database User', 'url' => array('create')),
//    array('label' => Yii::t('main', 'Update'), 'url' => array('update', 'id' => $model->db_user_id)),
//    array('label' => Yii::t('main', 'Delete'), 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->db_user_id), 'confirm' => 'Are you sure you want to delete this item?')),
//    array('label' => 'Manage Database Users', 'url' => array('admin')),
);
?>

<h1>View Database User Data #<?php echo $model->db_user_id; ?></h1>
<?php
$this->beginWidget('zii.widgets.CPortlet', array(
    'title' => '',
));
$this->widget('zii.widgets.CMenu', array(
    'items' => $this->menu,
    'htmlOptions' => array('class' => 'param'),
));
$this->endWidget();
?>

<?php
echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
echo '<div class=" updateForm col-xs-12 col-sm-12 col-md-5 col-lg-4">';
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'db_user_id',
        'db_user',
        'db_user_pass',
        'db_host',
    ),
));
echo '</div>';
echo '</div>';
