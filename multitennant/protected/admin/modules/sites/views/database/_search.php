<?php
/* @var $this DomainController */
/* @var $model Domain */
/* @var $form CActiveForm */
?>

<div class="row">
    <div class="widget-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-6 col-lg-offset-1 col-lg-6">
            <div class="form smart-form">

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'action' => Yii::app()->createUrl($this->route),
                    'method' => 'get',
                ));
                ?>
                <fieldset>
                    <section>
                        <label class="label"><?php echo Yii::t('main', 'DB User ID') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">

                                <?php echo $form->textField($model, 'db_user_id', array('style' => 'margin: 0px 0px 0px 15px; width: 96%;')); ?>
                            </div>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Database User Name') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">

                                <?php echo $form->textField($model, 'db_user', array('style' => 'margin: 0px 0px 0px 15px; width: 96%;')); ?>
                            </div>
                    </section>


                    <footer>
                        <?php echo CHtml::submitButton(Yii::t('main', 'Search'), array('class' => 'btn btn-primary')); ?>
                    </footer>

                    <?php $this->endWidget(); ?>

            </div><!-- smart-form -->
        </div>
    </div>
</div>