<?php
/* @var $this DatabaseUsersController */
/* @var $model DatabaseUsers */

$this->breadcrumbs = array(
    Yii::t('main', 'Database Users') => array('index'),
    $model->db_user_id => array('view', 'id' => $model->db_user_id),
    Yii::t('main','Update'),
);

$this->menu = array(
    array('label' => Yii::t('main','List Database Users'), 'url' => array('index')),
    array('label' => Yii::t('main','Add New Database User'), 'url' => array('create')),
    array('label' => Yii::t('main','View Database User'), 'url' => array('view', 'id' => $model->db_user_id)),
    array('label' => Yii::t('main','Manage Database Users'), 'url' => array('admin')),
);
?>

<h1><?php echo Yii::t('main', 'Update Database User') ?> <i> <?php echo $model->db_user_id; ?> </i></h1>

<?php
$this->renderPartial('_form', array('model' => $model));
