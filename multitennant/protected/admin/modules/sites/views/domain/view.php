<?php
/* @var $this DomainController */
/* @var $model Domain */

$this->breadcrumbs = array(
    Yii::t('main', 'Users') => array('index'),
    $model->domain_name,
);

$this->menu = array(
    array('label' => Yii::t('main', 'List'), 'url' => array('index')),
//    array('label' => 'Create Domains', 'url' => array('create')),
//    array('label' => Yii::t('main', 'Update'), 'url' => array('update', 'id' => $model->domain_id)),
//    array('label' => Yii::t('main', 'Change Passwords'), 'url' => array('update', 'id' => $model->domain_id)),
//    array('label' => Yii::t('main', 'Delete'), 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->domain_id), 'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'))),
//    array('label' => 'Manage Domains', 'url' => array('admin')),
);
?>

<h1> <?php echo Yii::t('main', 'View Domain') ?> <i><?php echo $model->domain_name; ?> </i></h1>

<?php
$this->beginWidget('zii.widgets.CPortlet', array(
    'title' => '',
));
$this->widget('zii.widgets.CMenu', array(
    'items' => $this->menu,
    'htmlOptions' => array('class' => 'param'),
));
$this->endWidget();
?>

<?php
echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
echo '<div class=" updateForm col-xs-12 col-sm-12 col-md-5 col-lg-4">';
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model->with('users'),
    'attributes' => array(
        'domain_id',
        'domain_name',
        'domain_url',
        'domain_db',
        'domain_db_user',
        'domain_db_user_pass',
        'domain_user'
    ),
));

echo '</div>';
echo '</div>';
