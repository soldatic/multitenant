<?php
/* @var $this DomainController */
/* @var $data Domain */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('domain_id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->domain_id), array('view', 'id' => $data->domain_id)); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('domain_name')); ?>:</b>
    <?php echo CHtml::encode($data->domain_name); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('domain_url')); ?>:</b>
    <?php echo CHtml::encode($data->domain_url); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('domain_db')); ?>:</b>
    <?php echo CHtml::encode($data->domain_db); ?>
    <br />
    
    <b><?php echo CHtml::encode($data->getAttributeLabel('domain_user_id')); ?>:</b>
    <?php echo CHtml::encode($data->domain_db); ?>
    <br />

</div>