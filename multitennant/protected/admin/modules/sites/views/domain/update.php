<?php
/* @var $this DomainController */
/* @var $model Domain */

$this->breadcrumbs = array(
    Yii::t('main', 'Domains') => array('index'),
    $model->domain_id => array('view', 'id' => $model->domain_id),
    Yii::t('main', 'Update'),
);

$this->menu = array(
    array('label' => Yii::t('main', 'List'), 'url' => array('index')),
//    array('label' => 'Create Domains', 'url' => array('create')),
    array('label' => Yii::t('main', 'View'), 'url' => array('view', 'id' => $model->domain_id)),
//    array('label' => Yii::t('main', 'Delete'), 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->domain_id), 'confirm' => 'Are you sure you want to delete this item?')),
//    array('label' => 'Manage Domains', 'url' => array('admin')),
);
?>

<h1><?php echo Yii::t('main', 'Update Domain') ?> <i><?php echo $model->domain_id; ?></i></h1>

<?php
$this->beginWidget('zii.widgets.CPortlet', array(
    'title' => '',
));
 $this->widget('zii.widgets.CMenu', array(
    'items' => $this->menu,
    'htmlOptions' => array('class' => 'param'),
));
$this->endWidget();
?>

<?php
$this->renderPartial('_form', array(
    'model' => $model,
    'list' => $list,
    'listUsers' => $listUsers
));
