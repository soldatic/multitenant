<?php
/* @var $this DomainController */
/* @var $model Domain */

$this->breadcrumbs = array(
    Yii::t('main', 'Domains') => array('index'),
    Yii::t('main', 'Manage'),
);

$this->menu = array(
    array('label' => Yii::t('main', 'Create Domain'), 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#multi-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('main', 'Manage Domains') ?></h1>

<!--<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->


<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->beginWidget('zii.widgets.CPortlet', array(
    'title' => '',
));
$this->widget('zii.widgets.CMenu', array(
    'items' => $this->menu,
    'htmlOptions' => array('class' => 'param'),
));
$this->endWidget();
?>
<?php
echo CHtml::form();

echo '<div class="pull-left buttonintabbles">';
echo CHtml::link(Yii::t('main', 'Search'), '#', array('class' => 'search-button btn btn-primary '));
echo '</div>';
echo '<div class="pull-left buttonintabbles">';
echo CHtml::submitButton(Yii::t('main', 'Activate'), array('name' => 'Activate', 'class' => 'ui-pg-button ui-corner-all btn btn-sm btn-success'));
echo '</div>';
echo '<div class="pull-left buttonintabbles">';
echo CHtml::submitButton(Yii::t('main', 'Disactivate'), array('name' => 'Disactivate', 'class' => 'ui-pg-button ui-corner-all btn btn-sm btn-warning'));
echo '</div>';
?>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'multi-grid',
    'selectableRows' => 2,
    'dataProvider' => $model->search(),
//    'filter' => $model,
    'columns' => array(
        'domain_id',
        array('class' => 'CCheckBoxColumn',
            'id' => 'domain_id',
        ),
        'domain_name',
        'domain_url',
        'domain_db',
//        'domain_db_user',
        'domain_db_host',
        'domain_user',
        'status' => array(
            'name' => 'status',
            'value' => '($data->status==0)? "' . Yii::t('main', 'Not Active') . '":"' . Yii::t('main', 'Active') . '"',
            'filter' => array(0 => Yii::t('main', 'Active'), 1 => Yii::t('main', ' Not Active')),
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}{delete}'
        ),
    ),
));

echo CHtml::endForm();
