<?php
/* @var $this DomainController */
/* @var $model Domain */

$this->breadcrumbs = array(
    Yii::t('main', 'Domains') => array('index'),
    Yii::t('main', 'Create'),
);
?>

<h1><?php echo Yii::t('main', 'Add New Domain') ?></h1>

<?php
$this->renderPartial('_form', array(
    'model' => $model,
//    'list' => $list,
//    'listUsers' => $listUsers
));
