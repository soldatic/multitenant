<?php
/* @var $this DomainController */
/* @var $model Domain */
/* @var $form CActiveForm */
?>
<script>
    function populateform(val) {
        if (val === 0) {
            document.getElementById('Domain_domain_db_user_pass').value = generatepass(10);
        } else if (val === 1) {
            document.getElementById('Domain_domain_user_pass').value = generatepass(10);
        } else {
            document.getElementById('Domain_domain_db_user_pass').value = generatepass(10);
            document.getElementById('Domain_domain_user_pass').value = generatepass(10);
        }
    }

    $(document).ready(function () {
<?php
if ($model->isNewRecord) {
    ?>
            populateform();
    <?php
}
?>
    });

    $(document).ready(function () {
        $('#Domain_domain_url').change(function () {
            replaceVal($('#Domain_domain_url'), 'url');
            $('#Domain_domain_db').val($('#Domain_domain_url').val());
            replaceVal($('#Domain_domain_db'));
            $('#Domain_domain_db_user').val($('#Domain_domain_db').val());
        }).keyup(function () {
            replaceVal($('#Domain_domain_url'), 'url');
            $('#Domain_domain_db').val($('#Domain_domain_url').val());
            replaceVal($('#Domain_domain_db'));
            $('#Domain_domain_db_user').val($('#Domain_domain_db').val());
        });

        $('#Domain_domain_db').change(function () {
            replaceVal($('#Domain_domain_db'));
            $('#Domain_domain_db_user').val($('#Domain_domain_db').val());
            replaceVal($('#Domain_domain_db_user'));
        }).keyup(function () {
            replaceVal($('#Domain_domain_db'));
            $('#Domain_domain_db_user').val($('#Domain_domain_db').val());
            replaceVal($('#Domain_domain_db_user'));
        });

    });
</script>
<div class="row">
    <div class="widget-body  col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="widget-body updateForm col-xs-12 col-sm-12 col-md-5 col-lg-4">
            <div class="form smart-form">

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'domain-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                    'htmlOptions' => array(
                        'class' => 'smart-form client-form',
                    )
                ));
                ?>

    <!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->
                <fieldset>
                    <?php echo $form->errorSummary($model); ?>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Domain Name') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">
                                <?php echo $form->textField($model, 'domain_name', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;', 'placeholder' => Yii::t('main', 'Domain Name'))); ?>
                                <?php echo $form->error($model, 'domain_name'); ?>
                            </div>
                        </label>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Domain Url') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">
                                <?php echo $form->textField($model, 'domain_url', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;', 'placeholder' => Yii::t('main', 'Domain Url'))); ?>
                                <?php echo $form->error($model, 'domain_url'); ?>
                            </div>
                        </label>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Domain DataBase') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">
                                <?php echo $form->textField($model, 'domain_db', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;', 'placeholder' => Yii::t('main', 'Domain DataBase'))); ?>
                                <?php echo $form->error($model, 'domain_db'); ?>
                            </div>
                        </label>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Domain DataBase User') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">
                                <?php
                                echo $form->textField($model, 'domain_db_user', array(
                                    'style' => 'margin: 0px 0px 0px 15px; width: 94%;',
                                    'placeholder' => Yii::t('main', 'Domain DataBase User'),
                                    'readonly' => 'readonly'));
                                ?>
                                <?php echo $form->error($model, 'domain_db_user'); ?>
                            </div>
                        </label>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Domain DataBase User Password') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">
                                <?php echo $form->textField($model, 'domain_db_user_pass', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;', 'placeholder' => Yii::t('main', 'Domain DataBase User Password'))); ?>
                                <?php echo CHtml::htmlButton(Yii::t('main', 'Generate Password'), array('onclick' => 'populateform(0);', 'class' => 'btn btn-primary pull-right', 'style' => 'margin: 5px 15px 0px 0px;')); ?>
                                <?php echo $form->error($model, 'domain_db_user_pass'); ?>
                            </div>
                        </label>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Domain DataBase Host') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">
                                <?php echo $form->textField($model, 'domain_db_host', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;', 'placeholder' => Yii::t('main', 'Domain DataBase Host'))); ?>
                                <?php echo $form->error($model, 'domain_db_host'); ?>
                            </div>
                        </label>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Admin(Email)') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">
                                <?php echo $form->textField($model, 'domain_user', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;', 'placeholder' => Yii::t('main', 'Email'))); ?>
                                <?php echo $form->error($model, 'domain_user'); ?>
                            </div>
                        </label>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Admin(Password)') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">
                                <?php echo $form->textField($model, 'domain_user_pass', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;', 'placeholder' => Yii::t('main', 'Password'))); ?>
                                <?php echo CHtml::htmlButton(Yii::t('main', 'Generate Password'), array('onclick' => 'populateform(1);', 'class' => 'btn btn-primary pull-right', 'style' => 'margin: 5px 15px 0px 0px;')); ?>
                                <?php echo $form->error($model, 'domain_user_pass'); ?>
                            </div>
                        </label>
                    </section>

                    <footer>
                        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), array('class' => 'btn btn-primary')); ?>
                    </footer>

                    <?php $this->endWidget(); ?>
                </fieldset>

            </div><!-- form -->
        </div><!-- widget-body no-padding -->
    </div>
</div>