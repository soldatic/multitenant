<?php

/**
 * This is the model class for table "cms_users".
 *
 * The followings are the available columns in table 'cms_users':
 * @property integer $uid
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property string $create_date
 * @property string $last_login
 * @property string $update_date
 * @property string $status
 * @property string $forgot_key
 * @property string $forgot_date
 * @property string $email_key
 *
 * The followings are the available model relations:
 * @property RolesUsers[] $rolesUsers
 */
class Users extends CActiveRecord {

    const ROLE_SUPERADMIN = 'superadministrator';
    const ROLE_ADMIN = 'administrator';
    const ROLE_MODER = 'moderator';
    const ROLE_USER = 'user';
    
    public $first_name;

//    const ROLE_BANNED = 'banned';

    public $verifyCode;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cms_users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('first_name', 'safe'),
            array('first_name, last_name, email, password', 'required'),
            array('email', 'unique'),
            array('email', 'email'),
            array('first_name, last_name, email, password, phone, forgot_key, email_key', 'length', 'max' => 255),
            array('status', 'length', 'max' => 1),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('uid, first_name, last_name, email, password, phone, create_date, last_login, update_date, status, forgot_key, forgot_date, email_key', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rolesUsers' => array(self::HAS_MANY, 'RolesUsers', 'uid'),
            'domain' => array(self::HAS_MANY, 'Domain', 'uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'uid' =>  Yii::t('main','uid'),
            'first_name' => Yii::t('main','First Name'),
            'last_name' => Yii::t('main','Last Name'),
            'email' => Yii::t('main','Email'),
            'password' => Yii::t('main','Password'),
            'role' => Yii::t('main','Role'),
            'phone' => Yii::t('main','Phone'),
            'create_date' => Yii::t('main','Create Date'),
            'last_login' => Yii::t('main','Last Login'),
            'update_date' => Yii::t('main','Update Date'),
            'status' => Yii::t('main','Status'),
            'forgot_key' => Yii::t('main','Forgot Key'),
            'forgot_date' => Yii::t('main','Forgot Date'),
            'email_key' => Yii::t('main','Email Key'),       
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('uid', $this->uid);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('create_date', $this->create_date, true);
        $criteria->compare('last_login', $this->last_login, true);
        $criteria->compare('update_date', $this->update_date, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('forgot_key', $this->forgot_key, true);
        $criteria->compare('forgot_date', $this->forgot_date, true);
        $criteria->compare('email_key', $this->email_key, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeSave() {
        if ($this->isNewRecord)
//               $this->created=time();
            $this->role = 'user';

        $this->update_date = date('Y-m-d H:i:s');
        $this->password = md5($this->password);
        return parent::beforeSave();
    }

}
