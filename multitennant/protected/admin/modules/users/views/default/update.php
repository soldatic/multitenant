<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
   Yii::t('main', 'Users') => array('index'),
    $model->uid => array(Yii::t('main', 'view'), 'id' => $model->uid),
    Yii::t('main', 'Update'),
);
$this->menu = array(
    array('label' => Yii::t('main', 'List'), 'url' => array('index')),
//    array('label' => 'Create Users', 'url' => array('create')),
    array('label' => Yii::t('main', 'View'), 'url' => array('view', 'id' => $model->uid)),
//    array('label' => 'Delete', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->uid), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => Yii::t('main', 'Change Password'), 'url' => array('password', 'id' => $model->uid)),
);
?>
<h1><?php echo Yii::t('main', 'Update Users'); ?> <i><?php echo $model->first_name; ?></i></h1>

<?php
$this->beginWidget('zii.widgets.CPortlet', array(
    'title' => '',
));
$this->widget('zii.widgets.CMenu', array(
    'items' => $this->menu,
    'htmlOptions' => array('class' => 'param'),
));
$this->endWidget();
?>


<?php
$this->renderPartial('_form_update', array('model' => $model));
//fb($model);
