<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    Yii::t('main', 'Users') => array('index'),
    Yii::t('main', 'Manage'),
);

$this->menu = array(
    array('label' => Yii::t('main', 'Create User'), 'url' => array('/users/create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>



<?php if (Yii::app()->user->hasFlash('delete')) { ?>

    <script>
        $(window).load(function () {
            new PNotify({
                title: '<?php echo Yii::app()->user->getFlash('delete'); ?>',
                text: ''
            });
        })
    </script>

<?php } ?>


<?php if (Yii::app()->user->hasFlash('activate')) { ?>

    <script>
        $(window).load(function () {
            new PNotify({
                title: '<?php echo Yii::app()->user->getFlash('activate'); ?>',
                text: ''
            });
        })
    </script>

<?php } ?>

<?php if (Yii::app()->user->hasFlash('disactivate')) { ?>

    <script>
        $(window).load(function () {
            new PNotify({
                title: '<?php echo Yii::app()->user->getFlash('disactivate'); ?>',
                text: ''
            });
        });
    </script>

<?php } ?>    

<h1> <?php echo Yii::t('main', 'Manage Users') ?> </h1>



<!--<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->



<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->




<?php
$this->beginWidget('zii.widgets.CPortlet', array(
    'title' => '',
));
$this->widget('zii.widgets.CMenu', array(
    'items' => $this->menu,
    'htmlOptions' => array('class' => 'param'),
));
$this->endWidget();
?>
<?php
echo CHtml::form();

echo '<div class="pull-left buttonintabbles">';
echo CHtml::link(Yii::t('main', 'Search'), '#', array('class' => 'search-button btn btn-primary '));
echo '</div>';
echo '<div class="pull-left buttonintabbles">';
echo CHtml::submitButton(Yii::t('main', 'Activate'), array('name' => 'Activate', 'class' => 'ui-pg-button ui-corner-all btn btn-sm btn-success'));
echo '</div>';
echo '<div class="pull-left buttonintabbles">';
echo CHtml::submitButton(Yii::t('main', 'Disactivate'), array('name' => 'Disactivate', 'class' => 'ui-pg-button ui-corner-all btn btn-sm btn-warning'));
echo '</div>';
echo '<div class="pull-left buttonintabbles">';
echo CHtml::submitButton(Yii::t('main', 'Delete'), array('name' => 'Delete', 'class' => 'ui-pg-button ui-corner-all btn btn-sm btn-danger'));
echo '</div>';
?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'users-grid',
    'selectableRows' => 2,
    'dataProvider' => $model->search(),
//    'filter' => $model,
    'columns' => array(
        'uid',
        array('class' => 'CCheckBoxColumn',
            'id' => 'uid',
        ),
        'first_name',
        'last_name',
        'email',
//		'phone',
        'create_date',
        'status' => array(
            'name' => 'status',
            'value' => '($data->status==0)? "' . Yii::t('main', 'Not Active') . '":"' . Yii::t('main', 'Active').'"',
            'filter' => array(0 => Yii::t('main','Active'), 1 => Yii::t('main',' Not Active')),
        ),
        'role',
//            => array(
//                        'name' => 'role',
//                        'value' => '($data->role==)?"user":"moderator":"administrator":"superadministrator"',
//                        'filter' => array("user"=>"user","moderator"=>"moderator","administrator"=>"administrator","superadministrator"=>"superadministrator"),
//                                 ),

        /*
          'last_login',
          'update_date',
          'status',
          'forgot_key',
          'forgot_date',
          'email_key',
         */
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));

echo CHtml::endForm();
