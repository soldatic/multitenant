<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<div class="row">
    <div class="widget-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="widget-body updateForm col-xs-12 col-sm-12 col-md-5 col-lg-4">
            <div class="form smart-form">


                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'users-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                ));
                ?>


                <fieldset>
                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Password') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">
                                <?php echo $form->PasswordField($model, 'password', array('size' => 60, 'maxlength' => 255, 'value' => '', 'placeholder' => Yii::t('main', 'Password'), 'autocomplete' => "off", 'style' => 'margin: 0px 0px 0px 15px; width: 94%;')); ?>
                                <?php echo $form->error($model, 'password'); ?>
                            </div>                           
                    </section>


                    <footer>
                        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), array('class' => 'btn btn-primary')); ?>
                    </footer>

                    <?php $this->endWidget(); ?>
                </fieldset>

            </div><!-- form -->
        </div>
    </div>
</div>