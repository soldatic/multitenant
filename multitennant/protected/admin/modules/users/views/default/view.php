<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    Yii::t('main', 'Users') => array('index'),
    $model->first_name,
);

$this->menu = array(
    array('label' => Yii::t('main', 'List'), 'url' => array('index')),
//    array('label' => 'Create Users', 'url' => array('create')),
    array('label' => Yii::t('main', 'Update'), 'url' => array('update', 'id' => $model->uid)),
//    array('label' => 'Delete', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->uid), 'confirm' => 'Are you sure you want to delete this item?')),
);
?>

<h1><?php echo Yii::t('main', 'View') ?> <i><?php echo $model->first_name; ?></i></h1>

<?php
$this->beginWidget('zii.widgets.CPortlet', array(
    'title' => '',
));
$this->widget('zii.widgets.CMenu', array(
    'items' => $this->menu,
    'htmlOptions' => array('class' => 'param'),
));
$this->endWidget();
?>

<?php
echo CHtml::form();
echo '<div class="pull-left buttonintabbles">';
//echo CHtml::submitButton('Delete', array('name' => 'Delete', 'class' => 'ui-pg-button ui-corner-all btn btn-sm btn-danger'));
echo '</div>';
?>


<?php
echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
echo '<div class=" updateForm col-xs-12 col-sm-12 col-md-5 col-lg-4">';
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'uid',
        'first_name',
        'last_name',
        'email',
//        'password',
        'phone',
        'create_date',
        'last_login',
        'update_date',
        'status' => array(
            'name' => 'status',
            'value' => ($model->status==0)? Yii::t('main', 'Not Active') :Yii::t('main', 'Active') ,
        ),
//        'forgot_key',
//        'forgot_date',
//        'email_key',
    ),
));
echo '</div>';
echo '</div>';
