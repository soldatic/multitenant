<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    Yii::t('main', 'Users') => array('index'),
    Yii::t('main', 'Create'),
);

$this->menu = array(
    array('label' =>Yii::t('main', 'List Users'), 'url' => array('index')),
);
?>

<h1> <?php echo Yii::t('main', 'Create Users') ?> </h1>

<?php
$this->renderPartial('_form', array('model' => $model));
