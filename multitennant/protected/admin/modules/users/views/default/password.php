<?php
$this->menu = array(
    array('label' => Yii::t('main', 'List Users'), 'url' => array('index')),
//    array('label' => 'Create Users', 'url' => array('create')),   
    array('label' => Yii::t('main', 'View Users'), 'url' => array('view', 'id' => $model->uid)),
    array('label' => Yii::t('main', 'Back'), 'url' => array('update', 'id' => $model->uid)),
);
?>


<h1><?php echo Yii::t('main', 'Change password for') ?> <i><?php echo $model->first_name; ?></i></h1>

    
    <?php
$this->beginWidget('zii.widgets.CPortlet', array(
    'title' => '',
));
$this->widget('zii.widgets.CMenu', array(
    'items' => $this->menu,
    'htmlOptions' => array('class' => 'param'),
));
$this->endWidget();

$this->renderPartial('_password', array('model' => $model));

