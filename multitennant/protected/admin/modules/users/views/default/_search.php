<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>



<div class="row">
    <div class="widget-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-6 col-lg-offset-1 col-lg-6">
            <div class="form smart-form">

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'action' => Yii::app()->createUrl($this->route),
                    'method' => 'get',
                ));
                ?>
                <fieldset>
                    <section>
                        <label class="label"><?php echo Yii::t('main', 'User ID') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">

                                <?php echo $form->textField($model, 'uid', array('style' => 'margin: 0px 0px 0px 15px; width: 96%;')); ?>
                            </div>                          
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'First Name') ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">

                                <?php echo $form->textField($model, 'first_name', array('size' => 60, 'maxlength' => 255, 'style' => 'margin: 0px 0px 0px 15px; width: 96%;')); ?>
                            </div>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Last Name')?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">

                                <?php echo $form->textField($model, 'last_name', array('size' => 60, 'maxlength' => 255, 'style' => 'margin: 0px 0px 0px 15px; width: 96%;')); ?>
                            </div>
                    </section>


                    <section>
                        <label class="label"><?php echo Yii::t('main', 'E-mail')?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">

                                <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 255, 'style' => 'margin: 0px 0px 0px 15px; width: 96%;')); ?>
                            </div>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Phone number')?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">

                                <?php echo $form->textField($model, 'phone', array('size' => 60, 'maxlength' => 255, 'style' => 'margin: 0px 0px 0px 15px; width: 96%;')); ?>
                            </div>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Create Date')?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <div class="row">

                                <?php echo $form->textField($model, 'create_date', array('style' => 'margin: 0px 0px 0px 15px; width: 96%;')); ?>
                            </div>
                    </section>

                    <section>
                        <label class="label"><?php echo Yii::t('main', 'Status')?></label>

                        <div class="row">

                            <?php echo $form->dropDownList($model, 'status', array("" => "", 1 => Yii::t('main','Active'), 0 => Yii::t('main','Not Active')), array('style' => 'margin: 0px 0px 0px 15px; width: 30%;')); ?>
                        </div>

                    </section>


                    <footer>
                        <?php echo CHtml::submitButton(Yii::t('main', 'Search'), array('class' => 'btn btn-primary')); ?>
                    </footer>
                </fieldset>

                <?php $this->endWidget(); ?>

            </div><!-- smart-form -->
        </div>
    </div>
</div>