<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<div class="row">
    <div class="widget-body col-xs-12 col-sm-12 col-md-5 col-lg-4">
        <div class="form smart-form">

            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'users-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>



<!--        <p class="note">Fields with <span class="required">*</span> are required.</p>-->
            <fieldset>
                <?php echo $form->errorSummary($model); ?>



                <section>
                    <label class="label"><?php echo Yii::t('main', 'First Name') ?></label>
                    <label class="input"> <i class="icon-append fa fa-user"></i>
                        <div class="row">

                            <?php echo $form->textField($model, 'first_name', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;', 'placeholder' => Yii::t('main', 'First Name'))); ?>
                            <?php echo $form->error($model, 'first_name'); ?>
                        </div>

                </section>

                <section>
                    <label class="label"><?php echo Yii::t('main', 'Last Name')?></label>
                    <label class="input"> <i class="icon-append fa fa-user"></i>
                        <div class="row">
                            <?php echo $form->textField($model, 'last_name', array('size' => 60, 'maxlength' => 255, 'value' => '', 'placeholder' => Yii::t('main', 'Last Name'), 'autocomplete' => "off", 'style' => 'margin: 0px 0px 0px 15px; width: 94%;')); ?>
                            <?php echo $form->error($model, 'last_name'); ?>
                        </div>

                </section>
                <section>
                    <label class="label"><?php echo Yii::t('main', 'E-mail')?></label>
                    <label class="input"> <i class="icon-append fa fa-user"></i>
                        <div class="row">

                            <?php echo $form->textField($model, 'email', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;', 'size' => 60, 'maxlength' => 255, 'value' => '', 'placeholder' =>Yii::t('main', 'Email'), 'autocomplete' => "off")); ?>
                            <?php echo $form->error($model, 'email'); ?>
                        </div>

                </section>
                <section>
                    <label class="label"><?php echo Yii::t('main', 'Password')?></label>
                    <label class="input"> <i class="icon-append fa fa-lock"></i>
                        <div class="row">

                            <?php echo $form->passwordField($model, 'password', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;', 'size' => 60, 'maxlength' => 255, 'value' => '', 'placeholder' => Yii::t('main', 'Password'), 'autocomplete' => "off")); ?>
                            <?php echo $form->error($model, 'password'); ?>

                        </div>
                       
                </section>
                <section>
                    <label class="label"><?php echo Yii::t('main', 'Phone number')?></label>
                    <label class="input"> <i class="icon-append fa fa-user"></i>
                        <div class="row">

                            <?php echo $form->textField($model, 'phone', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;', 'size' => 60, 'maxlength' => 255, 'value' => '', 'placeholder' => Yii::t('main', 'Phone number'), 'autocomplete' => "off")); ?>
                            <?php echo $form->error($model, 'phone'); ?>
                        </div>

                </section>

                <footer>
                    <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), array('class' => 'btn btn-primary')); ?>
                </footer>


                <?php $this->endWidget(); ?>
            </fieldset>

        </div><!-- form -->

    </div><!-- widget-body no-padding -->
</div>