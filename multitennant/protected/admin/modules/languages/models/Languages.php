<?php

/**
 * This is the model class for table "cms_languages".
 *
 * The followings are the available columns in table 'cms_languages':
 * @property integer $lid
 * @property integer $language_id
 */
class Languages extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cms_languages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('language_id', 'required'),
            array('language_id', 'unique'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('language_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'language' => array(self::BELONGS_TO, 'AllLanguages', 'language_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'lid' => Yii::t('main', 'Language Id'),
            'language_id' => Yii::t('main', 'Language'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('language');
        $criteria->compare('lid', false);
        $criteria->compare('language_id', false);
        $criteria->compare('sort_position', false);
        $criteria->compare('language.name', false);
        $criteria->compare('language.nativeName', false);
        $criteria->compare('language.code2', false);
        $criteria->order = 'sort_position ASC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function sort($sortData) {
        if ($sortData && count($sortData) > 0) {
            foreach ($sortData as $key => $sData) {
                $ln = self::model()->findByPk($sData);
                $ln->sort_position = $key;
                $ln->save();
            }
            return true;
        }

        return false;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Settings the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeSave() {
        if ($this->isNewRecord) {
            $count = count($this->model()->findAll()) + 1;
            $this->sort_position = $count;
        }
        return parent::beforeSave();
    }

}
