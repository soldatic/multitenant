<?php

/**
 * This is the model class for table "cms_all_languages".
 *
 * The followings are the available columns in table 'cms_all_languages':
 * @property integer $id
 * @property string $name
 * @property string $nativeName
 * @property string $code2
 */
class AllLanguages extends CActiveRecord {

    
    public function getDbConnection(){
        return Yii::app()->dbDefault;
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cms_all_languages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, nativeName,code2', 'required'),
            array('code2', 'unique'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,name, nativeName,code2', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user_lang' => array(self::HAS_MANY, 'Languages', 'id'),
            'user_lang_m' => array(self::HAS_MANY, 'LanguagesMulti', 'id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Yii::t('main', 'Id'),
            'name' => Yii::t('main', 'Language Name'),
            'nativeName' => Yii::t('main', 'Language Native Name'),
            'code2' => Yii::t('main', 'Language Code'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Settings the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
