<?php

class DefaultController extends AdminController {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'password'),
                'roles' => array('user'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'password', 'update'),
                'roles' => array('moderator'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'password', 'update', 'create'),
                'roles' => array('administrator'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('index', 'view', 'password', 'admin', 'delete', 'create', 'update', 'password'),
                'roles' => array('superadministrator'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $model = new Languages('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Languages']))
            $model->attributes = $_GET['Languages'];
        $lIds = $model::model()->findAll();
        $languageData = array();
        foreach ($lIds as $d) {
            $allLanguages = AllLanguages::model()->findAll('id = :language_id', array(':language_id' => $d->language_id));
            $languageData[] = array(
                'lid' => $d->lid,
                'name' => $allLanguages[0]['name'],
                'nativeName' => $allLanguages[0]['nativeName'],
                'code2' => $allLanguages[0]['code2'],
                'sort_position' => $d->sort_position
            );
        }
        $arrayDataProvider = new CArrayDataProvider($languageData, array(
            'keyField' => 'lid',
            'sort' => array(
                'defaultOrder' => 'sort_position ASC',
            )
        ));
        $this->render('admin', array(
            'model' => $model,
            'arrayDataProvider' => $arrayDataProvider,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Languages;

        $defaultLanguage = Yii::app()->params->defaultLanguage;
        $url = Yiibase::getPathOfAlias('admin') . '/messages/';

        $keys = CHtml::listData(LanguagesDefault::model()->findAll(), 'id', 'key');

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'languages-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['Languages'])) {
            $model->attributes = $_POST['Languages'];
            if (Yii::app()->params->domain == 0) {
                if (isset($_POST['keys'])) {
                    $count = count($_POST['keys']);
                    $strFile = '<?php return array(';
                    $i = 0;
                    foreach ($_POST['keys'] as $k => $v) {
                        ++$i;
                        $strFile .= '"' . $keys[$k] . '" => "' . addslashes(stripslashes(stripslashes($v))) . '"';
                        if ($i != $count) {
                            $strFile .= ', ';
                        }
                    }
                    $strFile .= ');';
                    $languageId = $_POST['Languages']['language_id'];
                    $language = AllLanguages::model()->findByPk($languageId);
                    if (mkdir($url . $language->code2, 0777)) {
                        $fp = fopen($url . $language->code2 . '/main.php', "w");
                        fwrite($fp, $strFile);
                        fclose($fp);
                        if ($model->save()) {
                            $this->redirect('index');
                        }
                    }
                }
            } else {
                $languageId = $_POST['Languages']['language_id'];
                if ($model->save()) {
                    $this->redirect('index');
                }
            }
        }

        $dropdownListAttributes = array();

        if (Yii::app()->params->domain == 0) {
            $dataLanguages = AllLanguages::model()->findAll();
            $dropdownListLanguages = CHtml::listData($dataLanguages, 'id', 'nativeName');
            foreach ($dataLanguages as $d) {
                $dropdownListAttributes[$d->id] = array(
                    'data-lang' => $d->code2
                );
            }
        } else {
            $m = LanguagesMulti::model()->with('languag')->findAll(array('order' => 'sort_position ASC'));
            $data = array();
            foreach ($m as $mData) {
                $data[] = array(
                    'id' => $mData['languag']->id,
                    'nativeName' => $mData['languag']->name . ' - ' . $mData['languag']->nativeName,
                    'code2' => $mData['languag']->code2
                );
            }
            $dropdownListLanguages = CHtml::listData($data, 'id', 'nativeName');
        }

        $this->render('create', array(
            'model' => $model,
            'languages' => $dropdownListLanguages,
            'attributes' => $dropdownListAttributes,
            'language_keys' => $keys
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        if (Yii::app()->params->domain == 0) {
            $model = $this->loadModel($id);
            $languageCode = $model->language->code2;
            $url = Yiibase::getPathOfAlias('admin') . '/messages/';
            $keys = CHtml::listData(LanguagesDefault::model()->findAll(), 'id', 'key');
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'languages-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if (isset($_POST['keys'])) {
                $count = count($_POST['keys']);
                $strFile = '<?php return array(';
                $i = 0;
                foreach ($_POST['keys'] as $k => $v) {
                    ++$i;
                    $strFile .= '"' . $keys[$k] . '" => "' . addslashes(stripslashes(stripslashes($v))) . '"';
                    if ($i != $count) {
                        $strFile .= ', ';
                    }
                }
                $strFile .= ');';
                $fp = fopen($url . $languageCode . '/main.php', "w");
                fwrite($fp, $strFile);
                fclose($fp);
                if ($model->save()) {
                    $this->redirect('index');
                }
            }
            $content = include($url . $languageCode . '/main.php');

            $this->render('update', array(
                'model' => $model,
//            'language_content' => $handle
                'language_keys' => $keys,
                'content' => $content
            ));
        } else {
            $this->redirect('index');
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $model = $this->loadModel($id);
        $languageCode = $model->language->code2;
        $url = Yiibase::getPathOfAlias('admin') . '/messages/';
        if ($model) {
            if (Yii::app()->params->domain == 0) {
                if ($this->delTree($url . $languageCode)) {
                    $model->delete();
                }
            } else {
                $model->delete();
            }
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
    }

    public static function delTree($dir) {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Settings the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Languages::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Settings $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'language-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
