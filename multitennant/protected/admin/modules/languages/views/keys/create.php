<?php
$this->breadcrumbs = array(
    'Keys' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List Keys', 'url' => array('index')),
);
?>


<h1>New Language</h1>

<?php
$this->renderPartial('_form', array(
    'model' => $model,
));
