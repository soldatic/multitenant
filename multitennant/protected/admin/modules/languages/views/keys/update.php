<?php
$this->breadcrumbs = array(
    'Keys' => array('index'),
    'Update',
);

$this->menu = array(
    array('label' => 'List Keys', 'url' => array('index')),
);
?>


<h1>Update Keys</h1>

<?php
$this->renderPartial('_form', array(
    'model' => $model
));
