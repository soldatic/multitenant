<div class="row">
    <div class="widget-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form smart-form">

            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'languages-keys-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>



<!--        <p class="note">Fields with <span class="required">*</span> are required.</p>-->
            <fieldset>
                <?php echo $form->errorSummary($model); ?>

                <section>
                    <label class="label"><?php echo Yii::t('main', 'Key') ?></label>
                        <div class="row">
                            <?php echo $form->textField($model, 'key', array('style' => 'margin: 0px 0px 0px 15px; width: 94%;',)); ?>
                            <?php echo $form->error($model, 'key'); ?>
                        </div>
                </section>

                <footer>
                    <?php echo CHtml::submitButton($model->isNewRecord ?  Yii::t('main', 'Create') : Yii::t('main', 'Save'), array('class' => 'btn btn-primary')); ?>
                </footer>


                <?php $this->endWidget(); ?>
            </fieldset>

        </div><!-- form -->

    </div><!-- widget-body no-padding -->
</div>