<?php
$this->breadcrumbs = array(
    Yii::t('main', 'Keys')  => array('index'),
    Yii::t('main','Manage'),
);

$this->menu = array(
    array('label' => Yii::t('main','List Keys'), 'url' => array('index')),
    array('label' => Yii::t('main','Create Key'), 'url' => array('create')),
);
?>

<h1> <?php echo Yii::t('main', 'Manage Keys') ?></h1>


<?php
echo '<div class="pull-left buttonintabbles">';
echo CHtml::link(Yii::t('main','Add new key'), '/admin/languages/keys/create', array('class' => 'btn btn-primary '));
echo '</div>';
?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'language-keys-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        'key',
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}'
        ),
    ),
));
