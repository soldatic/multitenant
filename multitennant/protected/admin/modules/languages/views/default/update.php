<?php
$this->breadcrumbs = array(
    'Languages' => array('index'),
    'Update',
);

$this->menu = array(
    array('label' => 'List Users', 'url' => array('index')),
);
?>


<h1>Update Language</h1>

<?php
$this->renderPartial('_formUpdate', array(
    'model' => $model,
//    'language_content' => $language_content
    'language_keys' => $language_keys,
    'content' => $content
));
