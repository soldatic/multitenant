<?php
$this->breadcrumbs = array(
    Yii::t('main', 'Languages') => array('index'),
    Yii::t('main', 'Manage'),
);

$this->menu = array(
    array('label' => Yii::t('main', 'List Language'), 'url' => array('index')),
    array('label' => Yii::t('main', 'Create Language'), 'url' => array('create')),
);
?>

<h1> <?php echo Yii::t('main', 'Manage Language') ?></h1>


<?php
echo '<div class="pull-left buttonintabbles">';
echo CHtml::link(Yii::t('main', 'Add new language'), '/admin/languages/create', array('class' => 'btn btn-primary '));
echo '</div>';
?>

<script>
    $(document).ready(function () {
        $('#sortable').sortable({
            items: ".jsDynamicData",
            stop: function (event, ui) {
                var i = 0;
                var params = [];
                $.each($('.jsDynamicData'), function () {
                    params[i] = $(this).attr('data-id');
                    i++;
                });
                $.ajax({
                    url: '/admin/ajax/setPosition',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        sortData: params,
                        param: 'languages'
                    },
                    success: function (data) {
                        if (data.result === 1) {

                        } else {

                        }
                    },
                    error: function (e) {
                        setAlert('Server error', 'error');
                    }
                });
            }
        });
    });
</script>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'sortable',
//    'dataProvider' => $model->search(),
    'dataProvider' => $arrayDataProvider,
    'columns' => array(
        array(
            'name' => Yii::t('main', 'Language Name'),
            'value' => '$data["name"]',
            'sortable' => true,
        ),
        array(
            'name' => Yii::t('main', 'Language Native Name'),
            'value' => '$data["nativeName"]'
        ),
        array(
            'name' => Yii::t('main', 'Language Code'),
            'value' => '$data["code2"]'
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => (Yii::app()->params->domain == 0) ? '{update}{delete}' : '{delete}',
            'buttons' => (Yii::app()->params->domain == 0) ? array(
                'update' => array(
                    'url' => 'Yii::app()->createUrl("admin/languages/update?id=").$data["lid"]'
                ),
                'delete' => array(
                    'url' => 'Yii::app()->createUrl("admin/languages/delete?id=").$data["lid"]'
                )
                    ) : array('delete' => array(
                    'url' => 'Yii::app()->createUrl("admin/languages/delete?id=").$data["lid"]'
                ))
        ),
    ),
    'rowHtmlOptionsExpression' => '[ "data-id" => $data["lid"], "data-sort" => 0, "class" => "jsDynamicData" ]',
));
