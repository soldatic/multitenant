<div class="row">
    <div class="widget-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form smart-form">

            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'languages-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>

<!--        <p class="note">Fields with <span class="required">*</span> are required.</p>-->
            <fieldset>
                <?php echo $form->errorSummary($model); ?>

                <section>
                    <label class="label"><?php echo Yii::t('main', 'Language') ?></label>
                    <div class="row">
                        <?php
                        echo $form->dropDownList($model, 'language_id', $languages, array(
                            'empty' => Yii::t('main', 'Select Language'),
                            'style' => 'margin: 0px 0px 0px 15px;',
                            'options' => $attributes
                        ));
                        ?>
                        <?php echo $form->error($model, 'language_id'); ?>
                    </div>

                </section>
                <?php
                foreach ($language_keys as $k => $v) {
                    ?>
                    <section>
                        <div class="row">
                            <?php echo '<span class="sourceT">' . $v . '</span> : ' . CHtml::textField('keys[' . $k . ']', (isset($_POST['keys[' . $k . ']'])) ? $_POST['keys[' . $k . ']'] : '', array('style' => 'margin: 0px 0px 0px 15px;')); ?>
                            <?php
                            echo CHtml::link(Yii::t('main', 'Translate'), '#', array(
                                'class' => 'btn btn-primary btn-translate disabled',
                            ));
                            ?>
                        </div>
                    </section>
                <?php } ?>

                <footer>
                    <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), array('class' => 'btn btn-primary')); ?>
                </footer>


                <?php $this->endWidget(); ?>
            </fieldset>

        </div><!-- form -->

    </div><!-- widget-body no-padding -->
</div>
<?php
if (Yii::app()->params->domain == 0) {
    ?>
    <script>
        var newScript = document.createElement('script');
        newScript.type = 'text/javascript';

        $(document).ready(function () {
            var targetLang = 'en';
            $('#Languages_language_id').change(function () {
                targetLang = $(this).find('option:selected').attr('data-lang');
                $.ajax({
                    url: 'https://www.googleapis.com/language/translate/v2/languages?key=<?php echo SettingsHelper::getSettingValue('googleApiKey'); ?>&target=' + targetLang,
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        $('.btn-translate-all').removeClass('disabled');
                        $('.btn-translate').removeClass('disabled');
                    },
                    error: function (e) {
                        $('.btn-translate-all').addClass('disabled');
                        $('.btn-translate').addClass('disabled');
                    }
                });
            });
            $('.btn-translate').click(function () {
                if ($('#Languages_language_id').val() !== "") {
                    var el = $(this);
                    var sourceText = el.prev().prev().html();
                    var targetRes = el.prev('input');
                    $.ajax({
                        url: 'https://www.googleapis.com/language/translate/v2?key=<?php echo SettingsHelper::getSettingValue('googleApiKey'); ?>&source=en&target=' + targetLang + '&q=' + sourceText,
                        type: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            var translatedText = data.data.translations[0].translatedText;
                            targetRes.val(translatedText);
                        },
                        error: function (e) {
                            alert('Error data!');
                        }
                    });
                } else {
                    alert('Please select language!');
                }
            });
            $('.btn-translate-all').click(function () {
                if ($('#Languages_language_id').val() !== "") {
                    var allTranslateElements = $('.sourceT');
                    var requestStr = '';
                    var countEl = allTranslateElements.length;
                    $.each(allTranslateElements, function (index) {
                        var w = str_replace(" ", "+", $(this).html());
                        requestStr += 'q=' + w;
                        if (index + 1 !== countEl) {
                            requestStr += '&';
                        }
                    });
                    $.ajax({
                        url: 'https://www.googleapis.com/language/translate/v2?key=<?php echo SettingsHelper::getSettingValue('googleApiKey'); ?>&source=en&target=' + targetLang + '&' + requestStr,
                        type: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            var translatedData = data.data.translations;
                            $.each(allTranslateElements, function (index) {
                                $(this).next('input').val(translatedData[index].translatedText);
                            });
                        },
                        error: function (e) {
                            alert('Error data!');
                        }
                    });
                } else {
                    alert('Please select language!');
                }
            });
        });
    </script>
    <?php
}