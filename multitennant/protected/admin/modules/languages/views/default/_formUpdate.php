<div class="row">
    <div class="widget-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form smart-form">

            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'languages-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>



<!--        <p class="note">Fields with <span class="required">*</span> are required.</p>-->
            <fieldset>
                <?php echo $form->errorSummary($model); ?>

                <?php
                foreach ($language_keys as $k => $v) {
                    $value = (isset($content[$v])) ? stripslashes($content[$v]) : '';
                    ?>
                    <section>
                        <!--<label class="label"><?php // echo $v;       ?></label>-->
                        <div class="row">
                            <?php echo $v . ': ' . CHtml::textField('keys[' . $k . ']', (isset($_POST['keys[' . $k . ']'])) ? $_POST['keys[' . $k . ']'] : $value, array('style' => 'margin: 0px 0px 0px 15px;')); ?>
                        </div>
                    </section>
                <?php } ?>

<!--                <section>
<label class="label"><?php echo Yii::t('main', 'Language Contents') ?></label>
<label class="input"> <i class="icon-append fa fa-user"></i>
    <div class="row">
        <textarea id="code" name="language_content"><?php // echo $language_content;   ?></textarea>
    </div>
</section>-->

                <footer>
                    <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), array('class' => 'btn btn-primary')); ?>
                </footer>
                <?php $this->endWidget(); ?>
            </fieldset>

        </div><!-- form -->

    </div><!-- widget-body no-padding -->
</div>
<!--code mirror-->
<!--<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/codemirror-4.7/lib/codemirror.css">
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/codemirror-4.7/lib/codemirror.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/codemirror-4.7/mode/xml/xml.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/codemirror-4.7/addon/selection/active-line.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/codemirror-4.7/addon/edit/closetag.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/codemirror-4.7/addon/fold/xml-fold.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/codemirror-4.7/mode/javascript/javascript.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/codemirror-4.7/mode/css/css.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/codemirror-4.7/mode/htmlmixed/htmlmixed.js"></script>
<style type="text/css">
    .CodeMirror {border-top: 1px solid #888; border-bottom: 1px solid #888;}
</style>
<script>
    var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
        mode: "application/xml",
        styleActiveLine: true,
        lineNumbers: true,
        lineWrapping: true,
        autoCloseTags: true
    });
</script>-->
<!--end code mirror-->