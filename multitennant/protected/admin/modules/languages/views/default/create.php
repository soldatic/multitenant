<?php
$this->breadcrumbs = array(
    'Languages' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List Users', 'url' => array('index')),
);
?>


<h1>New Language</h1>

<?php
if (Yii::app()->params->domain == 0) {
    echo '<div class="pull-left buttonintabbles">';
    echo CHtml::link(Yii::t('main', 'Translate All'), '#', array(
        'class' => 'btn btn-primary btn-translate-all disabled'
    ));
    echo '</div>';
}
?>
<?php
$this->renderPartial('_form', array(
    'model' => $model,
    'languages' => $languages,
    'attributes' => $attributes,
//    'language_content' => $language_content
    'language_keys' => $language_keys
));
