<?php

/**
 * This is the model class for table "cms_menus".
 *
 * The followings are the available columns in table 'cms_menus':
 * @property integer $menu_id
 * @property string $menu_name
 * @property string $menu_type
 * @property integer $page_id
 * @property string $menu_link
 * @property string $menu_level
 * @property string $menu_position
 * @property string $menu_create_date
 * @property string $menu_update_date
 */
class Menus extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cms_menus';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('menu_name, menu_type, page_id, menu_link, menu_level, menu_position, menu_create_date, menu_update_date', 'required'),
            array('page_id', 'numerical', 'integerOnly' => true),
            array('menu_name, menu_link, menu_level, menu_position', 'length', 'max' => 255),
            array('menu_type', 'length', 'max' => 4),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('menu_id, menu_name, menu_type, page_id, menu_link, menu_level, menu_position, menu_create_date, menu_update_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'menu_id' => 'Menu',
            'menu_name' => 'Menu Name',
            'menu_type' => 'Menu Type',
            'page_id' => 'Page',
            'menu_link' => 'Menu Link',
            'menu_level' => 'Menu Level',
            'menu_position' => 'Menu Position',
            'menu_create_date' => 'Menu Create Date',
            'menu_update_date' => 'Menu Update Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('menu_id', $this->menu_id);
        $criteria->compare('menu_name', $this->menu_name, true);
        $criteria->compare('menu_type', $this->menu_type, true);
        $criteria->compare('page_id', $this->page_id);
        $criteria->compare('menu_link', $this->menu_link, true);
        $criteria->compare('menu_level', $this->menu_level, true);
        $criteria->compare('menu_position', $this->menu_position, true);
        $criteria->compare('menu_create_date', $this->menu_create_date, true);
        $criteria->compare('menu_update_date', $this->menu_update_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Menus the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
