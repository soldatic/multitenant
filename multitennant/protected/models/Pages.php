<?php

/**
 * This is the model class for table "cms_pages".
 *
 * The followings are the available columns in table 'cms_pages':
 * @property integer $page_id
 * @property string $page_name
 * @property string $page_title
 * @property string $page_url
 * @property string $page_content
 * @property string $page_create_date
 * @property string $page_update_date
 * @property integer $page_template
 */
class Pages extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cms_pages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('page_name, page_title, page_url, page_content, page_create_date, page_template', 'required'),
            array('page_template', 'numerical', 'integerOnly' => true),
            array('page_name, page_title, page_url', 'length', 'max' => 255),
            array('page_update_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('page_id, page_name, page_title, page_url, page_content, page_create_date, page_update_date, page_template', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'page_id' => 'Page',
            'page_name' => 'Page Name',
            'page_title' => 'Page Title',
            'page_url' => 'Page Url',
            'page_content' => 'Page Content',
            'page_create_date' => 'Page Create Date',
            'page_update_date' => 'Page Update Date',
            'page_template' => 'Page Template',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('page_id', $this->page_id);
        $criteria->compare('page_name', $this->page_name, true);
        $criteria->compare('page_title', $this->page_title, true);
        $criteria->compare('page_url', $this->page_url, true);
        $criteria->compare('page_content', $this->page_content, true);
        $criteria->compare('page_create_date', $this->page_create_date, true);
        $criteria->compare('page_update_date', $this->page_update_date, true);
        $criteria->compare('page_template', $this->page_template);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Pages the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
