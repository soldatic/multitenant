<?php

/**
 * This is the model class for table "cms_modules".
 *
 * The followings are the available columns in table 'cms_modules':
 * @property integer $mod_id
 * @property string $mod_name
 * @property string $mod_type
 * @property string $mod_css_class
 * @property string $mod_css_class_cms
 * @property string $mod_js_class
 * @property string $mod_js_class_cms
 * @property string $mod_js_method
 * @property string $mod_js_method_cms
 */
class Modules extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cms_modules';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('mod_name, mod_type, mod_css_class, mod_css_class_cms, mod_js_class, mod_js_class_cms, mod_js_method, mod_js_method_cms', 'required'),
            array('mod_name, mod_css_class, mod_css_class_cms, mod_js_class, mod_js_class_cms, mod_js_method, mod_js_method_cms', 'length', 'max' => 64),
            array('mod_type', 'length', 'max' => 1),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('mod_id, mod_name, mod_type, mod_css_class, mod_css_class_cms, mod_js_class, mod_js_class_cms, mod_js_method, mod_js_method_cms', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'mod_id' => 'Mod',
            'mod_name' => 'Mod Name',
            'mod_type' => 'Mod Type',
            'mod_css_class' => 'Mod Css Class',
            'mod_css_class_cms' => 'Mod Css Class Cms',
            'mod_js_class' => 'Mod Js Class',
            'mod_js_class_cms' => 'Mod Js Class Cms',
            'mod_js_method' => 'Mod Js Method',
            'mod_js_method_cms' => 'Mod Js Method Cms',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('mod_id', $this->mod_id);
        $criteria->compare('mod_name', $this->mod_name, true);
        $criteria->compare('mod_type', $this->mod_type, true);
        $criteria->compare('mod_css_class', $this->mod_css_class, true);
        $criteria->compare('mod_css_class_cms', $this->mod_css_class_cms, true);
        $criteria->compare('mod_js_class', $this->mod_js_class, true);
        $criteria->compare('mod_js_class_cms', $this->mod_js_class_cms, true);
        $criteria->compare('mod_js_method', $this->mod_js_method, true);
        $criteria->compare('mod_js_method_cms', $this->mod_js_method_cms, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Modules the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
